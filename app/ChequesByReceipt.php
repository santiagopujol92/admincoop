<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ChequesByReceipt extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'cheques_by_receipts';
    protected $fillable = [ 
        'id_receipt',
        'nro_cheque',
        'banco',
        'concepto',
        'nombre',
        'cuit',
        'importe',
        'fecha_emision',
        'fecha_pago'
      ];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
