<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ChequesByReceiptPurchase extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'cheques_by_receipt_purchases';
    protected $fillable = [ 
        'id_receipt_purchase',
        'nro_cheque',
        'banco',
        'concepto',
        'nombre',
        'cuit',
        'importe',
        'fecha_emision',
        'fecha_pago'
      ];
    protected $dates = ['created_at', 'updated_at'];
}
