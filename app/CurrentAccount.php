<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CurrentAccount extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'current_accounts';
    protected $fillable = ['description', 'id_person', 'balance', 'id_status_current_account', 'last_movement_account'];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
