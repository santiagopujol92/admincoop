<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use OwenIt\Auditing\Models\Audit;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas

class AuditController extends Controller
{

	/*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
	private $titulo;
 	private $modulo_msg;
	private $form;
	private $module;
	private $name_file;
	private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        $this->middleware('root');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'REGISTROS DE ACTIVIDAD';
        $this->modulo_msg = 'Registros de Actividad';
        $this->form = 'RegistroLog';
        $this->module = 'registros_logs';
        $this->name_file = 'registros_log';
        $this->modals_btns = 'RegistroLog';
        $this->model = new Audit;
    }

    public function index()
    {
        $data_controller = $this->model->select('audits.*','users.lastname as user_lastname', 'users.name as user_name', 'type_users.description as type_user')
        	->join('users', 'audits.user_id', '=', 'users.id')
        	->join('type_users', 'users.type', '=', 'type_users.id')
        	->orderBy('users.id', 'desc')
        	->get();
			
        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller'))
        		->with('titulo', $this->titulo)
        		->with('modulo_msg', $this->modulo_msg)
        		->with('form', $this->form)
        		->with('module', $this->module)
        		->with('name_file', $this->name_file)
        		->with('modals_btns', $this->modals_btns);
    }

    /**
     * Retorna la auditoria formateada para notificacion del usuario logeado, solo cambios del usuario logeado
     */
    public function getDataAuditToNotificaction($limit)
    {   
        //Si es 0 p falso. Limite infinito
        if ($limit == 0 || $limit == false){
            $limit = 999999;
        }

        $data_audit_for_notification = $this->model
            ->select(
                'id', 
                'user_id', 
                'event', 
                'url', 
                'created_at', 
                'auditable_type',
                DB::raw("DATEDIFF(NOW(), created_at) AS days"))
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->where('auditable_id', '!=', 26)
            // ->where('id', '=', Auth::user()->id)
            ->get();

        return response()->json(
            $data_audit_for_notification->toArray()
        );
    }
}
