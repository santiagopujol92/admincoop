<?php

namespace AdminCoop\Http\Controllers;

use \AdminCoop\Person; 
use \AdminCoop\Invoice; 
use \AdminCoop\TypePerson; 
use Illuminate\Http\Request;
use \AdminCoop\CurrentAccount; 
use \AdminCoop\MovementsAccount; 
use \AdminCoop\StatusCurrentAccount;
use \AdminCoop\TypeMovementAccount; 
use Illuminate\Support\Facades\Auth;
use AdminCoop\Http\Controllers\Controller;

class CurrentAccountController extends Controller
{
	/*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;
    private $rol;

    public function __construct(){
        
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'CUENTA CORRIENTE';
        $this->modulo_msg = 'Cuenta Corriente';
        $this->form = 'CuentaCorriente';
        $this->module = 'cuentas_corrientes';
        $this->name_file = 'currents_account';
        $this->modals_btns = 'CurrentsAccount';
        $this->model = new CurrentAccount;
    }

    public function listing(){
        $data_controller = $this->model->join('people as p', 'current_accounts.id_person', '=', 'p.id')
       		->join('status_current_account as sca', 'current_accounts.id_status_current_account', '=', 'sca.id')
            ->join('type_people as tp', 'tp.id', '=', 'p.id_type_people')
            ->select('current_accounts.*', 'tp.description as desc_type_person', 'tp.id as id_type_people',
            'p.name as person_name', 'p.lastname as person_lastname',
            'sca.id as id_status_current_account', 'sca.description as desc_status_current_account'
        	)
            ->orderBy('person_name', 'desc')
            ->get();

        return response()->json([
            'data' => $data_controller->toArray(),
            'data_user' =>  Auth::user(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_persons = Person::select('name','lastname', 'id', 'cuit')
            ->where('id_type_people', '=', '1') // Cliente
            ->orWhere('id_type_people', '=', '2') // Proveedor
            ->orWhere('id_type_people', '=', '4') // Cliente Proveedor
            ->orderBy('id_type_people', 'asc')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_type_person = TypePerson::All();

        $data_status_current_account = StatusCurrentAccount::All();

        $data_types_movement_account = TypeMovementAccount::All();

        $data_invoices = Invoice::join('people as p', 'p.id', '=', 'invoices.id_person')
        ->select('invoices.id', 'p.name as person_name', 'p.lastname as person_lastname', 'invoices.id_sale', 'invoices.total_amount')
        ->orderBy('invoices.created_at', 'desc')
        ->get();

        return view($this->module . '.' . $this->name_file . '_index', compact(
        	'data_persons', 
        	'data_status_current_account',
            'data_types_movement_account',
            'data_invoices',
            'data_type_person'
        	))
            ->with('titulo', $this->titulo)
            ->with('modulo_msg', $this->modulo_msg)
            ->with('form', $this->form)
            ->with('module', $this->module)
            ->with('name_file', $this->name_file)
            ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All()); //Rellena el elemento usuario con fill
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'      
        ]);
    }

    public function GetMovimientosByCuentaCorriente($id_cuenta_corriente){
        $data_controller = MovementsAccount::join('type_movement_accounts as tma', 'movements_accounts.id_type_movement_account', '=', 'tma.id')
            ->select('movements_accounts.*', 'tma.description as desc_type_movement_account')
            ->where('movements_accounts.id_current_account', '=', $id_cuenta_corriente)
            ->orderBy('movements_accounts.date_movement', 'desc')
            ->get();

        return response()->json(
            $data_controller->toArray()          
        );
    }

}
