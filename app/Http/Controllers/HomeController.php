<?php

namespace AdminCoop\Http\Controllers;

use \AdminCoop\Producto; 
use \AdminCoop\Person;
use \AdminCoop\Purchase;
use \AdminCoop\CurrentAccount;
use \AdminCoop\Production;
use \AdminCoop\Sale;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use Carbon\Carbon;

class HomeController extends Controller
{

    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
        //Permitir acceso solo autenticando
        $this->middleware('auth');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'INICIO';
        $this->modulo_msg = 'Inicio';
        $this->form = 'Home';
        $this->module = 'home';
        $this->name_file = 'home';
        $this->modals_btns = 'Home';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Datas totales
        $count_people = Person::all()->groupBy('id')->count();
        $count_products = Producto::all()->groupBy('id')->count();
        $count_purchases = Purchase::all()->groupBy('id')->count();
        $count_productions = Production::all()->groupBy('id')->count();
        $count_sales = Sale::all()->groupBy('id')->count();
        //Gastos
        $count_current_accounts = CurrentAccount::all()->groupBy('id')->count();

        //Data de ventas totales
        $data_sales = Sale::all();
        $sell_price_sales = $data_sales->sum('sell_cost');

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        //Clientes con mas ventas
        $data_best_clients = DB::table('people')
            ->join('sales', 'people.id', '=', 'sales.id_person')
            ->select('people.lastname','people.name', DB::raw('COUNT(sales.id) as cant_ventas'), DB::raw('SUM(sales.sell_cost) as monto_ventas'))
            ->where('people.id_type_people', '=', '1')
            ->where('sales.deleted_at', '=', null)
            ->groupBy('people.id', 'people.name', 'people.lastname')
            ->orderBy("cant_ventas", 'desc')
            ->limit('5')
            ->get();

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////


        //Productos con bajo stock
        //Si es producto por unidad si tiene menos de 10 unidades se muestra
        //Y si es producto por gramos si tiene menos de 1000 gramos se muestra
        //Y se ordena por unidad y valor
        $data_products_lowstock = Producto::join('type_products', 'productos.id_type_product', '=', 'type_products.id')
            ->select('productos.*',
                'type_products.description as type_description')
            ->orderBy('productos.id_type_product', 'desc')
            ->orderBy('productos.stock', 'asc')

            //Gramos
            ->where('productos.id_type_product', '=', 39)
            ->where('productos.stock', '<', 1000)

            //Unidades
            ->orWhere('productos.id_type_product', '=', 40)
            ->where('productos.stock', '<', 10)

            ->where('productos.deleted_at', '=', null)
            ->limit('6')
            ->get();

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        //Cantidad Productos proximos a recargar, bajo stock
        $query = DB::table('productos');
        $query->select(
            DB::raw('COUNT(id) AS contador'),
            DB::raw('id')
        );

        //En Gramos
        $query->where(function($query)
            {
                $query->where('productos.id_type_product', '=', 39)
                      ->where('productos.stock', '<', 1000)
                      ->where('productos.deleted_at', '=', null);
            });
        //O En Unidades
        $query->orwhere(function($query)
            {
                $query->where('productos.id_type_product', 40)
                      ->where('productos.stock', 10)
                      ->where('productos.deleted_at', '=', null);
            });

        $query->groupBy('id');
        $data = $query->get();
        $data_count_products_lowstock = count($data);

        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////

        //Data de Ventas por periodo:
        //Ventas del dia de  Hoy
        $data_sales_today = DB::table('sales')
            ->where('created_at', '>=', [Carbon::today()])
            ->where('deleted_at', '=', null);

        $count_sales_today = $data_sales_today->count();
        $sum_sell_price_sales_today = $data_sales_today->sum('sell_cost');

        //Ventas de hace 1 dia exactamente
        $data_sales_yesterday = DB::table('sales')
            ->where('created_at', '>=', [Carbon::yesterday()])
            ->where('created_at', '<', [Carbon::today()])
            ->where('deleted_at', '=', null);

        $count_sales_yesterday = $data_sales_yesterday->count();
        $sum_cost_price_sales_yesterday = $data_sales_yesterday->sum('sell_cost');

        //Ventas de hace 1 semana exactamente
        $data_sales_last_week = DB::table('sales')
            ->where('created_at', '>=', [Carbon::today()->subWeek()])
            ->where('created_at', '<', [Carbon::today()])
            ->where('deleted_at', '=', null);

        $count_sales_last_week = $data_sales_last_week->count();
        $sum_cost_price_sales_last_week = $data_sales_last_week->sum('sell_cost');

        //Ventas de mes de hace 1 mes atras hasta hoy
        $data_sales_last_month = DB::table('sales')
            ->where('created_at', '>=', [Carbon::today()->subMonth()])
            ->where('created_at', '<', [Carbon::today()])
            ->where('deleted_at', '=', null);

        $count_sales_last_month = $data_sales_last_month->count();
        $sum_cost_price_sales_last_month = $data_sales_last_month->sum('sell_cost');

        //Ventas desde hace 1 año atras exactamente hasta hoy
        $data_sales_last_year = DB::table('sales')
            ->where('created_at', '>=', [Carbon::today()->subYear()])
            ->where('created_at', '<', [Carbon::today()])
            ->where('deleted_at', '=', null);

        $count_sales_last_year = $data_sales_last_year->count();
        $sum_cost_price_sales_last_year = $data_sales_last_year->sum('sell_cost');


        ////////////////////////////////////////////////////
        ////////////////////////////////////////////////////
        //Ventas por empleado, cantidad y total por periodo

        //Ventas por empleado, cantidad y total hoy
        $data_sales_by_employee_today = DB::table('people')
            ->join('sales', 'people.id', '=', 'sales.id_person')
            ->select('people.lastname','people.name', DB::raw('COUNT(sales.id) as cant_ventas'), DB::raw('SUM(sales.sell_cost) as monto_ventas'))
            ->where('people.id_type_people', '=', '2')
            ->where('sales.deleted_at', '=', null)
            ->where('sales.created_at', '>=', [Carbon::today()])
            ->groupBy('people.id', 'people.name', 'people.lastname')
            ->orderBy("cant_ventas", 'desc')
            ->get();

        //Ventas por empleado, cantidad y total ultima semana
        $data_sales_by_employee_last_week = DB::table('people')
            ->join('sales', 'people.id', '=', 'sales.id_person')
            ->select('people.lastname','people.name', DB::raw('COUNT(sales.id) as cant_ventas'), DB::raw('SUM(sales.sell_cost) as monto_ventas'))
            ->where('people.id_type_people', '=', '2')
            ->where('sales.deleted_at', '=', null)
            ->where('sales.created_at', '>=', [Carbon::today()->subWeek()])
            ->where('sales.created_at', '<', [Carbon::today()])
            ->groupBy('people.id', 'people.name', 'people.lastname')
            ->orderBy("cant_ventas", 'desc')
            ->get();

        //Ventas por empleado, cantidad y total ultimo mes
        $data_sales_by_employee_last_month = DB::table('people')
            ->join('sales', 'people.id', '=', 'sales.id_person')
            ->select('people.lastname','people.name', DB::raw('COUNT(sales.id) as cant_ventas'), DB::raw('SUM(sales.sell_cost) as monto_ventas'))
            ->where('people.id_type_people', '=', '2')
            ->where('sales.deleted_at', '=', null)
            ->where('sales.created_at', '>=', [Carbon::today()->subMonth()])
            ->where('sales.created_at', '<', [Carbon::today()])
            ->groupBy('people.id', 'people.name', 'people.lastname')
            ->orderBy("cant_ventas", 'desc')
            ->get();

        //Ventas por empleado, cantidad y total desde el prinicpio
        $data_sales_by_employee_all = DB::table('people')
            ->join('sales', 'people.id', '=', 'sales.id_person')
            ->select('people.lastname','people.name', DB::raw('COUNT(sales.id) as cant_ventas'), DB::raw('SUM(sales.sell_cost) as monto_ventas'))
            ->where('people.id_type_people', '=', '2')
            ->where('sales.deleted_at', '=', null)
            ->groupBy('people.id', 'people.name', 'people.lastname')
            ->orderBy("cant_ventas", 'desc')
            ->get();

        //Respuesta
        return view($this->module . '.' . $this->name_file . '_index', compact(
                    'data_people', 'data_products',
                    'data_best_clients', 
                    'data_products_lowstock', 'data_count_products_lowstock', 
                    'count_people', 'count_products', 'count_purchases', 'count_productions', 'count_current_accounts',
                    'count_sales', 'cost_price_sales', 'sell_price_sales', 
                    'count_sales_today', 'sum_cost_price_sales_today', 'sum_sell_price_sales_today', 
                    'count_sales_yesterday', 'sum_cost_price_sales_yesterday', 'sum_sell_price_sales_yesterday', 
                    'count_sales_last_week', 'sum_cost_price_sales_last_week', 'sum_sell_price_sales_last_week',
                    'count_sales_last_month', 'sum_cost_price_sales_last_month', 'sum_sell_price_sales_last_month', 
                    'count_sales_last_year', 'sum_cost_price_sales_last_year', 'sum_sell_price_sales_last_year',
                    'data_sales_by_employee_today', 'data_sales_by_employee_last_week', 'data_sales_by_employee_last_month', 'data_sales_by_employee_all'
                ))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }
}
