<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use AdminCoop\Http\Controllers\Controller;
use \AdminCoop\Machine; 

class MachineController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct(){
        
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        $this->middleware('root');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'MÁQUINA';
        $this->modulo_msg = 'Máquina';
        $this->form = 'Maquina';
        $this->module = 'maquinas';
        $this->name_file = 'machine';
        $this->modals_btns = 'Machine';
        $this->model = new Machine;
    }

    public function listing(){
        $data_controller = $this->model->select('id', 'description', 'created_at')
            ->orderBy('description', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()          
        );
    }
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->module . '.' . $this->name_file . 's_index')
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All()); //Rellena el elemento usuario con fill
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'      
        ]);

    }
}
