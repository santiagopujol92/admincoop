<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use AdminCoop\Http\Controllers\Controller;
use \AdminCoop\MovementsAccount; 
use \AdminCoop\CurrentAccount; 
use Illuminate\Support\Facades\DB;

class MovementsAccountController extends Controller
{
/*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct(){
        
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'MOVIMIENTOS CUENTA';
        $this->modulo_msg = 'MovimientosCuenta';
        $this->form = 'MovimientosCuenta';
        $this->module = 'movimientos_cuentas';
        $this->name_file = 'movements_account';
        $this->modals_btns = 'MovementsAccount';
        $this->model = new MovementsAccount;
    }

    public function listing(){
        $data_controller = $this->model->All();

        return response()->json(
            $data_controller->toArray()          
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->module . '.' . $this->name_file . '_index')
            ->with('titulo', $this->titulo)
            ->with('modulo_msg', $this->modulo_msg)
            ->with('form', $this->form)
            ->with('module', $this->module)
            ->with('name_file', $this->name_file)
            ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //total_amount -> monto aplicado al movimiento
        //saldo_actual -> balance actual o saldo actual de la cuenta
        //Esta data se usa para actualizar el estado final de la cuenta
        
        if($request->ajax())
        {
            $saldo_actual = $request->balance;
            $request['current_balance_account'] = $request->balance;
            $insertMovimiento = $request->except('balance'); //GUARDO EL REQUEST SIN SALDO ACTUAL PARA INSERT MOV
            $this->model->create($insertMovimiento);

            //UNA VEZ CARGADA LA OPERACION PREPARO LA DATA PARA GENERAR UNA ACTUALIZACION DE SALDO Y FECHA ULT MOV DE LA CUENTA 
            $resultUpdate = $this->updateBalanceAndLastMovementCurrentAccount($request, $saldo_actual);

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente',
                'cuenta_corriente_actualizada' => $resultUpdate
            ]);
            
        }
    }

    //ACTUALIZAR SALDO Y FECHA DE ULTIMO MOVIMIENTO 
    public function updateBalanceAndLastMovementCurrentAccount(Request $request, $saldo_actual){

        $id_cuenta_corriente = $request->id_current_account; //ID CUENTA CORRIENTE
        // SI ES DEBITO RESTO
        if ($request->type_operation == 'DEBITO'){ 
            $request['balance'] = ((float)$saldo_actual - (float)$request->total_amount);
        //SI ES CREDITO SUMO
        } else if ($request->type_operation == 'CREDITO'){ 
            $request['balance'] = ((float)$saldo_actual + (float)$request->total_amount);
        } 
        
        //Agrego la fecha del momento para la actualizacion
        $request['last_movement_account'] = date('Y-m-d H:i:m');
        //Quito los campos que no me sirven
        $updateCuentaCorriente = $request->except('id_type_movement_account', 'total_amount', 'id_current_account');

        $cuenta_corriente = CurrentAccount::find($id_cuenta_corriente);
        $cuenta_corriente->fill($updateCuentaCorriente);
        $cuenta_corriente->save();

        return $cuenta_corriente->save() ? 1 : 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All()); //Rellena el elemento usuario con fill
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'      
        ]);

    }

    // Insertar un movimiento luego de crear una factura y actualizar cuenta corriente
    public function addMovementCurrentAccount($data_current_account, $data_invoice) {
        $saldo_actual_cuenta = $data_current_account->balance;

        $id_movement_account = DB::table('movements_accounts')->insertGetId([
                'id_type_movement_account' => $data_invoice->id_type_movement_account,
                'nro_receipt' => $data_invoice->nro_receipt,
                'type_operation' => $data_invoice->type_operation,
                'id_current_account' => $data_current_account->id,
                'total_amount' => $data_invoice->total_amount_invoice,
                'current_balance_account' => $saldo_actual_cuenta == 0 ? $data_invoice->total_amount_invoice : $saldo_actual_cuenta,
                'date_movement' => date('Y-m-d H:i:m'),
                'created_at' => date('Y-m-d H:i:m')
            ]
        );

        //UNA VEZ CARGADA LA OPERACION PREPARO LA DATA PARA GENERAR UNA ACTUALIZACION DE SALDO Y FECHA ULT MOV DE LA CUENTA 

        // SI ES DEBITO RESTO
        if ($data_invoice->type_operation == 'DEBITO'){ 
            $data_current_account->balance = ((float)$saldo_actual_cuenta - (float)$data_invoice->total_amount_invoice);
        //SI ES CREDITO SUMO
        } else if ($data_invoice->type_operation == 'CREDITO'){ 
            // if ($saldo_actual_cuenta == 0) {
            //     $saldo_actual_cuenta = (float)$data_invoice->total_amount_invoice;
            // } else {
                $data_current_account->balance = ((float)$saldo_actual_cuenta + (float)$data_invoice->total_amount_invoice);
            // }
        } 
        
        //Actualizo saldo de la cuenta y fecha ultimo movimiento
        $resultUpdate = DB::table('current_accounts')
        ->where('id', $data_current_account->id)
        ->update([
            'balance' => $data_current_account->balance, 
            'last_movement_account' => date('Y-m-d H:i:m'),
            'updated_at' => date('Y-m-d H:i:m')
        ]);

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Creado Correctamente',
            'cuenta_corriente_actualizada' => $resultUpdate
        ]);
    }
}
