<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use AdminCoop\Http\Controllers\Controller;
use \AdminCoop\Production; 
use \AdminCoop\Person; 
use \AdminCoop\TypeProduct;
use \AdminCoop\Machine; 
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas

class ProductionController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'PRODUCCIÓN';
        $this->modulo_msg = 'Producción';
        $this->form = 'Produccion';
        $this->module = 'produccion';
        $this->name_file = 'production';
        $this->modals_btns = 'Production';
        $this->model = new Production;
    }

    public function listing(){
        $data_controller = $this->model->select('productions.*', 'p.name as person_name', 'p.lastname as person_lastname',
            'p.cuit as person_cuit', 'pr.description as product_desc', 'm.description as machine_desc')
            ->join('people as p', 'productions.id_person', '=', 'p.id')
            ->join('machines as m', 'productions.id_machine', '=', 'm.id')
            ->join('productos as pr', 'productions.id_product', '=', 'pr.id')
            ->orderBy('productions.created_at', 'desc')
            ->get();

        return response()->json(
            $data_controller->toArray()          
        );
    }

 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_persons = Person::All()->where('id_type_people', '=', '3');
        $data_type_products = TypeProduct::All();
        $data_machines = Machine::All();

        return view($this->module . '.' . $this->name_file . '_index', compact('data_persons', 'data_type_products', 'data_machines'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $production = $this->model->create($request->all());

            //Obtenemos el producto ahora, para saber el stock en este mismo momento
            $productoNow = DB::table('productos')
            ->where('id', $production->id_product)
            ->select('stock')
            ->get();

            //Actualizamos producto aumentando el stock y modificamos ultimo cambio stock
            $update = DB::table('productos')
            ->where('id', $request->id_product)
            ->update([
                'stock' => (float)$productoNow[0]->stock + (float)$production->quantity_product, 
                'last_change_stock' => date('Y-m-d H:i:m')
            ]);

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente y Stock incrementado de producto',
                'result' => $production
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->select('productions.*', 'p.name as person_name', 'p.lastname as person_lastname',
            'p.cuit as person_cuit', 'pr.description as product_desc', 'm.description as machine_desc', 'pr.id_type_product as id_type_product')
            ->join('people as p', 'productions.id_person', '=', 'p.id')
            ->join('machines as m', 'productions.id_machine', '=', 'm.id')
            ->join('productos as pr', 'productions.id_product', '=', 'pr.id')
            ->orderBy('productions.created_at', 'desc')
            ->where('productions.id', '=', $id)
            ->get();

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificada Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        //Obtenemos el producto ahora, para saber el stock en este mismo momento
        $productoNow = DB::table('productos')
            ->where('id', $data_controller['id_product'])
            ->select('stock')
            ->get();

        //Actualizamos producto descontando el stock y modificamos ultimo cambio stock
        $update = DB::table('productos')
        ->where('id', $data_controller['id_product'])
        ->update([
            'stock' => (float)$productoNow[0]->stock - (float)$data_controller->quantity_product, 
            'last_change_stock' => date('Y-m-d H:i:m')
        ]);

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminada Correctamente'      
        ]);

    }
}
