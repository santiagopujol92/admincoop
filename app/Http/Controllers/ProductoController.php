<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use \AdminCoop\Producto; 
use \AdminCoop\TypeProduct; 
use Session;
use Redirect;

class ProductoController extends Controller
{

    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'MATERIAL';
        $this->modulo_msg = 'Material';
        $this->form = 'Producto';
        $this->module = 'productos';
        $this->name_file = 'product';
        $this->modals_btns = 'Product';
        $this->model = new Producto;  
    }

    public function listing(){
        $data_controller = $this->model->join('type_products', 'productos.id_type_product', '=', 'type_products.id')
            ->select('productos.id', 
                'productos.description',
                'productos.code_product', 
                'productos.type_metric_product', 
                'productos.max_purchase_price',
                'productos.min_purchase_price',
                'productos.last_purchase_price',
                'productos.last_sale_price',
                'productos.max_quantity_stock',
                'productos.min_quantity_stock',
                'productos.id_type_product', 
                'productos.stock',
                'type_products.description as type_description')
            ->orderBy('productos.id', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_type_products = TypeProduct::All();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_type_products'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Si viene desde el boton aumentar osea trayendo la key increment_stock
        //El ultimo cambio de stock solo se registra desde el aumentar stock
        if ($request['increment_stock']){
            $request['last_change_stock'] = date('Y-m-d H:i:m', strtotime(date('Y-m-d H:i:m') . " -3 hours"));

            $data_controller = $this->model->find($id);
            $data_controller->fill([
                                    'last_change_stock' => $request['last_change_stock'], 
                                    'stock' => (float)$data_controller->stock + (float)$request->increment_stock
                                ]); 

        //Si no viene con incrment stock, (osea esta haciendo el update general)
        }else{
            $data_controller = $this->model->find($id);
            $data_controller->fill($request->All());
        }

        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }
    
    //Buscar producto por tipo de producto
    function findByTypeProductId($id){
        $products = $this->model->join('type_products as tp', 'productos.id_type_product', '=', 'tp.id')
            ->select('productos.id', 
            'productos.description', 
            'productos.type_metric_product', 
            'productos.last_sale_price', 
            'productos.last_purchase_price',
            'productos.stock',
            'productos.min_purchase_price',
            'productos.max_purchase_price',
            'productos.code_product'
            )
            ->where('productos.id_type_product', '=', $id)
            ->get();

        return response()->json(
            $products->toArray()
        );
    }
}
