<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use \AdminCoop\Country; 
use \AdminCoop\Province; 
use Session;
use Redirect;

class ProvinceController extends Controller
{
 
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        $this->middleware('root');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'PROVINCIA';
        $this->modulo_msg = 'Provincia';
        $this->form = 'Provincia';
        $this->module = 'provincias';
        $this->name_file = 'province';
        $this->modals_btns = 'Province';
        $this->model = new Province;    
    }

    public function listing(){
        $data_controller = $this->model->join('countries as c', 'provinces.id_country', '=', 'c.id')
            ->select('provinces.*', 'c.description as country_name')
            ->orderBy('provinces.id', 'asc')
            ->get();
        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_countries = Country::All();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_countries'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_controller = $this->model->find($id);

        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All());
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'     
        ]);

    }

    /**
     * Retorna las provincias filtradas por id de pais
     */
    public function findByCountryId($id){

        $provinces = $this->model->join('countries as c', 'provinces.id_country', '=', 'c.id')
            ->select('provinces.id', 'provinces.description')
            ->where('provinces.id_country', '=', $id)
            ->get();

        return response()->json(
            $provinces->toArray()
        );
    }
}
