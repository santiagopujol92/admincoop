<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use \AdminCoop\Person; 
use \AdminCoop\Producto;
use \AdminCoop\TypeProduct; 
use AdminCoop\ProductsByPurchase;
use \AdminCoop\StatusShipment; 
use \AdminCoop\TypeMovementAccount; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \AdminCoop\Purchase; 

class PurchaseController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'COMPRA';
        $this->modulo_msg = 'Compra';
        $this->form = 'Compra';
        $this->module = 'compras';
        $this->name_file = 'purchase';
        $this->modals_btns = 'Purchase';
        $this->model = new Purchase;  
    }

    public function listing()
    {
        $data_controller = $this->model->join('people as p', 'purchases.id_person', '=', 'p.id')
            ->join('status_shipments as ss', 'purchases.id_status_shipment', '=', 'ss.id')
            ->leftJoin('receipt_purchases as rp', 'rp.id_purchase', '=', 'purchases.id')
            ->select('purchases.*', 'p.name as person_name',
                'p.lastname as person_lastname', 
                'ss.description as desc_status_shipment',
                DB::raw('COUNT(rp.id) AS cant_recibos'),
                DB::raw('SUM(rp.total_amount) AS total_monto_orden_de_pago'))
            ->groupBy('purchases.id', 'purchases.id_person', 'purchases.alicuota_iva', 'purchases.alicuota_iva_porc', 'purchases.id_status_shipment', 'purchases.purchase_cost', 'purchases.purchase_date',
            'purchases.created_at', 'purchases.updated_at', 'purchases.deleted_at', 'purchases.purchase_total_deuda', 'person_name', 'person_lastname', 
            'ss.description')
            ->orderBy('purchases.id', 'desc')
            ->get();

        $data_productos_by_purchase = ProductsByPurchase::select('products_by_purchase.id_product', 'products_by_purchase.id_purchase', 
        'p.id_type_product as id_type_product', 'products_by_purchase.quantity as cantidad_producto', 'p.code_product')
        ->join('productos as p', 'p.id', '=', 'products_by_purchase.id_product')
        ->get();

        return response()->json([
            'data' => $data_controller->toArray(),
            'data_productos_by_purchase' => $data_productos_by_purchase,
            'data_user' =>  Auth::user(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_new_purchase = DB::table('purchases')->max('id') + 1;

        $data_persons = Person::select('people.name','people.lastname', 'people.id', 'people.cuit')
            ->join('current_accounts as ca', 'people.id', '=', 'ca.id_person')
            ->orWhere('id_type_people', '=', '2')
            ->orWhere('id_type_people', '=', '4')
            ->orderBy('id_type_people', 'asc')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_products = Producto::select('*')->orderBy('description', 'desc')->get();

        $data_status_shipments = StatusShipment::All();

        $data_type_products = TypeProduct::All();

        $data_types_movement_account = TypeMovementAccount::All();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_persons', 
        'data_products', 'data_status_shipments', 
        'data_type_products', 'id_new_purchase', 'data_types_movement_account'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    //Obtener nuevo id proxima compra
    public function getNewPurchaseId(){
        $id_new_purchase = DB::table('purchases')->max('id') + 1;
        return response()->json([
            'id_new_purchase' => $id_new_purchase
        ]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {

            $id_compra = 0;

            //Excluimos lo de productos y pesajes que viene por el form
            $productos = json_decode($request->productos, true);
            $pesajes = json_decode($request->pesajes, true);
            $data_compra = $request->except('id_product', 'id_type_product', 'quantity', 'purchase_price', 'productos', 'bruto_chasis', 'neto_chasis', 'tara_chasis', 'bruto_acoplado', 'neto_acoplado', 'tara_acoplado');

            //formateo de fecha timestamp con datetime
            $carbon = new Carbon;
            if (isset($data_compra['purchase_date']) && $data_compra['purchase_date'] != ''){
                $data_compra['purchase_date'] = $carbon->formatDateToSaveBD($data_compra['purchase_date'], 'Y-m-d H:i:s');
            }

            // Creo la compra
            if (isset($data_compra)){
                $id_compra = DB::table('purchases')->insertGetId([
                        'id_person' => $data_compra['id_person'], 
                        'id_status_shipment' => $data_compra['id_status_shipment'],
                        'purchase_cost' => $data_compra['purchase_cost'],
                        'purchase_total_deuda' => $data_compra['purchase_cost'],
                        'purchase_date' => $data_compra['purchase_date'],
                        'alicuota_iva' => $data_compra['alicuota_iva'],
                        'created_at' => date('Y-m-d H:i:m')
                    ]
                );
            }

            $result_pesajes_inserted = false;
            // agregamos pesajes por compra
            if ($id_compra > 0 && count($pesajes) > 0) {
                $result_pesajes_inserted = $this->insertPesajesByPurchase($pesajes, $id_compra);
            }

            // Si se creo la compra y existen productos para asignar a la compra
            if ($id_compra > 0 && count($productos) > 0) {

                $result_products_inserted = false;

                // acumulamos stock de productos
                $countsProducts = $this->countStockProducts($productos);

                //Insertando productos de compra
                if ($countsProducts) {
                    $result_products_inserted = $this->insertProductsByPurchase($productos, $id_compra);
                }

                // Seleccionar data Compra
                $data_compra = Purchase::select('purchases.*' , 'p.name as person_name', 'p.lastname as person_lastname', 
                'p.cuit as person_cuit', 'p.adress', 'ci.description as condicion_iva_desc')
                    ->join('people as p', 'purchases.id_person', '=', 'p.id')
                    ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
                    ->orderBy('purchases.id', 'asc')
                    ->where('purchases.id', '=', $id_compra)
                    ->get();

                return response()->json([
                    'mensaje' => 'Compra creada, productos asignados con éxito, Pesajes asignados con éxito. Stock de productos acumulados con éxito',
                    'productos_agregados' => $result_products_inserted,
                    'pesajes_agregados' => $result_pesajes_inserted,
                    'data_compra' => $data_compra,
                    'id_compra' => $id_compra
                ]);

            } else {
                return response()->json([
                    'mensaje' => 'Compra creada con éxito pero no se agregaron productos',
                    'result' => 0
                ]);
            }

        } else {
            return response()->json([
                'mensaje' => 'No se recibieron datos',
                'result' => -1
            ]);
        }
    }

    // Aumentamos stock de productos
    function countStockProducts($productos)
    {
        if (count($productos) > 0) {
            foreach ($productos as $key => $product) {

                //Obtenemos el producto ahora, para saber el stock en este mismo momento
                $productoNow = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->select('stock')
                    ->get();
                
                //Actualizamos producto aumentando el stock y modificamos ultimo precio compra y ultimo cambio stock
                $update = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->update([
                        'stock' => (float)$productoNow[0]->stock + (float)$product['quantity'], 
                        'last_purchase_price' => $product['purchase_price'],
                        'last_change_stock' => date('Y-m-d H:i:m')
                    ]);
            }
        }
        return true;
    }

    //Limpiar y inserta productos por compra
    public function insertProductsByPurchase($productos, $id_purchase, $limpiarProductosCompra = false)
    {
        //Borramos todos los productos de esta compra. Solo para el update.
        if ($limpiarProductosCompra){
            DB::table('products_by_purchase')->where('id_purchase', '=', $id_purchase)->delete();
        }

        //Luego de acumularse los stocks. Insertamos en productos by purchase
        foreach ($productos as $key => $producto) {
            if (isset($producto['id_product'])){
                DB::table('products_by_purchase')->insert([
                    array(
                        'id_purchase' => $id_purchase, 
                        'id_product' => $producto['id_product'],
                        'quantity' => $producto['quantity'],
                        'purchase_price' => $producto['purchase_price'],
                        'precio_total_producto' => (float)$producto['quantity'] * (float)$producto['purchase_price'],
                        'created_at' => date('Y-m-d H:i:m')
                    )]
                );
            }
        }
        return true;
    }

    //Limpiar y inserta pesajes por compra
    public function insertPesajesByPurchase($pesajes, $id_purchase, $limpiarPesajesCompra = false)
    {
        //Borramos todos los pesajes de esta compra. Solo para el update.
        if ($limpiarPesajesCompra){
            DB::table('pesajes_by_purchases')->where('id_purchase', '=', $id_purchase)->delete();
        }

        //Insertamos en pesajes by purchase
        foreach ($pesajes as $key => $pesaje) {
            DB::table('pesajes_by_purchases')->insert([
                array(
                    'id_purchase' => $id_purchase, 
                    'bruto_chasis' => (float)$pesaje['bruto_chasis'],
                    'neto_chasis' => (float)$pesaje['neto_chasis'],
                    'tara_chasis' => (float)$pesaje['tara_chasis'],
                    'bruto_acoplado' => (float)$pesaje['bruto_acoplado'],
                    'neto_acoplado' => (float)$pesaje['neto_acoplado'],
                    'tara_acoplado' => (float)$pesaje['tara_acoplado'],
                    'created_at' => date('Y-m-d H:i:m')
                )]
            );
        }
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Datos compra
        $data_compra = Purchase::select('purchases.*' , 'p.name as person_name', 'p.lastname as person_lastname', 
        'p.cuit as person_cuit', 'p.adress', 'ci.description as condicion_iva_desc')
            ->join('people as p', 'purchases.id_person', '=', 'p.id')
            ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
            ->orderBy('purchases.id', 'asc')
            ->where('purchases.id', '=', $id)
            ->get();

        //Productos de compra
        $data_products_by_compra = DB::table('products_by_purchase')
            ->join('productos as p', 'products_by_purchase.id_product', '=', 'p.id')
            ->select('products_by_purchase.*', 'p.description as nombre_producto', 'p.code_product', 'p.type_metric_product')
            ->orderBy('products_by_purchase.id_product', 'asc')
            ->where('products_by_purchase.id_purchase', '=', $id)
            ->get();

        //Pesajes de compra
        $data_pesajes_by_compra = DB::table('pesajes_by_purchases')
        ->select('pesajes_by_purchases.*')
        ->orderBy('pesajes_by_purchases.id', 'asc')
        ->where('pesajes_by_purchases.id_purchase', '=', $id)
        ->get();

        //Return array con 3 array adentro
        return response()->json(
            array(
                'data_compra' => $data_compra->toArray(),
                'data_products_by_compra' => $data_products_by_compra->toArray(),
                'data_pesajes_by_compra' => $data_pesajes_by_compra->toArray()
            )
        );
    }

    public function getPurchasesData() {
        $data_compras = DB::table('purchases')
        ->join('people as p', 'purchases.id_person', '=', 'p.id')
        ->select('purchases.*', 'p.name as person_name', 'p.lastname as person_lastname', 'p.cuit as person_cuit')
        ->where('purchases.id_status_shipment', '!=', 2)
        ->get();

        return response()->json([
            'compras' => $data_compras
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
