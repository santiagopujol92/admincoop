<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \Carbon\Carbon;
use \AdminCoop\Receipt; 
use \AdminCoop\ChequesByReceipt; 

class ReceiptController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'RECIBO';
        $this->modulo_msg = 'Recibo';
        $this->form = 'Recibo';
        $this->module = 'recibos';
        $this->name_file = 'receipt';
        $this->modals_btns = 'Receipt';
        $this->model = new Receipt;  
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {

            $id_receipt = 0;

            //Guardo los cheques
            $cheques = json_decode($request->cheques, true);
            $data_movimiento = $request->except('nombre', 'cuit', 'importe', 'banco', 'nro_cheque', 'concepto', 'id_current_account', 'monto_venta', 'balance', 'id_type_movement_account', 'type_operation');
            // Excluimos datos para insertar recibo
            $data_receipt = $request->except('nombre', 'cuit', 'importe', 'banco', 'nro_cheque', 'concepto', 'id_current_account', 'monto_venta', 'balance', 'id_type_movement_account', 'type_operation');
            
            $id_receipt = $this->model->create($data_receipt)->id;

            //Insertar cheques si hay
            $carbon = new Carbon;
            foreach ($cheques as $key => $cheque) {
                if (isset($cheque['fecha_emision']) && $cheque['fecha_emision'] != ''){
                    $cheque['fecha_emision'] = $carbon->formatDateToSaveBD($cheque['fecha_emision'], 'Y-m-d H:i:s');
                } else {
                    $cheque['fecha_emision'] = date('Y-m-d H:i:m');
                }

                if (isset($cheque['fecha_pago']) && $cheque['fecha_pago'] != ''){
                    $cheque['fecha_pago'] = $carbon->formatDateToSaveBD($cheque['fecha_pago'], 'Y-m-d H:i:s');
                } else {
                    $cheque['fecha_pago'] = date('Y-m-d H:i:m');
                }

                $cheque['id_receipt'] = $id_receipt;
                ChequesByReceipt::create($cheque);
            }

            // Actualizar monto adeudado de la factura

            //Obtenemos la invoices ahora, para saber el total_amount_deuda que tiene
            $invoiceNow = DB::table('invoices')
            ->where('id', $data_receipt['id_invoice'])
            ->select('total_amount_deuda')
            ->get();

            $total_monto_adeudado = (float)$invoiceNow[0]->total_amount_deuda - (float)$data_receipt['total_amount'];

            $update = DB::table('invoices')
            ->where('id', $data_receipt['id_invoice'])
            ->update([
                'total_amount_deuda' => $total_monto_adeudado, 
            ]);

            // Si el monto adeudado es 0 entonces actualizo el estado de la factura
            if ($total_monto_adeudado == 0){
                $update = DB::table('invoices')
                ->where('id', $data_receipt['id_invoice'])
                ->update([
                    'id_status_invoice' => 3, 
                ]);
            }

            // Crear movimiento llamando al controlador de movimientos o crearlo desde aca
            if (isset($data_receipt)){
                $id_receipt = DB::table('movements_accounts')->insertGetId([
                        'id_current_account' => $request->id_current_account, 
                        'nro_receipt' => $request->id_invoice, 
                        'type_operation' => $request->type_operation, 
                        'id_type_movement_account' => $request->id_type_movement_account,
                        'total_amount' => $request->total_amount,
                        'current_balance_account' => (float)$request->balance - (float)$request->total_amount,
                        'date_movement' => date('Y-m-d H:i:m'),
                        'created_at' => date('Y-m-d H:i:m')
                    ]
                );
            }

            //Actualizar saldo cuenta corriente
            $update = DB::table('current_accounts')
            ->where('id', $request->id_current_account)
            ->update([
                'balance' => (float)$request->balance - (float)$request->total_amount, 
                'last_movement_account' => date('Y-m-d H:i:m'), 

            ]);

            return response()->json([
                'mensaje' => 'Recibo creado con exito, Monto Adeudado de factura actualizado, movimiento de cuenta creado con éxito ',
                'result' => $id_receipt
            ]);

        } else {
            return response()->json([
                'mensaje' => 'No se recibieron datos',
                'result' => -1
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Obtener recibos y movimiento relacionado por factura
    public function GetRecibosByInvoice($id_invoice){
        $data_controller = DB::table('receipts')
            ->leftJoin('cheques_by_receipts as cbr', 'receipts.id', '=', 'cbr.id_receipt')
            ->select('receipts.id', 'receipts.created_at', 'receipts.id_invoice', 'receipts.effective_amount', 'receipts.card_amount', 
                'receipts.retencion_amount', 'receipts.retencion2_amount', 'receipts.total_amount', 'receipts.nro_orden_pago',
            DB::raw('COUNT(cbr.id) AS cant_cheques'))
            ->where('receipts.id_invoice', '=', $id_invoice)
            ->groupBy('receipts.id', 'receipts.created_at', 'receipts.id_invoice', 'receipts.effective_amount', 'receipts.card_amount', 
            'receipts.retencion_amount', 'receipts.retencion2_amount', 'receipts.total_amount', 'receipts.nro_orden_pago')
            ->orderBy('receipts.id', 'desc')
            ->get();

        return response()->json(
            $data_controller->toArray()          
        );
    }

    // Obtener recibo por id recibo
    public function GetReciboByIdRecibo($id_receipt){
        $data_recibo = DB::table('receipts')
            ->select('receipts.*')
            ->where('receipts.id', '=', $id_receipt)
            ->get();

        $data_cheques = DB::table('cheques_by_receipts')
            ->select('cheques_by_receipts.*')
            ->where('cheques_by_receipts.id_receipt', '=', $id_receipt)
            ->get();

        // Seleccionar data Factura
        $data_invoice_receipt = DB::table('invoices')
        ->join('people as p', 'invoices.id_person', '=', 'p.id')
        ->join('status_invoices as st', 'invoices.id_status_invoice', '=', 'st.id')
        ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
        ->leftjoin('sales as s', 's.id', '=', 'invoices.id_sale')
        ->select('invoices.*', 's.sell_cost as sell_cost', 'p.name as person_name', 'p.lastname as person_lastname', 
            'p.cuit as person_cuit', 'st.description as status_invoice_desc', 'p.adress',
            'ci.description as condicion_iva_desc')
        ->where('invoices.id', '=', $data_recibo[0]->id_invoice)
        ->get();

        return response()->json(
            array(
                'data_recibo' => $data_recibo->toArray(),
                'data_cheques' => $data_cheques->toArray(),
                'data_invoice_receipt' => $data_invoice_receipt
            )
        );
    }
}
