<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas
use \Carbon\Carbon;
use \AdminCoop\ReceiptPurchase; 
use \AdminCoop\ChequesByReceiptPurchase; 

class ReceiptPurchaseController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'RECIBO COMPRA';
        $this->modulo_msg = 'Recibo Compra';
        $this->form = 'ReciboCompra';
        $this->module = 'recibos_compras';
        $this->name_file = 'receipt_purchase';
        $this->modals_btns = 'ReceiptPurchase';
        $this->model = new ReceiptPurchase;  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {

            $id_receipt = 0;
            
            //Guardo los cheques
            $cheques = json_decode($request->cheques, true);

            // Excluimos datos para insertar recibo
            $data_receipt = $request->except('nombre', 'cuit', 'importe', 'banco', 'nro_cheque', 'concepto', 'fecha_emision', 'fecha_pago', 
            'id_current_account', 'monto_venta', 'balance', 'id_type_movement_account', 'type_operation');
            
            $id_receipt = $this->model->create($data_receipt)->id;

            //Insertar cheques si hay
            $carbon = new Carbon;
            foreach ($cheques as $key => $cheque) {
                if (isset($cheque['fecha_emision']) && $cheque['fecha_emision'] != ''){
                    $cheque['fecha_emision'] = $carbon->formatDateToSaveBD($cheque['fecha_emision'], 'Y-m-d H:i:s');
                } else {
                    $cheque['fecha_emision'] = date('Y-m-d H:i:m');
                }

                if (isset($cheque['fecha_pago']) && $cheque['fecha_pago'] != ''){
                    $cheque['fecha_pago'] = $carbon->formatDateToSaveBD($cheque['fecha_pago'], 'Y-m-d H:i:s');
                } else {
                    $cheque['fecha_pago'] = date('Y-m-d H:i:m');
                }

                $cheque['id_receipt_purchase'] = $id_receipt;
                ChequesByReceiptPurchase::create($cheque);
            }

            // Actualizar monto adeudado de la compra
            //Obtenemos la compra ahora, para saber el purchase_total_deuda que tiene
            $purchaseNow = DB::table('purchases')
            ->where('id', $data_receipt['id_purchase'])
            ->select('purchase_total_deuda')
            ->get();

            $purchase_total_deuda = (float)$purchaseNow[0]->purchase_total_deuda - (float)$data_receipt['total_amount'];

            $update = DB::table('purchases')
            ->where('id', $data_receipt['id_purchase'])
            ->update([
                'purchase_total_deuda' => $purchase_total_deuda, 
            ]);

            // Si el monto adeudado es 0 entonces actualizo el estado de la compra
            if ($purchase_total_deuda == 0){
                $update = DB::table('purchases')
                ->where('id', $data_receipt['id_purchase'])
                ->update([
                    'id_status_shipment' => 3, 
                ]);
            }

            return response()->json([
                'mensaje' => 'Orden de Pago creada con exito',
                'result' => $id_receipt,
            ]);

        } else {
            return response()->json([
                'mensaje' => 'No se recibieron datos',
                'result' => -1
            ]);
        }
    }

    // Obtener recibos  relacionado por compro
    public function GetRecibosByPurchase($id_purchase){
        $data_controller = DB::table('receipt_purchases')
            ->leftJoin('cheques_by_receipt_purchases as cbr', 'receipt_purchases.id', '=', 'cbr.id_receipt_purchase')
            ->select('receipt_purchases.id', 'receipt_purchases.created_at', 'receipt_purchases.id_purchase', 'receipt_purchases.effective_amount', 'receipt_purchases.card_amount', 
                'receipt_purchases.retencion_amount', 'receipt_purchases.retencion2_amount', 'receipt_purchases.total_amount', 'receipt_purchases.nro_invoice',
            DB::raw('COUNT(cbr.id) AS cant_cheques'))
            ->where('receipt_purchases.id_purchase', '=', $id_purchase)
            ->groupBy('receipt_purchases.id', 'receipt_purchases.created_at', 'receipt_purchases.id_purchase', 'receipt_purchases.effective_amount', 'receipt_purchases.card_amount', 
            'receipt_purchases.retencion_amount', 'receipt_purchases.retencion2_amount', 'receipt_purchases.total_amount', 'receipt_purchases.nro_invoice')
            ->orderBy('receipt_purchases.id', 'desc')
            ->get();

        return response()->json(
            $data_controller->toArray()          
        );
    }

    // Obtener recibo por id recibo
    public function GetReciboByIdRecibo($id_receipt){
        $data_recibo = DB::table('receipt_purchases')
            ->select('receipt_purchases.*')
            ->where('receipt_purchases.id', '=', $id_receipt)
            ->get();

        $data_cheques = DB::table('cheques_by_receipt_purchases')
            ->select('cheques_by_receipt_purchases.*')
            ->where('cheques_by_receipt_purchases.id_receipt_purchase', '=', $id_receipt)
            ->get();


        // Seleccionar data Compra
        $data_purchase_receipt = DB::table('purchases')
            ->select('purchases.*' , 'p.name as person_name', 'p.lastname as person_lastname', 
            'p.cuit as person_cuit', 'p.adress', 'ci.description as condicion_iva_desc')
            ->join('people as p', 'purchases.id_person', '=', 'p.id')
            ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
            ->orderBy('purchases.id', 'asc')
            ->where('purchases.id', '=', $data_recibo[0]->id_purchase)
            ->get();
        
        return response()->json(
            array(
                'data_recibo' => $data_recibo->toArray(),
                'data_cheques' => $data_cheques->toArray(),
                'data_purchase_receipt' => $data_purchase_receipt
            )
        );
    }
}
