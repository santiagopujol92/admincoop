<?php

namespace AdminCoop\Http\Controllers;

use \Carbon\Carbon;
use \AdminCoop\Sale; 
use \AdminCoop\Person; 
use \AdminCoop\Invoice; 
use \AdminCoop\Producto;
use \AdminCoop\TypeProduct; 
use Illuminate\Http\Request;
use AdminCoop\ProductsBySale;
use \AdminCoop\StatusInvoice; 
use \AdminCoop\StatusShipment; 
use \AdminCoop\TypeMovementAccount; 
use Illuminate\Support\Facades\Auth;
use AdminCoop\Http\Controllers\MovementsAccountController;
use Illuminate\Support\Facades\DB; //Traeomos el objeto DB para tener acceso a todas las tablas

class SaleController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        if ($this->getMiddleware() == 'root'){
            $this->middleware('root');
        }

        //Permitir acceso para rol admin
        if ($this->getMiddleware() == 'admin'){
            $this->middleware('admin');
        }

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'VENTA';
        $this->modulo_msg = 'Venta';
        $this->form = 'Venta';
        $this->module = 'ventas';
        $this->name_file = 'sale';
        $this->modals_btns = 'Sale';
        $this->model = new Sale;  
    }

    public function listing()
    {
        $data_controller = $this->model->join('people as p', 'sales.id_person', '=', 'p.id')
            ->join('status_shipments as ss', 'sales.id_status_shipment', '=', 'ss.id')
            ->leftJoin('invoices as i', 'i.id_sale', '=', 'sales.id')
            ->leftJoin('status_invoices as si', 'si.id', '=', 'i.id_status_invoice')
            ->leftjoin('current_accounts as ca', 'ca.id_person', '=', 'p.id')
            ->leftJoin('receipts as r', 'r.id_invoice', '=', 'i.id')
            ->select('sales.*', 'p.name as person_name', 'ca.balance as balance_account', 'ca.id as id_current_account',
                'p.lastname as person_lastname', 'ss.description as desc_status_shipment',
                'i.id as invoice_id', 'si.description as desc_status_invoice', 'i.total_amount as monto_factura', 'i.total_amount_deuda as monto_factura_deuda',
                DB::raw('COUNT(r.id) AS cant_recibos'))
            ->groupBy('sales.id', 'sales.id_person', 'sales.id_status_shipment', 'sales.sell_cost', 'sales.sell_date',
            'sales.created_at', 'sales.updated_at', 'sales.deleted_at', 'person_name', 'ca.balance', 'ca.id', 'person_lastname',
            'ss.description', 'i.id', 'si.description', 'i.total_amount', 'i.total_amount_deuda')
            ->orderBy('sales.id', 'desc')
            ->get();

        $data_productos_by_invoice = ProductsBySale::select('products_by_sale.id_product', 
        'products_by_sale.id_sale', 'p.id_type_product as id_type_product', 'p.code_product', 'products_by_sale.quantity as cantidad_producto')
        ->join('productos as p', 'p.id', '=', 'products_by_sale.id_product')
        ->get();

        return response()->json([
            'data' => $data_controller->toArray(),
            'data_productos_by_invoice' => $data_productos_by_invoice,
            'data_user' =>  Auth::user(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_new_sale = DB::table('sales')->max('id') + 1;
        $id_new_invoice = DB::table('invoices')->max('id') + 1;

        $data_persons = Person::select('people.name','people.lastname', 'people.id', 'people.cuit', 
            'ci.description as condicion_iva_desc', 'people.adress')
            ->join('current_accounts as ca', 'people.id', '=', 'ca.id_person')
            ->join('condicion_ivas as ci', 'ci.id', '=', 'people.id_condicion_iva')
            ->where('id_type_people', '=', '1')
            ->orWhere('id_type_people', '=', '2')
            ->orWhere('id_type_people', '=', '4')
            ->orderBy('id_type_people', 'asc')
            ->orderBy('lastname', 'desc')
            ->get();

        $data_products = Producto::select('*')->orderBy('description', 'desc')->get();

        $data_status_shipments = StatusShipment::All();

        $data_status_invoices = StatusInvoice::All();

        $data_type_products = TypeProduct::All();

        $data_types_movement_account = TypeMovementAccount::All();

        $data_invoices = Invoice::All();

        $data_invoices = DB::table('invoices')
        ->join('people as p', 'invoices.id_person', '=', 'p.id')
        ->leftjoin('current_accounts as ca', 'ca.id_person', '=', 'p.id')
        ->select('invoices.*', 'p.name as person_name', 'p.lastname as person_lastname', 'p.cuit as person_cuit',
        'ca.balance as balance_account', 'ca.id as id_current_account')
        ->where('invoices.id_status_invoice', '!=', 3)
        ->where('invoices.id_status_invoice', '!=', 2)
        ->get();

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_persons', 
        'data_products', 'data_status_shipments', 
        'data_type_products', 'id_new_sale', 'id_new_invoice',
        'data_status_invoices', 'data_types_movement_account', 'data_invoices'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {

            $id_venta = 0;

            //Excluimos lo de productos que viene por el form
            $productos = json_decode($request->productos, true);
            $data_venta = $request->except('id_product', 'id_type_product', 'quantity', 'purchase_price', 'productos');

            //formateo de fecha timestamp con datetime
            $carbon = new Carbon;
            if (isset($data_venta['sell_date']) && $data_venta['sell_date'] != ''){
                $data_venta['sell_date'] = $carbon->formatDateToSaveBD($data_venta['sell_date'], 'Y-m-d H:i:s');
            }

            // Creo la venta
            if (isset($data_venta)){
                $id_venta = DB::table('sales')->insertGetId([
                        'id_person' => $data_venta['id_person'], 
                        'id_status_shipment' => $data_venta['id_status_shipment'],
                        'sell_cost' => $data_venta['sell_cost'],
                        'sell_date' => $data_venta['sell_date'],
                        'created_at' => date('Y-m-d H:i:m')
                    ]
                );
            }

            // Si se creo la venta y existen productos para asignar a la venta
            if ($id_venta > 0 && count($productos) > 0) {

                //Si no pueden descontarse todos los productos entonces corto la operacion
                $productosPuedenDescontarse = $this->verifyIfCanDiscountStockProducts($productos);

                //Si pueden descontarse continuo, si no pudieron salta en la funcion la respuesta del que no pudo
                if ($productosPuedenDescontarse){

                    $result_products_inserted = false;

                    // Descontamos stock de productos
                    $discountProducts = $this->discountStockProducts($productos);

                    //Insertando productos de venta
                    if ($discountProducts) {
                        $result_products_inserted = $this->insertProductsBySale($productos, $id_venta);
                    }

                    $id_new_invoice = $this->getNewInvoiceId();
                    
                    return response()->json([
                        'mensaje' => 'Venta creada y productos asignados con éxito. Stock de productos descontados con éxito',
                        'result' => $result_products_inserted,
                        'id_new_invoice' => $id_new_invoice
                    ]);
                }

            } else {
                return response()->json([
                    'mensaje' => 'Venta creada con éxito pero no se agregaron productos',
                    'result' => 0
                ]);
            }

        } else {
            return response()->json([
                'mensaje' => 'No se recibieron datos',
                'result' => -1
            ]);
        }
    }

    // Verificar si todos los productos estan aptos para descontar stock
    function verifyIfCanDiscountStockProducts($productos)
    {
        $productosPuedenDescontarse = true;
        if (count($productos) > 0) {
            foreach ($productos as $key => $product) {

                //Obtenemos el producto ahora, para saber el stock en este mismo momento
                $productoNow = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->select('stock')
                    ->get();

                //Si al descontarse da menor que 0 entonces no se puede descontar y retorno
                if (((float)$productoNow[0]->stock - (float)$product['quantity']) < 0 ) {
                    $productosPuedenDescontarse = false;
                }

                //Si no pueden descontarse todos los productos entonces corto la operacion
                if (!$productosPuedenDescontarse){
                    return response()->json([
                        'mensaje' => 'El producto ' . $product['nombre_producto'] . 'no posee el stock suficiente para continuar con la operación. El stock es de ' . $productoNow[0]->stock . ' y se desea usar ' . $product['quantity'],
                        'puedeDescontarse' =>  $productosPuedenDescontarse,
                    ]);
                }
            }
        }
        return $productosPuedenDescontarse;
    }

    //Descontamos productos, antes deben haber pasado por verifyIfCanDiscountStockProducts
    function discountStockProducts($productos)
    {
        if (count($productos) > 0) {
            foreach ($productos as $key => $product) {

                //Obtenemos el producto ahora, para saber el stock en este mismo momento
                $productoNow = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->select('stock')
                    ->get();
                
                //Actualizamos producto descontando el stock y modificamos ultimo precio venta y ultimo cambio stock
                $update = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->update([
                        'stock' => (float)$productoNow[0]->stock - (float)$product['quantity'], 
                        'last_sale_price' => $product['purchase_price'],
                        'last_change_stock' => date('Y-m-d H:i:m')
                    ]);
            }
        }
        return true;
    }

    //Limpiar y inserta productos en venta
    public function insertProductsBySale($productos, $id_sale, $limpiarProductosVenta = false)
    {
        //Borramos todos los productos de esta venta. Solo para el update.
        if ($limpiarProductosVenta){
            DB::table('products_by_sale')->where('id_sale', '=', $id_sale)->delete();
        }

        //Luego de pasar las validaciones de stock y descontarse. Insertamos en productos by sales
        foreach ($productos as $key => $producto) {
            if (isset($producto['id_product'])){
                DB::table('products_by_sale')->insert([
                    array(
                        'id_sale' => $id_sale, 
                        'id_product' => $producto['id_product'],
                        'quantity' => $producto['quantity'],
                        'purchase_price' => $producto['purchase_price'],
                        'precio_total_producto' => (float)$producto['quantity'] * (float)$producto['purchase_price'],
                        'created_at' => date('Y-m-d H:i:m')
                    )]
                );
            }
        }
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Datos venta
        $data_venta = Sale::select('sales.*')
            ->orderBy('sales.id', 'asc')
            ->where('sales.id', '=', $id)
            ->get();

        //Productos de venta
        $data_products_by_venta = DB::table('products_by_sale')
            ->join('productos as p', 'products_by_sale.id_product', '=', 'p.id')
            ->select('products_by_sale.*', 'p.description as nombre_producto', 'p.code_product', 'p.type_metric_product')
            ->orderBy('products_by_sale.id_sale', 'asc')
            ->where('products_by_sale.id_sale', '=', $id)
            ->get();

        //Factura
        $data_invoice = DB::table('invoices')
            ->join('people as p', 'invoices.id_person', '=', 'p.id')
            ->join('status_invoices as st', 'invoices.id_status_invoice', '=', 'st.id')
            ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
            ->select('invoices.*', 'p.name as person_name', 'p.lastname as person_lastname', 
                'p.cuit as person_cuit', 'st.description as status_invoice_desc', 'p.adress',
                'ci.description as condicion_iva_desc')
            ->where('invoices.id_sale', '=', $id)
            ->get();

        //Return array con 2 array adentro
        return response()->json(
            array(
                'data_venta' => $data_venta->toArray(),
                'data_products_by_venta' => $data_products_by_venta->toArray(),
                'data_invoice' => $data_invoice->toArray()
            )
        );
    }

    //Obtener nuevo id proxima venta
    public function getNewSaleId(){
        $id_new_sale = DB::table('sales')->max('id') + 1;
        return response()->json([
            'id_new_sale' => $id_new_sale
        ]);
    }

    //Obtener nuevo id proxima venta
    public function getNewInvoiceId(){
        $id_new_invoice = DB::table('invoices')->max('id') + 1;
        // return response()->json([
        //     'id_new_invoice' => $id_new_invoice
        // ]);

        return $id_new_invoice;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_venta)
    {
        if($request->ajax()) {

            //Excluimos lo de productos que viene por el form
            $productos = json_decode($request->productos, true);
            $data_venta = $request->except('id_product', 'id_type_product', 'quantity', 'purchase_price', 'productos');

            //formateo de fecha timestamp con datetime
            $carbon = new Carbon;
            if (isset($data_venta['sell_date']) && $data_venta['sell_date'] != ''){
                $data_venta['sell_date'] = $carbon->formatDateToSaveBD($data_venta['sell_date'], 'Y-m-d H:i:s');
            }

            //Update de venta
            if (isset($id_venta)){
                $data_controller = Sale::find($id_venta);
                $data_controller->fill($data_venta);
                $data_controller->save();
            }
            
            return response()->json([
                'mensaje' => 'Venta modificada con éxito sin modificación productos',
                'result' => 0
            ]);

        } else {
            return response()->json([
                'mensaje' => 'No se recibieron datos',
                'result' => -1
            ]);
        }
    }

    // Insertar la factura luego del remito y aumento el saldo en la cuenta de este cliente
    public function InsertInvoiceSale($data_factura) {
        $data_factura = json_decode($data_factura, true);
        $id_invoice = 0;
        
        //formateo de fecha timestamp con datetime
        $carbon = new Carbon;
        if (isset($data_factura['issue_date']) && $data_factura['issue_date'] != ''){
            $data_factura['issue_date'] = $carbon->formatDateToSaveBD($data_factura['issue_date'], 'Y-m-d H:i:s');
        }

        if (isset($data_factura['expiration_date']) && $data_factura['expiration_date'] != ''){
            $data_factura['expiration_date'] = $carbon->formatDateToSaveBD($data_factura['expiration_date'], 'Y-m-d H:i:s');
        }

        // Creo la Factura
        if (isset($data_factura)){
            $id_invoice = DB::table('invoices')->insertGetId([
                    'id_person' => $data_factura['id_person'], 
                    'id_sale' => $data_factura['id_sale'],
                    'id_status_invoice' => $data_factura['id_status_invoice'],
                    'total_amount' => $data_factura['total_amount'],
                    'total_amount_deuda' => $data_factura['total_amount'],
                    'retencion' => $data_factura['retencion'],
                    'alicuota_iva' => $data_factura['alicuota_iva'],
                    'descuento' => $data_factura['descuento'],
                    'issue_date' => $data_factura['issue_date'],
                    'retencion_porc' => $data_factura['retencion_porc'],
                    'alicuota_iva_porc' => $data_factura['alicuota_iva_porc'],
                    'descuento_porc' => $data_factura['descuento_porc'],
                    'issue_date' => $data_factura['issue_date'],
                    'expiration_date' => $data_factura['expiration_date'],
                    'created_at' => date('Y-m-d H:i:m')
                ]
            );
        }

        // Si se creo la factura con exito, cargo en la cuenta corriente del cliente el monto de la misma
        if ($id_invoice > 0) {

            //Datos cuenta corriente de esa persona 
            $data_current_account = DB::table('current_accounts')
            ->where('id_person', $data_factura['id_person'])
            ->select('*')
            ->get();

            //Data de la factura
            $data_invoice = (object) array(
                'total_amount_invoice' => $data_factura['total_amount'], 
                'nro_receipt' => $id_invoice,
                'id_type_movement_account' => 1, //Factura
                'type_operation' => 'CREDITO',
            );

            $controllerMovement = new MovementsAccountController();
            $resultMovCuenta = $controllerMovement->addMovementCurrentAccount($data_current_account[0], $data_invoice);

        }
            
        $id_sale = $data_factura['id_sale'];

        // Seleccionar data Factura
        $data_invoice = DB::table('invoices')
            ->join('people as p', 'invoices.id_person', '=', 'p.id')
            ->join('status_invoices as st', 'invoices.id_status_invoice', '=', 'st.id')
            ->join('condicion_ivas as ci', 'ci.id', '=', 'p.id_condicion_iva')
            ->leftjoin('sales as s', 's.id', '=', 'invoices.id_sale')
            ->select('invoices.*', 's.sell_cost as sell_cost', 'p.name as person_name', 'p.lastname as person_lastname', 
                'p.cuit as person_cuit', 'st.description as status_invoice_desc', 'p.adress',
                'ci.description as condicion_iva_desc')
            ->where('invoices.id_sale', '=', $id_sale)
            ->get();

        return response()->json([
            'mensaje' => 'Factura agregada con éxito y movimiento generado en cuenta corriente',
            'id_factura' => $id_invoice,
            'data_invoice' => $data_invoice,
            'id_sale' => $id_sale
        ]);
    }

    // Elimina productos por venta y recupera los stocks de esos productos
    public function destroyProductsBySale($id_sale, $productos) {
        $result = false;
        $productos = json_decode($productos, true);

        if (count($productos) > 0) {
            foreach ($productos as $key => $product) {

                //Obtenemos el producto ahora, para saber el stock en este mismo momento
                $productoNow = DB::table('productos')
                ->where('id', $product['id_product'])
                ->select('stock')
                ->get();
                
                //Actualizamos producto aumentando el stock y ultimo cambio stock
                $update = DB::table('productos')
                    ->where('id', $product['id_product'])
                    ->update([
                        'stock' => (float)$productoNow[0]->stock + (float)$product['quantity'], 
                        'last_change_stock' => date('Y-m-d H:i:m')
                    ]);
   
            }

            // Eliminamos los productos de la venta
            DB::table('products_by_sale')->where('id_sale', '=', $id_sale)->delete();
            $result = true;
        }
        
        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id -> ID VENTA
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Buscar la factura que contengaa esta venta (solo 1 factura)
        $data_factura = DB::table('invoices')->where('id_sale', '=', $id)->select('*')->get();
        
        if (count($data_factura) > 0) {
            $id_factura = $data_factura[0]->id;

            // Buscar recibos, si encuentro eliminarlos
            $data_recibos = DB::table('receipts')->where('id_invoice', '=', $id_factura)->select('*')->get();

            // Si posee Eliminar Recibos
            if (count($data_recibos) > 0){

                // Recorro los recibos
                foreach ($data_recibos as $key => $recibo) {
                    $id_recibo = $recibo->id;
                    // Buscar cheques por recibo y eliminarlos
                    DB::table('cheques_by_receipts')->where('id_receipt', '=', $id_recibo)->delete();
                }

                // Eliminamos los recibos de esa factura
                DB::table('receipts')->where('id_invoice', '=', $id_factura)->delete();
            }

            // Buscar los movimientos de la factura
            $data_movements = DB::table('movements_accounts')->where('nro_receipt', '=', $id_factura)->select('*')->get();

            // Recorro los movimientos
            $monto_a_descontar_cuenta = 0;
            $monto_factura_inicial = 0;
            $monto_acumulado_debitos = 0;
            $id_cuenta_corriente = 0;
            foreach ($data_movements as $key => $movimiento) {
                $id_cuenta_corriente = $movimiento->id_current_account;
                // Recorrerlos, guardar el monto del movimiento credito de la factura y acumulo los de debito
                if ($movimiento->type_operation == 'CREDITO') {
                    $monto_factura_inicial = (float)$movimiento->total_amount;
                } else {
                    $monto_acumulado_debitos = (float)$monto_acumulado_debitos + (float)$movimiento->total_amount;
                }
            }

            // Gaurdo el monto final a descontar en la cuenta
            $monto_a_descontar_cuenta = (float)$monto_factura_inicial - (float)$monto_acumulado_debitos;

            // Actualizo la cuenta corriente con el valor que tiene restandole este monto calculado
            $currentAccountNow = DB::table('current_accounts')
            ->where('id', $id_cuenta_corriente)
            ->select('balance')
            ->get();

            DB::table('current_accounts')
            ->where('id', '=', $id_cuenta_corriente)
            ->update([
                'balance' => (float)$currentAccountNow[0]->balance - (float)$monto_a_descontar_cuenta, 
                'updated_at' => date('Y-m-d H:i:m')
            ]);

            // Elimino los movimientos
            DB::table('movements_accounts')->where('nro_receipt', '=', $id_factura)->delete();

            // Eliminar la factura
            DB::table('invoices')->where('id', '=', $id_factura)->delete();
        }


        // Buscar productos
        $productos = DB::table('products_by_sale')->where('id_sale', '=', $id)->select('*')->get();

        //Eliminado de productos del remito
        $this->destroyProductsBySale($id, $productos);

        // Buscar venta
        $data_sale = Sale::find($id);

        // Borrado logico de venta (remito)
        $data_sale->delete();

        return response()->json([
            'mensaje' => 'Factura, Remito y Recibos Eliminados Correctamente, Cuenta Corriente actualizada y Movimientos eliminados',
            'result' => true   
        ]);

    }

    public function getInvoicesData() {
        $data_invoices = DB::table('invoices')
        ->join('people as p', 'invoices.id_person', '=', 'p.id')
        ->leftjoin('current_accounts as ca', 'ca.id_person', '=', 'p.id')
        ->select('invoices.*', 'p.name as person_name', 'p.lastname as person_lastname', 'p.cuit as person_cuit',
        'ca.balance as balance_account', 'ca.id as id_current_account')
        ->where('invoices.id_status_invoice', '!=', 3)
        ->where('invoices.id_status_invoice', '!=', 2)
        ->get();

        return response()->json([
            'factura' => $data_invoices   
        ]);
    }
}

