<?php

namespace AdminCoop\Http\Controllers;

use Illuminate\Http\Request;
use \AdminCoop\User;
use \AdminCoop\TypeUser;
use Auth;
use Session;
use Redirect;
use Illuminate\Ŗouting\Rooute; //LIBRERIA PARA PARAMETRIZAR ADENTRO DEL CONTROLADOR

class UserController extends Controller
{

    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    //CORROBORAR AUTENTIFICACION
    public function __construct()
    {
        //Permitir acceso siempre autenticado
        $this->middleware('auth');

        //Permitir acceso para rol root
        $this->middleware('root');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'USUARIO';
        $this->modulo_msg = 'Usuario';
        $this->form = 'Usuario';
        $this->module = 'usuarios';
        $this->name_file = 'user';
        $this->modals_btns = 'User';
        $this->model = new User;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //METODO QUE OBTIENE TODOS DATOS, RUTEAR DESDE WEB.PHP Y LLAMAR EN PETICION AJAX
    public function listing(){
        $users = $this->model->join('type_users', 'users.type', '=', 'type_users.id')
            ->select('users.id', 'users.username', 'users.name', 'users.lastname', 'users.password', 'users.email', 'users.status', 'users.created_at', 'type_users.description as type_description')
            ->orderBy('users.lastname', 'asc')
            ->get();

        return response()->json(
            $users->toArray()          
        );
    }

    public function index(Request $request)
    {   
        $data_type_users = TypeUser::All();

        // $users = $this->model->onlyTrashed()->paginate(6); //MOSTRAR USUARIOS ELIMINADOS

        return view($this->module . '.' . $this->name_file . 's_index', compact('data_type_users'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            if ($request['status'] == '')
                $request['status'] = 'off';
            
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->model->find($id);
        return response()->json(
            $user->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensaje_deslogeo = '';

        if ($request['status'] == '')
        $request['status'] = 'off';

        //Excluyo el confirm del request
        $update = $request->except('confirm');

        //Si la password no se asigna Excluyo el confirm y passowrd del request para q no modifique la pass
        if ($request['password'] == ''){
            $update = $request->except('password', 'confirm');
        }

        $user = $this->model->find($id);
        $user->fill($update); //Rellena el elemento usuario con fill
        
        $user->save();

        //Si se modifico el usuario logeado Deslogear y mandar mensaje para que se actualize
        if (Auth::user()->id == $id){
            Auth::logout();
            $mensaje_deslogeo = 'Por favor cierre sesión e inicie nuevamente para tomar la nueva configuración de su cuenta';
        }

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente',   
            'mensaje_deslogeo' => $mensaje_deslogeo 
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->model->find($id);
        $user->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente' 
        ]);
    }

}