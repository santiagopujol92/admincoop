<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Invoice extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'invoices';

    protected $fillable = [
					    	'id_person', 
					    	'id_sale', 
					    	'id_status_invoice', 
							'total_amount',
							'retencion',
							'alicuota_iva',
							'descuento',
							'retencion_porc',
							'alicuota_iva_porc',
							'descuento_porc',
					    	'issue_date',
					    	'expiration_date',
							'payment_date',
							'total_amount_deuda',
				   		];

    protected $dates = ['deleted_at', 'created_at'];
}
