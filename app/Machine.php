<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Machine extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'machines';
    protected $fillable = ['description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
