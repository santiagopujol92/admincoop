<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class MovementsAccount extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'movements_accounts';
    protected $fillable = [
							'id', 
							'id_type_movement_account', 
							'type_operation',
							'nro_receipt',
							'id_current_account', 
							'administrative_expenses', 
							'total_amount', 
							'current_balance_account',
							'date_movement'
						];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

}
