<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Person extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'people';

    protected $fillable = [
					    	'name', 
					    	'lastname', 
					    	'cuit', 
					    	'id_condicion_iva', 
					    	'id_type_people',
					    	'id_city',
					    	'phone_1', 
					    	'phone_2', 
					    	'adress',
					    	'email',
					    	'floor',
					    	'department',
					    	'personality'
				   		];

    protected $dates = ['deleted_at'];
}
