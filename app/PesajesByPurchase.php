<?php

namespace AdminCoop;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PesajesByPurchase extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'pesajes_by_purchase';
    protected $fillable = [
        'id_purchase', 
        'bruto_chasis', 
        'neto_chasis', 
        'tara_chasis', 
        'bruto_acoplado', 
        'neto_acoplado', 
        'tara_acoplado',
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
