<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Production extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'productions';
    protected $fillable = 	['lote',
                            'id_product', 
    						'id_machine',
						    'turno', 
						    'quantity_product',
						    'id_person'];
    protected $dates = 		['created_at', 'updated_at', 'deleted_at'];
}
