<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 		'productos';
    protected $fillable = 	['description', 
    						'code_product',
						    'id_type_product', 
						    'type_metric_product',
						    'max_purchase_price',
						    'min_purchase_price',
						    'last_purchase_price', 
						    'last_sale_price', 
						    'stock', 
						    'last_change_stock',
							'max_quantity_stock',
						    'min_quantity_stock',];
    protected $dates = 		['created_at', 'updated_at', 'deleted_at'];

}
