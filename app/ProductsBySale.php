<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ProductsBySale extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'products_by_sale';
    protected $fillable = ['id_sale', 'id_product', 'quantity', 'purchase_price', 'precio_total_producto'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
