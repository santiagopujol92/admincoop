<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Province extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'provinces';
    protected $fillable = ['description', 'id_country'];
    protected $dates = ['deleted_at'];
}
