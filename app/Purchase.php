<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Purchase extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'purchases';
    protected $fillable = [ 
						    'id_person', 
						    'id_status_shipment', 
                'purchase_cost', 
                'alicuota_iva',
                'alicuota_iva_porc',
                'purchase_date',
                'purchase_total_deuda'
						  ];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
