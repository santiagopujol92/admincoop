<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Receipt extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;
    
    protected $table = 'receipts';
    protected $fillable = [ 
        'id_invoice', 
        'effective_amount', 
        'card_amount', 
        'retencion_amount',
        'retencion2_amount',
        'total_amount',
        'nro_orden_pago'
      ];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

}
