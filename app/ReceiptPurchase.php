<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ReceiptPurchase extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'receipt_purchases';
    protected $fillable = [ 
        'id_purchase', 
        'effective_amount', 
        'card_amount', 
        'retencion_amount',
        'retencion2_amount',
        'total_amount',
        'nro_invoice'
      ];
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
