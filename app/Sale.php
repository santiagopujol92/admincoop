<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Sale extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'sales';
    protected $fillable = [ 
						    'id_person', 
						    'id_status_shipment', 
						    'sell_cost', 
						    'sell_date'
						  ];
    protected $dates = ['deleted_at'];
}
