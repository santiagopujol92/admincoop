<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class StatusCurrentAccount extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'status_current_account';
    protected $fillable = ['description'];
    protected $dates = ['deleted_at', 'updated_at', 'creatd_at'];
}
