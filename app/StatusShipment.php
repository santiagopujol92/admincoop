<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class StatusShipment extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'status_shipments';
    protected $fillable = ['description'];
    protected $dates = ['deleted_at'];
}
