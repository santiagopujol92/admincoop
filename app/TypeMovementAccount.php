<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TypeMovementAccount extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

	protected $table = 'type_movement_accounts';
	protected $fillable = ['description'];
	protected $dates = ['deleted_at', 'updated_at', 'created_at'];
}
