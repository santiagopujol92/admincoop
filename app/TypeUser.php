<?php

namespace AdminCoop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TypeUser extends Model implements AuditableContract
{
	use Auditable, SoftDeletes;
	
    protected $table = 'type_users';
    protected $fillable = ['id', 'description'];
    protected $dates = ['deleted_at'];
    
}
