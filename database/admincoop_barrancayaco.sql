-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-06-2019 a las 18:14:23
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `admincoop_barrancayaco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` int(10) UNSIGNED NOT NULL,
  `auditable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci,
  `new_values` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `audits`
--

INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(1, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"vaMguckkKovU9kTcsnSBuqQNOIV2x91cwjh7k7sm1GSrpielzbXvW6TdQBxI"}', '{"remember_token":"dwltLAaez3vTg3uAOcHjKXjPHJB1pAfw8lxDmACxtjzfENHoJIN6bRj1rwYN"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:01'),
(2, 26, 'updated', 26, 'AdminCoop\\User', '{"remember_token":"2dhgF6m7p28zrMUB51qTjNV6RzI8RdfXfzGsGkq6R0rW9eNjIwBzcYhOiURs"}', '{"remember_token":"0Z2zCgelooWsyZiZg8VUc4oLxihU6pgcKhG1UQt2T1sNsG9klfkjFdC1SRrV"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:21'),
(3, 26, 'updated', 26, 'AdminCoop\\User', '{"remember_token":"0Z2zCgelooWsyZiZg8VUc4oLxihU6pgcKhG1UQt2T1sNsG9klfkjFdC1SRrV"}', '{"remember_token":"3rR03rEenF8btpVyS71YKeJH8zAeEpNv7Nnt6aUc31msAWP9bWK2JmR24GZu"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:16:28'),
(4, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"dwltLAaez3vTg3uAOcHjKXjPHJB1pAfw8lxDmACxtjzfENHoJIN6bRj1rwYN"}', '{"remember_token":"1iVLuBPbyyplHHTaQA4rkYTScGMY8KGFftZ8QfbvxO0YoaD1rcwGlsYS27Jp"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:07'),
(5, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"1iVLuBPbyyplHHTaQA4rkYTScGMY8KGFftZ8QfbvxO0YoaD1rcwGlsYS27Jp"}', '{"remember_token":"3XgRG5NbHdNMzrhsrLtBVOQLDJueMnuGJuGSL8GBlNmPfmJsFXgxPXcx9wgf"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:20'),
(6, 26, 'updated', 26, 'AdminCoop\\User', '{"remember_token":"3rR03rEenF8btpVyS71YKeJH8zAeEpNv7Nnt6aUc31msAWP9bWK2JmR24GZu"}', '{"remember_token":"9bu8nSdPleo3uou0J8fFv0dgU1s3cMuC0y1UOXSHCuQNGgwqMgfOOr233rab"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 00:17:41'),
(7, 27, 'created', 8, 'AdminCoop\\StatusShipment', '[]', '{"description":"Test","id":8}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:04:22'),
(8, 27, 'deleted', 8, 'AdminCoop\\StatusShipment', '{"id":8,"description":"Test","deleted_at":"2019-01-11 22:04:25"}', '[]', 'http://localhost:8000/estados_remitos/8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:04:25'),
(9, 27, 'created', 3, 'AdminCoop\\CondicionIva', '[]', '{"description":"Monotributo","id":3}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:05:07'),
(10, 27, 'created', 4, 'AdminCoop\\CondicionIva', '[]', '{"description":"Responsable Inscripto","id":4}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:05:15'),
(11, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{"description":"Gramos"}', '{"description":"Tipo 1"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:07:48'),
(12, 27, 'updated', 40, 'AdminCoop\\TypeProduct', '{"description":"Unidad\\/es"}', '{"description":"Tipo 2"}', 'http://localhost:8000/tipo_productos/40', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-12 01:07:52'),
(13, 27, 'created', 2, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol","email":"santiagopujol92@gmail.com","cuit":"20363548459","adress":"Chascomus 1782","floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"3","personality":"F","id_type_people":"1","id_city":"1","id":2}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:07:51'),
(14, 27, 'updated', 2, 'AdminCoop\\Person', '{"personality":"F"}', '{"personality":"J"}', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:09:35'),
(15, 27, 'updated', 1, 'AdminCoop\\Person', '{"id_condicion_iva":1}', '{"id_condicion_iva":"3"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 04:13:22'),
(16, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{"description":"Tipo 1"}', '{"description":"Duro"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-14 23:16:19'),
(17, 27, 'created', 2, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol Cliente","email":"santiagopujol93@gmail.com","cuit":"20363548456","adress":"San Carlos","floor":null,"department":null,"phone_1":"34234324234","phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"1","id_city":"1","id":2}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:38:54'),
(18, 27, 'updated', 1, 'AdminCoop\\Person', '{"lastname":"Pujol"}', '{"lastname":"Pujol Proveedor"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:39:02'),
(19, 27, 'created', 3, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol Empleado","email":"santiagopujol52@gmail.com","cuit":"20363548454","adress":"Chascomus 1782","floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"3","id_city":"1","id":3}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:39:32'),
(20, 27, 'created', 4, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol Proveedor Empresa","email":"santiagopuj2@gmail.com","cuit":"234234324","adress":"43534531782","floor":null,"department":null,"phone_1":"234234234","phone_2":null,"id_condicion_iva":"2","personality":"J","id_type_people":"2","id_city":"1","id":4}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-15 03:40:30'),
(21, 27, 'created', 1, 'AdminCoop\\Producto', '[]', '{"description":"BIDON","id_type_product":"40","type_metric_product":"KG","min_purchase_price":null,"max_purchase_price":null,"last_purchase_price":null,"id":1}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:05:10'),
(22, 27, 'updated', 1, 'AdminCoop\\Producto', '{"max_purchase_price":null,"min_purchase_price":null,"last_purchase_price":null}', '{"max_purchase_price":"4","min_purchase_price":"3","last_purchase_price":"5"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:08'),
(23, 27, 'updated', 1, 'AdminCoop\\Producto', '{"max_purchase_price":4}', '{"max_purchase_price":"7"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:49'),
(24, 27, 'updated', 1, 'AdminCoop\\Producto', '{"max_purchase_price":7}', '{"max_purchase_price":"7.33"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:15:56'),
(25, 27, 'updated', 1, 'AdminCoop\\Producto', '{"min_purchase_price":3,"last_purchase_price":5}', '{"min_purchase_price":"3.50","last_purchase_price":"5.6"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:16:05'),
(26, 27, 'updated', 1, 'AdminCoop\\Producto', '{"min_purchase_price":3.5}', '{"min_purchase_price":"3.50"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:16:05'),
(27, 27, 'updated', 40, 'AdminCoop\\TypeProduct', '{"description":"Tipo 2"}', '{"description":"SILO"}', 'http://localhost:8000/tipo_productos/40', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:17:04'),
(28, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{"description":"Duro"}', '{"description":"DURO"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-17 03:17:11'),
(29, 27, 'created', 1, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"SOPLADO GRANDE","id":1}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:28:54'),
(30, 27, 'created', 2, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"SOPLADO CHICO","id":2}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:04'),
(31, 27, 'created', 3, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"PP","id":3}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:24'),
(32, 27, 'created', 4, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"PEBO","id":4}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:33'),
(33, 27, 'created', 5, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"ROTOMOLDEO (TODO TIPO)","id":5}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:46'),
(34, 27, 'created', 6, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"PEAD CAJON MOLIDO","id":6}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:29:58'),
(35, 27, 'created', 7, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"PEAD MOL","id":7}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:30:12'),
(36, 27, 'created', 8, 'AdminCoop\\SaleTypeProduct', '[]', '{"description":"SILLA MOL","id":8}', 'http://localhost:8000/tipo_productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 01:30:18'),
(37, 27, 'created', 1, 'AdminCoop\\SaleProduct', '[]', '{"description":"Blanco","id_sale_type_product":"2","type_metric_product":"KG","id":1}', 'http://localhost:8000/productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:06'),
(38, 27, 'created', 2, 'AdminCoop\\SaleProduct', '[]', '{"description":"Rojo","id_sale_type_product":"3","type_metric_product":"KG","id":2}', 'http://localhost:8000/productos_venta', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:26'),
(39, 27, 'updated', 2, 'AdminCoop\\SaleProduct', '{"id_sale_type_product":3}', '{"id_sale_type_product":"6"}', 'http://localhost:8000/productos_venta/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:37'),
(40, 27, 'updated', 1, 'AdminCoop\\SaleProduct', '{"id_sale_type_product":2}', '{"id_sale_type_product":"6"}', 'http://localhost:8000/productos_venta/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:51'),
(41, 27, 'updated', 2, 'AdminCoop\\SaleProduct', '{"id_sale_type_product":6}', '{"id_sale_type_product":"8"}', 'http://localhost:8000/productos_venta/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-18 02:11:58'),
(42, 27, 'created', 1, 'AdminCoop\\TypeMovementAccount', '[]', '{"description":"DEBITO","id":1}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-21 04:05:52'),
(43, 27, 'created', 2, 'AdminCoop\\TypeMovementAccount', '[]', '{"description":"CREDITO","id":2}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-21 04:05:58'),
(44, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"3XgRG5NbHdNMzrhsrLtBVOQLDJueMnuGJuGSL8GBlNmPfmJsFXgxPXcx9wgf"}', '{"remember_token":"4sirrpBBi8Fi9RRzmCbsr37fFNBM8sTnl81oQJSJzJwbsjQiVz3AVHwA1M2n"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-28 04:47:57'),
(45, 27, 'created', 41, 'AdminCoop\\TypeProduct', '[]', '{"description":"ROTOMOLDEO","id":41}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-29 22:39:53'),
(46, 27, 'created', 2, 'AdminCoop\\Producto', '[]', '{"description":"TANQUES","id_type_product":"41","type_metric_product":"UNIDAD","min_purchase_price":"5","max_purchase_price":"7","last_purchase_price":"50","id":2}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-29 22:41:01'),
(47, 27, 'created', 1, 'AdminCoop\\StatusCurrentAccount', '[]', '{"description":"ACTIVA","id":1}', 'http://localhost:8000/estados_cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-31 02:40:58'),
(48, 27, 'created', 2, 'AdminCoop\\StatusCurrentAccount', '[]', '{"description":"INACTIVA","id":2}', 'http://localhost:8000/estados_cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-31 02:41:06'),
(49, 27, 'created', 1, 'AdminCoop\\TypeOperation', '[]', '{"description":"COMPRA","id":1}', 'http://localhost:8000/tipo_operaciones', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-31 03:15:13'),
(50, 27, 'created', 2, 'AdminCoop\\TypeOperation', '[]', '{"description":"VENTA","id":2}', 'http://localhost:8000/tipo_operaciones', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-31 03:15:16'),
(51, 27, 'created', 3, 'AdminCoop\\TypeOperation', '[]', '{"description":"PRODUCCION","id":3}', 'http://localhost:8000/tipo_operaciones', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-01-31 03:15:22'),
(52, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"4sirrpBBi8Fi9RRzmCbsr37fFNBM8sTnl81oQJSJzJwbsjQiVz3AVHwA1M2n"}', '{"remember_token":"U0gM2oU2X3sofzTBssBuR41n1yrJqZdC7nuycJM52jubSLOkuArn7msban7h"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:06:18'),
(53, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"U0gM2oU2X3sofzTBssBuR41n1yrJqZdC7nuycJM52jubSLOkuArn7msban7h"}', '{"remember_token":"tyP4iBkOB8HRfcknspi8NAVCQLb5Jcvj1qadCLibftM000yWKdY4sVULNrUZ"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:12:13'),
(54, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"tyP4iBkOB8HRfcknspi8NAVCQLb5Jcvj1qadCLibftM000yWKdY4sVULNrUZ"}', '{"remember_token":"PPt1cn6rJBTvhCeAmSTTzN1JQHarTi1p7bRSnFoHRjxnL05cVEIK2yXIZxHO"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:13:12'),
(55, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"PPt1cn6rJBTvhCeAmSTTzN1JQHarTi1p7bRSnFoHRjxnL05cVEIK2yXIZxHO"}', '{"remember_token":"ROkB0AGAE8kQUegS6q8tk6qMdgMKblj3C38caS7XRG3azyBBgvetpa8y0Fy3"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:13:44'),
(56, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"ROkB0AGAE8kQUegS6q8tk6qMdgMKblj3C38caS7XRG3azyBBgvetpa8y0Fy3"}', '{"remember_token":"g9wZfmFGUZWXnvZrm0ZFF20FhzGaksRPpWmGkrDw4ILFGHxQTLIOVlAXBWN0"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:15:09'),
(57, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"g9wZfmFGUZWXnvZrm0ZFF20FhzGaksRPpWmGkrDw4ILFGHxQTLIOVlAXBWN0"}', '{"remember_token":"rSPmdSKtuc3wV9hdnAsILXTdfZwt8q2ogOCKk3oaAg6rkhGw1qGjvUq28Fi4"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:25:33'),
(58, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"rSPmdSKtuc3wV9hdnAsILXTdfZwt8q2ogOCKk3oaAg6rkhGw1qGjvUq28Fi4"}', '{"remember_token":"34onT9V1s7sNKmmPSM12SG8U1n7sOLSNFcWEqihIo4bwzdrzKgYbQ6haLYMg"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:25:48'),
(59, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"34onT9V1s7sNKmmPSM12SG8U1n7sOLSNFcWEqihIo4bwzdrzKgYbQ6haLYMg"}', '{"remember_token":"L1kHUVEzv8GMHq6p36qfiNBPpODdcSJx6lrMJQDTOJMb9mAwRNrJdnje2H97"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 14:27:42'),
(60, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"L1kHUVEzv8GMHq6p36qfiNBPpODdcSJx6lrMJQDTOJMb9mAwRNrJdnje2H97"}', '{"remember_token":"P1V6hUwbkBHHqSyuwA7NrnJ7Ti6jPvTKFIWWSv7NYQqYUnTbL9IxgIXXYHbI"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 15:06:31'),
(61, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"P1V6hUwbkBHHqSyuwA7NrnJ7Ti6jPvTKFIWWSv7NYQqYUnTbL9IxgIXXYHbI"}', '{"remember_token":"5QFC8FSmn6gSZmyvSHNgb8owD4xL0EueXEaC3K2FVyilg1ENVaefyiuI0KTJ"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 15:06:41'),
(62, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"5QFC8FSmn6gSZmyvSHNgb8owD4xL0EueXEaC3K2FVyilg1ENVaefyiuI0KTJ"}', '{"remember_token":"q3olCOZHUl1RMEeuLHrc4GbkIZQrJb1LIhSWElOFNyEceDwDXndmMejT8zXG"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 15:33:14'),
(63, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":null}', '{"stock":"500"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:04:36'),
(64, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":null}', '{"stock":"344"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:08:32'),
(65, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":500}', '{"stock":"5004"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:12:08'),
(66, 27, 'updated', 1, 'AdminCoop\\Producto', '{"type_metric_product":"Kg","stock":5004}', '{"type_metric_product":"KG","stock":"500"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:13:42'),
(67, 27, 'updated', 1, 'AdminCoop\\Producto', '{"type_metric_product":"KG"}', '{"type_metric_product":"UNIDADES"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:14:58'),
(68, 27, 'updated', 1, 'AdminCoop\\Producto', '{"type_metric_product":"UNIDADES"}', '{"type_metric_product":"KG"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 16:15:03'),
(69, 27, 'updated', 1, 'AdminCoop\\Producto', '{"last_change_stock":null}', '{"last_change_stock":"2019-02-03 11:07:02"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:07:33'),
(70, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":500,"last_change_stock":"2019-02-03 11:07:02"}', '{"stock":800,"last_change_stock":"2019-02-03 11:17:02"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:17:46'),
(71, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":344,"last_change_stock":null}', '{"stock":844,"last_change_stock":"2019-02-03 11:17:02"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:17:57'),
(72, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":800,"last_change_stock":"2019-02-03 11:17:02"}', '{"stock":822,"last_change_stock":"2019-02-03 11:18:02"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:18:35'),
(73, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":822}', '{"stock":1055}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:18:40'),
(74, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":844,"last_change_stock":"2019-02-03 11:17:02"}', '{"stock":1044,"last_change_stock":"2019-02-03 11:27:02"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:27:59'),
(75, 27, 'created', 3, 'AdminCoop\\Producto', '[]', '{"description":"AZUL","id_type_product":"39","type_metric_product":"kg","min_purchase_price":"3","max_purchase_price":"5","last_purchase_price":"6","stock":"500","id":3}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:31:07'),
(76, 27, 'updated', 3, 'AdminCoop\\Producto', '{"stock":500,"last_change_stock":null}', '{"stock":700,"last_change_stock":"2019-02-03 11:31:02"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:31:19'),
(77, 27, 'created', 4, 'AdminCoop\\Producto', '[]', '{"description":"ROJO","id_type_product":"39","type_metric_product":"kg","min_purchase_price":"34","max_purchase_price":"3","last_purchase_price":"5","stock":"1500","id":4}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:46:07'),
(78, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":1055}', '{"stock":"555"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:56:40'),
(79, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":1044}', '{"stock":"111"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:59:06'),
(80, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":555}', '{"stock":"222"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 17:59:47'),
(81, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":222}', '{"stock":"545"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 18:00:48'),
(82, 27, 'updated', 4, 'AdminCoop\\Producto', '{"stock":1500}', '{"stock":"345"}', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 18:01:01'),
(83, 27, 'updated', 3, 'AdminCoop\\Producto', '{"stock":700}', '{"stock":"453"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 18:05:19'),
(84, 27, 'updated', 3, 'AdminCoop\\Producto', '{"stock":453}', '{"stock":"111"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:35:29'),
(85, 27, 'updated', 3, 'AdminCoop\\Producto', '{"stock":111}', '{"stock":"6542"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:36:10'),
(86, 27, 'updated', 4, 'AdminCoop\\Producto', '{"stock":345}', '{"stock":"2323"}', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:36:21'),
(87, 27, 'updated', 3, 'AdminCoop\\Producto', '{"stock":6542}', '{"stock":"1213"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:36:52'),
(88, 27, 'updated', 3, 'AdminCoop\\Producto', '{"description":"AZUL"}', '{"description":"ROJO FEO"}', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:37:04'),
(89, 27, 'deleted', 4, 'AdminCoop\\Producto', '{"id":4,"description":"ROJO","id_type_product":39,"type_metric_product":"kg","value_product":null,"max_purchase_price":3,"min_purchase_price":34,"real_purchase_price":null,"last_purchase_price":5,"quantity_default_value":null,"quantity_accumulator_value":null,"stock":2323,"last_change_stock":null,"deleted_at":"2019-02-03 16:38:10"}', '[]', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:38:10'),
(90, 27, 'deleted', 3, 'AdminCoop\\Producto', '{"id":3,"description":"ROJO FEO","id_type_product":39,"type_metric_product":"kg","value_product":null,"max_purchase_price":5,"min_purchase_price":3,"real_purchase_price":null,"last_purchase_price":6,"quantity_default_value":null,"quantity_accumulator_value":null,"stock":1213,"last_change_stock":"2019-02-03 11:31:02","deleted_at":"2019-02-03 16:38:18"}', '[]', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:38:18'),
(91, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":111,"last_change_stock":"2019-02-03 11:27:02"}', '{"stock":131,"last_change_stock":"2019-02-03 13:41:02"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:41:24'),
(92, 27, 'created', 5, 'AdminCoop\\Producto', '[]', '{"description":"ROJO","id_type_product":"39","type_metric_product":"kg","min_purchase_price":"1","max_purchase_price":"2","last_purchase_price":"1.5","stock":"111","id":5}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:56:43'),
(93, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":545,"last_change_stock":"2019-02-03 11:18:02"}', '{"stock":1100,"last_change_stock":"2019-02-03 13:57:02"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:57:02'),
(94, 27, 'updated', 2, 'AdminCoop\\Producto', '{"type_metric_product":"unidades"}', '{"type_metric_product":"kg"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:57:15'),
(95, 27, 'updated', 2, 'AdminCoop\\Producto', '{"type_metric_product":"kg"}', '{"type_metric_product":"unidades"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 19:57:27'),
(96, 27, 'updated', 2, 'AdminCoop\\Producto', '{"stock":131}', '{"stock":"545"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 22:44:23'),
(97, 27, 'updated', 1, 'AdminCoop\\Person', '{"lastname":"Pujol Proveedor"}', '{"lastname":"sabe"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 22:52:57'),
(98, 27, 'updated', 1, 'AdminCoop\\Person', '{"lastname":"sabe"}', '{"lastname":"Pujol Proveedor"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 22:53:07'),
(99, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"q3olCOZHUl1RMEeuLHrc4GbkIZQrJb1LIhSWElOFNyEceDwDXndmMejT8zXG"}', '{"remember_token":"RC1JtS5aSS0UK6wJFCUGYpIIQSMl3QX8WsHfcG7aUeuDkYe2hisXg9X13ZlB"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:11:44'),
(100, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{"description":"DURO"}', '{"description":"asdasdas"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:16:22'),
(101, 27, 'updated', 39, 'AdminCoop\\TypeProduct', '{"description":"asdasdas"}', '{"description":"DURO"}', 'http://localhost:8000/tipo_productos/39', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:16:29'),
(102, 27, 'updated', 3, 'AdminCoop\\TypePerson', '{"description":"EMPLEADO"}', '{"description":"EMPLEADO SALAME"}', 'http://localhost:8000/tipo_personas/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:18:01'),
(103, 27, 'updated', 3, 'AdminCoop\\TypePerson', '{"description":"EMPLEADO SALAME"}', '{"description":"EMPLEADO"}', 'http://localhost:8000/tipo_personas/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:18:09'),
(104, 27, 'created', 3, 'AdminCoop\\CondicionIva', '[]', '{"description":"DAAA","id":3}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:20:10'),
(105, 27, 'deleted', 3, 'AdminCoop\\CondicionIva', '{"id":3,"description":"DAAA","deleted_at":"2019-02-03 20:20:14"}', '[]', 'http://localhost:8000/condiciones_ivas/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:20:14'),
(106, 27, 'created', 5, 'AdminCoop\\PaymentMethod', '[]', '{"description":"DA\\u00d1E","id":5}', 'http://localhost:8000/formas_de_pago', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:22:29'),
(107, 27, 'deleted', 5, 'AdminCoop\\PaymentMethod', '{"id":5,"description":"DA\\u00d1E","deleted_at":"2019-02-03 20:22:34"}', '[]', 'http://localhost:8000/formas_de_pago/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:22:34'),
(108, 27, 'created', 6, 'AdminCoop\\StatusShipment', '[]', '{"description":"test","id":6}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:24:01'),
(109, 27, 'deleted', 6, 'AdminCoop\\StatusShipment', '{"id":6,"description":"test","deleted_at":"2019-02-03 20:24:05"}', '[]', 'http://localhost:8000/estados_remitos/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:24:05'),
(110, 27, 'created', 8, 'AdminCoop\\StatusInvoice', '[]', '{"description":"sabe","id":8}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:25:24'),
(111, 27, 'deleted', 8, 'AdminCoop\\StatusInvoice', '{"id":8,"description":"sabe","deleted_at":"2019-02-03 20:25:27"}', '[]', 'http://localhost:8000/estados_facturas/8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:25:27'),
(112, 27, 'created', 3, 'AdminCoop\\TypeMovementAccount', '[]', '{"description":"sabe","id":3}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:26:41'),
(113, 27, 'deleted', 3, 'AdminCoop\\TypeMovementAccount', '{"id":3,"description":"sabe","deleted_at":"2019-02-03 20:26:45"}', '[]', 'http://localhost:8000/tipo_movimientos_cuentas/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:26:45'),
(114, 27, 'created', 3, 'AdminCoop\\StatusCurrentAccount', '[]', '{"description":"asd","id":3}', 'http://localhost:8000/estados_cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:27:54'),
(115, 27, 'deleted', 3, 'AdminCoop\\StatusCurrentAccount', '{"id":3,"description":"asd","deleted_at":"2019-02-03 20:28:12"}', '[]', 'http://localhost:8000/estados_cuentas_corrientes/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:28:12'),
(116, 27, 'created', 4, 'AdminCoop\\StatusCurrentAccount', '[]', '{"description":"asdasd","id":4}', 'http://localhost:8000/estados_cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:46:08'),
(117, 27, 'deleted', 4, 'AdminCoop\\StatusCurrentAccount', '{"id":4,"description":"asdasd","deleted_at":"2019-02-03 20:46:13"}', '[]', 'http://localhost:8000/estados_cuentas_corrientes/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:46:13'),
(118, 27, 'created', 5, 'AdminCoop\\Country', '[]', '{"description":"asdasd","id":5}', 'http://localhost:8000/paises', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:46:21'),
(119, 27, 'deleted', 5, 'AdminCoop\\Country', '{"id":5,"description":"asdasd","deleted_at":"2019-02-03 20:46:37"}', '[]', 'http://localhost:8000/paises/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:46:37'),
(120, 27, 'created', 8, 'AdminCoop\\Province', '[]', '{"id_country":"1","description":"sadasd","id":8}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:48:49'),
(121, 27, 'deleted', 8, 'AdminCoop\\Province', '{"id":8,"description":"sadasd","id_country":1,"deleted_at":"2019-02-03 20:48:53"}', '[]', 'http://localhost:8000/provincias/8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:48:53'),
(122, 27, 'created', 3, 'AdminCoop\\City', '[]', '{"id_province":"7","description":"test","id":3}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:51:25'),
(123, 27, 'deleted', 3, 'AdminCoop\\City', '{"id":3,"description":"test","id_province":7,"deleted_at":"2019-02-03 20:51:29"}', '[]', 'http://localhost:8000/ciudades/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:51:29'),
(124, 27, 'created', 0, 'AdminCoop\\TypeUser', '[]', '{"id":0,"description":"test"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:54:47'),
(125, 27, 'deleted', 5, 'AdminCoop\\TypeUser', '{"id":5,"description":"test","deleted_at":"2019-02-03 20:54:51"}', '[]', 'http://localhost:8000/tipo_usuarios/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-03 23:54:51'),
(126, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"RC1JtS5aSS0UK6wJFCUGYpIIQSMl3QX8WsHfcG7aUeuDkYe2hisXg9X13ZlB"}', '{"remember_token":"jcW5PsfUnc6d8nwbGenFeYTkXEseRDXdPRbvzFtqTyECWjvbyuXlXypy7KSC"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:02:21'),
(127, 27, 'created', 28, 'AdminCoop\\User', '[]', '{"name":"TEST","lastname":"TEST","username":"TEST","email":"TEST@HOTMAIL.COM","password":"$2y$10$icbBfHNDhP8wV1rQtWyUK.671pzN\\/DxhOUG8eUFPziQ0bjI3OfAfO","type":"3","status":"on","id":28}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:12:29'),
(128, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"jcW5PsfUnc6d8nwbGenFeYTkXEseRDXdPRbvzFtqTyECWjvbyuXlXypy7KSC"}', '{"remember_token":"Wn7gkcKKiFHpeLfyGFQDXRU6IL6ylIOyecB58fLoaus3CGinZ9dtOLLQEDXX"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:12:37'),
(129, 28, 'updated', 28, 'AdminCoop\\User', '{"remember_token":null}', '{"remember_token":"7B8vdgUqtoqBxtXLu1V70c75RRKuNhTDFTT9yFBBClCpSACtCBYhwp0fOR5r"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:14:09'),
(130, 27, 'deleted', 28, 'AdminCoop\\User', '{"id":28,"name":"TEST","lastname":"TEST","username":"TEST","email":"TEST@HOTMAIL.COM","password":"$2y$10$icbBfHNDhP8wV1rQtWyUK.671pzN\\/DxhOUG8eUFPziQ0bjI3OfAfO","type":3,"status":"on","remember_token":"7B8vdgUqtoqBxtXLu1V70c75RRKuNhTDFTT9yFBBClCpSACtCBYhwp0fOR5r","deleted_at":"2019-02-03 21:14:28"}', '[]', 'http://localhost:8000/usuarios/28', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:14:28'),
(131, 27, 'updated', 26, 'AdminCoop\\User', '{"email":"admin"}', '{"email":"admin@hotmail.com"}', 'http://localhost:8000/usuarios/26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:18:23'),
(132, 27, 'updated', 26, 'AdminCoop\\User', '{"password":"$2y$10$DSf4JEQwb0LuC.S10Y.gH.tPgBW7wXarmKUaN0Zyfyi7X3zj4PSy."}', '{"password":"$2y$10$8KgzEMbjXrWZV42kUvVCBeVD.dq.x1Q0NG\\/L2n030nlQ9r0yXGGxq"}', 'http://localhost:8000/usuarios/26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:18:37'),
(133, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"Wn7gkcKKiFHpeLfyGFQDXRU6IL6ylIOyecB58fLoaus3CGinZ9dtOLLQEDXX"}', '{"remember_token":"BdVqKanFrusqFq7p6kkFWQZ92WAnmqGrO5U7zP8vGyrY2TUNDJXt1eiwVAZm"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:18:40'),
(134, 26, 'updated', 26, 'AdminCoop\\User', '{"remember_token":"9bu8nSdPleo3uou0J8fFv0dgU1s3cMuC0y1UOXSHCuQNGgwqMgfOOr233rab"}', '{"remember_token":"7FioDi1X3yocOljHWqPCk6bOzcYEYclAQSWwh9JELNA3ijUbzu1MNXV7W9m9"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:19:02'),
(135, 27, 'updated', 26, 'AdminCoop\\User', '{"password":"$2y$10$8KgzEMbjXrWZV42kUvVCBeVD.dq.x1Q0NG\\/L2n030nlQ9r0yXGGxq"}', '{"password":"$2y$10$IORVST9i7fkfJhtYfwO5XeiwZj0SDetCQO3va177xUqga0DxAsdNe"}', 'http://localhost:8000/usuarios/26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:19:37'),
(136, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"BdVqKanFrusqFq7p6kkFWQZ92WAnmqGrO5U7zP8vGyrY2TUNDJXt1eiwVAZm"}', '{"remember_token":"6vcaC1qPYUK7sSQBkGRhHsf7VQnl9z4ZR7cy0xg0Y6XPPgNH9pEwe8fM0bK1"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:19:39'),
(137, 26, 'updated', 26, 'AdminCoop\\User', '{"remember_token":"7FioDi1X3yocOljHWqPCk6bOzcYEYclAQSWwh9JELNA3ijUbzu1MNXV7W9m9"}', '{"remember_token":"iv7yvuwHV7jtoj14k5VkSVImFs5wTmivPrfjozWgDZox2Alrdrj2CleceucN"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-04 00:51:00'),
(138, 27, 'created', 1, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"2","id_status_current_account":"1","balance":"0","description":"43543","id":1}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 01:58:53'),
(139, 27, 'created', 2, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"2","id_status_current_account":"2","balance":"555","description":"TEST","id":2}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 02:11:08'),
(140, 27, 'created', 5, 'AdminCoop\\Person', '[]', '{"name":"Juan","lastname":"Perez","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"1","id_city":"1","id":5}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:12:12'),
(141, 27, 'created', 4, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"5","id_status_current_account":"1","balance":"500","description":"TEST","id":4}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:12:25'),
(142, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"description":"TEST"}', '{"description":"TEST213"}', 'http://localhost:8000/cuentas_corrientes/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:16:59'),
(143, 27, 'created', 4, 'AdminCoop\\TypeMovementAccount', '[]', '{"description":"TEST","id":4}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:38:18'),
(144, 27, 'deleted', 1, 'AdminCoop\\TypeMovementAccount', '{"id":1,"description":"DEBITO","deleted_at":"2019-02-06 00:38:37"}', '[]', 'http://localhost:8000/tipo_movimientos_cuentas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:38:37'),
(145, 27, 'updated', 4, 'AdminCoop\\TypeMovementAccount', '{"description":"TEST"}', '{"description":"DEBITO"}', 'http://localhost:8000/tipo_movimientos_cuentas/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:38:44'),
(146, 27, 'updated', 4, 'AdminCoop\\TypeMovementAccount', '{"description":"DEBITO"}', '{"description":"D\\u00c9BITO"}', 'http://localhost:8000/tipo_movimientos_cuentas/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:39:14');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(147, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":1100,"last_change_stock":"2019-02-03 13:57:02"}', '{"stock":1554,"last_change_stock":"2019-02-05 21:57:02"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:57:06'),
(148, 27, 'updated', 1, 'AdminCoop\\Producto', '{"stock":1554}', '{"stock":124677}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 03:57:12'),
(149, 27, 'created', 1, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"435","id":1}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:19:15'),
(150, 27, 'created', 2, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"545","id":2}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:22:58'),
(151, 27, 'created', 3, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"45","id":3}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:25:09'),
(152, 27, 'created', 4, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"4","total_amount":"454","id":4}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:26:28'),
(153, 27, 'created', 5, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"45","id":5}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:26:57'),
(154, 27, 'created', 6, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"4","total_amount":"0454","id":6}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:29:50'),
(155, 27, 'created', 7, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"454","id":7}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:30:39'),
(156, 27, 'created', 8, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"345","id":8}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:35:33'),
(157, 27, 'created', 9, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"435435","id":9}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:37:52'),
(158, 27, 'created', 10, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"32","id":10}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:38:23'),
(159, 27, 'created', 11, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"32","id":11}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:38:49'),
(160, 27, 'created', 12, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"32","id":12}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:39:03'),
(161, 27, 'created', 13, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"435","id":13}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:39:24'),
(162, 27, 'created', 14, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"345","id":14}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:39:51'),
(163, 27, 'created', 15, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"34543","id":15}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:41:04'),
(164, 27, 'created', 16, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"34543","id":16}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:41:49'),
(165, 27, 'created', 17, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"34543","id":17}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:42:02'),
(166, 27, 'created', 18, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"34543","id":18}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:42:19'),
(167, 27, 'created', 19, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"34543","id":19}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:42:20'),
(168, 27, 'created', 20, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"4","total_amount":"34543","id":20}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:42:20'),
(169, 27, 'created', 21, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"435","id":21}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:42:27'),
(170, 27, 'created', 22, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"435","id":22}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:43:12'),
(171, 27, 'created', 23, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"435","id":23}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:43:42'),
(172, 27, 'created', 24, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"4","total_amount":"435","id":24}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 04:44:23'),
(173, 27, 'created', 25, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":25}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:01:31'),
(174, 27, 'created', 26, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"45","id":26}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:02:04'),
(175, 27, 'created', 27, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"123","id":27}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:04:09'),
(176, 27, 'created', 28, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"445","id":28}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:04:21'),
(177, 27, 'created', 29, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"55","id":29}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:04:32'),
(178, 27, 'created', 30, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"45","id":30}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:04:52'),
(179, 27, 'created', 31, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"045","id":31}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:06:52'),
(180, 27, 'created', 32, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"4544","id":32}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:07:05'),
(181, 27, 'created', 33, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"444","id":33}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:07:38'),
(182, 27, 'created', 34, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"454","id":34}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:08:03'),
(183, 27, 'created', 35, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"045","id":35}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:08:24'),
(184, 27, 'created', 36, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"444","id":36}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:08:33'),
(185, 27, 'created', 37, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"435","id":37}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:10:32'),
(186, 27, 'created', 38, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"435","id":38}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:11:48'),
(187, 27, 'created', 39, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"435","id":39}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:12:04'),
(188, 27, 'created', 40, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"0435","id":40}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:13:41'),
(189, 27, 'created', 41, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"0435","id":41}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:14:33'),
(190, 27, 'created', 42, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"500","id":42}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:14:46'),
(191, 27, 'created', 43, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"100","id":43}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:14:55'),
(192, 27, 'created', 44, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"45","id":44}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:17:53'),
(193, 27, 'created', 45, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":45}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:19:16'),
(194, 27, 'created', 46, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":46}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:20:05'),
(195, 27, 'created', 47, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":47}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:20:13'),
(196, 27, 'created', 48, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":48}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:20:22'),
(197, 27, 'created', 49, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"345","id":49}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:20:41'),
(198, 27, 'created', 50, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"435","id":50}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:21:50'),
(199, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":500,"last_movement_account":null}', '{"balance":65,"last_movement_account":"2019-02-06 02:21:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:21:50'),
(200, 27, 'created', 51, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"555","id":51}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:05'),
(201, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":65,"last_movement_account":"2019-02-06"}', '{"balance":620,"last_movement_account":"2019-02-06 02:22:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:05'),
(202, 27, 'created', 52, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"700","id":52}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:39'),
(203, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":620,"last_movement_account":"2019-02-06"}', '{"balance":-80,"last_movement_account":"2019-02-06 02:22:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:39'),
(204, 27, 'created', 53, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"800","id":53}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:49'),
(205, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":-80,"last_movement_account":"2019-02-06"}', '{"balance":720,"last_movement_account":"2019-02-06 02:22:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:22:49'),
(206, 27, 'created', 54, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"44","id":54}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:33'),
(207, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":720,"last_movement_account":"2019-02-06"}', '{"balance":676,"last_movement_account":"2019-02-06 02:25:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:33'),
(208, 27, 'created', 55, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"444","id":55}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:41'),
(209, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":676,"last_movement_account":"2019-02-06"}', '{"balance":1120,"last_movement_account":"2019-02-06 02:25:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:41'),
(210, 27, 'created', 56, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"434","id":56}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:49'),
(211, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"balance":555,"last_movement_account":null}', '{"balance":121,"last_movement_account":"2019-02-06 02:25:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:25:49'),
(212, 27, 'created', 57, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"1","total_amount":"45","id":57}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:30:29'),
(213, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":1120,"last_movement_account":"2019-02-06 00:00:00"}', '{"balance":1075,"last_movement_account":"2019-02-06 02:30:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:30:29'),
(214, 27, 'created', 58, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"45","id":58}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:30:35'),
(215, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"balance":121,"last_movement_account":"2019-02-06 00:00:00"}', '{"balance":166,"last_movement_account":"2019-02-06 02:30:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-06 05:30:35'),
(216, 27, 'created', 59, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"500","id":59}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:11:41'),
(217, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"balance":166,"last_movement_account":"2019-02-06 02:30:02"}', '{"balance":666,"last_movement_account":"2019-02-08 03:11:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:11:41'),
(218, 27, 'created', 60, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"1","total_amount":"656","id":60}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:12:05'),
(219, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"balance":666,"last_movement_account":"2019-02-08 03:11:02"}', '{"balance":10,"last_movement_account":"2019-02-08 03:12:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:12:05'),
(220, 27, 'updated', 4, 'AdminCoop\\PaymentMethod', '{"description":"Cheque"}', '{"description":"Chequee"}', 'http://localhost:8000/formas_de_pago/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:49:50'),
(221, 27, 'updated', 4, 'AdminCoop\\PaymentMethod', '{"description":"Chequee"}', '{"description":"Cheque"}', 'http://localhost:8000/formas_de_pago/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:49:54'),
(222, 27, 'updated', 4, 'AdminCoop\\StatusShipment', '{"description":"A confirmar"}', '{"description":"A confirmare"}', 'http://localhost:8000/estados_remitos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:50:19'),
(223, 27, 'updated', 4, 'AdminCoop\\StatusShipment', '{"description":"A confirmare"}', '{"description":"A confirmar"}', 'http://localhost:8000/estados_remitos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:50:22'),
(224, 27, 'updated', 4, 'AdminCoop\\StatusInvoice', '{"description":"Activa"}', '{"description":"Activaa"}', 'http://localhost:8000/estados_facturas/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:04'),
(225, 27, 'updated', 4, 'AdminCoop\\StatusInvoice', '{"description":"Activaa"}', '{"description":"Activa"}', 'http://localhost:8000/estados_facturas/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:08'),
(226, 27, 'updated', 2, 'AdminCoop\\TypeMovementAccount', '{"description":"CR\\u00c9DITO"}', '{"description":"CR\\u00c9DITOe"}', 'http://localhost:8000/tipo_movimientos_cuentas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:19'),
(227, 27, 'updated', 2, 'AdminCoop\\TypeMovementAccount', '{"description":"CR\\u00c9DITOe"}', '{"description":"CR\\u00c9DITO"}', 'http://localhost:8000/tipo_movimientos_cuentas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:24'),
(228, 27, 'updated', 1, 'AdminCoop\\StatusCurrentAccount', '{"description":"ACTIVA"}', '{"description":"ACTIVAa"}', 'http://localhost:8000/estados_cuentas_corrientes/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:42'),
(229, 27, 'updated', 1, 'AdminCoop\\StatusCurrentAccount', '{"description":"ACTIVAa"}', '{"description":"ACTIVA"}', 'http://localhost:8000/estados_cuentas_corrientes/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:45'),
(230, 27, 'updated', 2, 'AdminCoop\\Country', '{"description":"Brazil"}', '{"description":"Brazile"}', 'http://localhost:8000/paises/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:51:58'),
(231, 27, 'updated', 2, 'AdminCoop\\Country', '{"description":"Brazile"}', '{"description":"Brazil"}', 'http://localhost:8000/paises/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:02'),
(232, 27, 'updated', 5, 'AdminCoop\\Province', '{"description":"Buenos Aires"}', '{"description":"Buenos Airese"}', 'http://localhost:8000/provincias/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:12'),
(233, 27, 'updated', 5, 'AdminCoop\\Province', '{"description":"Buenos Airese"}', '{"description":"Buenos Aires"}', 'http://localhost:8000/provincias/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:16'),
(234, 27, 'updated', 2, 'AdminCoop\\City', '{"description":"Alta Gracia"}', '{"description":"Alta Gracias"}', 'http://localhost:8000/ciudades/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:24'),
(235, 27, 'updated', 2, 'AdminCoop\\City', '{"description":"Alta Gracias"}', '{"description":"Alta Gracia"}', 'http://localhost:8000/ciudades/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:27'),
(236, 27, 'updated', 1, 'AdminCoop\\TypeUser', '{"description":"Admin"}', '{"description":"Admine"}', 'http://localhost:8000/tipo_usuarios/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:52:59'),
(237, 27, 'updated', 1, 'AdminCoop\\TypeUser', '{"description":"Admine"}', '{"description":"Admin"}', 'http://localhost:8000/tipo_usuarios/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 06:53:02'),
(238, 27, 'updated', 1, 'AdminCoop\\City', '{"id_province":1}', '{"id_province":"3"}', 'http://localhost:8000/ciudades/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-08 07:04:57'),
(239, 27, 'created', 61, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"4","id_type_movement_account":"2","total_amount":"10","id":61}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 13:54:19'),
(240, 27, 'updated', 4, 'AdminCoop\\CurrentAccount', '{"balance":1075,"last_movement_account":"2019-02-06 02:30:02"}', '{"balance":1085,"last_movement_account":"2019-02-09 10:54:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 13:54:19'),
(241, 27, 'created', 62, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"2","id_type_movement_account":"2","total_amount":"020","id":62}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 13:54:31'),
(242, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"balance":10,"last_movement_account":"2019-02-08 03:12:02"}', '{"balance":30,"last_movement_account":"2019-02-09 10:54:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 13:54:31'),
(243, 27, 'updated', 2, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:08:07'),
(244, 27, 'deleted', 4, 'AdminCoop\\CurrentAccount', '{"id":4,"description":"TEST","id_person":5,"balance":1085,"id_status_current_account":1,"last_movement_account":"2019-02-09 10:54:02","deleted_at":"2019-02-09 17:10:02"}', '[]', 'http://localhost:8000/cuentas_corrientes/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:10:02'),
(245, 27, 'created', 12, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"2","id_status_current_account":"1","description":null,"id":12}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:16:25'),
(246, 27, 'created', 13, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"2","id_status_current_account":"1","description":null,"id":13}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:17:10'),
(247, 27, 'created', 1, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","total_amount":"200","id":1}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:17:19'),
(248, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":null}', '{"balance":-200,"last_movement_account":"2019-02-09 17:17:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:17:19'),
(249, 27, 'created', 2, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"2","total_amount":"200","id":2}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:18:53'),
(250, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-200,"last_movement_account":"2019-02-09 17:17:02"}', '{"balance":0,"last_movement_account":"2019-02-09 17:18:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:18:53'),
(251, 27, 'created', 3, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","total_amount":"1","id":3}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:18:59'),
(252, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":0}', '{"balance":-1}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:18:59'),
(253, 27, 'created', 4, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"2","total_amount":"200","id":4}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:04'),
(254, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-1,"last_movement_account":"2019-02-09 17:18:02"}', '{"balance":199,"last_movement_account":"2019-02-09 17:20:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:04'),
(255, 27, 'created', 5, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","total_amount":"1","id":5}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:16'),
(256, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":199}', '{"balance":198}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:16'),
(257, 27, 'created', 6, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"2","total_amount":"1","id":6}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:20'),
(258, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":198}', '{"balance":199}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:20:20'),
(259, 27, 'created', 7, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","total_amount":"520","id":7}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:23:17'),
(260, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":199,"last_movement_account":"2019-02-09 17:20:02"}', '{"balance":-321,"last_movement_account":"2019-02-09 17:23:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:23:17'),
(261, 27, 'created', 15, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"5","id_status_current_account":"1","description":null,"id":15}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:27:31'),
(262, 27, 'created', 8, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"2","total_amount":"200","id":8}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:27:45'),
(263, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":null}', '{"balance":200,"last_movement_account":"2019-02-09 17:27:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 20:27:45'),
(264, 27, 'updated', 2, 'AdminCoop\\TypeMovementAccount', '{"description":"CR\\u00c9DITO"}', '{"description":"FACTURA"}', 'http://localhost:8000/tipo_movimientos_cuentas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:25:35'),
(265, 27, 'updated', 2, 'AdminCoop\\TypeMovementAccount', '{"description":"FACTURA"}', '{"description":"ORDEN DE PAGO"}', 'http://localhost:8000/tipo_movimientos_cuentas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:25:42'),
(266, 27, 'updated', 1, 'AdminCoop\\TypeMovementAccount', '{"description":"D\\u00c9BITO"}', '{"description":"FACTURA"}', 'http://localhost:8000/tipo_movimientos_cuentas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:25:49'),
(267, 27, 'created', 9, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"234","type_operation":"CREDITO","total_amount":"0","id":9}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:31:48'),
(268, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"last_movement_account":"2019-02-09 17:23:02"}', '{"last_movement_account":"2019-02-09 18:31:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:31:48'),
(269, 27, 'created', 10, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"33434","type_operation":"CREDITO","total_amount":"3434","id":10}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:51:56'),
(270, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-321,"last_movement_account":"2019-02-09 18:31:02"}', '{"balance":-3755,"last_movement_account":"2019-02-09 18:51:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:51:56'),
(271, 27, 'created', 11, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"2525","type_operation":"DEBITO","total_amount":"200","id":11}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:55:30'),
(272, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":"2019-02-09 18:51:02"}', '{"balance":-200,"last_movement_account":"2019-02-09 18:55:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:55:30'),
(273, 27, 'created', 12, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"2223","type_operation":"CREDITO","total_amount":"500","id":12}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:57:17'),
(274, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-200,"last_movement_account":"2019-02-09 18:55:02"}', '{"balance":-700,"last_movement_account":"2019-02-09 18:57:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:57:17'),
(275, 27, 'created', 13, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"525","type_operation":"CREDITO","total_amount":"500","id":13}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:57:37'),
(276, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-700}', '{"balance":-1200}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:57:37'),
(277, 27, 'created', 14, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"4545","type_operation":"CREDITO","total_amount":"1200","id":14}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:23'),
(278, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-1200,"last_movement_account":"2019-02-09 18:57:02"}', '{"balance":0,"last_movement_account":"2019-02-09 18:59:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:23'),
(279, 27, 'created', 15, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"5252","type_operation":"CREDITO","total_amount":"500","id":15}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:39'),
(280, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":0}', '{"balance":500}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:39'),
(281, 27, 'created', 16, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"3565","type_operation":"DEBITO","total_amount":"200","id":16}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:48'),
(282, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":500}', '{"balance":300}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:48'),
(283, 27, 'created', 17, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"2","nro_receipt":"5255","type_operation":"DEBITO","total_amount":"500","id":17}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:58'),
(284, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":"2019-02-09 17:27:02"}', '{"balance":-500,"last_movement_account":"2019-02-09 18:59:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 21:59:58'),
(285, 27, 'created', 18, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"232","type_operation":"CREDITO","total_amount":"200","current_balance_account":"300","id":18}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:10:35'),
(286, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":300,"last_movement_account":"2019-02-09 18:59:02"}', '{"balance":500,"last_movement_account":"2019-02-09 19:10:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:10:35'),
(287, 27, 'created', 19, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"55656","type_operation":"CREDITO","total_amount":"500","current_balance_account":"500","id":19}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:10:48'),
(288, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":500}', '{"balance":1000}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:10:48');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(289, 27, 'created', 20, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"435","type_operation":"DEBITO","total_amount":"666","current_balance_account":"1000","id":20}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:15:58'),
(290, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":1000,"last_movement_account":"2019-02-09 19:10:02"}', '{"balance":334,"last_movement_account":"2019-02-09 19:15:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:15:58'),
(291, 27, 'created', 21, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"3345","type_operation":"CREDITO","total_amount":"454","current_balance_account":"0","id":21}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:16:40'),
(292, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":"2019-02-09 19:15:02"}', '{"balance":454,"last_movement_account":"2019-02-09 19:16:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:16:40'),
(293, 27, 'created', 22, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"1","nro_receipt":"324234","type_operation":"DEBITO","total_amount":"34","current_balance_account":"0","id":22}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:20:12'),
(294, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":0,"last_movement_account":"2019-02-09 18:59:02"}', '{"balance":-34,"last_movement_account":"2019-02-09 19:20:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:20:12'),
(295, 27, 'created', 23, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"1","nro_receipt":"555","type_operation":"CREDITO","total_amount":"500","current_balance_account":"-34","id":23}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:00'),
(296, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":-34,"last_movement_account":"2019-02-09 19:20:02"}', '{"balance":466,"last_movement_account":"2019-02-09 19:21:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:00'),
(297, 27, 'created', 24, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"2","nro_receipt":"565","type_operation":"CREDITO","total_amount":"200","current_balance_account":"466","id":24}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:22'),
(298, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":466}', '{"balance":666}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:22'),
(299, 27, 'created', 25, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"5","type_operation":"DEBITO","total_amount":"200","current_balance_account":"454","id":25}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:36'),
(300, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":454,"last_movement_account":"2019-02-09 19:16:02"}', '{"balance":254,"last_movement_account":"2019-02-09 19:21:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:21:36'),
(301, 27, 'created', 26, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"2","nro_receipt":"488","type_operation":"DEBITO","total_amount":"500","current_balance_account":"254","id":26}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:22:03'),
(302, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":254,"last_movement_account":"2019-02-09 19:21:02"}', '{"balance":-246,"last_movement_account":"2019-02-09 19:22:02"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:22:03'),
(303, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":1}', '{"id_status_current_account":"2"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:23:56'),
(304, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:24:51'),
(305, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":1}', '{"id_status_current_account":"2"}', 'http://localhost:8000/cuentas_corrientes/15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:24:56'),
(306, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:25:01'),
(307, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":1}', '{"id_status_current_account":"2"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:25:21'),
(308, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:25:42'),
(309, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":1}', '{"id_status_current_account":"2"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '2019-02-09 22:25:46'),
(310, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"6vcaC1qPYUK7sSQBkGRhHsf7VQnl9z4ZR7cy0xg0Y6XPPgNH9pEwe8fM0bK1"}', '{"remember_token":"DCrPtSy0KSdtLkowdP85W0J2AE94sCJLaD5NOjGQcltfyaJJdOHBgsGd3nyz"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 17:06:45'),
(311, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"DCrPtSy0KSdtLkowdP85W0J2AE94sCJLaD5NOjGQcltfyaJJdOHBgsGd3nyz"}', '{"remember_token":"DJK81vxOGvMUFmn6kBW73UBYqDAEyepl1GBJYM0Pib8kSADzSDI6TO7WCkcC"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 17:11:49'),
(312, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"DJK81vxOGvMUFmn6kBW73UBYqDAEyepl1GBJYM0Pib8kSADzSDI6TO7WCkcC"}', '{"remember_token":"0gnELLzLfe5QBEf0sW08WGMKJfy1ynxE6Ulrv2NdqjOA1uFNVPYx0Qslwypq"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 17:14:07'),
(313, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"0gnELLzLfe5QBEf0sW08WGMKJfy1ynxE6Ulrv2NdqjOA1uFNVPYx0Qslwypq"}', '{"remember_token":"e2QvR7zjbYo8zCaeR5uzzM1TeaoXgLnhJJ9semhbzWVUDg3XcGIRQwXoSIVn"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 17:15:40'),
(314, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"e2QvR7zjbYo8zCaeR5uzzM1TeaoXgLnhJJ9semhbzWVUDg3XcGIRQwXoSIVn"}', '{"remember_token":"e9KrPR0WE9CP8tJgaZjF5j7ug0nuKyDnLuAFKwXcpIiMlKou4Mh40FfFpwrA"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 17:36:23'),
(315, 27, 'updated', 1, 'AdminCoop\\Producto', '{"code_product":null,"last_sale_price":null,"max_quantity_stock":null,"min_quantity_stock":null}', '{"code_product":"44rrr","last_sale_price":"5.8","max_quantity_stock":"1245","min_quantity_stock":"123"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 18:41:53'),
(316, 27, 'updated', 1, 'AdminCoop\\Producto', '{"code_product":"44rrr"}', '{"code_product":"1.1"}', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 18:44:12'),
(317, 27, 'updated', 5, 'AdminCoop\\Producto', '{"last_purchase_price":1.5}', '{"last_purchase_price":null}', 'http://localhost:8000/productos/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 18:53:27'),
(318, 27, 'updated', 5, 'AdminCoop\\Producto', '{"last_purchase_price":null}', '{"last_purchase_price":"55.6"}', 'http://localhost:8000/productos/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 18:55:04'),
(319, 27, 'updated', 5, 'AdminCoop\\Producto', '{"last_purchase_price":55.6}', '{"last_purchase_price":"1.1"}', 'http://localhost:8000/productos/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-16 18:55:12'),
(320, 27, 'updated', 4, 'AdminCoop\\StatusShipment', '{"description":"A confirmar"}', '{"description":"Activa"}', 'http://localhost:8000/estados_remitos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:15:45'),
(321, 27, 'updated', 1, 'AdminCoop\\StatusShipment', '{"description":"Activo"}', '{"description":"Pendiente de Pago Total"}', 'http://localhost:8000/estados_remitos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:16:16'),
(322, 27, 'updated', 5, 'AdminCoop\\StatusShipment', '{"description":"Preparado Para Facturar"}', '{"description":"Pendiente de Pago Parcial"}', 'http://localhost:8000/estados_remitos/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:16:29'),
(323, 27, 'updated', 2, 'AdminCoop\\StatusShipment', '{"description":"Pendiente de Pago"}', '{"description":"Cerrado"}', 'http://localhost:8000/estados_remitos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:16:44'),
(324, 27, 'updated', 4, 'AdminCoop\\StatusInvoice', '{"description":"Activa"}', '{"description":"Emitida"}', 'http://localhost:8000/estados_facturas/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:17:32'),
(325, 27, 'updated', 7, 'AdminCoop\\StatusInvoice', '{"description":"Finalizada"}', '{"description":"Cobrada"}', 'http://localhost:8000/estados_facturas/7', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:17:46'),
(326, 27, 'created', 9, 'AdminCoop\\StatusInvoice', '[]', '{"description":"Recibida por el Cliente","id":9}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:18:26'),
(327, 27, 'updated', 4, 'AdminCoop\\StatusShipment', '{"description":"Activa"}', '{"description":"Activo"}', 'http://localhost:8000/estados_remitos/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:18:51'),
(328, 27, 'updated', 3, 'AdminCoop\\StatusShipment', '{"description":"Pagado"}', '{"description":"Finalizado"}', 'http://localhost:8000/estados_remitos/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', '2019-02-18 03:19:09'),
(329, 27, 'updated', 155, 'AdminCoop\\Sale', '{"id_status_shipment":4}', '{"id_status_shipment":"3"}', 'http://localhost:8000/ventas/155', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-02-28 02:27:31'),
(330, 27, 'updated', 155, 'AdminCoop\\Sale', '{"id_status_shipment":3}', '{"id_status_shipment":"1"}', 'http://localhost:8000/ventas/155', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-02-28 02:27:40'),
(331, 27, 'updated', 156, 'AdminCoop\\Sale', '{"sell_cost":1000}', '{"sell_cost":"19865"}', 'http://localhost:8000/ventas/156', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-02 21:53:07'),
(332, 27, 'updated', 156, 'AdminCoop\\Sale', '{"sell_cost":19865}', '{"sell_cost":"500000"}', 'http://localhost:8000/ventas/156', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-02 22:37:16'),
(333, 27, 'updated', 155, 'AdminCoop\\Sale', '{"sell_cost":21380}', '{"sell_cost":"10000"}', 'http://localhost:8000/ventas/155', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-02 23:55:41'),
(334, 27, 'updated', 155, 'AdminCoop\\Sale', '{"sell_cost":10000}', '{"sell_cost":"1836"}', 'http://localhost:8000/ventas/155', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 00:04:33'),
(335, 27, 'updated', 156, 'AdminCoop\\Sale', '{"sell_cost":500000}', '{"sell_cost":"1000"}', 'http://localhost:8000/ventas/156', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 00:05:37'),
(336, 27, 'updated', 201, 'AdminCoop\\Sale', '{"id_person":2,"sell_date":"2019-03-02 22:19:00"}', '{"id_person":"5","sell_date":"2019-03-31 22:19:00"}', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 01:26:32'),
(337, 27, 'updated', 201, 'AdminCoop\\Sale', '{"id_person":5}', '{"id_person":"2"}', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 01:26:43'),
(338, 27, 'updated', 201, 'AdminCoop\\Sale', '{"id_status_shipment":2}', '{"id_status_shipment":"1"}', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 01:26:49'),
(339, 27, 'updated', 201, 'AdminCoop\\Sale', '{"sell_date":"2019-03-31 22:19:00"}', '{"sell_date":"2019-03-08 22:19:00"}', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 01:26:55'),
(340, 27, 'updated', 7, 'AdminCoop\\StatusInvoice', '{"description":"Cobrada"}', '{"description":"Pagada"}', 'http://localhost:8000/estados_facturas/7', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:19:20'),
(341, 27, 'updated', 9, 'AdminCoop\\StatusInvoice', '{"description":"Recibida por el Cliente"}', '{"description":"Vencida"}', 'http://localhost:8000/estados_facturas/9', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:19:35'),
(342, 27, 'deleted', 5, 'AdminCoop\\StatusShipment', '{"id":5,"description":"Pendiente de Pago Parcial","deleted_at":"2019-03-03 18:20:27"}', '[]', 'http://localhost:8000/estados_remitos/5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:20:27'),
(343, 27, 'deleted', 1, 'AdminCoop\\StatusShipment', '{"id":1,"description":"Pendiente de Pago Total","deleted_at":"2019-03-03 18:20:31"}', '[]', 'http://localhost:8000/estados_remitos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:20:31'),
(344, 27, 'updated', 2, 'AdminCoop\\StatusShipment', '{"description":"Cerrado"}', '{"description":"Anulado"}', 'http://localhost:8000/estados_remitos/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:20:44'),
(345, 27, 'updated', 201, 'AdminCoop\\Sale', '{"id_status_shipment":1}', '{"id_status_shipment":"4"}', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:22:28'),
(346, 27, 'updated', 204, 'AdminCoop\\Sale', '{"id_status_shipment":2}', '{"id_status_shipment":"4"}', 'http://localhost:8000/ventas/204', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:22:38'),
(347, 27, 'deleted', 201, 'AdminCoop\\Sale', '{"id":201,"id_person":2,"id_status_shipment":4,"sell_cost":1036,"sell_date":"2019-03-08 22:19:00","deleted_at":"2019-03-03 18:46:05"}', '[]', 'http://localhost:8000/ventas/201', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:46:05'),
(348, 27, 'deleted', 1, 'AdminCoop\\Producto', '{"id":1,"description":"BIDON","code_product":"1.1","id_type_product":40,"type_metric_product":"KG","max_purchase_price":7.33,"min_purchase_price":3.5,"last_purchase_price":5.6,"last_sale_price":4545,"stock":844,"last_change_stock":"2019-03-03 18:45:03","max_quantity_stock":1245,"min_quantity_stock":123,"deleted_at":"2019-03-03 18:55:48"}', '[]', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 21:55:48'),
(349, 27, 'deleted', 202, 'AdminCoop\\Sale', '{"id":202,"id_person":5,"id_status_shipment":4,"sell_cost":2420,"sell_date":"2019-03-02 22:21:00","deleted_at":"2019-03-03 19:05:58"}', '[]', 'http://localhost:8000/ventas/202', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:05:58'),
(350, 27, 'deleted', 203, 'AdminCoop\\Sale', '{"id":203,"id_person":5,"id_status_shipment":4,"sell_cost":2925,"sell_date":"2019-03-02 22:22:00","deleted_at":"2019-03-03 19:06:55"}', '[]', 'http://localhost:8000/ventas/203', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:06:55'),
(351, 27, 'deleted', 208, 'AdminCoop\\Sale', '{"id":208,"id_person":2,"id_status_shipment":4,"sell_cost":2312,"sell_date":"2019-03-03 19:07:00","deleted_at":"2019-03-03 19:27:27"}', '[]', 'http://localhost:8000/ventas/208', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:27:27'),
(352, 27, 'deleted', 207, 'AdminCoop\\Sale', '{"id":207,"id_person":2,"id_status_shipment":4,"sell_cost":3136,"sell_date":"2019-03-02 22:32:00","deleted_at":"2019-03-03 19:29:20"}', '[]', 'http://localhost:8000/ventas/207', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:29:20'),
(353, 27, 'deleted', 206, 'AdminCoop\\Sale', '{"id":206,"id_person":2,"id_status_shipment":4,"sell_cost":1156,"sell_date":"2019-03-02 22:25:00","deleted_at":"2019-03-03 19:29:31"}', '[]', 'http://localhost:8000/ventas/206', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:29:31'),
(354, 27, 'deleted', 205, 'AdminCoop\\Sale', '{"id":205,"id_person":2,"id_status_shipment":2,"sell_cost":204525,"sell_date":"2019-03-02 22:24:00","deleted_at":"2019-03-03 19:31:34"}', '[]', 'http://localhost:8000/ventas/205', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:31:34'),
(355, 27, 'deleted', 204, 'AdminCoop\\Sale', '{"id":204,"id_person":5,"id_status_shipment":4,"sell_cost":2025,"sell_date":"2019-03-02 22:23:00","deleted_at":"2019-03-03 19:33:08"}', '[]', 'http://localhost:8000/ventas/204', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-03 22:33:08'),
(356, 27, 'created', 27, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"1","nro_receipt":"12","type_operation":"CREDITO","total_amount":"500","current_balance_account":"-246","id":27}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-06 04:56:48'),
(357, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":-246,"last_movement_account":"2019-02-09 19:22:02"}', '{"balance":254,"last_movement_account":"2019-03-06 01:56:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-06 04:56:48'),
(358, 27, 'created', 29, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","id_type_movement_account":"2","nro_receipt":"415","type_operation":"DEBITO","total_amount":"0","current_balance_account":"254","id":29}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-06 05:18:26'),
(359, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"last_movement_account":"2019-03-06 01:56:03"}', '{"last_movement_account":"2019-03-06 02:18:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', '2019-03-06 05:18:26'),
(360, 27, 'deleted', 10, 'AdminCoop\\Sale', '{"id":10,"id_person":4,"id_status_shipment":1,"sell_cost":1904,"sell_date":"2019-03-09 18:58:00","deleted_at":"2019-03-09 19:09:33"}', '[]', 'http://localhost:8000/ventas/10', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:09:33'),
(361, 27, 'deleted', 9, 'AdminCoop\\Sale', '{"id":9,"id_person":1,"id_status_shipment":1,"sell_cost":4424,"sell_date":"2019-03-09 18:57:00","deleted_at":"2019-03-09 19:09:42"}', '[]', 'http://localhost:8000/ventas/9', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:09:42'),
(362, 27, 'deleted', 7, 'AdminCoop\\Sale', '{"id":7,"id_person":4,"id_status_shipment":1,"sell_cost":28500,"sell_date":"2019-03-09 18:48:00","deleted_at":"2019-03-09 19:09:50"}', '[]', 'http://localhost:8000/ventas/7', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:09:50'),
(363, 27, 'created', 16, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"4","id_status_current_account":"1","description":null,"id":16}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:10:15'),
(364, 27, 'created', 18, 'AdminCoop\\CurrentAccount', '[]', '{"id_person":"1","id_status_current_account":"1","description":null,"id":18}', 'http://localhost:8000/cuentas_corrientes', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:10:30'),
(365, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-09 22:10:38'),
(366, 27, 'deleted', 1, 'AdminCoop\\Sale', '{"id":1,"id_person":2,"id_status_shipment":1,"sell_cost":3060,"sell_date":"2019-03-09 14:03:00","deleted_at":"2019-03-10 20:32:35"}', '[]', 'http://localhost:8000/ventas/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:32:35'),
(367, 27, 'deleted', 2, 'AdminCoop\\Sale', '{"id":2,"id_person":5,"id_status_shipment":1,"sell_cost":2025,"sell_date":"2019-03-09 14:26:00","deleted_at":"2019-03-10 20:32:39"}', '[]', 'http://localhost:8000/ventas/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:32:39'),
(368, 27, 'deleted', 8, 'AdminCoop\\Sale', '{"id":8,"id_person":5,"id_status_shipment":1,"sell_cost":1530,"sell_date":"2019-03-09 18:56:00","deleted_at":"2019-03-10 20:32:50"}', '[]', 'http://localhost:8000/ventas/8', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:32:50'),
(369, 27, 'deleted', 6, 'AdminCoop\\Sale', '{"id":6,"id_person":2,"id_status_shipment":1,"sell_cost":1836,"sell_date":"2019-03-09 15:40:00","deleted_at":"2019-03-10 20:32:56"}', '[]', 'http://localhost:8000/ventas/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:32:56'),
(370, 27, 'deleted', 11, 'AdminCoop\\Sale', '{"id":11,"id_person":2,"id_status_shipment":1,"sell_cost":1904,"sell_date":"2019-03-09 19:07:00","deleted_at":"2019-03-10 20:32:59"}', '[]', 'http://localhost:8000/ventas/11', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:32:59'),
(371, 27, 'deleted', 12, 'AdminCoop\\Sale', '{"id":12,"id_person":4,"id_status_shipment":1,"sell_cost":1000,"sell_date":"2019-03-09 19:11:00","deleted_at":"2019-03-10 20:33:01"}', '[]', 'http://localhost:8000/ventas/12', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:33:01'),
(372, 27, 'deleted', 13, 'AdminCoop\\Sale', '{"id":13,"id_person":4,"id_status_shipment":1,"sell_cost":1904,"sell_date":"2019-03-09 20:48:00","deleted_at":"2019-03-10 20:33:06"}', '[]', 'http://localhost:8000/ventas/13', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:33:06'),
(373, 27, 'deleted', 14, 'AdminCoop\\Sale', '{"id":14,"id_person":2,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-03-10 20:42:00","deleted_at":"2019-03-10 20:57:04"}', '[]', 'http://localhost:8000/ventas/14', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-10 23:57:04'),
(374, 27, 'created', 3, 'AdminCoop\\TypeMovementAccount', '[]', '{"description":"RECIBO","id":3}', 'http://localhost:8000/tipo_movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 00:22:29'),
(375, 27, 'created', 61, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"1","nro_receipt":"4","type_operation":"DEBITO","total_amount":"200","current_balance_account":"1980","id":61}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 00:39:18'),
(376, 27, 'updated', 18, 'AdminCoop\\CurrentAccount', '{"balance":1980,"last_movement_account":"2019-03-10 21:36:03"}', '{"balance":1780,"last_movement_account":"2019-03-10 21:39:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 00:39:18'),
(377, 27, 'deleted', 3, 'AdminCoop\\TypeMovementAccount', '{"id":3,"description":"RECIBO","deleted_at":"2019-03-10 22:03:03"}', '[]', 'http://localhost:8000/tipo_movimientos_cuentas/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 01:03:03'),
(378, 27, 'created', 63, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"2","nro_receipt":"12","type_operation":"CREDITO","total_amount":"0","current_balance_account":"1780","id":63}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:52:13'),
(379, 27, 'updated', 18, 'AdminCoop\\CurrentAccount', '{"last_movement_account":"2019-03-10 21:39:03"}', '{"last_movement_account":"2019-03-10 23:52:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:52:13'),
(380, 27, 'created', 64, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"2","nro_receipt":"3","type_operation":"CREDITO","total_amount":"0","current_balance_account":"1780","id":64}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:53:25'),
(381, 27, 'updated', 18, 'AdminCoop\\CurrentAccount', '{"last_movement_account":"2019-03-10 23:52:03"}', '{"last_movement_account":"2019-03-10 23:53:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:53:25'),
(382, 27, 'created', 65, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"1","nro_receipt":"4","type_operation":"CREDITO","total_amount":"0","current_balance_account":"1780","id":65}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:53:31'),
(383, 27, 'created', 66, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"1","nro_receipt":"5","type_operation":"DEBITO","total_amount":"0","current_balance_account":"1780","id":66}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:53:44'),
(384, 27, 'created', 67, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"18","id_type_movement_account":"1","nro_receipt":"2","type_operation":"CREDITO","total_amount":"45","current_balance_account":"1780","id":67}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:54:02'),
(385, 27, 'updated', 18, 'AdminCoop\\CurrentAccount', '{"balance":1780,"last_movement_account":"2019-03-10 23:53:03"}', '{"balance":1825,"last_movement_account":"2019-03-10 23:54:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 02:54:02'),
(386, 27, 'deleted', 18, 'AdminCoop\\Sale', '{"id":18,"id_person":5,"id_status_shipment":1,"sell_cost":225,"sell_date":"2019-03-10 21:56:00","deleted_at":"2019-03-11 00:49:27"}', '[]', 'http://localhost:8000/ventas/18', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:49:27'),
(387, 27, 'deleted', 16, 'AdminCoop\\Sale', '{"id":16,"id_person":5,"id_status_shipment":1,"sell_cost":500,"sell_date":"2019-03-10 20:59:00","deleted_at":"2019-03-11 00:49:33"}', '[]', 'http://localhost:8000/ventas/16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:49:34'),
(388, 27, 'deleted', 17, 'AdminCoop\\Sale', '{"id":17,"id_person":1,"id_status_shipment":1,"sell_cost":2200,"sell_date":"2019-03-10 21:33:00","deleted_at":"2019-03-11 00:49:37"}', '[]', 'http://localhost:8000/ventas/17', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:49:37'),
(389, 27, 'deleted', 15, 'AdminCoop\\Sale', '{"id":15,"id_person":2,"id_status_shipment":1,"sell_cost":34,"sell_date":"2019-03-10 20:58:00","deleted_at":"2019-03-11 00:49:40"}', '[]', 'http://localhost:8000/ventas/15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:49:40'),
(390, 27, 'deleted', 19, 'AdminCoop\\Sale', '{"id":19,"id_person":2,"id_status_shipment":1,"sell_cost":2,"sell_date":"2019-03-11 00:50:00","deleted_at":"2019-03-11 00:50:33"}', '[]', 'http://localhost:8000/ventas/19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:50:33'),
(391, 27, 'created', 69, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"1","nro_receipt":"6","type_operation":"DEBITO","total_amount":"100","current_balance_account":"1440","id":69}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:53:37'),
(392, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1440,"last_movement_account":"2019-03-11 00:52:03"}', '{"balance":1340,"last_movement_account":"2019-03-11 00:53:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:53:37'),
(393, 27, 'created', 70, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","id_type_movement_account":"1","nro_receipt":"6","type_operation":"DEBITO","total_amount":"350","current_balance_account":"1340","id":70}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:53:47'),
(394, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1340}', '{"balance":990}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 03:53:47'),
(395, 27, 'created', 77, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"250","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"990","id":77}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 04:53:53'),
(396, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":990,"last_movement_account":"2019-03-11 00:53:03"}', '{"balance":740,"last_movement_account":"2019-03-11 01:53:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 04:53:55'),
(397, 27, 'created', 78, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"100","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1200","id":78}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:01:22'),
(398, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":740,"last_movement_account":"2019-03-11 01:53:03"}', '{"balance":1100,"last_movement_account":"2019-03-11 02:01:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:01:22'),
(399, 27, 'created', 79, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"100","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1200","id":79}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:02:02'),
(400, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"last_movement_account":"2019-03-11 02:01:03"}', '{"last_movement_account":"2019-03-11 02:02:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:02:02'),
(401, 27, 'created', 80, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"300","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1200","id":80}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:02:11'),
(402, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1100}', '{"balance":900}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:02:11'),
(403, 27, 'created', 81, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"0","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1200","id":81}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:06:15'),
(404, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":900,"last_movement_account":"2019-03-11 02:02:03"}', '{"balance":1200,"last_movement_account":"2019-03-11 02:06:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:06:15'),
(405, 27, 'created', 82, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"100","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1200","id":82}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:10:21'),
(406, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1200,"last_movement_account":"2019-03-11 02:06:03"}', '{"balance":1100,"last_movement_account":"2019-03-11 02:10:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:10:21'),
(407, 27, 'created', 83, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"1100","id":83}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:10:26'),
(408, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1100}', '{"balance":900}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:10:26'),
(409, 27, 'created', 84, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"100","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"900","id":84}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:17:05'),
(410, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":900,"last_movement_account":"2019-03-11 02:10:03"}', '{"balance":800,"last_movement_account":"2019-03-11 02:17:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:17:05'),
(411, 27, 'created', 85, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"900","id":85}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:18:11'),
(412, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":800,"last_movement_account":"2019-03-11 02:17:03"}', '{"balance":700,"last_movement_account":"2019-03-11 02:18:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:18:11'),
(413, 27, 'created', 86, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"300","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"900","id":86}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:18:16'),
(414, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":700}', '{"balance":600}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:18:16'),
(415, 27, 'created', 87, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"300","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"600","id":87}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:20:31'),
(416, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":600,"last_movement_account":"2019-03-11 02:18:03"}', '{"balance":300,"last_movement_account":"2019-03-11 02:20:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:20:31'),
(417, 27, 'created', 88, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"300","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"600","id":88}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:20:38'),
(418, 27, 'created', 89, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"300","id":89}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:23:22'),
(419, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":300,"last_movement_account":"2019-03-11 02:20:03"}', '{"balance":100,"last_movement_account":"2019-03-11 02:23:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:23:22'),
(420, 27, 'created', 90, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"300","id":90}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:23:23'),
(421, 27, 'created', 91, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"300","id":91}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:23:46'),
(422, 27, 'created', 92, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"100","id":92}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:25:10'),
(423, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1200,"last_movement_account":"2019-03-11 02:23:03"}', '{"balance":-100,"last_movement_account":"2019-03-11 02:25:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:25:10');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(424, 27, 'created', 93, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"15","total_amount":"200","nro_receipt":"6","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"-100","id":93}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:26:56'),
(425, 27, 'updated', 15, 'AdminCoop\\CurrentAccount', '{"balance":1200,"last_movement_account":"2019-03-11 02:25:03"}', '{"balance":-300,"last_movement_account":"2019-03-11 02:26:03"}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:26:56'),
(426, 27, 'created', 95, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","total_amount":"200","nro_receipt":"7","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"20000","id":95}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:19'),
(427, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":20000}', '{"balance":19800}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:19'),
(428, 27, 'created', 96, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","total_amount":"200","nro_receipt":"7","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"19800","id":96}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:26'),
(429, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":19800}', '{"balance":19600}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:26'),
(430, 27, 'created', 97, 'AdminCoop\\MovementsAccount', '[]', '{"id_current_account":"13","total_amount":"200","nro_receipt":"7","type_operation":"DEBITO","id_type_movement_account":"1","current_balance_account":"19600","id":97}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:29'),
(431, 27, 'updated', 13, 'AdminCoop\\CurrentAccount', '{"balance":19600}', '{"balance":19400}', 'http://localhost:8000/movimientos_cuentas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:28:29'),
(432, 27, 'deleted', 20, 'AdminCoop\\Sale', '{"id":20,"id_person":5,"id_status_shipment":1,"sell_cost":1200,"sell_date":"2019-03-11 00:50:00","deleted_at":"2019-03-11 02:29:17"}', '[]', 'http://localhost:8000/ventas/20', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-11 05:29:17'),
(433, 27, 'created', 1, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"234","card_amount":"234","retencion_amount":"234","retencion2_amount":"234234","total_amount":"234234","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-16 21:18:31'),
(434, 27, 'created', 2, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"234","card_amount":"234234","retencion_amount":"234234","retencion2_amount":"234","total_amount":"234234","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-16 21:24:30'),
(435, 27, 'created', 3, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"213","card_amount":"3213","retencion_amount":"123213","retencion2_amount":"12312","total_amount":"123","id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-16 21:24:50'),
(436, 27, 'created', 4, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"234","card_amount":"02341","retencion_amount":"0123","retencion2_amount":"0123","total_amount":"3019.00","id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:09:58'),
(437, 27, 'created', 5, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":5}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:10:25'),
(438, 27, 'created', 6, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":6}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:11:04'),
(439, 27, 'created', 7, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":7}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:11:12'),
(440, 27, 'created', 8, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":8}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:13:11'),
(441, 27, 'created', 9, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":9}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:13:37'),
(442, 27, 'created', 10, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":10}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:14:06'),
(443, 27, 'created', 11, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":11}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:17:54'),
(444, 27, 'created', 12, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"12","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"650.00","id":12}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:10'),
(445, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"345","banco":"345","concepto":"345","nombre":"345","cuit":"345","importe":"345.00","id_receipt":"7","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:10'),
(446, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"345","banco":"345","concepto":"345","nombre":"345","cuit":"345","importe":"345.00","id_receipt":"7","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:10'),
(447, 27, 'created', 13, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"123","card_amount":"432","retencion_amount":"234","retencion2_amount":"234","total_amount":"1011.00","id":13}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:34'),
(448, 27, 'created', 3, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"345","banco":"345","concepto":"345","nombre":"345","cuit":"345","importe":"345.00","id_receipt":"7","id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:36'),
(449, 27, 'created', 4, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"345","banco":"345","concepto":"345","nombre":"345","cuit":"345","importe":"345.00","id_receipt":"7","id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:37'),
(450, 27, 'created', 5, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"234","banco":"234","concepto":"234","nombre":"234","cuit":"234","importe":"234.00","id_receipt":"7","id":5}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:19:38'),
(451, 27, 'created', 14, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"25","card_amount":"52","retencion_amount":"52","retencion2_amount":"52","total_amount":"77.00","id":14}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:30:22'),
(452, 27, 'created', 6, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"52","banco":"52","concepto":"52","nombre":"52","cuit":"52","importe":"52.00","id_receipt":"7","id":6}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:30:25'),
(453, 27, 'created', 7, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"52","banco":"52","concepto":"52","nombre":"52","cuit":"52","importe":"52.00","id_receipt":"7","id":7}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:30:25'),
(454, 27, 'created', 15, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"123","card_amount":"345","retencion_amount":"345","retencion2_amount":"345","total_amount":"123.00","id":15}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:31:30'),
(455, 27, 'created', 8, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"345","banco":"345","concepto":"345","nombre":"345","cuit":"345","importe":"345.00","id_receipt":"7","id":8}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:31:30'),
(456, 27, 'created', 16, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"012","card_amount":"052","retencion_amount":"052","retencion2_amount":"052","total_amount":"12.00","id":16}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:55:29'),
(457, 27, 'created', 9, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"20","banco":"20","concepto":"52","nombre":"52","cuit":"52","importe":"52.00","id_receipt":"7","id":9}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 00:55:29'),
(458, 27, 'created', 17, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"15555","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"15555.00","id":17}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:05:35'),
(459, 27, 'created', 18, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"20","card_amount":"50","retencion_amount":"50","retencion2_amount":"050","total_amount":"4970.00","id":18}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:49:52'),
(460, 27, 'created', 10, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"50","banco":"50","concepto":"5","nombre":"0","cuit":"50","importe":"5000.00","id_receipt":"7","id":10}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:49:55'),
(461, 27, 'created', 19, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"20","card_amount":"50","retencion_amount":"50","retencion2_amount":"050","total_amount":"4970.00","id":19}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:50:53'),
(462, 27, 'created', 11, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"50","banco":"50","concepto":"5","nombre":"0","cuit":"50","importe":"5000.00","id_receipt":"7","id":11}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:50:55'),
(463, 27, 'created', 20, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"20","card_amount":"50","retencion_amount":"50","retencion2_amount":"050","total_amount":"4970.00","id":20}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:51:18'),
(464, 27, 'created', 12, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"50","banco":"50","concepto":"5","nombre":"0","cuit":"50","importe":"5000.00","id_receipt":"7","id":12}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:51:20'),
(465, 27, 'created', 21, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"585","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"585.00","id":21}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:53:44'),
(466, 27, 'created', 22, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"1000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":22}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:53:57'),
(467, 27, 'created', 23, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"1000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":23}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 01:54:02'),
(468, 27, 'created', 24, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"0345","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"345.00","id":24}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:04:09'),
(469, 27, 'created', 25, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"655","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"655.00","id":25}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:04:30'),
(470, 27, 'created', 26, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"500","card_amount":"100","retencion_amount":"100","retencion2_amount":"100","total_amount":"500.00","id":26}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:07:01'),
(471, 27, 'created', 27, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"500","card_amount":"100","retencion_amount":"100","retencion2_amount":"100","total_amount":"500.00","id":27}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:07:09'),
(472, 27, 'created', 28, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"500","card_amount":"100","retencion_amount":"100","retencion2_amount":"100","total_amount":"500.00","id":28}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:07:17'),
(473, 27, 'created', 29, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"500","card_amount":"100","retencion_amount":"100","retencion2_amount":"100","total_amount":"500.00","id":29}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:07:25'),
(474, 27, 'created', 30, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"555","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"555.00","id":30}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:09:08'),
(475, 27, 'created', 31, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"19445","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"19445.00","id":31}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:10:29'),
(476, 27, 'created', 32, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":32}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:11:17'),
(477, 27, 'created', 33, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"220.00","id":33}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:14:14'),
(478, 27, 'created', 1, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"111","card_amount":"11","retencion_amount":"0","retencion2_amount":"0","total_amount":"368.00","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:17:09'),
(479, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"1","banco":"123","concepto":"12","nombre":"3","cuit":"123","importe":"123.00","id_receipt":"8","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:17:09'),
(480, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"123","banco":"123","concepto":"213","nombre":"123","cuit":"213","importe":"123.00","id_receipt":"8","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:17:09'),
(481, 27, 'created', 2, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"111","card_amount":"34","retencion_amount":"034","retencion2_amount":"034","total_amount":"77.00","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:18:34'),
(482, 27, 'created', 1, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"34","card_amount":"34","retencion_amount":"0","retencion2_amount":"0","total_amount":"136.00","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:19:20'),
(483, 27, 'created', 2, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"34","card_amount":"34","retencion_amount":"0","retencion2_amount":"0","total_amount":"136.00","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:19:31'),
(484, 27, 'created', 3, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"34","card_amount":"34","retencion_amount":"0","retencion2_amount":"0","total_amount":"136.00","id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:20:24'),
(485, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"34","banco":"34","concepto":"34","nombre":"34","cuit":"34","importe":"34.00","id_receipt":3,"id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:20:27'),
(486, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"34","banco":"34","concepto":"34","nombre":"34","cuit":"34","importe":"34.00","id_receipt":3,"id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:20:27'),
(487, 27, 'created', 1, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"1000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1500.00","id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:22:43'),
(488, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"50","banco":"50","concepto":"50","nombre":"50","cuit":"50","importe":"500.00","id_receipt":1,"id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:22:43'),
(489, 27, 'created', 2, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"250","card_amount":"250","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:25:07'),
(490, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"250","banco":"250","concepto":"250","nombre":"250","cuit":"250","importe":"250.00","id_receipt":2,"id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:25:07'),
(491, 27, 'created', 3, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"250","banco":"250","concepto":"250","nombre":"250","cuit":"250","importe":"250.00","id_receipt":2,"id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:25:07'),
(492, 27, 'created', 3, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"500","card_amount":"500","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:27:58'),
(493, 27, 'created', 4, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"500","card_amount":"500","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:28:12'),
(494, 27, 'created', 5, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"500","card_amount":"500","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":5}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:29:02'),
(495, 27, 'created', 6, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"500","card_amount":null,"retencion_amount":"0","retencion2_amount":"0","total_amount":"500.00","id":6}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:29:55'),
(496, 27, 'created', 7, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"8","effective_amount":"250","card_amount":"250","retencion_amount":"0","retencion2_amount":"0","total_amount":"500.00","id":7}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 02:31:54'),
(497, 27, 'created', 8, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"1000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":8}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-17 22:02:19'),
(498, 27, 'created', 9, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"234","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"234.00","id":9}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:33:30'),
(499, 27, 'created', 10, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"34","card_amount":"03","retencion_amount":"0","retencion2_amount":"0","total_amount":"61.00","id":10}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:51:58'),
(500, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"34","banco":"34","concepto":"324","nombre":"324","cuit":"324","importe":"12.00","id_receipt":10,"id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:51:58'),
(501, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"234","banco":"324","concepto":"234","nombre":"234","cuit":"234","importe":"12.00","id_receipt":10,"id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:51:58'),
(502, 27, 'created', 11, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"34","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"34.00","id":11}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:52:14'),
(503, 27, 'created', 12, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"34","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"34.00","id":12}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:52:39'),
(504, 27, 'created', 13, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"02","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"26.00","id":13}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:52:53'),
(505, 27, 'created', 3, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"23","banco":"23","concepto":"23","nombre":"23","cuit":"23","importe":"12.00","id_receipt":13,"id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:52:53'),
(506, 27, 'created', 4, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"324","concepto":"324","nombre":"324","cuit":"324","importe":"12.00","id_receipt":13,"id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 01:52:54'),
(507, 27, 'created', 14, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"14","effective_amount":"0234","card_amount":"0234","retencion_amount":"0234","retencion2_amount":"0234","total_amount":"558.00","id":14}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 02:04:10'),
(508, 27, 'created', 5, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"234","concepto":"234","nombre":"234","cuit":"324","importe":"234.00","id_receipt":14,"id":5}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 02:04:10'),
(509, 27, 'created', 6, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"234","concepto":"234","nombre":"324","cuit":"324","importe":"324.00","id_receipt":14,"id":6}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 02:04:10'),
(510, 27, 'created', 15, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"13","effective_amount":"0324","card_amount":"0324","retencion_amount":"0324","retencion2_amount":"0324","total_amount":"324.00","id":15}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 02:04:20'),
(511, 27, 'created', 7, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"324","concepto":"324","nombre":"324","cuit":"324","importe":"324.00","id_receipt":15,"id":7}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-18 02:04:20'),
(512, 27, 'created', 16, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":16}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-20 22:52:33'),
(513, 27, 'created', 17, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":17}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', '2019-03-23 05:30:34'),
(514, 27, 'created', 18, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":18}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 04:51:40'),
(515, 27, 'created', 19, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"7","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":19}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 04:52:24'),
(516, 27, 'created', 20, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"233","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"233.00","id":20}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:02:03'),
(517, 27, 'created', 21, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"10","effective_amount":"333","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"333.00","id":21}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:03:20'),
(518, 27, 'created', 22, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":22}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:06:43'),
(519, 27, 'created', 23, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"9","effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":23}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:07:04'),
(520, 27, 'deleted', 23, 'AdminCoop\\Sale', '{"id":23,"id_person":2,"id_status_shipment":1,"sell_cost":408,"sell_date":"2019-03-11 02:39:00","deleted_at":"2019-03-26 02:17:07"}', '[]', 'http://localhost:8000/ventas/23', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:17:07'),
(521, 27, 'created', 24, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"13","effective_amount":"021000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"21000.00","id":24}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:18:39'),
(522, 27, 'created', 25, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"13","effective_amount":"12000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"12000.00","id":25}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:18:54'),
(523, 27, 'created', 26, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"12","effective_amount":"1156","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1156.00","id":26}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-26 05:21:21'),
(524, 27, 'created', 4, 'AdminCoop\\TypePerson', '[]', '{"description":"CLIENTE - PROVEEDOR","id":4}', 'http://localhost:8000/tipo_personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:02:14'),
(525, 27, 'created', 6, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":6}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:09:00'),
(526, 27, 'created', 7, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":7}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:09:57'),
(527, 27, 'created', 8, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":8}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:10:27'),
(528, 27, 'created', 9, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":9}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:11:18'),
(529, 27, 'created', 10, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":10}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:11:49'),
(530, 27, 'created', 11, 'AdminCoop\\Person', '[]', '{"name":"Cliente Proveedor Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"2","personality":"F","id_type_people":"4","id_city":"2","id":11}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:12:23'),
(531, 27, 'updated', 19, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":1}', '{"id_status_current_account":"2"}', 'http://localhost:8000/cuentas_corrientes/19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:22:23'),
(532, 27, 'updated', 19, 'AdminCoop\\CurrentAccount', '{"id_status_current_account":2}', '{"id_status_current_account":"1"}', 'http://localhost:8000/cuentas_corrientes/19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-03-31 00:22:28'),
(533, 27, 'created', 6, 'AdminCoop\\Producto', '[]', '{"description":"AZUL","code_product":"0012","id_type_product":"39","type_metric_product":"KG","min_purchase_price":null,"max_purchase_price":null,"last_purchase_price":null,"last_sale_price":null,"min_quantity_stock":null,"max_quantity_stock":null,"id":6}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-07 18:15:35'),
(534, 27, 'updated', 6, 'AdminCoop\\Producto', '{"stock":null,"last_change_stock":null}', '{"stock":3434,"last_change_stock":"2019-04-07 14:09:04"}', 'http://localhost:8000/productos/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-07 20:09:19'),
(535, 27, 'updated', 6, 'AdminCoop\\Producto', '{"max_purchase_price":null,"min_purchase_price":null}', '{"max_purchase_price":"22","min_purchase_price":"12"}', 'http://localhost:8000/productos/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-07 20:09:49'),
(536, 27, 'updated', 6, 'AdminCoop\\Producto', '{"last_purchase_price":null,"last_sale_price":null}', '{"last_purchase_price":"12","last_sale_price":"13"}', 'http://localhost:8000/productos/6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-07 20:09:56'),
(537, 27, 'created', 3, 'AdminCoop\\City', '[]', '{"id_province":"1","description":"Cordoba","id":3}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:29:45'),
(538, 27, 'created', 12, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"1","id_city":"3","id":12}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:22'),
(539, 27, 'deleted', 4, 'AdminCoop\\Country', '{"id":4,"description":"Bolivia","deleted_at":"2019-04-09 20:30:32"}', '[]', 'http://localhost:8000/paises/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:32'),
(540, 27, 'deleted', 2, 'AdminCoop\\Country', '{"id":2,"description":"Brazil","deleted_at":"2019-04-09 20:30:35"}', '[]', 'http://localhost:8000/paises/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:35'),
(541, 27, 'deleted', 3, 'AdminCoop\\Country', '{"id":3,"description":"Venezuela","deleted_at":"2019-04-09 20:30:38"}', '[]', 'http://localhost:8000/paises/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:38'),
(542, 27, 'deleted', 7, 'AdminCoop\\Province', '{"id":7,"description":"Caracas","id_country":3,"deleted_at":"2019-04-09 20:30:49"}', '[]', 'http://localhost:8000/provincias/7', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:49'),
(543, 27, 'deleted', 3, 'AdminCoop\\Province', '{"id":3,"description":"Tucuman","id_country":1,"deleted_at":"2019-04-09 20:30:52"}', '[]', 'http://localhost:8000/provincias/3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:52'),
(544, 27, 'deleted', 2, 'AdminCoop\\Province', '{"id":2,"description":"Sao Pablo","id_country":2,"deleted_at":"2019-04-09 20:30:55"}', '[]', 'http://localhost:8000/provincias/2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:30:55'),
(545, 27, 'deleted', 1, 'AdminCoop\\City', '{"id":1,"description":"C\\u00f3rdoba","id_province":3,"deleted_at":"2019-04-09 20:31:06"}', '[]', 'http://localhost:8000/ciudades/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:31:06'),
(546, 27, 'created', 4, 'AdminCoop\\City', '[]', '{"id_province":"5","description":"Capital Federal","id":4}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:31:21'),
(547, 27, 'created', 13, 'AdminCoop\\Person', '[]', '{"name":"Santiago","lastname":"Pujol Cliente Proveedor","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"4","id_city":"2","id":13}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:31:51'),
(548, 27, 'created', 14, 'AdminCoop\\Person', '[]', '{"name":"Santiago Pujol","lastname":"Cliente","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"1","id_city":"3","id":14}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:38:06'),
(549, 27, 'created', 15, 'AdminCoop\\Person', '[]', '{"name":"Santiago Pujol","lastname":"Cliente Proveedor","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"J","id_type_people":"4","id_city":"4","id":15}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-09 23:38:23'),
(550, 27, 'created', 27, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"21","effective_amount":"600","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"600.00","id":27}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '2019-04-10 00:16:56'),
(551, 27, 'created', 28, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"21","effective_amount":"123","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"123.00","id":28}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 20:35:53'),
(552, 27, 'created', 29, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"21","effective_amount":"388","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"388.00","id":29}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 20:39:10'),
(553, 27, 'created', 30, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"38","effective_amount":"555","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"555.00","id":30}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 21:04:04');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(554, 27, 'created', 31, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"38","effective_amount":"222","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"222.00","id":31}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 21:05:03'),
(555, 27, 'created', 32, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"40","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":32}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 21:48:37'),
(556, 27, 'created', 33, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"42","effective_amount":"500","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"500.00","id":33}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 21:50:53'),
(557, 27, 'created', 34, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"39","effective_amount":"1000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1000.00","id":34}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:07:10'),
(558, 27, 'created', 35, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"39","effective_amount":"2000","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"2000.00","id":35}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:08:14'),
(559, 27, 'deleted', 44, 'AdminCoop\\Sale', '{"id":44,"id_person":15,"id_status_shipment":1,"sell_cost":76518,"sell_date":"2019-04-13 18:22:00","deleted_at":"2019-04-13 19:25:42"}', '[]', 'http://localhost:8000/ventas/44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:25:42'),
(560, 27, 'created', 36, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"44","effective_amount":"500","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"500.00","id":36}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:27:03'),
(561, 27, 'created', 37, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"44","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"240.00","id":37}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:27:14'),
(562, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"20","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","id_receipt":37,"id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:27:14'),
(563, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"20","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","id_receipt":37,"id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:27:14'),
(564, 27, 'deleted', 48, 'AdminCoop\\Sale', '{"id":48,"id_person":14,"id_status_shipment":1,"sell_cost":1600,"sell_date":"2019-04-13 19:26:00","deleted_at":"2019-04-13 19:28:10"}', '[]', 'http://localhost:8000/ventas/48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:28:10'),
(565, 27, 'created', 38, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"45","effective_amount":"500","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"500.00","id":38}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:28:58'),
(566, 27, 'created', 39, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"45","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"70.00","id":39}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:29:08'),
(567, 27, 'created', 3, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"20","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","id_receipt":39,"id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:29:08'),
(568, 27, 'created', 4, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"50","banco":"50","concepto":"50","nombre":"50","cuit":"50","importe":"50.00","id_receipt":39,"id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:29:08'),
(569, 27, 'created', 40, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"47","effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":40}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:29:57'),
(570, 27, 'deleted', 51, 'AdminCoop\\Sale', '{"id":51,"id_person":14,"id_status_shipment":1,"sell_cost":11000,"sell_date":"2019-04-13 19:29:00","deleted_at":"2019-04-13 19:30:16"}', '[]', 'http://localhost:8000/ventas/51', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:30:16'),
(571, 27, 'created', 41, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"46","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":41}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:30:45'),
(572, 27, 'deleted', 49, 'AdminCoop\\Sale', '{"id":49,"id_person":15,"id_status_shipment":1,"sell_cost":2000,"sell_date":"2019-04-13 19:28:00","deleted_at":"2019-04-13 19:30:50"}', '[]', 'http://localhost:8000/ventas/49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:30:50'),
(573, 27, 'deleted', 50, 'AdminCoop\\Sale', '{"id":50,"id_person":14,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 19:29:00","deleted_at":"2019-04-13 19:59:15"}', '[]', 'http://localhost:8000/ventas/50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 22:59:15'),
(574, 27, 'deleted', 52, 'AdminCoop\\Sale', '{"id":52,"id_person":14,"id_status_shipment":1,"sell_cost":5000,"sell_date":"2019-04-13 20:00:00","deleted_at":"2019-04-13 20:00:53"}', '[]', 'http://localhost:8000/ventas/52', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:00:53'),
(575, 27, 'deleted', 53, 'AdminCoop\\Sale', '{"id":53,"id_person":14,"id_status_shipment":1,"sell_cost":21000,"sell_date":"2019-04-13 20:01:00","deleted_at":"2019-04-13 20:01:47"}', '[]', 'http://localhost:8000/ventas/53', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:01:47'),
(576, 27, 'deleted', 54, 'AdminCoop\\Sale', '{"id":54,"id_person":14,"id_status_shipment":1,"sell_cost":500000,"sell_date":"2019-04-13 20:03:00","deleted_at":"2019-04-13 20:04:33"}', '[]', 'http://localhost:8000/ventas/54', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:04:33'),
(577, 27, 'deleted', 55, 'AdminCoop\\Sale', '{"id":55,"id_person":14,"id_status_shipment":1,"sell_cost":26660,"sell_date":"2019-04-13 20:04:00","deleted_at":"2019-04-13 20:06:22"}', '[]', 'http://localhost:8000/ventas/55', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:06:22'),
(578, 27, 'deleted', 58, 'AdminCoop\\Sale', '{"id":58,"id_person":14,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 20:08:00","deleted_at":"2019-04-13 20:09:39"}', '[]', 'http://localhost:8000/ventas/58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:09:39'),
(579, 27, 'deleted', 59, 'AdminCoop\\Sale', '{"id":59,"id_person":15,"id_status_shipment":1,"sell_cost":6000,"sell_date":"2019-04-13 20:10:00","deleted_at":"2019-04-13 20:11:15"}', '[]', 'http://localhost:8000/ventas/59', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:11:15'),
(580, 27, 'deleted', 60, 'AdminCoop\\Sale', '{"id":60,"id_person":14,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 20:15:00","deleted_at":"2019-04-13 20:15:55"}', '[]', 'http://localhost:8000/ventas/60', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:15:55'),
(581, 27, 'deleted', 57, 'AdminCoop\\Sale', '{"id":57,"id_person":14,"id_status_shipment":1,"sell_cost":6720,"sell_date":"2019-04-13 20:07:00","deleted_at":"2019-04-13 20:16:07"}', '[]', 'http://localhost:8000/ventas/57', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:16:07'),
(582, 27, 'deleted', 56, 'AdminCoop\\Sale', '{"id":56,"id_person":14,"id_status_shipment":1,"sell_cost":26660,"sell_date":"2019-04-13 20:06:00","deleted_at":"2019-04-13 20:17:35"}', '[]', 'http://localhost:8000/ventas/56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:17:35'),
(583, 27, 'deleted', 61, 'AdminCoop\\Sale', '{"id":61,"id_person":15,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 20:17:00","deleted_at":"2019-04-13 20:18:11"}', '[]', 'http://localhost:8000/ventas/61', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:18:11'),
(584, 27, 'deleted', 62, 'AdminCoop\\Sale', '{"id":62,"id_person":14,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 20:18:00","deleted_at":"2019-04-13 20:20:14"}', '[]', 'http://localhost:8000/ventas/62', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:20:14'),
(585, 27, 'deleted', 63, 'AdminCoop\\Sale', '{"id":63,"id_person":14,"id_status_shipment":1,"sell_cost":240000,"sell_date":"2019-04-13 20:20:00","deleted_at":"2019-04-13 20:20:58"}', '[]', 'http://localhost:8000/ventas/63', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:20:58'),
(586, 27, 'deleted', 64, 'AdminCoop\\Sale', '{"id":64,"id_person":15,"id_status_shipment":1,"sell_cost":60000,"sell_date":"2019-04-13 20:21:00","deleted_at":"2019-04-13 20:21:55"}', '[]', 'http://localhost:8000/ventas/64', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:21:55'),
(587, 27, 'deleted', 65, 'AdminCoop\\Sale', '{"id":65,"id_person":14,"id_status_shipment":1,"sell_cost":20200,"sell_date":"2019-04-13 20:22:00","deleted_at":"2019-04-13 20:22:42"}', '[]', 'http://localhost:8000/ventas/65', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:22:42'),
(588, 27, 'deleted', 66, 'AdminCoop\\Sale', '{"id":66,"id_person":14,"id_status_shipment":1,"sell_cost":25000,"sell_date":"2019-04-13 20:23:00","deleted_at":"2019-04-13 20:24:13"}', '[]', 'http://localhost:8000/ventas/66', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:24:13'),
(589, 27, 'deleted', 67, 'AdminCoop\\Sale', '{"id":67,"id_person":15,"id_status_shipment":1,"sell_cost":10000,"sell_date":"2019-04-13 20:24:00","deleted_at":"2019-04-13 20:24:55"}', '[]', 'http://localhost:8000/ventas/67', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-13 23:24:55'),
(590, 27, 'created', 42, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"63","effective_amount":"133","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"133.00","id":42}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-14 01:53:43'),
(591, 27, 'created', 1, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":1}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 18:55:15'),
(592, 27, 'created', 2, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"100","card_amount":"200","retencion_amount":"300","retencion2_amount":"400","total_amount":"-270.00","id":2}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 18:56:10'),
(593, 27, 'created', 3, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"100","card_amount":"200","retencion_amount":"300","retencion2_amount":"400","total_amount":"-270.00","id":3}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 18:57:33'),
(594, 27, 'created', 4, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"100","card_amount":"200","retencion_amount":"300","retencion2_amount":"400","total_amount":"-270.00","id":4}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 19:03:50'),
(595, 27, 'created', 1, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"10","banco":"20","concepto":"30","nombre":"40","cuit":"50","importe":"60.00","id_receipt_purchase":4,"id":1}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 19:03:51'),
(596, 27, 'created', 2, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"20","banco":"30","concepto":"40","nombre":"50","cuit":"60","importe":"70.00","id_receipt_purchase":4,"id":2}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 19:03:52'),
(597, 27, 'created', 5, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":5}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 19:06:35'),
(598, 27, 'created', 6, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":6}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 19:06:38'),
(599, 27, 'created', 7, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":7}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 20:07:18'),
(600, 27, 'created', 8, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"200","card_amount":"10","retencion_amount":"0","retencion2_amount":"0","total_amount":"210.00","id":8}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 20:08:05'),
(601, 27, 'created', 9, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"122.00","id":9}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 20:08:25'),
(602, 27, 'created', 3, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"10","banco":"20","concepto":"12","nombre":"Santiago","cuit":"10121212","importe":"122.00","id_receipt_purchase":9,"id":3}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2019-04-18 20:08:25'),
(603, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"e9KrPR0WE9CP8tJgaZjF5j7ug0nuKyDnLuAFKwXcpIiMlKou4Mh40FfFpwrA"}', '{"remember_token":"SekmTw2dhaJzMHrefeKuxK7DxQkvJKrIpWNN7QLintkemGiZNctFNdPASTrv"}', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-10 19:44:34'),
(604, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"SekmTw2dhaJzMHrefeKuxK7DxQkvJKrIpWNN7QLintkemGiZNctFNdPASTrv"}', '{"remember_token":"IBF1fUKCgVLQfEqcwWRB5qhXrEzgaVrIkcFebufJoArQCEycrhBxn3j6fIIX"}', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-10 19:46:15'),
(605, 27, 'created', 10, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":null,"card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":10}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 15:51:53'),
(606, 27, 'created', 11, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"9","effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":11}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 15:54:38'),
(607, 27, 'created', 12, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":12}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 15:58:09'),
(608, 27, 'created', 13, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":13}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 15:59:38'),
(609, 27, 'created', 14, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":14}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:00:17'),
(610, 27, 'created', 15, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"1","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1.00","id":15}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:01:28'),
(611, 27, 'created', 16, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":16}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:24:12'),
(612, 27, 'created', 17, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":17}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:25:02'),
(613, 27, 'created', 18, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":18}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:25:32'),
(614, 27, 'created', 4, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"120","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","fecha_emision":"1969-12-31 21:00:00","fecha_pago":"1969-12-31 21:00:00","id_receipt_purchase":18,"id":4}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:25:32'),
(615, 27, 'created', 19, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"20","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":19}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:26:36'),
(616, 27, 'created', 20, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":20}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:28:15'),
(617, 27, 'created', 5, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"20","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","fecha_emision":"1969-12-31","fecha_pago":"1969-12-31","id_receipt_purchase":20,"id":5}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:28:15'),
(618, 27, 'created', 21, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"234.00","id":21}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:30:07'),
(619, 27, 'created', 6, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"234","banco":"234","concepto":"234","nombre":"234","cuit":"234","importe":"234.00","fecha_emision":"1969-12-31","fecha_pago":"1969-12-31","id_receipt_purchase":21,"id":6}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:30:07'),
(620, 27, 'created', 22, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"234.00","id":22}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:31:08'),
(621, 27, 'created', 23, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"12.00","id":23}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:32:17'),
(622, 27, 'created', 7, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"12","banco":"5","concepto":"52","nombre":"52","cuit":"10","importe":"12.00","fecha_emision":"2019-05-13","fecha_pago":"2019-05-31","id_receipt_purchase":23,"id":7}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 16:32:17'),
(623, 27, 'created', 24, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"557.00","id":24}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:49:09'),
(624, 27, 'created', 25, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"557.00","id":25}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:50:25'),
(625, 27, 'created', 26, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"557.00","id":26}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:51:24'),
(626, 27, 'created', 27, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"557.00","id":27}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:51:59'),
(627, 27, 'created', 28, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"434.00","id":28}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:52:27'),
(628, 27, 'created', 29, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"434","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"434.00","id":29}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:52:48'),
(629, 27, 'created', 30, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"123.00","id":30}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:53:20'),
(630, 27, 'created', 8, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"123","banco":"123","concepto":"123","nombre":"123","cuit":"123","importe":"123.00","fecha_emision":"2019-05-13 00:00:00","fecha_pago":"2019-05-13 00:00:00","id_receipt_purchase":30,"id":8}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:53:23'),
(631, 27, 'created', 31, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"234.00","id":31}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:58:30'),
(632, 27, 'created', 9, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"23423","banco":"234","concepto":"223423","nombre":"234","cuit":"23423","importe":"234.00","fecha_emision":"2019-05-13 00:00:00","fecha_pago":"2019-05-26 00:00:00","id_receipt_purchase":31,"id":9}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 19:58:37'),
(633, 27, 'created', 43, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"65","effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":43}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 20:11:27'),
(634, 27, 'created', 44, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"65","effective_amount":"20","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":44}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 20:11:47'),
(635, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"IBF1fUKCgVLQfEqcwWRB5qhXrEzgaVrIkcFebufJoArQCEycrhBxn3j6fIIX"}', '{"remember_token":"DPoH5Kn4mPb0s3dePo1jo0vKsZsdeat3LASe0JjtMRbnzHeW5tBJCnZ9HuuJ"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-13 20:28:00'),
(636, 27, 'created', 32, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"11","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"11.00","id":32}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-14 19:19:04'),
(637, 27, 'created', 33, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"10","effective_amount":"1","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1.00","id":33}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-14 19:23:04'),
(638, 27, 'created', 34, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","effective_amount":"10","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":34}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-14 19:25:50'),
(639, 27, 'created', 35, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","nro_invoice":"50","effective_amount":"10","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":35}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-14 19:26:12'),
(640, 27, 'created', 36, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","nro_invoice":null,"effective_amount":"58","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"58.00","id":36}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 13:21:49'),
(641, 27, 'created', 16, 'AdminCoop\\Person', '[]', '{"name":"Nicolas","lastname":"Perez","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"4","id_city":"3","id":16}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 13:43:28'),
(642, 27, 'created', 7, 'AdminCoop\\Producto', '[]', '{"description":"Polvo","code_product":"001","id_type_product":"40","type_metric_product":"KG","min_purchase_price":"2","max_purchase_price":"4","last_purchase_price":"3","last_sale_price":"5","min_quantity_stock":"500","max_quantity_stock":"10000","id":7}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 13:47:59'),
(643, 27, 'created', 8, 'AdminCoop\\Producto', '[]', '{"description":"Polvo 2","code_product":"002","id_type_product":"39","type_metric_product":"KG","min_purchase_price":"5","max_purchase_price":"10","last_purchase_price":"6","last_sale_price":"8","min_quantity_stock":"200","max_quantity_stock":"5000","id":8}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 13:48:32'),
(644, 27, 'created', 45, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"66","effective_amount":"757","card_amount":"0","retencion_amount":null,"retencion2_amount":"0","total_amount":"3757.00","id":45}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:02:59'),
(645, 27, 'created', 1, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"00012","banco":"1","concepto":"2","nombre":"Nicolas Perez","cuit":"3656546","importe":"2000.00","id_receipt":45,"id":1}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:02:59'),
(646, 27, 'created', 2, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"1212","banco":"1","concepto":"12","nombre":"Nicolas Perz","cuit":"253453543","importe":"1000.00","id_receipt":45,"id":2}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:02:59'),
(647, 27, 'created', 46, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"66","effective_amount":"1500","card_amount":"500","retencion_amount":"0","retencion2_amount":"0","total_amount":"2000.00","id":46}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:04:19'),
(648, 27, 'deleted', 1, 'AdminCoop\\StatusCurrentAccount', '{"id":1,"description":"ACTIVA","deleted_at":"2019-05-15 11:07:48"}', '[]', 'http://localhost:8000/estados_cuentas_corrientes/1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:07:48'),
(649, 27, 'created', 47, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"67","effective_amount":"1","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1.00","id":47}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:31:27'),
(650, 27, 'created', 3, 'AdminCoop\\StatusShipment', '[]', '{"description":"Cerrado","id":3}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:49:30'),
(651, 27, 'created', 37, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"11","nro_invoice":null,"effective_amount":"200","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"200.00","id":37}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:53:51'),
(652, 27, 'created', 38, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"13","nro_invoice":null,"effective_amount":"20","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":38}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:57:30'),
(653, 27, 'created', 39, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"13","nro_invoice":null,"effective_amount":"60","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"60.00","id":39}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 14:58:02'),
(654, 27, 'created', 40, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"14","nro_invoice":"20","effective_amount":"2500","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"2500.00","id":40}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:01:29'),
(655, 27, 'created', 41, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"14","nro_invoice":null,"effective_amount":"400","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"400.00","id":41}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:01:44'),
(656, 27, 'created', 42, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":42}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:10:16'),
(657, 27, 'created', 43, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"0","card_amount":"100","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":43}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:10:21'),
(658, 27, 'created', 44, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":44}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:10:37'),
(659, 27, 'created', 45, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":45}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:11:09'),
(660, 27, 'created', 46, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":46}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:12:22'),
(661, 27, 'created', 1, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"20","banco":"2","concepto":"1","nombre":"121","cuit":"1212","importe":"100.00","fecha_emision":"2019-05-15 12:12:05","fecha_pago":"2019-05-15 12:12:05","id_receipt_purchase":46,"id":1}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:12:22'),
(662, 27, 'created', 47, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"15","nro_invoice":null,"effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"30.00","id":47}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:12:50'),
(663, 27, 'created', 2, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"1212","banco":"1212","concepto":"121","nombre":"121","cuit":"121","importe":"30.00","fecha_emision":"2019-05-15 00:00:00","fecha_pago":"2019-05-31 00:00:00","id_receipt_purchase":47,"id":2}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:12:50'),
(664, 27, 'created', 48, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"50","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"50.00","id":48}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:57:54'),
(665, 27, 'created', 49, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":49}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:58:07'),
(666, 27, 'created', 3, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"10","banco":"10","concepto":"10","nombre":"10","cuit":"10","importe":"10.00","fecha_emision":"2019-05-15 12:58:05","fecha_pago":"2019-05-15 12:58:05","id_receipt_purchase":49,"id":3}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:58:07'),
(667, 27, 'created', 50, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"0","card_amount":"10","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":50}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:58:17'),
(668, 27, 'created', 51, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"0","card_amount":"10","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":51}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 15:58:32'),
(669, 27, 'created', 48, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"63","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":48}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 18:15:41'),
(670, 27, 'created', 49, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"63","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"0.00","id":49}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 18:17:06'),
(671, 27, 'created', 52, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"22","nro_invoice":null,"effective_amount":"212","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"212.00","id":52}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 19:47:24'),
(672, 27, 'created', 50, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"70","effective_amount":"100","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"100.00","id":50}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 20:07:19'),
(673, 27, 'created', 51, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"71","effective_amount":"50","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"50.00","id":51}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-15 20:09:32'),
(674, 27, 'created', 52, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"71","effective_amount":"20","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"20.00","id":52}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 12:56:15'),
(675, 27, 'created', 53, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"67","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"392.00","id":53}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:04:16'),
(676, 27, 'created', 54, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"67","effective_amount":"0","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"392.00","id":54}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:04:30'),
(677, 27, 'created', 3, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"234","banco":"234","concepto":"34","nombre":"34","cuit":"34","importe":"324.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-16 00:00:00","id_receipt":54,"id":3}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:04:30');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(678, 27, 'created', 4, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"324","concepto":"324","nombre":"34","cuit":"34","importe":"34.00","fecha_emision":"2019-05-16 10:04:05","fecha_pago":"2019-05-26 00:00:00","id_receipt":54,"id":4}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:04:30'),
(679, 27, 'created', 5, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"324","banco":"324","concepto":"324","nombre":"34","cuit":"34","importe":"34.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-26 00:00:00","id_receipt":54,"id":5}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:04:30'),
(680, 27, 'created', 55, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"71","nro_orden_pago":"12","effective_amount":"20","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"21.00","id":55}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:17:13'),
(681, 27, 'created', 6, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"1","banco":"1","concepto":"1","nombre":"1","cuit":"1","importe":"1.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-16 00:00:00","id_receipt":55,"id":6}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:17:13'),
(682, 27, 'created', 56, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"71","nro_orden_pago":"1","effective_amount":"2","card_amount":"2","retencion_amount":"0","retencion2_amount":"0","total_amount":"4.00","id":56}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:19:37'),
(683, 27, 'created', 57, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"69","nro_orden_pago":"20","effective_amount":"50","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"70.00","id":57}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:23:49'),
(684, 27, 'created', 7, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"20","banco":"20","concepto":"20","nombre":"20","cuit":"20","importe":"20.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-31 00:00:00","id_receipt":57,"id":7}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:23:49'),
(685, 27, 'created', 53, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"10","card_amount":"10","retencion_amount":"0","retencion2_amount":"0","total_amount":"32.00","id":53}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:27:43'),
(686, 27, 'created', 4, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"12","banco":"12","concepto":"12","nombre":"12","cuit":"12","importe":"12.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-16 00:00:00","id_receipt_purchase":53,"id":4}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:27:43'),
(687, 27, 'created', 54, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":"12","effective_amount":"10","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":54}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:28:23'),
(688, 27, 'created', 55, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"16","nro_invoice":null,"effective_amount":"20","card_amount":"10","retencion_amount":"0","retencion2_amount":"0","total_amount":"47.00","id":55}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:35:08'),
(689, 27, 'created', 5, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"12","banco":"12","concepto":"12","nombre":"12","cuit":"12","importe":"12.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-25 00:00:00","id_receipt_purchase":55,"id":5}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:35:08'),
(690, 27, 'created', 6, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"5","banco":"5","concepto":"5","nombre":"5","cuit":"5","importe":"5.00","fecha_emision":"2019-05-31 00:00:00","fecha_pago":"2019-06-19 00:00:00","id_receipt_purchase":55,"id":6}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 13:35:08'),
(691, 27, 'created', 56, 'AdminCoop\\ReceiptPurchase', '[]', '{"id_purchase":"17","nro_invoice":null,"effective_amount":"5","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":56}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 16:40:22'),
(692, 27, 'created', 7, 'AdminCoop\\ChequesByReceiptPurchase', '[]', '{"nro_cheque":"5","banco":"5","concepto":"5","nombre":"5","cuit":"5","importe":"5.00","fecha_emision":"2019-05-16 00:00:00","fecha_pago":"2019-05-16 00:00:00","id_receipt_purchase":56,"id":7}', 'http://localhost:8000/recibos_compras', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-16 16:40:22'),
(693, 27, 'updated', 15, 'AdminCoop\\Person', '{"cuit":null,"adress":null,"phone_1":null}', '{"cuit":"353535323","adress":"Ohguiins 1523","phone_1":"23432423"}', 'http://localhost:8000/personas/15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-17 16:41:11'),
(694, 27, 'updated', 14, 'AdminCoop\\Person', '{"cuit":null,"adress":null}', '{"cuit":"234234324","adress":"Copina 1234"}', 'http://localhost:8000/personas/14', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-17 16:41:20'),
(695, 27, 'updated', 16, 'AdminCoop\\Person', '{"cuit":null,"adress":null}', '{"cuit":"16232323","adress":"Colon 5213"}', 'http://localhost:8000/personas/16', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-17 16:41:31'),
(696, 27, 'created', 58, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"89","nro_orden_pago":null,"effective_amount":"10","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"10.00","id":58}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', '2019-05-17 16:48:17'),
(697, 27, 'created', 59, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"63","nro_orden_pago":"45","effective_amount":"202.45","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"202.45","id":59}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 16:02:01'),
(698, 27, 'created', 60, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"64","nro_orden_pago":null,"effective_amount":"111","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1098.00","id":60}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 17:30:55'),
(699, 27, 'created', 8, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"65656","banco":"Bancor","concepto":"sabe","nombre":"test","cuit":"3123","importe":"555.00","fecha_emision":"2019-05-21 14:30:05","fecha_pago":"2019-05-21 14:30:05","id_receipt":60,"id":8}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 17:30:55'),
(700, 27, 'created', 9, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"33344","banco":"nacion","concepto":"sabe","nombre":"test","cuit":"213213","importe":"432.00","fecha_emision":"2019-05-21 14:30:05","fecha_pago":"2019-05-21 14:30:05","id_receipt":60,"id":9}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 17:30:55'),
(701, 27, 'created', 61, 'AdminCoop\\Receipt', '[]', '{"id_invoice":"64","nro_orden_pago":null,"effective_amount":"111","card_amount":"0","retencion_amount":"0","retencion2_amount":"0","total_amount":"1055.00","id":61}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 18:42:23'),
(702, 27, 'created', 10, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"121","banco":"Bancor","concepto":"Test","nombre":"Hola","cuit":"20353554","importe":"500.00","fecha_emision":"2019-05-21 15:42:05","fecha_pago":"2019-05-21 15:42:05","id_receipt":61,"id":10}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 18:42:23'),
(703, 27, 'created', 11, 'AdminCoop\\ChequesByReceipt', '[]', '{"nro_cheque":"122","banco":"Nacion","concepto":"123","nombre":"Test2","cuit":"213123123","importe":"444.00","fecha_emision":"2019-05-21 15:42:05","fecha_pago":"2019-05-21 15:42:05","id_receipt":61,"id":11}', 'http://localhost:8000/recibos', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', '2019-05-21 18:42:23'),
(704, 27, 'created', 17, 'AdminCoop\\Person', '[]', '{"name":"Empleado","lastname":"Test","email":null,"cuit":null,"adress":null,"floor":null,"department":null,"phone_1":null,"phone_2":null,"id_condicion_iva":"1","personality":"F","id_type_people":"1","id_city":"3","id":17}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-05-28 19:53:56'),
(705, 27, 'updated', 17, 'AdminCoop\\Person', '{"id_type_people":1}', '{"id_type_people":"3"}', 'http://localhost:8000/personas/17', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-05-28 19:54:24'),
(706, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"DPoH5Kn4mPb0s3dePo1jo0vKsZsdeat3LASe0JjtMRbnzHeW5tBJCnZ9HuuJ"}', '{"remember_token":"GcS3qYzhfpzdKh5KzaiQ2aVoWrEU4N16J0p4PXqhRJFzEdkvEsQG8WmVboAq"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:45:03'),
(707, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"GcS3qYzhfpzdKh5KzaiQ2aVoWrEU4N16J0p4PXqhRJFzEdkvEsQG8WmVboAq"}', '{"remember_token":"2etMG2rryGlBzDFiUycsowTGVhid6comQI0rfcsnh8POJCTtorwbCnN0Yal9"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:49:43'),
(708, 3, 'updated', 3, 'AdminCoop\\User', '{"remember_token":null}', '{"remember_token":"NqdP3lAg0GaoMpgFug5DLy0yDwslUKvFpO6iDHNIjmfAZBSocMPWfhqbN7Zc"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:50:47'),
(709, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"2etMG2rryGlBzDFiUycsowTGVhid6comQI0rfcsnh8POJCTtorwbCnN0Yal9"}', '{"remember_token":"VQPrDnkjEIQiKwnS24OcOSKFGJspFWb8AOjId5eqOzny8Y5PwXXG1FfB3DX0"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:51:45'),
(710, 3, 'updated', 3, 'AdminCoop\\User', '{"remember_token":"NqdP3lAg0GaoMpgFug5DLy0yDwslUKvFpO6iDHNIjmfAZBSocMPWfhqbN7Zc"}', '{"remember_token":"mX8hVxrnJ3nc1BP1QwI45DdK9nwwl4nxCdnshTiD2htxq38fQUDJWwvzMO96"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:52:10'),
(711, 27, 'updated', 27, 'AdminCoop\\User', '{"remember_token":"VQPrDnkjEIQiKwnS24OcOSKFGJspFWb8AOjId5eqOzny8Y5PwXXG1FfB3DX0"}', '{"remember_token":"IaS4YxCp7N9fwcVGkHRlwT5tklwVhKBOgA6y6kcqhuUIhAu3H6lyAvCodbi5"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 15:52:56'),
(712, 3, 'updated', 3, 'AdminCoop\\User', '{"remember_token":"mX8hVxrnJ3nc1BP1QwI45DdK9nwwl4nxCdnshTiD2htxq38fQUDJWwvzMO96"}', '{"remember_token":"NbeT1JerOaBk0CnUN3BCPi2RyaMvgFOlDJ3ZGQrB7mfUQzMPrNuiqga8VMcS"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '2019-06-04 16:04:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cheques_by_receipts`
--

CREATE TABLE `cheques_by_receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_receipt` int(10) UNSIGNED NOT NULL,
  `nro_cheque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `concepto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `importe` double(8,2) NOT NULL,
  `fecha_emision` datetime DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cheques_by_receipts`
--

INSERT INTO `cheques_by_receipts` (`id`, `id_receipt`, `nro_cheque`, `banco`, `concepto`, `nombre`, `cuit`, `importe`, `fecha_emision`, `fecha_pago`, `created_at`, `updated_at`) VALUES
(1, 45, '00012', '1', '2', 'Nicolas Perez', '3656546', 2000.00, NULL, NULL, '2019-05-15 14:02:59', '2019-05-15 14:02:59'),
(2, 45, '1212', '1', '12', 'Nicolas Perz', '253453543', 1000.00, NULL, NULL, '2019-05-15 14:02:59', '2019-05-15 14:02:59'),
(3, 54, '234', '234', '34', '34', '34', 324.00, '2019-05-16 00:00:00', '2019-05-16 00:00:00', '2019-05-16 13:04:30', '2019-05-16 13:04:30'),
(4, 54, '324', '324', '324', '34', '34', 34.00, '2019-05-16 10:04:05', '2019-05-26 00:00:00', '2019-05-16 13:04:30', '2019-05-16 13:04:30'),
(5, 54, '324', '324', '324', '34', '34', 34.00, '2019-05-16 00:00:00', '2019-05-26 00:00:00', '2019-05-16 13:04:30', '2019-05-16 13:04:30'),
(6, 55, '1', '1', '1', '1', '1', 1.00, '2019-05-16 00:00:00', '2019-05-16 00:00:00', '2019-05-16 13:17:13', '2019-05-16 13:17:13'),
(7, 57, '20', '20', '20', '20', '20', 20.00, '2019-05-16 00:00:00', '2019-05-31 00:00:00', '2019-05-16 13:23:49', '2019-05-16 13:23:49'),
(8, 60, '65656', 'Bancor', 'sabe', 'test', '3123', 555.00, '2019-05-21 14:30:05', '2019-05-21 14:30:05', '2019-05-21 17:30:55', '2019-05-21 17:30:55'),
(9, 60, '33344', 'nacion', 'sabe', 'test', '213213', 432.00, '2019-05-21 14:30:05', '2019-05-21 14:30:05', '2019-05-21 17:30:55', '2019-05-21 17:30:55'),
(10, 61, '121', 'Bancor', 'Test', 'Hola', '20353554', 500.00, '2019-05-21 15:42:05', '2019-05-21 15:42:05', '2019-05-21 18:42:23', '2019-05-21 18:42:23'),
(11, 61, '122', 'Nacion', '123', 'Test2', '213123123', 444.00, '2019-05-21 15:42:05', '2019-05-21 15:42:05', '2019-05-21 18:42:23', '2019-05-21 18:42:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cheques_by_receipt_purchases`
--

CREATE TABLE `cheques_by_receipt_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_receipt_purchase` int(10) UNSIGNED NOT NULL,
  `nro_cheque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `concepto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `importe` double(8,2) NOT NULL,
  `fecha_emision` datetime DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cheques_by_receipt_purchases`
--

INSERT INTO `cheques_by_receipt_purchases` (`id`, `id_receipt_purchase`, `nro_cheque`, `banco`, `concepto`, `nombre`, `cuit`, `importe`, `fecha_emision`, `fecha_pago`, `created_at`, `updated_at`) VALUES
(1, 46, '20', '2', '1', '121', '1212', 100.00, '2019-05-15 12:12:05', '2019-05-15 12:12:05', '2019-05-15 15:12:22', '2019-05-15 15:12:22'),
(2, 47, '1212', '1212', '121', '121', '121', 30.00, '2019-05-15 00:00:00', '2019-05-31 00:00:00', '2019-05-15 15:12:50', '2019-05-15 15:12:50'),
(3, 49, '10', '10', '10', '10', '10', 10.00, '2019-05-15 12:58:05', '2019-05-15 12:58:05', '2019-05-15 15:58:07', '2019-05-15 15:58:07'),
(4, 53, '12', '12', '12', '12', '12', 12.00, '2019-05-16 00:00:00', '2019-05-16 00:00:00', '2019-05-16 13:27:43', '2019-05-16 13:27:43'),
(5, 55, '12', '12', '12', '12', '12', 12.00, '2019-05-16 00:00:00', '2019-05-25 00:00:00', '2019-05-16 13:35:08', '2019-05-16 13:35:08'),
(6, 55, '5', '5', '5', '5', '5', 5.00, '2019-05-31 00:00:00', '2019-06-19 00:00:00', '2019-05-16 13:35:08', '2019-05-16 13:35:08'),
(7, 56, '5', '5', '5', '5', '5', 5.00, '2019-05-16 00:00:00', '2019-05-16 00:00:00', '2019-05-16 16:40:22', '2019-05-16 16:40:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_province` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `description`, `id_province`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Córdoba', 3, '2017-08-21 09:09:37', '2019-04-09 23:31:06', '2019-04-09 23:31:06'),
(2, 'Alta Gracia', 1, '2017-08-21 09:10:41', '2019-02-08 06:52:27', NULL),
(3, 'Cordoba', 1, '2019-04-09 23:29:45', '2019-04-09 23:29:45', NULL),
(4, 'Capital Federal', 5, '2019-04-09 23:31:21', '2019-04-09 23:31:21', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_ivas`
--

CREATE TABLE `condicion_ivas` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `condicion_ivas`
--

INSERT INTO `condicion_ivas` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Monotributo', '2017-08-02 06:13:04', '2017-11-18 23:50:42', NULL),
(2, 'Responsable Inscripto', '2017-08-02 06:13:15', '2017-11-18 23:50:40', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Argentina', '2017-08-21 08:19:03', '2017-08-21 08:21:15', NULL),
(2, 'Brazil', '2017-08-21 08:19:31', '2019-04-09 23:30:35', '2019-04-09 23:30:35'),
(3, 'Venezuela', '2017-08-21 08:19:33', '2019-04-09 23:30:38', '2019-04-09 23:30:38'),
(4, 'Bolivia', '2017-08-21 08:19:35', '2019-04-09 23:30:32', '2019-04-09 23:30:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `current_accounts`
--

CREATE TABLE `current_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `balance` double(12,2) DEFAULT '0.00',
  `id_status_current_account` int(10) UNSIGNED NOT NULL,
  `last_movement_account` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `current_accounts`
--

INSERT INTO `current_accounts` (`id`, `description`, `id_person`, `balance`, `id_status_current_account`, `last_movement_account`, `created_at`, `updated_at`, `deleted_at`) VALUES
(22, NULL, 14, 214139.90, 1, '2019-05-20 16:04:05', '2019-04-09 23:38:04', '2019-05-20 19:04:05', NULL),
(23, NULL, 15, 804.64, 1, '2019-05-20 16:25:05', '2019-04-09 23:38:04', '2019-05-20 19:25:05', NULL),
(24, NULL, 16, -638.03, 1, '2019-05-21 15:42:05', '2019-05-15 13:43:05', '2019-05-20 18:49:05', NULL),
(25, NULL, 17, 0.00, 1, NULL, '2019-05-28 19:53:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `id_sale` int(10) UNSIGNED NOT NULL,
  `id_status_invoice` int(10) UNSIGNED NOT NULL,
  `total_amount` double(12,2) DEFAULT NULL,
  `total_amount_deuda` float(12,2) NOT NULL,
  `retencion` float(12,2) DEFAULT NULL,
  `alicuota_iva` float(12,2) DEFAULT NULL,
  `descuento` float(12,2) DEFAULT NULL,
  `retencion_porc` float DEFAULT NULL,
  `alicuota_iva_porc` float DEFAULT NULL,
  `descuento_porc` float DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `id_person`, `id_sale`, `id_status_invoice`, `total_amount`, `total_amount_deuda`, `retencion`, `alicuota_iva`, `descuento`, `retencion_porc`, `alicuota_iva_porc`, `descuento_porc`, `issue_date`, `expiration_date`, `payment_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(63, 14, 68, 1, 200000.00, 199664.55, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-13 20:27:00', NULL, NULL, '2019-04-13 23:27:04', NULL, NULL),
(64, 14, 69, 1, 3330.00, 1177.00, NULL, NULL, NULL, NULL, 21, 10, '2019-05-10 16:40:00', '2019-05-10 00:00:00', NULL, '2019-05-10 19:40:05', NULL, NULL),
(65, 14, 70, 3, 220.00, 0.00, NULL, NULL, NULL, NULL, 10, NULL, '2019-05-13 17:10:00', '2019-05-31 00:00:00', NULL, '2019-05-13 20:11:05', NULL, NULL),
(66, 16, 71, 3, 5757.00, 0.00, NULL, NULL, NULL, 10, 21, 10, '2019-05-15 10:57:00', '2019-05-31 00:00:00', NULL, '2019-05-15 14:00:05', NULL, NULL),
(67, 16, 72, 1, 800.00, 407.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-15 11:06:00', NULL, NULL, '2019-05-15 14:07:05', NULL, NULL),
(68, 16, 73, 1, 20.00, 20.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-15 16:57:00', '2019-05-15 00:00:00', NULL, '2019-05-15 19:57:05', NULL, NULL),
(69, 16, 74, 1, 110.00, 40.00, NULL, NULL, NULL, NULL, 10, NULL, '2019-05-15 17:04:00', NULL, NULL, '2019-05-15 20:04:05', NULL, NULL),
(70, 16, 75, 1, 107.10, 7.10, NULL, NULL, NULL, NULL, 5, NULL, '2019-05-15 17:06:00', '2019-05-15 00:00:00', NULL, '2019-05-15 20:07:05', NULL, NULL),
(71, 15, 76, 1, 100.00, 5.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-15 17:09:00', NULL, NULL, '2019-05-15 20:09:05', NULL, NULL),
(72, 16, 79, 1, 18.58, 18.58, NULL, NULL, 1.00, 33, 22, NULL, '2019-05-16 09:24:00', NULL, NULL, '2019-05-16 12:24:05', NULL, NULL),
(73, 14, 80, 1, 6.00, 6.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:14:00', NULL, NULL, '2019-05-16 14:14:05', NULL, NULL),
(74, 16, 81, 1, 24.00, 24.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:18:00', NULL, NULL, '2019-05-16 14:18:05', NULL, NULL),
(75, 15, 82, 1, 23.00, 23.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:20:00', NULL, NULL, '2019-05-16 14:20:05', NULL, NULL),
(76, 14, 83, 1, 20.00, 20.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:22:00', NULL, NULL, '2019-05-16 14:23:05', NULL, NULL),
(77, 14, 84, 1, 4.00, 4.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:23:00', NULL, NULL, '2019-05-16 14:23:05', NULL, NULL),
(78, 14, 85, 1, 2.00, 2.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:27:00', NULL, NULL, '2019-05-16 14:27:05', NULL, NULL),
(79, 16, 86, 1, 20.00, 20.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:30:00', NULL, NULL, '2019-05-16 14:30:05', NULL, NULL),
(80, 16, 87, 1, 2.00, 2.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:32:00', NULL, NULL, '2019-05-16 14:32:05', NULL, NULL),
(81, 16, 88, 1, 6.00, 6.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:36:00', NULL, NULL, '2019-05-16 14:36:05', NULL, NULL),
(82, 16, 89, 1, 6.00, 6.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:38:00', NULL, NULL, '2019-05-16 14:38:05', NULL, NULL),
(83, 14, 90, 1, 8.00, 8.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:40:00', NULL, NULL, '2019-05-16 14:40:05', NULL, NULL),
(84, 14, 91, 1, 6.00, 6.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:41:00', NULL, NULL, '2019-05-16 14:41:05', NULL, NULL),
(85, 15, 92, 1, 6.00, 6.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:41:00', NULL, NULL, '2019-05-16 14:42:05', NULL, NULL),
(86, 16, 93, 1, 1665.00, 1665.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:42:00', NULL, NULL, '2019-05-16 14:42:05', NULL, NULL),
(87, 16, 94, 1, 11016.00, 11016.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:46:00', NULL, NULL, '2019-05-16 14:46:05', NULL, NULL),
(88, 14, 95, 1, 50.00, 50.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 11:53:00', NULL, NULL, '2019-05-16 14:53:05', NULL, NULL),
(89, 16, 97, 1, 24.00, 14.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 13:47:00', NULL, NULL, '2019-05-17 16:48:05', NULL, NULL),
(90, 14, 98, 1, 299.60, 299.60, NULL, NULL, NULL, NULL, 12, 5, '2019-05-20 10:52:00', '2019-05-20 00:00:00', NULL, '2019-05-20 13:58:05', NULL, NULL),
(91, 16, 99, 1, 24.00, 24.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 11:02:00', '2019-05-24 00:00:00', NULL, '2019-05-20 14:02:05', NULL, NULL),
(92, 16, 100, 1, 23.28, 23.28, NULL, NULL, NULL, NULL, 2, 5, '2019-05-20 11:10:00', '2019-05-30 00:00:00', NULL, '2019-05-20 14:10:05', NULL, NULL),
(93, 16, 101, 1, 99.96, 99.96, NULL, NULL, NULL, NULL, 3, 5, '2019-05-20 11:15:00', '2019-05-20 00:00:00', NULL, '2019-05-20 14:16:05', NULL, NULL),
(94, 14, 103, 1, 435.00, 435.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 12:26:00', NULL, NULL, '2019-05-20 15:27:05', NULL, NULL),
(95, 14, 104, 1, 388.00, 388.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 12:27:00', NULL, NULL, '2019-05-20 15:27:05', NULL, NULL),
(96, 14, 105, 1, 379.28, 379.28, NULL, NULL, 200.00, 3, 7, NULL, '2019-05-20 12:49:00', NULL, NULL, '2019-05-20 15:50:05', NULL, NULL),
(97, 16, 106, 1, 238.62, 238.62, NULL, NULL, NULL, 7, 7, 3, '2019-05-20 13:49:00', NULL, NULL, '2019-05-20 16:49:05', NULL, NULL),
(98, 16, 107, 1, 76.00, 76.00, NULL, NULL, NULL, 23, 21, 22, '2019-05-20 13:50:00', NULL, NULL, '2019-05-20 16:50:05', NULL, NULL),
(99, 16, 108, 1, 3231.20, 3231.20, 500.00, NULL, NULL, NULL, 21, 15, '2019-05-20 13:52:00', NULL, NULL, '2019-05-20 16:53:05', NULL, NULL),
(100, 16, 109, 1, 4906.00, 4906.00, 500.00, NULL, NULL, NULL, 21, 15, '2019-05-20 13:54:00', '2019-05-31 00:00:00', NULL, '2019-05-20 16:54:05', NULL, NULL),
(101, 14, 110, 1, 413.28, 413.28, NULL, NULL, NULL, NULL, 12, NULL, '2019-05-20 14:58:00', NULL, NULL, '2019-05-20 17:59:05', NULL, NULL),
(102, 14, 111, 1, 446.49, 446.49, NULL, NULL, NULL, NULL, 21, NULL, '2019-05-20 15:00:00', NULL, NULL, '2019-05-20 18:01:05', NULL, NULL),
(103, 15, 112, 1, 664.64, 664.64, NULL, NULL, NULL, NULL, 34, NULL, '2019-05-20 15:30:00', NULL, NULL, '2019-05-20 18:31:05', NULL, NULL),
(104, 14, 113, 1, 780.45, 780.45, NULL, NULL, NULL, NULL, 21, NULL, '2019-05-20 15:32:00', NULL, NULL, '2019-05-20 18:32:05', NULL, NULL),
(105, 14, 114, 1, 87.12, 87.12, NULL, NULL, NULL, NULL, 21, NULL, '2019-05-20 15:33:00', NULL, NULL, '2019-05-20 18:34:05', NULL, NULL),
(106, 14, 115, 1, 465.00, 465.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 15:35:00', NULL, NULL, '2019-05-20 18:36:05', NULL, NULL),
(107, 16, 116, 1, 551.04, 551.04, NULL, NULL, NULL, 4, 21, 5, '2019-05-20 15:42:00', '2019-05-30 00:00:00', NULL, '2019-05-20 18:43:05', NULL, NULL),
(108, 16, 117, 1, 416.97, 416.97, NULL, NULL, NULL, 3, 21, 5, '2019-05-20 15:48:00', '2019-05-31 00:00:00', NULL, '2019-05-20 18:49:05', NULL, NULL),
(109, 14, 118, 1, 3298.00, 3298.00, 800.00, NULL, 500.00, NULL, 21, NULL, '2019-05-20 15:59:00', '2019-05-29 00:00:00', NULL, '2019-05-20 18:59:05', NULL, NULL),
(110, 14, 119, 1, 3854.68, 3854.68, NULL, NULL, NULL, 2, 21, 3, '2019-05-20 16:03:00', NULL, NULL, '2019-05-20 19:04:05', NULL, NULL),
(111, 15, 120, 1, 102.00, 102.00, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 16:25:00', NULL, NULL, '2019-05-20 19:25:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `machines`
--

CREATE TABLE `machines` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `machines`
--

INSERT INTO `machines` (`id`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'MAQUINA NU', '2019-05-10 20:13:58', '2019-05-10 20:11:33', '2019-05-10 20:13:58'),
(2, 'MAQUINA NUEVA', NULL, '2019-05-10 20:11:46', '2019-05-10 20:11:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_07_14_112334_create_productos_table', 1),
(2, '2019_01_17_221809_create_sale_type_products_table', 2),
(3, '2019_01_17_224118_create_sale_products_table', 3),
(4, '2019_01_17_224120_create_sale_products_table', 4),
(5, '2019_01_21_000516_status_current_account', 5),
(6, '2019_01_20_236600_create_current_accounts_table', 6),
(7, '2019_01_21_001638_create_type_movement_accounts_table', 7),
(8, '2019_01_21_001639_create_type_movement_accounts_table', 8),
(9, '2019_01_21_001645_create_type_movement_accounts_table', 9),
(10, '2019_01_21_001058_create_movements_accounts_table', 10),
(11, '2019_01_30_235146_create_type_operations_table', 11),
(12, '2019_01_20_236685_create_current_accounts_table', 12),
(13, '2017_11_13_054124_create_sales_table', 13),
(14, '2017_11_13_054127_create_sales_table', 14),
(15, '2017_11_17_054019_create_products_by_sale_table', 15),
(16, '2017_08_31_045750_create_invoices_table', 16),
(17, '2019_03_14_013730_create_receipts_table', 17),
(18, '2019_03_14_014105_create_cheques_by_receipts_table', 17),
(19, '2019_04_13_211914_create_purchases_table', 18),
(20, '2019_04_13_225546_create_products_by_purchases_table', 18),
(21, '2019_04_13_230632_create_pesajes_by_purchases_table', 19),
(22, '2019_04_18_145534_create_receipt_purchases_table', 20),
(23, '2019_04_18_160012_create_cheques_by_receipt_purchases_table', 21),
(24, '2019_05_10_165800_create_machines_table', 22),
(25, '2019_05_27_163452_create_productions_table', 23),
(26, '2019_05_27_163455_create_productions_table', 24),
(27, '2019_05_27_163457_create_productions_table', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movements_accounts`
--

CREATE TABLE `movements_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_type_movement_account` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nro_receipt` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_operation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_current_account` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `administrative_expenses` double(12,2) DEFAULT NULL,
  `total_amount` double(12,2) NOT NULL,
  `current_balance_account` float(12,2) DEFAULT NULL,
  `date_movement` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `movements_accounts`
--

INSERT INTO `movements_accounts` (`id`, `id_type_movement_account`, `nro_receipt`, `type_operation`, `id_current_account`, `administrative_expenses`, `total_amount`, `current_balance_account`, `date_movement`, `created_at`, `updated_at`, `deleted_at`) VALUES
(26, 1, '63', 'CREDITO', 22, NULL, 200000.00, 200000.00, '2019-04-13 20:27:04', '2019-04-13 23:27:04', NULL, NULL),
(27, 1, '63', 'DEBITO', 22, NULL, 133.00, 199867.00, '2019-04-13 22:53:04', '2019-04-14 01:53:04', NULL, NULL),
(28, 1, '64', 'CREDITO', 22, NULL, 3330.00, 199867.00, '2019-05-10 16:40:05', '2019-05-10 19:40:05', NULL, NULL),
(29, 1, '65', 'CREDITO', 22, NULL, 220.00, 203197.00, '2019-05-13 17:11:05', '2019-05-13 20:11:05', NULL, NULL),
(30, 1, '65', 'DEBITO', 22, NULL, 200.00, 203217.00, '2019-05-13 17:11:05', '2019-05-13 20:11:05', NULL, NULL),
(31, 1, '65', 'DEBITO', 22, NULL, 20.00, 203197.00, '2019-05-13 17:11:05', '2019-05-13 20:11:05', NULL, NULL),
(32, 1, '66', 'CREDITO', 24, NULL, 5757.00, 5757.00, '2019-05-15 11:00:05', '2019-05-15 14:00:05', NULL, NULL),
(33, 1, '66', 'DEBITO', 24, NULL, 3757.00, 2000.00, '2019-05-15 11:02:05', '2019-05-15 14:02:05', NULL, NULL),
(34, 1, '66', 'DEBITO', 24, NULL, 2000.00, 0.00, '2019-05-15 11:04:05', '2019-05-15 14:04:05', NULL, NULL),
(35, 1, '67', 'CREDITO', 24, NULL, 800.00, 800.00, '2019-05-15 11:07:05', '2019-05-15 14:07:05', NULL, NULL),
(36, 1, '67', 'DEBITO', 24, NULL, 1.00, 799.00, '2019-05-15 11:31:05', '2019-05-15 14:31:05', NULL, NULL),
(37, 1, '63', 'DEBITO', 22, NULL, 0.00, 203197.00, '2019-05-15 15:15:05', '2019-05-15 18:15:05', NULL, NULL),
(38, 1, '63', 'DEBITO', 22, NULL, 0.00, 203197.00, '2019-05-15 15:17:05', '2019-05-15 18:17:05', NULL, NULL),
(39, 1, '68', 'CREDITO', 24, NULL, 20.00, 799.00, '2019-05-15 16:57:05', '2019-05-15 19:57:05', NULL, NULL),
(40, 1, '69', 'CREDITO', 24, NULL, 110.00, 819.00, '2019-05-15 17:04:05', '2019-05-15 20:04:05', NULL, NULL),
(41, 1, '70', 'CREDITO', 24, NULL, 107.10, 929.00, '2019-05-15 17:07:05', '2019-05-15 20:07:05', NULL, NULL),
(42, 1, '71', 'CREDITO', 23, NULL, 100.00, 100.00, '2019-05-15 17:09:05', '2019-05-15 20:09:05', NULL, NULL),
(43, 1, '71', 'DEBITO', 23, NULL, 50.00, 50.00, '2019-05-15 17:09:05', '2019-05-15 20:09:05', NULL, NULL),
(44, 1, '72', 'CREDITO', 24, NULL, 18.58, 1036.10, '2019-05-16 09:24:05', '2019-05-16 12:24:05', NULL, NULL),
(45, 1, '71', 'DEBITO', 23, NULL, 20.00, 30.00, '2019-05-16 09:56:05', '2019-05-16 12:56:05', NULL, NULL),
(46, 1, '67', 'DEBITO', 24, NULL, 392.00, 662.68, '2019-05-16 10:04:05', '2019-05-16 13:04:05', NULL, NULL),
(47, 1, '71', 'DEBITO', 23, NULL, 21.00, 9.00, '2019-05-16 10:17:05', '2019-05-16 13:17:05', NULL, NULL),
(48, 1, '71', 'DEBITO', 24, NULL, 4.00, 14.58, '2019-05-16 10:19:05', '2019-05-16 13:19:05', NULL, NULL),
(49, 1, '69', 'DEBITO', 24, NULL, 70.00, -51.42, '2019-05-16 10:23:05', '2019-05-16 13:23:05', NULL, NULL),
(50, 1, '73', 'CREDITO', 22, NULL, 6.00, 203197.00, '2019-05-16 11:14:05', '2019-05-16 14:14:05', NULL, NULL),
(51, 1, '74', 'CREDITO', 24, NULL, 24.00, -51.42, '2019-05-16 11:18:05', '2019-05-16 14:18:05', NULL, NULL),
(52, 1, '75', 'CREDITO', 23, NULL, 23.00, 9.00, '2019-05-16 11:20:05', '2019-05-16 14:20:05', NULL, NULL),
(53, 1, '76', 'CREDITO', 22, NULL, 20.00, 203203.00, '2019-05-16 11:23:05', '2019-05-16 14:23:05', NULL, NULL),
(54, 1, '77', 'CREDITO', 22, NULL, 4.00, 203223.00, '2019-05-16 11:23:05', '2019-05-16 14:23:05', NULL, NULL),
(55, 1, '78', 'CREDITO', 22, NULL, 2.00, 203227.00, '2019-05-16 11:27:05', '2019-05-16 14:27:05', NULL, NULL),
(56, 1, '79', 'CREDITO', 24, NULL, 20.00, -27.42, '2019-05-16 11:30:05', '2019-05-16 14:30:05', NULL, NULL),
(57, 1, '80', 'CREDITO', 24, NULL, 2.00, -7.42, '2019-05-16 11:32:05', '2019-05-16 14:32:05', NULL, NULL),
(58, 1, '81', 'CREDITO', 24, NULL, 6.00, -5.42, '2019-05-16 11:36:05', '2019-05-16 14:36:05', NULL, NULL),
(59, 1, '82', 'CREDITO', 24, NULL, 6.00, 0.58, '2019-05-16 11:38:05', '2019-05-16 14:38:05', NULL, NULL),
(60, 1, '83', 'CREDITO', 22, NULL, 8.00, 203229.00, '2019-05-16 11:40:05', '2019-05-16 14:40:05', NULL, NULL),
(61, 1, '84', 'CREDITO', 22, NULL, 6.00, 203237.00, '2019-05-16 11:41:05', '2019-05-16 14:41:05', NULL, NULL),
(62, 1, '85', 'CREDITO', 23, NULL, 6.00, 32.00, '2019-05-16 11:42:05', '2019-05-16 14:42:05', NULL, NULL),
(63, 1, '86', 'CREDITO', 24, NULL, 1665.00, 6.58, '2019-05-16 11:42:05', '2019-05-16 14:42:05', NULL, NULL),
(64, 1, '87', 'CREDITO', 24, NULL, 11016.00, 1671.58, '2019-05-16 11:46:05', '2019-05-16 14:46:05', NULL, NULL),
(65, 1, '88', 'CREDITO', 22, NULL, 50.00, 203243.00, '2019-05-16 11:53:05', '2019-05-16 14:53:05', NULL, NULL),
(66, 1, '89', 'CREDITO', 24, NULL, 24.00, 12687.58, '2019-05-17 13:48:05', '2019-05-17 16:48:05', NULL, NULL),
(67, 1, '89', 'DEBITO', 24, NULL, 10.00, 14.00, '2019-05-17 13:48:05', '2019-05-17 16:48:05', NULL, NULL),
(68, 1, '90', 'CREDITO', 22, NULL, 299.60, 203293.00, '2019-05-20 10:58:05', '2019-05-20 13:58:05', NULL, NULL),
(69, 1, '91', 'CREDITO', 24, NULL, 24.00, 14.00, '2019-05-20 11:02:05', '2019-05-20 14:02:05', NULL, NULL),
(70, 1, '92', 'CREDITO', 24, NULL, 23.28, 38.00, '2019-05-20 11:10:05', '2019-05-20 14:10:05', NULL, NULL),
(71, 1, '93', 'CREDITO', 24, NULL, 99.96, 61.28, '2019-05-20 11:16:05', '2019-05-20 14:16:05', NULL, NULL),
(72, 1, '94', 'CREDITO', 22, NULL, 435.00, 203592.59, '2019-05-20 12:27:05', '2019-05-20 15:27:05', NULL, NULL),
(73, 1, '95', 'CREDITO', 22, NULL, 388.00, 204027.59, '2019-05-20 12:27:05', '2019-05-20 15:27:05', NULL, NULL),
(74, 1, '96', 'CREDITO', 22, NULL, 379.28, 204415.59, '2019-05-20 12:50:05', '2019-05-20 15:50:05', NULL, NULL),
(75, 1, '97', 'CREDITO', 24, NULL, 238.62, 161.24, '2019-05-20 13:49:05', '2019-05-20 16:49:05', NULL, NULL),
(76, 1, '98', 'CREDITO', 24, NULL, 76.00, 399.86, '2019-05-20 13:50:05', '2019-05-20 16:50:05', NULL, NULL),
(77, 1, '99', 'CREDITO', 24, NULL, 3231.20, 475.86, '2019-05-20 13:53:05', '2019-05-20 16:53:05', NULL, NULL),
(78, 1, '100', 'CREDITO', 24, NULL, 4906.00, 3707.06, '2019-05-20 13:54:05', '2019-05-20 16:54:05', NULL, NULL),
(79, 1, '101', 'CREDITO', 22, NULL, 413.28, 204794.88, '2019-05-20 14:59:05', '2019-05-20 17:59:05', NULL, NULL),
(80, 1, '102', 'CREDITO', 22, NULL, 446.49, 205208.16, '2019-05-20 15:01:05', '2019-05-20 18:01:05', NULL, NULL),
(81, 1, '103', 'CREDITO', 23, NULL, 664.64, 38.00, '2019-05-20 15:31:05', '2019-05-20 18:31:05', NULL, NULL),
(82, 1, '104', 'CREDITO', 22, NULL, 780.45, 205654.66, '2019-05-20 15:32:05', '2019-05-20 18:32:05', NULL, NULL),
(83, 1, '105', 'CREDITO', 22, NULL, 87.12, 206435.09, '2019-05-20 15:34:05', '2019-05-20 18:34:05', NULL, NULL),
(84, 1, '106', 'CREDITO', 22, NULL, 465.00, 206522.22, '2019-05-20 15:36:05', '2019-05-20 18:36:05', NULL, NULL),
(85, 1, '107', 'CREDITO', 24, NULL, 551.04, 8613.06, '2019-05-20 15:43:05', '2019-05-20 18:43:05', NULL, NULL),
(86, 1, '108', 'CREDITO', 24, NULL, 416.97, 9164.10, '2019-05-20 15:49:05', '2019-05-20 18:49:05', NULL, NULL),
(87, 1, '109', 'CREDITO', 22, NULL, 3298.00, 206987.22, '2019-05-20 15:59:05', '2019-05-20 18:59:05', NULL, NULL),
(88, 1, '110', 'CREDITO', 22, NULL, 3854.68, 210285.22, '2019-05-20 16:04:05', '2019-05-20 19:04:05', NULL, NULL),
(89, 1, '111', 'CREDITO', 23, NULL, 102.00, 702.64, '2019-05-20 16:25:05', '2019-05-20 19:25:05', NULL, NULL),
(90, 1, '63', 'DEBITO', 24, NULL, 202.45, 214.52, '2019-05-21 13:02:05', '2019-05-21 16:02:05', NULL, NULL),
(91, 1, '64', 'DEBITO', 24, NULL, 1098.00, -681.03, '2019-05-21 14:30:05', '2019-05-21 17:30:05', NULL, NULL),
(92, 1, '64', 'DEBITO', 24, NULL, 1055.00, -638.03, '2019-05-21 15:42:05', '2019-05-21 18:42:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$.6R/srIeNZguf04qZw1Wnu6RQOof3D9tAJM9pqz6VUByPyK7MtilW', '2017-06-18 02:54:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Tarjeta', '2017-08-02 06:26:43', '2017-08-02 06:26:43', NULL),
(2, 'Efectivo', '2017-08-02 06:26:47', '2017-08-02 06:26:47', NULL),
(3, 'Debito', '2017-08-02 06:26:53', '2017-08-02 06:26:53', NULL),
(4, 'Cheque', '2017-08-02 06:26:57', '2019-02-08 06:49:53', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuit` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_condicion_iva` int(10) UNSIGNED DEFAULT NULL,
  `id_type_people` int(10) UNSIGNED DEFAULT NULL,
  `id_city` int(10) UNSIGNED DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `personality` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `email`, `cuit`, `id_condicion_iva`, `id_type_people`, `id_city`, `adress`, `floor`, `department`, `phone_1`, `phone_2`, `personality`, `created_at`, `updated_at`, `deleted_at`) VALUES
(14, 'Santiago Pujol', 'Cliente', NULL, '234234324', 1, 1, 3, 'Copina 1234', NULL, NULL, NULL, NULL, 'F', '2019-04-09 23:38:06', '2019-05-17 16:41:20', NULL),
(15, 'Santiago Pujol', 'Cliente Proveedor', NULL, '353535323', 1, 4, 4, 'Ohguiins 1523', NULL, NULL, '23432423', NULL, 'J', '2019-04-09 23:38:23', '2019-05-17 16:41:11', NULL),
(16, 'Nicolas', 'Perez', NULL, '16232323', 1, 4, 3, 'Colon 5213', NULL, NULL, NULL, NULL, 'F', '2019-05-15 13:43:28', '2019-05-17 16:41:31', NULL),
(17, 'Empleado', 'Test', NULL, NULL, 1, 3, 3, NULL, NULL, NULL, NULL, NULL, 'F', '2019-05-28 19:53:55', '2019-05-28 19:54:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pesajes_by_purchases`
--

CREATE TABLE `pesajes_by_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_purchase` int(10) UNSIGNED NOT NULL,
  `bruto_chasis` double(12,2) DEFAULT NULL,
  `neto_chasis` double(12,2) DEFAULT NULL,
  `tara_chasis` double(12,2) DEFAULT NULL,
  `bruto_acoplado` double(12,2) DEFAULT NULL,
  `neto_acoplado` double(12,2) DEFAULT NULL,
  `tara_acoplado` double(12,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pesajes_by_purchases`
--

INSERT INTO `pesajes_by_purchases` (`id`, `id_purchase`, `bruto_chasis`, `neto_chasis`, `tara_chasis`, `bruto_acoplado`, `neto_acoplado`, `tara_acoplado`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 15, 100.00, 80.00, 20.00, 100.00, 80.00, 20.00, '2019-05-15 15:10:05', NULL, NULL),
(2, 16, 100.00, 90.00, 10.00, 100.00, 10.00, 90.00, '2019-05-15 15:57:05', NULL, NULL),
(3, 17, 10.00, 8.00, 2.00, 10.00, 2.00, 8.00, '2019-05-15 18:34:05', NULL, NULL),
(4, 18, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-15 19:18:05', NULL, NULL),
(5, 19, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-15 19:34:05', NULL, NULL),
(6, 20, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-15 19:40:05', NULL, NULL),
(7, 21, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-15 19:41:05', NULL, NULL),
(8, 22, 10.00, 8.00, 2.00, 100.00, 98.00, 2.00, '2019-05-15 19:47:05', NULL, NULL),
(9, 23, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-15 19:56:05', NULL, NULL),
(10, 24, 12.00, 9.00, 3.00, 12.00, 9.00, 3.00, '2019-05-16 14:52:05', NULL, NULL),
(11, 25, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-16 14:53:05', NULL, NULL),
(12, 26, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-16 14:54:05', NULL, NULL),
(13, 27, 15.00, 13.00, 2.00, 15.00, 13.00, 2.00, '2019-05-16 14:55:05', NULL, NULL),
(14, 28, 5.00, 3.00, 2.00, 15.00, 13.00, 2.00, '2019-05-16 14:55:05', NULL, NULL),
(15, 29, 10.00, 8.00, 2.00, 10.00, 8.00, 2.00, '2019-05-17 12:13:05', NULL, NULL),
(16, 30, 122.00, 120.00, 2.00, 344.00, 341.00, 3.00, '2019-05-20 14:15:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productions`
--

CREATE TABLE `productions` (
  `id` int(10) UNSIGNED NOT NULL,
  `lote` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `id_machine` int(10) UNSIGNED NOT NULL,
  `quantity_product` double(8,2) NOT NULL,
  `turno` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_product` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type_product` int(10) UNSIGNED NOT NULL,
  `type_metric_product` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'KG',
  `max_purchase_price` double(10,2) DEFAULT NULL,
  `min_purchase_price` double(10,2) DEFAULT NULL,
  `last_purchase_price` double(10,2) DEFAULT NULL,
  `last_sale_price` float(10,2) DEFAULT NULL,
  `stock` double(10,2) DEFAULT NULL,
  `last_change_stock` datetime DEFAULT NULL,
  `max_quantity_stock` float(10,2) DEFAULT NULL,
  `min_quantity_stock` float(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `description`, `code_product`, `id_type_product`, `type_metric_product`, `max_purchase_price`, `min_purchase_price`, `last_purchase_price`, `last_sale_price`, `stock`, `last_change_stock`, `max_quantity_stock`, `min_quantity_stock`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BIDON', '1.1', 40, 'KG', 7.33, 3.50, 2.00, 3.00, 1998357.00, '2019-05-20 16:04:05', 1245.00, 123.00, '2019-01-17 03:05:10', '2019-03-03 21:55:48', NULL),
(2, 'TANQUES', NULL, 41, 'UNIDAD/ES', 7.00, 5.00, 5.00, 3.00, 1999670.00, '2019-05-20 16:04:05', NULL, NULL, '2019-01-29 22:41:01', '2019-02-03 22:44:23', NULL),
(5, 'ROJO', NULL, 39, 'KG', 2.00, 1.00, 2.00, 3.00, 1998722.00, '2019-05-20 16:25:05', NULL, NULL, '2019-02-03 19:56:43', '2019-02-16 18:55:12', NULL),
(6, 'AZUL', '0012', 39, 'KG', 22.00, 12.00, 2.00, 4.00, 1997868.00, '2019-05-20 15:59:05', NULL, NULL, '2019-04-07 18:15:35', '2019-04-07 20:09:56', NULL),
(7, 'Polvo', '001', 40, 'KG', 4.00, 2.00, 3.00, 3.00, 221.00, '2019-05-20 12:50:05', 10000.00, 500.00, '2019-05-15 13:47:59', '2019-05-15 13:47:59', NULL),
(8, 'Polvo 2', '002', 39, 'KG', 10.00, 5.00, 6.00, 8.00, 0.00, '2019-05-15 11:07:05', 5000.00, 200.00, '2019-05-15 13:48:32', '2019-05-15 13:48:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_by_purchase`
--

CREATE TABLE `products_by_purchase` (
  `id_purchase` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `purchase_price` double(10,2) DEFAULT NULL,
  `precio_total_producto` double(10,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products_by_purchase`
--

INSERT INTO `products_by_purchase` (`id_purchase`, `id_product`, `purchase_price`, `precio_total_producto`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, 5, 2.00, 300.00, 150, '2019-05-15 15:10:05', NULL, NULL),
(15, 7, 3.00, 30.00, 10, '2019-05-15 15:10:05', NULL, NULL),
(16, 5, 2.00, 200.00, 100, '2019-05-15 15:57:05', NULL, NULL),
(17, 5, 2.00, 20.00, 10, '2019-05-15 18:34:05', NULL, NULL),
(18, 1, 2.00, 32.00, 16, '2019-05-15 19:18:05', NULL, NULL),
(19, 5, 2.00, 32.00, 16, '2019-05-15 19:34:05', NULL, NULL),
(20, 5, 2.00, 32.00, 16, '2019-05-15 19:40:05', NULL, NULL),
(21, 5, 2.00, 32.00, 16, '2019-05-15 19:41:05', NULL, NULL),
(22, 2, 2.00, 212.00, 106, '2019-05-15 19:47:05', NULL, NULL),
(23, 1, 2.00, 32.00, 16, '2019-05-15 19:56:05', NULL, NULL),
(24, 5, 2.00, 36.00, 18, '2019-05-16 14:52:05', NULL, NULL),
(25, 5, 2.00, 32.00, 16, '2019-05-16 14:53:05', NULL, NULL),
(26, 5, 2.00, 32.00, 16, '2019-05-16 14:54:05', NULL, NULL),
(27, 2, 5.00, 130.00, 26, '2019-05-16 14:55:05', NULL, NULL),
(28, 5, 2.00, 32.00, 16, '2019-05-16 14:55:05', NULL, NULL),
(29, 6, 2.00, 32.00, 16, '2019-05-17 12:13:05', NULL, NULL),
(30, 1, 2.00, 922.00, 461, '2019-05-20 14:15:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_by_sale`
--

CREATE TABLE `products_by_sale` (
  `id_sale` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `purchase_price` float(10,2) DEFAULT NULL,
  `precio_total_producto` float(12,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products_by_sale`
--

INSERT INTO `products_by_sale` (`id_sale`, `id_product`, `quantity`, `purchase_price`, `precio_total_producto`, `created_at`, `updated_at`, `deleted_at`) VALUES
(68, 5, 200, 500.00, 100000.00, '2019-04-13 23:27:04', NULL, NULL),
(68, 1, 200, 500.00, 100000.00, '2019-04-13 23:27:04', NULL, NULL),
(69, 1, 10, 200.00, 2000.00, '2019-05-10 19:40:05', NULL, NULL),
(69, 1, 20, 50.00, 1000.00, '2019-05-10 19:40:05', NULL, NULL),
(70, 6, 10, 20.00, 200.00, '2019-05-13 20:10:05', NULL, NULL),
(71, 7, 300, 7.00, 2100.00, '2019-05-15 13:58:05', NULL, NULL),
(71, 8, 400, 9.00, 3600.00, '2019-05-15 13:58:05', NULL, NULL),
(72, 8, 100, 8.00, 800.00, '2019-05-15 14:07:05', NULL, NULL),
(73, 5, 10, 2.00, 20.00, '2019-05-15 19:57:05', NULL, NULL),
(74, 6, 20, 5.00, 100.00, '2019-05-15 20:04:05', NULL, NULL),
(75, 2, 50, 2.00, 100.00, '2019-05-15 20:06:05', NULL, NULL),
(75, 5, 2, 1.00, 2.00, '2019-05-15 20:06:05', NULL, NULL),
(76, 1, 50, 2.00, 100.00, '2019-05-15 20:09:05', NULL, NULL),
(77, 1, 110, 2.00, 220.00, '2019-05-16 12:18:05', NULL, NULL),
(78, 1, 34, 3.00, 102.00, '2019-05-16 12:22:05', NULL, NULL),
(79, 5, 11, 2.00, 22.00, '2019-05-16 12:24:05', NULL, NULL),
(80, 5, 2, 3.00, 6.00, '2019-05-16 14:14:05', NULL, NULL),
(81, 6, 12, 2.00, 24.00, '2019-05-16 14:18:05', NULL, NULL),
(82, 1, 23, 1.00, 23.00, '2019-05-16 14:20:05', NULL, NULL),
(83, 1, 10, 2.00, 20.00, '2019-05-16 14:22:05', NULL, NULL),
(84, 1, 2, 2.00, 4.00, '2019-05-16 14:23:05', NULL, NULL),
(85, 5, 2, 1.00, 2.00, '2019-05-16 14:27:05', NULL, NULL),
(86, 5, 10, 2.00, 20.00, '2019-05-16 14:30:05', NULL, NULL),
(87, 6, 2, 1.00, 2.00, '2019-05-16 14:32:05', NULL, NULL),
(88, 5, 2, 3.00, 6.00, '2019-05-16 14:36:05', NULL, NULL),
(89, 1, 2, 3.00, 6.00, '2019-05-16 14:38:05', NULL, NULL),
(90, 6, 2, 4.00, 8.00, '2019-05-16 14:40:05', NULL, NULL),
(91, 1, 2, 3.00, 6.00, '2019-05-16 14:41:05', NULL, NULL),
(92, 5, 2, 3.00, 6.00, '2019-05-16 14:42:05', NULL, NULL),
(93, 6, 555, 3.00, 1665.00, '2019-05-16 14:42:05', NULL, NULL),
(94, 6, 324, 34.00, 11016.00, '2019-05-16 14:46:05', NULL, NULL),
(95, 5, 10, 5.00, 50.00, '2019-05-16 14:53:05', NULL, NULL),
(96, 1, 12, 2.00, 24.00, '2019-05-17 12:06:05', NULL, NULL),
(97, 1, 12, 2.00, 24.00, '2019-05-17 16:48:05', NULL, NULL),
(98, 5, 122, 2.00, 244.00, '2019-05-20 13:52:05', NULL, NULL),
(98, 2, 12, 3.00, 36.00, '2019-05-20 13:52:05', NULL, NULL),
(99, 1, 12, 2.00, 24.00, '2019-05-20 14:02:05', NULL, NULL),
(100, 5, 12, 2.00, 24.00, '2019-05-20 14:10:05', NULL, NULL),
(101, 5, 34, 3.00, 102.00, '2019-05-20 14:16:05', NULL, NULL),
(102, 1, 20, 5.00, 100.00, '2019-05-20 15:05:05', NULL, NULL),
(103, 5, 21, 5.00, 105.00, '2019-05-20 15:27:05', NULL, NULL),
(103, 1, 12, 5.00, 60.00, '2019-05-20 15:27:05', NULL, NULL),
(103, 7, 21, 5.00, 105.00, '2019-05-20 15:27:05', NULL, NULL),
(103, 2, 12, 5.00, 60.00, '2019-05-20 15:27:05', NULL, NULL),
(103, 1, 21, 5.00, 105.00, '2019-05-20 15:27:05', NULL, NULL),
(104, 5, 12, 5.00, 60.00, '2019-05-20 15:27:05', NULL, NULL),
(104, 1, 12, 5.00, 60.00, '2019-05-20 15:27:05', NULL, NULL),
(104, 2, 52, 4.00, 208.00, '2019-05-20 15:27:05', NULL, NULL),
(104, 7, 12, 5.00, 60.00, '2019-05-20 15:27:05', NULL, NULL),
(105, 1, 12, 3.00, 36.00, '2019-05-20 15:50:05', NULL, NULL),
(105, 2, 43, 3.00, 129.00, '2019-05-20 15:50:05', NULL, NULL),
(105, 2, 56, 4.00, 224.00, '2019-05-20 15:50:05', NULL, NULL),
(105, 7, 56, 3.00, 168.00, '2019-05-20 15:50:05', NULL, NULL),
(106, 1, 123, 2.00, 246.00, '2019-05-20 16:49:05', NULL, NULL),
(107, 5, 50, 2.00, 100.00, '2019-05-20 16:50:05', NULL, NULL),
(108, 5, 100, 2.00, 200.00, '2019-05-20 16:53:05', NULL, NULL),
(108, 1, 200, 5.00, 1000.00, '2019-05-20 16:53:05', NULL, NULL),
(108, 6, 500, 2.00, 1000.00, '2019-05-20 16:53:05', NULL, NULL),
(108, 1, 220, 6.00, 1320.00, '2019-05-20 16:53:05', NULL, NULL),
(109, 5, 200, 5.00, 1000.00, '2019-05-20 16:54:05', NULL, NULL),
(109, 1, 500, 2.00, 1000.00, '2019-05-20 16:54:05', NULL, NULL),
(109, 1, 500, 3.00, 1500.00, '2019-05-20 16:54:05', NULL, NULL),
(109, 6, 400, 4.00, 1600.00, '2019-05-20 16:54:05', NULL, NULL),
(110, 6, 123, 3.00, 369.00, '2019-05-20 17:59:05', NULL, NULL),
(111, 5, 123, 3.00, 369.00, '2019-05-20 18:00:05', NULL, NULL),
(112, 5, 124, 4.00, 496.00, '2019-05-20 18:31:05', NULL, NULL),
(113, 5, 215, 3.00, 645.00, '2019-05-20 18:32:05', NULL, NULL),
(114, 1, 24, 3.00, 72.00, '2019-05-20 18:34:05', NULL, NULL),
(115, 1, 155, 3.00, 465.00, '2019-05-20 18:35:05', NULL, NULL),
(116, 5, 123, 4.00, 492.00, '2019-05-20 18:42:05', NULL, NULL),
(117, 5, 123, 3.00, 369.00, '2019-05-20 18:48:05', NULL, NULL),
(118, 5, 200, 5.00, 1000.00, '2019-05-20 18:59:05', NULL, NULL),
(118, 2, 500, 4.00, 2000.00, '2019-05-20 18:59:05', NULL, NULL),
(118, 6, 200, 4.00, 800.00, '2019-05-20 18:59:05', NULL, NULL),
(119, 1, 233, 3.00, 699.00, '2019-05-20 19:04:05', NULL, NULL),
(119, 2, 123, 4.00, 492.00, '2019-05-20 19:04:05', NULL, NULL),
(119, 1, 234, 3.00, 702.00, '2019-05-20 19:04:05', NULL, NULL),
(119, 5, 34, 2.00, 68.00, '2019-05-20 19:04:05', NULL, NULL),
(119, 2, 454, 3.00, 1362.00, '2019-05-20 19:04:05', NULL, NULL),
(120, 5, 34, 3.00, 102.00, '2019-05-20 19:25:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `provinces`
--

INSERT INTO `provinces` (`id`, `description`, `id_country`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cordoba', 1, '2017-08-21 08:28:45', '2017-08-22 06:17:29', NULL),
(2, 'Sao Pablo', 2, '2017-08-21 08:29:37', '2019-04-09 23:30:55', '2019-04-09 23:30:55'),
(3, 'Tucuman', 1, '2017-08-21 08:31:51', '2019-04-09 23:30:52', '2019-04-09 23:30:52'),
(5, 'Buenos Aires', 1, '2017-08-22 06:15:00', '2019-02-08 06:52:16', NULL),
(7, 'Caracas', 3, '2017-08-22 06:17:54', '2019-04-09 23:30:49', '2019-04-09 23:30:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchases`
--

CREATE TABLE `purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `id_status_shipment` int(10) UNSIGNED NOT NULL,
  `purchase_cost` double(10,2) NOT NULL,
  `purchase_date` timestamp NULL DEFAULT NULL,
  `alicuota_iva` float(10,2) DEFAULT NULL,
  `alicuota_iva_porc` float(10,2) DEFAULT NULL,
  `purchase_total_deuda` float(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `purchases`
--

INSERT INTO `purchases` (`id`, `id_person`, `id_status_shipment`, `purchase_cost`, `purchase_date`, `alicuota_iva`, `alicuota_iva_porc`, `purchase_total_deuda`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, 16, 3, 330.00, '2019-05-15 15:09:00', NULL, NULL, 0.00, '2019-05-15 15:10:05', NULL, NULL),
(16, 15, 1, 200.00, '2019-05-15 15:57:00', NULL, NULL, 31.00, '2019-05-15 15:57:05', NULL, NULL),
(17, 16, 1, 20.00, '2019-05-15 18:34:00', NULL, NULL, 10.00, '2019-05-15 18:34:05', NULL, NULL),
(18, 15, 1, 32.00, '2019-05-15 19:18:00', NULL, NULL, 32.00, '2019-05-15 19:18:05', NULL, NULL),
(19, 16, 1, 32.00, '2019-05-15 19:34:00', NULL, NULL, 32.00, '2019-05-15 19:34:05', NULL, NULL),
(20, 15, 1, 32.00, '2019-05-15 19:40:00', NULL, NULL, 32.00, '2019-05-15 19:40:05', NULL, NULL),
(21, 16, 1, 32.00, '2019-05-15 19:40:00', NULL, NULL, 32.00, '2019-05-15 19:41:05', NULL, NULL),
(22, 16, 3, 212.00, '2019-05-15 19:46:00', NULL, NULL, 0.00, '2019-05-15 19:47:05', NULL, NULL),
(23, 16, 1, 32.00, '2019-05-15 19:56:00', NULL, NULL, 32.00, '2019-05-15 19:56:05', NULL, NULL),
(24, 15, 1, 36.00, '2019-05-16 14:52:00', NULL, NULL, 36.00, '2019-05-16 14:52:05', NULL, NULL),
(25, 15, 1, 32.00, '2019-05-16 14:53:00', NULL, NULL, 32.00, '2019-05-16 14:53:05', NULL, NULL),
(26, 15, 1, 32.00, '2019-05-16 14:54:00', NULL, NULL, 32.00, '2019-05-16 14:54:05', NULL, NULL),
(27, 16, 1, 130.00, '2019-05-16 14:54:00', NULL, NULL, 130.00, '2019-05-16 14:55:05', NULL, NULL),
(28, 16, 1, 32.00, '2019-05-16 14:55:00', NULL, NULL, 32.00, '2019-05-16 14:55:05', NULL, NULL),
(29, 16, 1, 32.00, '2019-05-17 12:13:00', NULL, NULL, 32.00, '2019-05-17 12:13:05', NULL, NULL),
(30, 16, 1, 922.00, '2019-05-20 14:15:00', NULL, NULL, 922.00, '2019-05-20 14:15:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receipts`
--

CREATE TABLE `receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_invoice` int(10) UNSIGNED NOT NULL,
  `effective_amount` double(8,2) DEFAULT NULL,
  `card_amount` double(8,2) DEFAULT NULL,
  `retencion_amount` double(8,2) DEFAULT NULL,
  `retencion2_amount` double(8,2) DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `nro_orden_pago` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `receipts`
--

INSERT INTO `receipts` (`id`, `id_invoice`, `effective_amount`, `card_amount`, `retencion_amount`, `retencion2_amount`, `total_amount`, `nro_orden_pago`, `created_at`, `updated_at`, `deleted_at`) VALUES
(42, 63, 133.00, 0.00, 0.00, 0.00, 133.00, NULL, '2019-04-14 01:53:43', '2019-04-14 01:53:43', NULL),
(43, 65, 200.00, 0.00, 0.00, 0.00, 200.00, NULL, '2019-05-13 20:11:27', '2019-05-13 20:11:27', NULL),
(44, 65, 20.00, 0.00, 0.00, 0.00, 20.00, NULL, '2019-05-13 20:11:47', '2019-05-13 20:11:47', NULL),
(45, 66, 757.00, 0.00, NULL, 0.00, 3757.00, NULL, '2019-05-15 14:02:59', '2019-05-15 14:02:59', NULL),
(46, 66, 1500.00, 500.00, 0.00, 0.00, 2000.00, NULL, '2019-05-15 14:04:19', '2019-05-15 14:04:19', NULL),
(47, 67, 1.00, 0.00, 0.00, 0.00, 1.00, NULL, '2019-05-15 14:31:27', '2019-05-15 14:31:27', NULL),
(48, 63, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '2019-05-15 18:15:41', '2019-05-15 18:15:41', NULL),
(49, 63, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '2019-05-15 18:17:06', '2019-05-15 18:17:06', NULL),
(50, 70, 100.00, 0.00, 0.00, 0.00, 100.00, NULL, '2019-05-15 20:07:19', '2019-05-15 20:07:19', NULL),
(51, 71, 50.00, 0.00, 0.00, 0.00, 50.00, NULL, '2019-05-15 20:09:32', '2019-05-15 20:09:32', NULL),
(52, 71, 20.00, 0.00, 0.00, 0.00, 20.00, NULL, '2019-05-16 12:56:15', '2019-05-16 12:56:15', NULL),
(53, 67, 0.00, 0.00, 0.00, 0.00, 392.00, NULL, '2019-05-16 13:04:16', '2019-05-16 13:04:16', NULL),
(54, 67, 0.00, 0.00, 0.00, 0.00, 392.00, NULL, '2019-05-16 13:04:30', '2019-05-16 13:04:30', NULL),
(55, 71, 20.00, 0.00, 0.00, 0.00, 21.00, '12', '2019-05-16 13:17:13', '2019-05-16 13:17:13', NULL),
(56, 71, 2.00, 2.00, 0.00, 0.00, 4.00, '1', '2019-05-16 13:19:37', '2019-05-16 13:19:37', NULL),
(57, 69, 50.00, 0.00, 0.00, 0.00, 70.00, '20', '2019-05-16 13:23:49', '2019-05-16 13:23:49', NULL),
(58, 89, 10.00, 0.00, 0.00, 0.00, 10.00, NULL, '2019-05-17 16:48:17', '2019-05-17 16:48:17', NULL),
(59, 63, 202.45, 0.00, 0.00, 0.00, 202.45, '45', '2019-05-21 16:02:01', '2019-05-21 16:02:01', NULL),
(60, 64, 111.00, 0.00, 0.00, 0.00, 1098.00, NULL, '2019-05-21 17:30:55', '2019-05-21 17:30:55', NULL),
(61, 64, 111.00, 0.00, 0.00, 0.00, 1055.00, NULL, '2019-05-21 18:42:23', '2019-05-21 18:42:23', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receipt_purchases`
--

CREATE TABLE `receipt_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_purchase` int(10) UNSIGNED NOT NULL,
  `effective_amount` double(8,2) DEFAULT NULL,
  `card_amount` double(8,2) DEFAULT NULL,
  `retencion_amount` double(8,2) DEFAULT NULL,
  `retencion2_amount` double(8,2) DEFAULT NULL,
  `total_amount` double(8,2) NOT NULL,
  `nro_invoice` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `receipt_purchases`
--

INSERT INTO `receipt_purchases` (`id`, `id_purchase`, `effective_amount`, `card_amount`, `retencion_amount`, `retencion2_amount`, `total_amount`, `nro_invoice`, `created_at`, `updated_at`) VALUES
(42, 15, 100.00, 0.00, 0.00, 0.00, 100.00, NULL, '2019-05-15 15:10:16', '2019-05-15 15:10:16'),
(43, 15, 0.00, 100.00, 0.00, 0.00, 100.00, NULL, '2019-05-15 15:10:21', '2019-05-15 15:10:21'),
(46, 15, 0.00, 0.00, 0.00, 0.00, 100.00, NULL, '2019-05-15 15:12:22', '2019-05-15 15:12:22'),
(47, 15, 0.00, 0.00, 0.00, 0.00, 30.00, NULL, '2019-05-15 15:12:50', '2019-05-15 15:12:50'),
(48, 16, 50.00, 0.00, 0.00, 0.00, 50.00, NULL, '2019-05-15 15:57:54', '2019-05-15 15:57:54'),
(49, 16, 0.00, 0.00, 0.00, 0.00, 10.00, NULL, '2019-05-15 15:58:07', '2019-05-15 15:58:07'),
(50, 16, 0.00, 10.00, 0.00, 0.00, 10.00, NULL, '2019-05-15 15:58:17', '2019-05-15 15:58:17'),
(51, 16, 0.00, 10.00, 0.00, 0.00, 10.00, NULL, '2019-05-15 15:58:32', '2019-05-15 15:58:32'),
(52, 22, 212.00, 0.00, 0.00, 0.00, 212.00, NULL, '2019-05-15 19:47:24', '2019-05-15 19:47:24'),
(53, 16, 10.00, 10.00, 0.00, 0.00, 32.00, NULL, '2019-05-16 13:27:43', '2019-05-16 13:27:43'),
(54, 16, 10.00, 0.00, 0.00, 0.00, 10.00, '12', '2019-05-16 13:28:23', '2019-05-16 13:28:23'),
(55, 16, 20.00, 10.00, 0.00, 0.00, 47.00, NULL, '2019-05-16 13:35:08', '2019-05-16 13:35:08'),
(56, 17, 5.00, 0.00, 0.00, 0.00, 10.00, NULL, '2019-05-16 16:40:22', '2019-05-16 16:40:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_person` int(10) UNSIGNED NOT NULL,
  `id_status_shipment` int(10) UNSIGNED NOT NULL,
  `sell_cost` double(14,2) NOT NULL,
  `sell_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sales`
--

INSERT INTO `sales` (`id`, `id_person`, `id_status_shipment`, `sell_cost`, `sell_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(48, 14, 1, 1600.00, '2019-04-13 22:26:00', '2019-04-13 22:26:04', '2019-04-13 22:28:10', '2019-04-13 22:28:10'),
(49, 15, 1, 2000.00, '2019-04-13 22:28:00', '2019-04-13 22:28:04', '2019-04-13 22:30:50', '2019-04-13 22:30:50'),
(50, 14, 1, 10000.00, '2019-04-13 22:29:00', '2019-04-13 22:29:04', '2019-04-13 22:59:15', '2019-04-13 22:59:15'),
(51, 14, 1, 11000.00, '2019-04-13 22:29:00', '2019-04-13 22:29:04', '2019-04-13 22:30:16', '2019-04-13 22:30:16'),
(52, 14, 1, 5000.00, '2019-04-13 23:00:00', '2019-04-13 23:00:04', '2019-04-13 23:00:53', '2019-04-13 23:00:53'),
(53, 14, 1, 21000.00, '2019-04-13 23:01:00', '2019-04-13 23:01:04', '2019-04-13 23:01:47', '2019-04-13 23:01:47'),
(54, 14, 1, 500000.00, '2019-04-13 23:03:00', '2019-04-13 23:03:04', '2019-04-13 23:04:33', '2019-04-13 23:04:33'),
(55, 14, 1, 26660.00, '2019-04-13 23:04:00', '2019-04-13 23:04:04', '2019-04-13 23:06:22', '2019-04-13 23:06:22'),
(56, 14, 1, 26660.00, '2019-04-13 23:06:00', '2019-04-13 23:06:04', '2019-04-13 23:17:35', '2019-04-13 23:17:35'),
(57, 14, 1, 6720.00, '2019-04-13 23:07:00', '2019-04-13 23:08:04', '2019-04-13 23:16:07', '2019-04-13 23:16:07'),
(58, 14, 1, 10000.00, '2019-04-13 23:08:00', '2019-04-13 23:09:04', '2019-04-13 23:09:39', '2019-04-13 23:09:39'),
(59, 15, 1, 6000.00, '2019-04-13 23:10:00', '2019-04-13 23:11:04', '2019-04-13 23:11:15', '2019-04-13 23:11:15'),
(60, 14, 1, 10000.00, '2019-04-13 23:15:00', '2019-04-13 23:15:04', '2019-04-13 23:15:55', '2019-04-13 23:15:55'),
(61, 15, 1, 10000.00, '2019-04-13 23:17:00', '2019-04-13 23:17:04', '2019-04-13 23:18:11', '2019-04-13 23:18:11'),
(62, 14, 1, 10000.00, '2019-04-13 23:18:00', '2019-04-13 23:19:04', '2019-04-13 23:20:14', '2019-04-13 23:20:14'),
(63, 14, 1, 240000.00, '2019-04-13 23:20:00', '2019-04-13 23:20:04', '2019-04-13 23:20:58', '2019-04-13 23:20:58'),
(64, 15, 1, 60000.00, '2019-04-13 23:21:00', '2019-04-13 23:21:04', '2019-04-13 23:21:55', '2019-04-13 23:21:55'),
(65, 14, 1, 20200.00, '2019-04-13 23:22:00', '2019-04-13 23:22:04', '2019-04-13 23:22:42', '2019-04-13 23:22:42'),
(66, 14, 1, 25000.00, '2019-04-13 23:23:00', '2019-04-13 23:23:04', '2019-04-13 23:24:13', '2019-04-13 23:24:13'),
(67, 15, 1, 10000.00, '2019-04-13 23:24:00', '2019-04-13 23:24:04', '2019-04-13 23:24:55', '2019-04-13 23:24:55'),
(68, 14, 1, 200000.00, '2019-04-13 23:27:00', '2019-04-13 23:27:04', NULL, NULL),
(69, 14, 1, 3000.00, '2019-05-10 19:40:00', '2019-05-10 19:40:05', NULL, NULL),
(70, 14, 1, 200.00, '2019-05-13 20:10:00', '2019-05-13 20:10:05', NULL, NULL),
(71, 16, 1, 5700.00, '2019-05-15 13:57:00', '2019-05-15 13:58:05', NULL, NULL),
(72, 16, 1, 800.00, '2019-05-15 14:06:00', '2019-05-15 14:07:05', NULL, NULL),
(73, 16, 1, 20.00, '2019-05-15 19:57:00', '2019-05-15 19:57:05', NULL, NULL),
(74, 16, 1, 100.00, '2019-05-15 20:04:00', '2019-05-15 20:04:05', NULL, NULL),
(75, 16, 1, 102.00, '2019-05-15 20:06:00', '2019-05-15 20:06:05', NULL, NULL),
(76, 15, 1, 100.00, '2019-05-15 20:09:00', '2019-05-15 20:09:05', NULL, NULL),
(77, 16, 1, 220.00, '2019-05-16 12:18:00', '2019-05-16 12:18:05', NULL, NULL),
(78, 16, 1, 102.00, '2019-05-16 12:21:00', '2019-05-16 12:22:05', NULL, NULL),
(79, 16, 1, 22.00, '2019-05-16 12:24:00', '2019-05-16 12:24:05', NULL, NULL),
(80, 14, 1, 6.00, '2019-05-16 14:14:00', '2019-05-16 14:14:05', NULL, NULL),
(81, 16, 1, 24.00, '2019-05-16 14:18:00', '2019-05-16 14:18:05', NULL, NULL),
(82, 15, 1, 23.00, '2019-05-16 14:20:00', '2019-05-16 14:20:05', NULL, NULL),
(83, 14, 1, 20.00, '2019-05-16 14:22:00', '2019-05-16 14:22:05', NULL, NULL),
(84, 14, 1, 4.00, '2019-05-16 14:23:00', '2019-05-16 14:23:05', NULL, NULL),
(85, 14, 1, 2.00, '2019-05-16 14:27:00', '2019-05-16 14:27:05', NULL, NULL),
(86, 16, 1, 20.00, '2019-05-16 14:30:00', '2019-05-16 14:30:05', NULL, NULL),
(87, 16, 1, 2.00, '2019-05-16 14:32:00', '2019-05-16 14:32:05', NULL, NULL),
(88, 16, 1, 6.00, '2019-05-16 14:36:00', '2019-05-16 14:36:05', NULL, NULL),
(89, 16, 1, 6.00, '2019-05-16 14:38:00', '2019-05-16 14:38:05', NULL, NULL),
(90, 14, 1, 8.00, '2019-05-16 14:40:00', '2019-05-16 14:40:05', NULL, NULL),
(91, 14, 1, 6.00, '2019-05-16 14:41:00', '2019-05-16 14:41:05', NULL, NULL),
(92, 15, 1, 6.00, '2019-05-16 14:41:00', '2019-05-16 14:42:05', NULL, NULL),
(93, 16, 1, 1665.00, '2019-05-16 14:42:00', '2019-05-16 14:42:05', NULL, NULL),
(94, 16, 1, 11016.00, '2019-05-16 14:46:00', '2019-05-16 14:46:05', NULL, NULL),
(95, 14, 1, 50.00, '2019-05-16 14:53:00', '2019-05-16 14:53:05', NULL, NULL),
(96, 16, 1, 24.00, '2019-05-17 12:06:00', '2019-05-17 12:06:05', NULL, NULL),
(97, 16, 1, 24.00, '2019-05-17 16:47:00', '2019-05-17 16:48:05', NULL, NULL),
(98, 14, 1, 280.00, '2019-05-20 13:52:00', '2019-05-20 13:52:05', NULL, NULL),
(99, 16, 1, 24.00, '2019-05-20 14:02:00', '2019-05-20 14:02:05', NULL, NULL),
(100, 16, 1, 24.00, '2019-05-20 14:10:00', '2019-05-20 14:10:05', NULL, NULL),
(101, 16, 1, 102.00, '2019-05-20 14:15:00', '2019-05-20 14:16:05', NULL, NULL),
(102, 16, 1, 100.00, '2019-05-20 15:05:00', '2019-05-20 15:05:05', NULL, NULL),
(103, 14, 1, 435.00, '2019-05-20 15:26:00', '2019-05-20 15:27:05', NULL, NULL),
(104, 14, 1, 388.00, '2019-05-20 15:27:00', '2019-05-20 15:27:05', NULL, NULL),
(105, 14, 1, 557.00, '2019-05-20 15:49:00', '2019-05-20 15:50:05', NULL, NULL),
(106, 16, 1, 246.00, '2019-05-20 16:49:00', '2019-05-20 16:49:05', NULL, NULL),
(107, 16, 1, 100.00, '2019-05-20 16:50:00', '2019-05-20 16:50:05', NULL, NULL),
(108, 16, 1, 3520.00, '2019-05-20 16:52:00', '2019-05-20 16:53:05', NULL, NULL),
(109, 16, 1, 5100.00, '2019-05-20 16:54:00', '2019-05-20 16:54:05', NULL, NULL),
(110, 14, 1, 369.00, '2019-05-20 17:58:00', '2019-05-20 17:59:05', NULL, NULL),
(111, 14, 1, 369.00, '2019-05-20 18:00:00', '2019-05-20 18:00:05', NULL, NULL),
(112, 15, 1, 496.00, '2019-05-20 18:30:00', '2019-05-20 18:31:05', NULL, NULL),
(113, 14, 1, 645.00, '2019-05-20 18:32:00', '2019-05-20 18:32:05', NULL, NULL),
(114, 14, 1, 72.00, '2019-05-20 18:33:00', '2019-05-20 18:34:05', NULL, NULL),
(115, 14, 1, 465.00, '2019-05-20 18:35:00', '2019-05-20 18:35:05', NULL, NULL),
(116, 16, 1, 492.00, '2019-05-20 18:42:00', '2019-05-20 18:42:05', NULL, NULL),
(117, 16, 1, 369.00, '2019-05-20 18:48:00', '2019-05-20 18:48:05', NULL, NULL),
(118, 14, 1, 3800.00, '2019-05-20 18:59:00', '2019-05-20 18:59:05', NULL, NULL),
(119, 14, 1, 3323.00, '2019-05-20 19:03:00', '2019-05-20 19:04:05', NULL, NULL),
(120, 15, 1, 102.00, '2019-05-20 19:25:00', '2019-05-20 19:25:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_current_account`
--

CREATE TABLE `status_current_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_current_account`
--

INSERT INTO `status_current_account` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ACTIVA', '2019-01-31 02:40:58', '2019-05-15 14:07:48', NULL),
(2, 'INACTIVA', '2019-01-31 02:41:06', '2019-01-31 02:41:06', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_invoices`
--

CREATE TABLE `status_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_invoices`
--

INSERT INTO `status_invoices` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Emitida', '2017-08-27 02:04:41', '2019-02-18 03:17:32', NULL),
(2, 'Anulada', '2017-08-27 02:04:50', '2017-08-27 02:04:50', NULL),
(3, 'Pagada', '2017-09-03 01:39:19', '2019-03-03 21:19:20', NULL),
(4, 'Vencida', '2019-02-18 03:18:26', '2019-03-03 21:19:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_shipments`
--

CREATE TABLE `status_shipments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_shipments`
--

INSERT INTO `status_shipments` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Activo', '2017-08-27 02:03:20', '2019-02-18 03:19:09', NULL),
(2, 'Inactivo', '2017-08-27 02:06:40', '2019-02-18 03:18:51', NULL),
(3, 'Pagado', '2019-05-15 14:49:30', '2019-05-15 14:49:30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_movement_accounts`
--

CREATE TABLE `type_movement_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_movement_accounts`
--

INSERT INTO `type_movement_accounts` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'FACTURA', '2019-01-21 04:05:52', '2019-02-09 21:25:49', NULL),
(2, 'ORDEN DE PAGO', '2019-01-21 04:05:58', '2019-02-09 21:25:42', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_people`
--

CREATE TABLE `type_people` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_people`
--

INSERT INTO `type_people` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CLIENTE', '2017-08-03 06:53:57', '2017-08-03 06:53:57', NULL),
(2, 'PROVEEDOR', '2017-08-03 06:54:43', '2017-08-03 06:54:43', NULL),
(3, 'EMPLEADO', '2017-08-03 06:55:05', '2019-02-03 23:18:09', NULL),
(4, 'CLIENTE - PROVEEDOR', '2019-03-31 00:02:14', '2019-03-31 00:02:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_products`
--

CREATE TABLE `type_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_products`
--

INSERT INTO `type_products` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(39, 'DURO', '2017-11-08 08:50:04', '2019-02-03 23:16:29', NULL),
(40, 'SILO', '2017-11-08 08:50:18', '2019-01-17 03:17:04', NULL),
(41, 'ROTOMOLDEO', '2019-01-29 22:39:53', '2019-01-29 22:39:53', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_users`
--

CREATE TABLE `type_users` (
  `id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_users`
--

INSERT INTO `type_users` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, '2019-02-08 06:53:02', NULL),
(2, 'Root', '2017-11-12 21:44:45', '2019-01-10 22:47:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `status` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `username`, `email`, `password`, `type`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Admin', '', 'admin', 'admin', '$2y$10$DSf4JEQwb0LuC.S10Y.gH.tPgBW7wXarmKUaN0Zyfyi7X3zj4PSy.', 1, 'on', 'NbeT1JerOaBk0CnUN3BCPi2RyaMvgFOlDJ3ZGQrB7mfUQzMPrNuiqga8VMcS', NULL, NULL, NULL),
(27, 'Root', '', 'root', 'root', '$2y$10$DSf4JEQwb0LuC.S10Y.gH.tPgBW7wXarmKUaN0Zyfyi7X3zj4PSy.', 2, 'on', 'IaS4YxCp7N9fwcVGkHRlwT5tklwVhKBOgA6y6kcqhuUIhAu3H6lyAvCodbi5', '2017-11-12 21:45:14', '2017-11-12 21:45:14', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cheques_by_receipts`
--
ALTER TABLE `cheques_by_receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CONSTRAIT_ID_RECEIP` (`id_receipt`);

--
-- Indices de la tabla `cheques_by_receipt_purchases`
--
ALTER TABLE `cheques_by_receipt_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cheques_by_receipt_purchases_id_receipt_foreign` (`id_receipt_purchase`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_id_province_foreign` (`id_province`);

--
-- Indices de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_person` (`id_person`),
  ADD KEY `current_accounts_id_status_current_account_foreign` (`id_status_current_account`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_id_person_foreign` (`id_person`),
  ADD KEY `invoices_id_sale_foreign` (`id_sale`),
  ADD KEY `invoices_id_status_invoice_foreign` (`id_status_invoice`);

--
-- Indices de la tabla `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movements_accounts_id_type_movement_account_foreign` (`id_type_movement_account`),
  ADD KEY `id_current_account` (`id_current_account`);

--
-- Indices de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `people_cuit_unique` (`cuit`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `people_id_condicion_iva_foreign` (`id_condicion_iva`),
  ADD KEY `people_id_type_people_foreign` (`id_type_people`),
  ADD KEY `id_city` (`id_city`);

--
-- Indices de la tabla `pesajes_by_purchases`
--
ALTER TABLE `pesajes_by_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pesajes_by_purchases_id_purchase_foreign` (`id_purchase`);

--
-- Indices de la tabla `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productions_id_product_foreign` (`id_product`),
  ADD KEY `productions_id_person_foreign` (`id_person`),
  ADD KEY `productions_id_machine_foreign` (`id_machine`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productos_id_type_product_foreign` (`id_type_product`);

--
-- Indices de la tabla `products_by_purchase`
--
ALTER TABLE `products_by_purchase`
  ADD KEY `products_by_purchase_id_product_foreign` (`id_product`),
  ADD KEY `products_by_purchase_id_purchase_foreign` (`id_purchase`);

--
-- Indices de la tabla `products_by_sale`
--
ALTER TABLE `products_by_sale`
  ADD KEY `products_by_sale_id_product_foreign` (`id_product`),
  ADD KEY `products_by_sale_id_sale_foreign` (`id_sale`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinces_id_country_foreign` (`id_country`);

--
-- Indices de la tabla `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchases_id_person_foreign` (`id_person`),
  ADD KEY `purchases_id_status_shipment_foreign` (`id_status_shipment`);

--
-- Indices de la tabla `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipts_id_invoice_foreign` (`id_invoice`);

--
-- Indices de la tabla `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipt_purchases_id_purchase_foreign` (`id_purchase`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_id_person_foreign` (`id_person`),
  ADD KEY `sales_id_status_shipment_foreign` (`id_status_shipment`);

--
-- Indices de la tabla `status_current_account`
--
ALTER TABLE `status_current_account`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_movement_accounts`
--
ALTER TABLE `type_movement_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_people`
--
ALTER TABLE `type_people`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_products`
--
ALTER TABLE `type_products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_users`
--
ALTER TABLE `type_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `usuario` (`username`),
  ADD KEY `users_type_foreign` (`type`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=713;
--
-- AUTO_INCREMENT de la tabla `cheques_by_receipts`
--
ALTER TABLE `cheques_by_receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `cheques_by_receipt_purchases`
--
ALTER TABLE `cheques_by_receipt_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT de la tabla `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `pesajes_by_purchases`
--
ALTER TABLE `pesajes_by_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `productions`
--
ALTER TABLE `productions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT de la tabla `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT de la tabla `status_current_account`
--
ALTER TABLE `status_current_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `type_movement_accounts`
--
ALTER TABLE `type_movement_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `type_people`
--
ALTER TABLE `type_people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `type_products`
--
ALTER TABLE `type_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cheques_by_receipts`
--
ALTER TABLE `cheques_by_receipts`
  ADD CONSTRAINT `CONSTRAIT_ID_RECEIP` FOREIGN KEY (`id_receipt`) REFERENCES `receipts` (`id`);

--
-- Filtros para la tabla `cheques_by_receipt_purchases`
--
ALTER TABLE `cheques_by_receipt_purchases`
  ADD CONSTRAINT `cheques_by_receipt_purchases_id_receipt_foreign` FOREIGN KEY (`id_receipt_purchase`) REFERENCES `receipt_purchases` (`id`);

--
-- Filtros para la tabla `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_id_province_foreign` FOREIGN KEY (`id_province`) REFERENCES `provinces` (`id`);

--
-- Filtros para la tabla `current_accounts`
--
ALTER TABLE `current_accounts`
  ADD CONSTRAINT `current_accounts_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `current_accounts_id_status_current_account_foreign` FOREIGN KEY (`id_status_current_account`) REFERENCES `status_current_account` (`id`);

--
-- Filtros para la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `invoices_id_sale_foreign` FOREIGN KEY (`id_sale`) REFERENCES `sales` (`id`),
  ADD CONSTRAINT `invoices_id_status_invoice_foreign` FOREIGN KEY (`id_status_invoice`) REFERENCES `status_invoices` (`id`);

--
-- Filtros para la tabla `movements_accounts`
--
ALTER TABLE `movements_accounts`
  ADD CONSTRAINT `movements_accounts_ibfk_1` FOREIGN KEY (`id_current_account`) REFERENCES `current_accounts` (`id`),
  ADD CONSTRAINT `movements_accounts_id_type_movement_account_foreign` FOREIGN KEY (`id_type_movement_account`) REFERENCES `type_movement_accounts` (`id`);

--
-- Filtros para la tabla `pesajes_by_purchases`
--
ALTER TABLE `pesajes_by_purchases`
  ADD CONSTRAINT `pesajes_by_purchases_id_purchase_foreign` FOREIGN KEY (`id_purchase`) REFERENCES `purchases` (`id`);

--
-- Filtros para la tabla `productions`
--
ALTER TABLE `productions`
  ADD CONSTRAINT `productions_id_machine_foreign` FOREIGN KEY (`id_machine`) REFERENCES `machines` (`id`),
  ADD CONSTRAINT `productions_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `productions_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_id_type_product_foreign` FOREIGN KEY (`id_type_product`) REFERENCES `type_products` (`id`);

--
-- Filtros para la tabla `products_by_purchase`
--
ALTER TABLE `products_by_purchase`
  ADD CONSTRAINT `products_by_purchase_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `products_by_purchase_id_purchase_foreign` FOREIGN KEY (`id_purchase`) REFERENCES `purchases` (`id`);

--
-- Filtros para la tabla `products_by_sale`
--
ALTER TABLE `products_by_sale`
  ADD CONSTRAINT `products_by_sale_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `products_by_sale_id_sale_foreign` FOREIGN KEY (`id_sale`) REFERENCES `sales` (`id`);

--
-- Filtros para la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_id_country_foreign` FOREIGN KEY (`id_country`) REFERENCES `countries` (`id`);

--
-- Filtros para la tabla `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `purchases_id_status_shipment_foreign` FOREIGN KEY (`id_status_shipment`) REFERENCES `status_shipments` (`id`);

--
-- Filtros para la tabla `receipts`
--
ALTER TABLE `receipts`
  ADD CONSTRAINT `receipts_id_invoice_foreign` FOREIGN KEY (`id_invoice`) REFERENCES `invoices` (`id`);

--
-- Filtros para la tabla `receipt_purchases`
--
ALTER TABLE `receipt_purchases`
  ADD CONSTRAINT `receipt_purchases_id_purchase_foreign` FOREIGN KEY (`id_purchase`) REFERENCES `purchases` (`id`);

--
-- Filtros para la tabla `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `sales_id_status_shipment_foreign` FOREIGN KEY (`id_status_shipment`) REFERENCES `status_shipments` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_foreign` FOREIGN KEY (`type`) REFERENCES `type_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
