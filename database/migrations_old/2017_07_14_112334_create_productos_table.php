<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 150);
            $table->string('code_product', 100)->nullable();
            $table->integer('id_type_product')->unsigned();
            $table->foreign('id_type_product')->references('id')->on('type_products');
            $table->string('type_metric_product', 20)->default('KG');
            $table->float('max_purchase_price', 10, 2)->nullable();
            $table->float('min_purchase_price', 10, 2)->nullable();
            $table->float('last_purchase_price', 10, 2)->nullable();
            $table->float('last_sale_price', 10, 2)->nullable();
            $table->float('stock', 10, 2)->nullable();
            $table->dateTime('last_change_stock')->nullable();
            $table->float('max_quantity_stock', 10, 2)->nullable();
            $table->float('min_quantity_stock', 10, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}