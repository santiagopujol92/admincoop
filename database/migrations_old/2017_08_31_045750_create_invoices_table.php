<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_person')->unsigned()->default(0);
            $table->foreign('id_person')->references('id')->on('people');
            $table->integer('id_sale')->unsigned();
            $table->foreign('id_sale')->references('id')->on('sales');
            $table->integer('id_status_invoice')->unsigned();
            $table->foreign('id_status_invoice')->references('id')->on('status_invoices');
            $table->float('total_amount', 12, 2)->nullable();
            $table->float('total_amount_deuda', 12, 2)->nullable();
            $table->float('retencion', 12, 2)->nullable();
            $table->float('alicuota_iva', 12, 2)->nullable();
            $table->float('descuento', 12, 2)->nullable();
            $table->float('retencion_porc', 12, 2)->nullable();
            $table->float('alicuota_iva_porc', 12, 2)->nullable();
            $table->float('descuento_porc', 12, 2)->nullable();
            $table->datetime('issue_date');
            $table->datetime('expiration_date')->nullable();;
            $table->datetime('payment_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
