<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('lastname', 100);
            $table->string('email', 100)->unique()->nullable();
            $table->string('cuit', 100)->unique()->nullable();
            $table->integer('id_condicion_iva')->unsigned();
            $table->foreign('id_condicion_iva')->references('id')->on('condicion_ivas');
            $table->integer('id_type_people')->unsigned();
            $table->foreign('id_type_people')->references('id')->on('type_people');
            $table->integer('id_city')->unsigned();
            $table->foreign('id_city')->references('id')->on('cities');
            $table->string('adress', 255)->nullable();
            $table->string('floor', 50)->nullable();
            $table->string('department', 50)->nullable();
            $table->string('phone_1', 50)->nullable();
            $table->string('phone_2', 50)->nullable();
            $table->string('personality', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
