<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('current_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 150)->nullable();
            $table->integer('id_person')->unsigned()->unique();;
            $table->foreign('id_person')->references('id')->on('people');
            $table->float('balance', 12, 2)->nullable();
            $table->integer('id_status_current_account')->unsigned();
            $table->foreign('id_status_current_account')->references('id')->on('status_current_account');
            $table->datetime('last_movement_account')->nullable();;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_accounts');
    }
}
