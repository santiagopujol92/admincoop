<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_type_movement_account')->unsigned()->default(0);
            $table->foreign('id_type_movement_account')->references('id')->on('type_movement_accounts');
            $table->string('type_operation', 50)->nullable();
            $table->string('nro_receipt', 50)->nullable();
            $table->integer('id_current_account')->unsigned()->default(0);
            $table->foreign('id_current_account')->references('id')->on('current_accounts');
            $table->float('administrative_expenses', 12, 2)->nullable();
            $table->float('total_amount', 12, 2);
            $table->float('current_balance_account', 12, 2);
            $table->timestamp('date_movement')->useCurrent();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements_accounts');
    }
}
