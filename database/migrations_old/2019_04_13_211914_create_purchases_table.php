<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_person')->unsigned();
            $table->foreign('id_person')->references('id')->on('people');
            $table->integer('id_status_shipment')->unsigned();
            $table->foreign('id_status_shipment')->references('id')->on('status_shipments');
            $table->float('purchase_cost', 10, 2);
            $table->float('alicuota_iva', 10, 2)->nullable();
            $table->float('alicuota_iva_porc', 10, 2)->nullable();
            $table->timestamp('purchase_date')->nullable();
            $table->float('purchase_total_deuda', 10, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
