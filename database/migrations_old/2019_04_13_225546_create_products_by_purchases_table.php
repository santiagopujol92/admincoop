<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsByPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_by_purchase', function (Blueprint $table) {
            $table->integer('id_purchase')->unsigned();
            $table->foreign('id_purchase')->references('id')->on('purchases');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('productos');
            $table->float('purchase_price', 10, 2)->nullable();
            $table->float('precio_total_producto', 10, 2)->nullable();
            $table->integer('quantity')->default(1);
            $table->timestamps();
            $table->softDeletes();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_by_purchases');
    }
}
