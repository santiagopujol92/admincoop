<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesajesByPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesajes_by_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_purchase')->unsigned();
            $table->foreign('id_purchase')->references('id')->on('purchases');
            $table->float('bruto_chasis', 12, 2)->nullable();
            $table->float('neto_chasis', 12, 2)->nullable();
            $table->float('tara_chasis', 12, 2)->nullable();
            $table->float('bruto_acoplado', 12, 2)->nullable();
            $table->float('neto_acoplado', 12, 2)->nullable();
            $table->float('tara_acoplado', 12, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesajes_by_purchases');
    }
}
