<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_purchase')->unsigned();
            $table->foreign('id_purchase')->references('id')->on('purchases');
            $table->float('effective_amount')->nullable();
            $table->float('card_amount')->nullable();
            $table->float('retencion_amount')->nullable();
            $table->float('retencion2_amount')->nullable();
            $table->float('total_amount');
            $table->string('nro_invoice', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_purchases');
    }
}
