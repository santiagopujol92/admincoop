<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequesByReceiptPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheques_by_receipt_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receipt_purchase')->unsigned();
            $table->foreign('id_receipt_purchase')->references('id')->on('receipt_purchases');
            $table->string('nro_cheque');
            $table->string('banco')->nullable();
            $table->string('concepto')->nullable();
            $table->string('nombre')->nullable();
            $table->string('cuit')->nullable();
            $table->float('importe');
            $table->datetime('fecha_emision')->nullable();
            $table->datetime('fecha_pago')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheques_by_receipt_purchases');
    }
}
