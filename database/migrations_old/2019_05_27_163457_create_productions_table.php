<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lote', 100)->nullable();
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('productos');
            $table->integer('id_person')->unsigned();
            $table->foreign('id_person')->references('id')->on('people');
            $table->integer('id_machine')->unsigned();
            $table->foreign('id_machine')->references('id')->on('machines');
            $table->float('quantity_product');
            $table->string('turno', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
