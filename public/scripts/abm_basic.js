/* SE IMPORTA EN CADA VISTA, YA QUE ALGUNAS TIENEN UN ABM PARTICULAR
LAS VARIABLES QUE SE USAN SE DECLARAN EN EL {slug_modulo}.js CORRESPONDIENTE QUE INCLUYE ESTE ARCHIVO
*/

//current_route VIENE de funciones.js

/* AGREGAR */
function agregar(validar){
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+form);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}

		//Validar email si existe el input con name email
		if ($("#form"+form+" input[name=email]").length > 0){
			if ($("#form"+form+" input[name=email]").val() != ''){
				if (!isEmail($("#form"+form+" input[name=email]").val())){
					showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
					return false;
				}
			}
		}
		
	}

	var route = current_route+"";
	var datos = $("#form"+form+"").serialize();
		$.ajax({
			url: route,
			headers: {  'X-CSRF-TOKEN': token},
			type: 'POST',
			dataType: 'json',
			data:datos,
            beforeSend: function () {
             	statusLoading('loading_modal', 1, 'Agregando '+modulo_msg+' ..');
            },
			success: function(result){

				//Si la respuesta_custom existe
				if (result.response_custom){

					//Si la respuesta custom es stock_error
					if (result.response_custom == 'stock_error'){
						//Recorremos los productos sin stock
						var productos = '';
						for (var i = 0; i < result.productos_sin_stock.length; i++) {
						   	var producto = result.productos_sin_stock[i];
							productos = productos + '- ' + producto + '<br>';
						}		
						showMessageModal('No se pudo cargar la '+modulo_msg+'.<br><br>Productos que no tienen disponible stock para las comidas seleccionadas:<br><b class="col-red">' + productos + '</b>', 'Alerta de Stock!', 'bg-yellow', 'warning');
					}
					//Otras respuestas
					//code
					
				//Si no tiene respuesta custom muestra la generica
				}else{
					showMessageModal(''+modulo_msg+' Agregado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
					listar();
					clearForm('form'+form+'');
					$('#modal'+modals_btns+'').modal('hide');
				}
				statusLoading('loading_modal', 0);
			},
			error: function(result){
				showMessageModal('No se pudo Agregar un/a '+modulo_msg+'. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'ATENCION', 'bg-yellow', 'warning');
				statusLoading('loading_modal', 0);
			}
		})	
}

/* UPDATE */
function update(id, validar){
	var route = current_route+"/"+id+"";
	var datos = $("#form"+form+"").serialize();

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+form);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}

		//Validar email si existe el input con name email
		if ($("#form"+form+" input[name=email]").length > 0){
			if ($("#form"+form+" input[name=email]").val() != ''){
				if (!isEmail($("#form"+form+" input[name=email]").val())){
					showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>El email ingresado no es valido', 'error');
					return false;
				}
			}
		}
	}
	
	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal', 1, 'Guardando ..');
        },
		success: function(result){
			// statusLoading('loading_modal', 0);
			// listar();
			// $('#modal'+modals_btns+'').modal('hide');
			// showMessageModal(modulo_msg+' Modificado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');

			//Si la respuesta_custom existe
			if (result.response_custom){

				//Si la respuesta custom es stock_error
				if (result.response_custom == 'stock_error'){
					//Recorremos los productos sin stock
					var productos = '';
					for (var i = 0; i < result.productos_sin_stock.length; i++) {
					   	var producto = result.productos_sin_stock[i];
						productos = productos + '- ' + producto + '<br>';
					}		
					showMessageModal('No se pudo cargar la '+modulo_msg+'.<br><br>Productos que no tienen disponible stock para las comidas seleccionadas:<br><b class="col-red">' + productos + '</b>', 'Alerta de Stock!', 'bg-yellow', 'warning');
				}
				//Otras respuestas
				//code
				
			//Si no tiene respuesta custom muestra la generica
			}else{
				showMessageModal(modulo_msg+' Modificado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
				listar();
				$('#modal'+modals_btns+'').modal('hide');
			}
			statusLoading('loading_modal', 0);
		},
		error: function(){
			showMessageModal('No se pudo Modificar el/la '+modulo_msg+'. <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal', 0);
		}
	})	
}

/* ELIMINAR */
function eliminar(id){
	var route = current_route+"/"+id+"";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		data: id,
        beforeSend: function () {
		    statusLoading('loading_modal_delete', 1, 'Eliminando/a '+modulo_msg+' ..');
        },
		success: function(result){
			statusLoading('loading_modal_delete', 0);
			listar();
			$('#modalDelete'+modals_btns+'').modal('hide');
			showMessageModal(modulo_msg+' Eliminado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
		},
		error: function(){
			statusLoading('loading_modal_delete', 0);
			$('#modalDelete'+modals_btns+'').modal('hide');
			showMessageModal('No se pudo eliminar el/la '+modulo_msg+'.', 'Atención!', 'bg-yellow', 'error');
		}
	});
}

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL DE EDIT EJECUTAR ACTION DEL BOTON*/
$("#modal"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#form"+form+" button[name=btnguardar"+modals_btns+"]").click();
    }
});

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL DE DELETE EJECUTAR ACTION DEL BOTON*/
$("#modalDelete"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#btnModalConfirmDelete"+modals_btns+"").click();
    }
});

//Agregar clase al modal de edit y Add para el tamaño
function configEditAddModal(class_modal){
	$("#modal_size_class").addClass(class_modal);
}

//Setear boton para agregar
function configSaveButtonAdd(){
	$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'agregar(true)');

	var titulo = 'Agregar '+modulo_msg;
	$(".titulo-modal-abm").html(titulo.toUpperCase());
}

/*ABRIR MODAL ELIMINAR*/
function abrirModalEliminar(id){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	$("#btnModalConfirmDelete"+modals_btns+"").attr('onclick', 'eliminar('+id+')');
}

/*ABRIR MODAL EDIT Y EJECUTAR FUNCION PARA TRAER DATOS*/
function prepareModalDataEdit(id){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');

	//Titulo modal
	var titulo = 'Editar '+modulo_msg;
	$(".titulo-modal-abm").html(titulo.toUpperCase());

	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+id+"/edit";
	$("#form"+form+"").attr('method', 'PUT');

	statusLoading('loading_modal', 1);

	getAndShowDataEdit(id, route);
}

//ABRIR MODAL DE OPCIONES Y CONFIGURAR ONCLICK DE BOTONES Y DATA-TARGET DE MODAL
function abrirModalOpciones(id_reg, modal_btns_name, edit, remove, add, unlock, lock, reserve){
	if (typeof(edit) == 'undefined'){
		edit = true;
	}
	if (typeof(remove) == 'undefined'){
		remove = true;
	}
	if (typeof(add) == 'undefined'){
		add = false;
	}
	if (typeof(unlock) == 'undefined'){
		unlock = false;
	}
	if (typeof(lock) == 'undefined'){
		lock = false;
	}
	if (typeof(reserve) == 'undefined'){
		reserve = false;
	}

    $("#executeOpcionesModal").click();

    if (edit){
	    $("#btn_modal_opc_edit_"+ modal_btns_name +"").attr('onclick', 'prepareModalDataEdit('+id_reg+');');
	    $("#btn_modal_opc_edit_"+ modal_btns_name +"").attr('data-target', '#modal'+modal_btns_name+'');
    }else{
    	 $("#btn_modal_opc_edit_"+ modal_btns_name +"").remove();
    }

    if (remove){
	    $("#btn_modal_opc_delete_"+ modal_btns_name +"").attr('onclick', 'abrirModalEliminar('+id_reg+');');
	    $("#btn_modal_opc_delete_"+ modal_btns_name +"").attr('data-target', '#modalDelete'+modal_btns_name+'');
    }else{
    	$("#btn_modal_opc_delete_"+ modal_btns_name +"").remove();
    }

    //Para Stock
    if (add){
	    $("#btn_modal_opc_add_"+ modal_btns_name +"").attr('onclick', 'abrirModalAumentarStock('+id_reg+');');
	    $("#btn_modal_opc_add_"+ modal_btns_name +"").attr('data-target', '#modalAumentarStock'+modal_btns_name+'');

    }else{
    	 $("#btn_modal_opc_add_"+ modal_btns_name +"").remove();
    }

    //Para Mesas
    //Cambiar estado a Libre
    if (unlock){
	    $("#btn_modal_opc_unlock_"+ modal_btns_name +"").attr('onclick', 'liberarMesa('+id_reg+')');
    }else{
    	$("#btn_modal_opc_unlock_"+ modal_btns_name +"").remove();
    	$("#modal_opciones_separar").remove();
    }

    //Cambiar estado a Ocupado
    if (lock){
	    $("#btn_modal_opc_lock_"+ modal_btns_name +"").attr('onclick', 'prepararModalOcuparMesa('+id_reg+');');
	    $("#btn_modal_opc_lock_"+ modal_btns_name +"").attr('data-target', '#modalOcuparMesa');
    }else{
    	$("#btn_modal_opc_lock_"+ modal_btns_name +"").remove();
    	$("#modal_opciones_separar").remove();
    }

    //Cambiar estado a Reservado
    if (lock){
	    $("#btn_modal_opc_reserve_"+ modal_btns_name +"").attr('onclick', 'reservarMesa('+id_reg+');');
    }else{
    	$("#btn_modal_opc_reserve_"+ modal_btns_name +"").remove();
    	$("#modal_opciones_separar").remove();
    }
}