$(document).ready(function(){
	activarMenu('compras');
	startDatePicker('.datetimepicker');
	startDatePicker('.datetimepickerFilter', null);

	listar();
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Compra';
var modals_btns = 'Purchase';
var form = 'Compra';
var module = 'compras';

var modulo_msg_nota_recepcion = 'NotaRecepcion';
var formNotaRecepcion = 'NotaRecepcion';
var modals_btns_nota_recepcion = 'NotaRecepcion';

var objetoNotaRecepcion = {};
var listaProductosAsignados = [];
var listaPesajesAsignados = [];

var objetoReciboCompra = {};
var listaChequesReciboAsignados = [];

localStorage.setItem('iva_compra', 0);
localStorage.setItem('total_purchase_cost', 0);
localStorage.setItem('total_cheques_recibo_compra', 0);

var data_user = null;

//Setear boton para agregar
function configSaveButtonNotaRecepcion(){
	$("#form"+formNotaRecepcion+" button[name=btnguardar"+modals_btns_nota_recepcion+"]").attr('onclick', 'agregarNotaRecepcionCompra(true)');
	$("#form"+formNotaRecepcion+" button[name=btnguardar"+modals_btns_nota_recepcion+"]").removeAttr('disabled');
	$("#form"+formNotaRecepcion+" button[name=btnImprimir"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');

	var titulo = 'Agregar '+modulo_msg;
	$(".titulo-modal-abm").html(titulo.toUpperCase());
}

//Inicializar formulario como esta por default donde corresponda
function restartConfigFormNotaRecepcion(){

	// Clear nota recepcion
	clearForm('form'+formNotaRecepcion);
	limpiarFormularioProductos();
	$("#form"+formNotaRecepcion+" button[name=btnAsignarProducto"+modals_btns_nota_recepcion+"]").removeAttr('disabled');
	$("#form"+formNotaRecepcion+" button[name=btnasignarPesaje"+modals_btns_nota_recepcion+"]").removeAttr('disabled');

	//INICIANDO FORMULARIO NOTA RECEPCION

	//Obteniendo id para nueva compra
	obtenerNuevoIdCompra();

	// Por defecto la nota de recepcion va en estado 4
	$("#form"+formNotaRecepcion+" select[name=id_status_shipment]").val(1).selectpicker('refresh');
	
	// inicia en fecha de hoy
	$("#form"+formNotaRecepcion+" input[name=purchase_date]").val(getTodayDateFormated(new Date()));
	
	// Limpio la lista de productos y pesajes
	listaProductosAsignados = [];
	listaPesajesAsignados = [];

	actualizarListaProductosAsignados();
	actualizarListaPesajesAsignados();
}

var displayContentDataNotaRecepcion = []; 
var displayTitleDataNotaRecepcion = [
	{ title: "Nro. Compra" },
	{ title: "Proveedor" },
	{ title: "Estado Compra" },
	{ title: "Fecha Compra" },
	{ title: "Total Costo Compra" },
	{ title: "Cant. Órdenes de Pago" },
	{ title: "Total Monto Órdenes de Pago" },
	{ title: "Total Adeudado Nota de Recepción" },
	{ title: "Opciones" },
];

var totalMontoGeneralListado = 0;
var totalCantidadProductoGeneralListado = 0;
var totalMontoAdeudadoGeneralListado = 0;

function listar(){
	displayContentDataNotaRecepcion = []; 
	totalMontoGeneralListado = 0;
	totalCantidadProductoGeneralListado = 0;
	totalMontoAdeudadoGeneralListado = 0;

    statusLoading('loading_list', 1, 'Actualizando Listado ..');
	var route = current_route+"_listar";
	var tabla = $("#table_"+modals_btns+"");

	var filtro_purchase_date_desde = $("input[name=filtro_purchase_date_desde]").val();
	var filtro_purchase_date_hasta = $("input[name=filtro_purchase_date_hasta]").val();
	var filtro_id_type_product = $("select[name=filtro_id_type_product]").val();
	var filtro_id_product = $("select[name=filtro_id_product]").val();

	$.get(route, function(result){
		data_user = result.data_user;
		$(result.data).each(function(key,value){
			var lst_ids_products = [];
			var lst_ids_type_products = [];
			var cant_producto_by_purchase = 0;

			// Agregar los id de productos y tipos de producto que tiene cada venta en una propiedad del objeto desde el otro array
			// Recorro la lista productos por compra
			$(result.data_productos_by_purchase).each(function(index, producto){

				//Si coinciden los id compra voy acumulando los ids productos y type productos
				if (producto.id_purchase == value.id) {
					lst_ids_products.push(producto.id_product);
					lst_ids_type_products.push(producto.id_type_product);
					cant_producto_by_purchase = cant_producto_by_purchase + producto.cantidad_producto;
				}
			});

			// Agrego los ids de typo products y id_products en el objeto
			value.ids_products = lst_ids_products;
			value.ids_type_products = lst_ids_type_products;
			value.cantidad_producto = cant_producto_by_purchase;

			// Agregar filtros, si cumple el filtro entonces se agrega al array y se muestra
			if ((filtro_purchase_date_desde == '' || filtro_purchase_date_desde <= formatDateTime(value.purchase_date, 'd-m-Y'))
				&& (filtro_purchase_date_hasta == '' || filtro_purchase_date_hasta >= formatDateTime(value.purchase_date, 'd-m-Y'))
				&& (filtro_id_type_product == '' || value.ids_type_products.find(p => filtro_id_type_product) == filtro_id_type_product)
				&& (filtro_id_product == '' || filtro_id_product == 0 || value.ids_products.find(p => filtro_id_product) == filtro_id_product))
				{
				applyDataInTable(key, value, result.data_user);
			}

		});
		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleDataNotaRecepcion, displayContentDataNotaRecepcion);
		statusLoading('loading_list', 0);

		$("#totalMonto" + modals_btns).text("$ " + totalMontoGeneralListado);
		$("#totalMontoAdeudado" + modals_btns).text("$ " + totalMontoAdeudadoGeneralListado);
		$("#totalCantidadProducto" + modals_btns).text(totalCantidadProductoGeneralListado);

	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

 function applyDataInTable(key, value, data_user){
	// Si el rol es ROOT agrego el boton eliminar
	var boton_eliminar_compra = '';
	if (data_user.type == 2) {
		boton_eliminar_compra = '<button type="button" href="#" title="Eliminar Compra" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
			+'<i class="material-icons">delete</i>'
		+'</button>';
	}

	var purchase_cost = (value.purchase_cost != null) ? value.purchase_cost : 0;
	var total_monto_orden_de_pago = (value.total_monto_orden_de_pago != null) ? value.total_monto_orden_de_pago : 0
	var purchase_total_deuda = ( value.purchase_total_deuda != null) ?  value.purchase_total_deuda : 0;

	totalMontoGeneralListado = totalMontoGeneralListado + parseFloat(value.purchase_cost != null ? value.purchase_cost: 0);
	totalMontoAdeudadoGeneralListado = (purchase_total_deuda >= 0) ? parseFloat(totalMontoAdeudadoGeneralListado) + parseFloat(purchase_total_deuda) : parseFloat(totalMontoAdeudadoGeneralListado);
	totalCantidadProductoGeneralListado = totalCantidadProductoGeneralListado + parseFloat(value.cantidad_producto != null ? value.cantidad_producto : 0);

	var colorEstadoCompra = value.desc_status_shipment == 'Cerrado' ? 'col-green' : ''
	var compraPagada = value.purchase_total_deuda == 0 ? 'disabled="true"' : '';

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentDataNotaRecepcion.push([ 
		value.id,
		value.person_lastname+' '+value.person_name,
		'<b class="'+colorEstadoCompra+'">' + value.desc_status_shipment + '</b>', 
		(value.purchase_date != null) ? formatDateTime(value.purchase_date, 'd-m-Y h:i', true) : '',
		(purchase_cost != null) ? '$ ' + purchase_cost : '-',
		(value.cant_recibos != null) ? value.cant_recibos : 0,
		(total_monto_orden_de_pago != null) ? '$ ' + total_monto_orden_de_pago : '$ 0',
		(purchase_total_deuda != null) ? '$ ' + (parseFloat(purchase_total_deuda)).toString() : '$ 0',
		'<div class="icon-button-demo" style="width:200px">'
			+'<button type="button" href="#" title="Cargar Orden de Pago" id="btnListadoCargarRecibo_'+value.id+'" '+compraPagada+' data-backdrop="static" data-keyboard="false" onclick="abrirModalCargarRecibo('+value.purchase_cost+', '+ purchase_total_deuda + ', \''+value.person_name + " " + value.person_lastname+' \', '+value.id+');" data-toggle="modal" data-target="#modalCargarRecibo'+modals_btns+'" class="btn bg-green btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">add</i>'
			+'</button>'
			+'<button type="button" href="#" title="Ver Ordenes de Pago" data-backdrop="static" data-keyboard="false" onclick="abrirModalVerRecibos('+value.id+', '+value.purchase_cost+', \''+value.person_name + " " + value.person_lastname+' \', '+ purchase_total_deuda + ');" data-toggle="modal" data-target="#modalVerRecibos'+modals_btns+'" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">receipt</i>'
			+'</button>'
			+'<button type="button" href="#" data-toggle="modal" title="Ver Compra" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">search</i>'
			+'</button>'
			+boton_eliminar_compra
		+'</div>'
	]);
}

/* METODOS DE NOTA RECEPCION */

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	objetoNotaRecepcion = {};
	localStorage.setItem('total_purchase_cost', 0);
	localStorage.setItem('iva_compra', 0);

	$("#btnTab"+modals_btns_nota_recepcion+"").click();
	$("#form"+formNotaRecepcion+" button[name=btnguardar"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');
	$("#form"+formNotaRecepcion+" button[name=btnAsignarProducto"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');
	$("#form"+formNotaRecepcion+" button[name=btnasignarPesaje"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');
	$("#form"+formNotaRecepcion+" button[name=btnImprimir"+modals_btns_nota_recepcion+"]").removeAttr('disabled');

	limpiarFormularioProductos();
	
	$.get(route, function(result){
		var data_compra =  result.data_compra[0];
		objetoNotaRecepcion = data_compra;
		
		objetoNotaRecepcion.person_desc = data_compra.person_lastname + ' ' + data_compra.person_name;
		objetoNotaRecepcion.person_cuit = (data_compra.person_cuit != null ? data_compra.person_cuit : '');
		objetoNotaRecepcion.person_domicilio = (data_compra.adress != null ? data_compra.adress : '');
		objetoNotaRecepcion.person_iva = (data_compra.condicion_iva_desc != null ? data_compra.condicion_iva_desc : '');
		objetoNotaRecepcion.productos = result.data_products_by_compra;
		objetoNotaRecepcion.pesajes = result.data_pesajes_by_compra;

		$("#form"+formNotaRecepcion+" [id=label_sale_id]").text(data_compra.id);
		$("#form"+formNotaRecepcion+" select[name=id_person]").val(data_compra.id_person).selectpicker('refresh');
		$("#form"+formNotaRecepcion+" select[name=id_status_shipment]").val(data_compra.id_status_shipment).selectpicker('refresh');
		$("#form"+formNotaRecepcion+" input[name=purchase_cost]").val(data_compra.purchase_cost);
		$("#form"+formNotaRecepcion+" input[name=alicuota_iva]").val(data_compra.alicuota_iva);
		$("#form"+formNotaRecepcion+" input[name=purchase_date]").val(formatDateTime(data_compra.purchase_date, 'd-m-Y h:i'));
		
		//Cargar productos a la lista para la tabla
		listaProductosAsignados = result.data_products_by_compra;
		actualizarListaProductosAsignados(false);

		//Cargar pesajes a la lista para la tabla
		listaPesajesAsignados = result.data_pesajes_by_compra;
		actualizarListaPesajesAsignados(false);

		calcularMontoTotalConIva(data_compra.alicuota_iva);

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+formNotaRecepcion);

		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}

//Asignar nuevo pesaje para compra
function asignarPesajeLista() {
	var bruto_chasis = $("#form"+formNotaRecepcion+" input[name=bruto_chasis]").val();
	var neto_chasis = $("#form"+formNotaRecepcion+" input[name=neto_chasis]").val();
	var tara_chasis = $("#form"+formNotaRecepcion+" input[name=tara_chasis]").val();
	var bruto_acoplado = $("#form"+formNotaRecepcion+" input[name=bruto_acoplado]").val();
	var neto_acoplado = $("#form"+formNotaRecepcion+" input[name=neto_acoplado]").val();
	var tara_acoplado = $("#form"+formNotaRecepcion+" input[name=tara_acoplado]").val();

	//Validcion general y cargamos el pesaje
	if (bruto_chasis != '' || neto_chasis != '' || bruto_acoplado != '' || neto_acoplado != '' ){
		var objPesaje = { 
			index: listaPesajesAsignados.length + 1,
			bruto_chasis: bruto_chasis,
			neto_chasis: neto_chasis,
			tara_chasis: tara_chasis,
			bruto_acoplado: bruto_acoplado,
			neto_acoplado: neto_acoplado,
			tara_acoplado: tara_acoplado,
		}

		listaPesajesAsignados.push(objPesaje);
		//Actualizamos la tabla
		actualizarListaPesajesAsignados();
		limpiarFormularioPesajes()

		showMessage('modal_messsage_pesajes_compras', 'bg-green','<strong> Bien ! </strong>Pesaje asignado con éxito', 'done');
	} else {
		showMessage('modal_messsage_pesajes_compras', 'bg-orange','<strong>Atención ! </strong>Debe completar alguno de los campos Bruto Chasis, Tara Chasis, Bruto Acoplado, Tara Acoplado para agregar un pesaje', 'error');
	}
}

//Actualizar la tabla de pesajes
function actualizarListaPesajesAsignados(activarBotonesEliminar = true) {
	var tabla = $("#tbody_pesajes_asignados");
	tabla.html('');
	var total_pesajes_neto = 0;
	var disabled = (!activarBotonesEliminar) ? 'disabled="disabled"' : '';

	$(listaPesajesAsignados).each(function(key,value){
			
		tabla.append('<tr>'
			            +'<td>'+value.bruto_chasis+'</td>'
			            +'<td>'+value.tara_chasis+'</td>'
			            +'<td><b>'+value.neto_chasis+'</b></td>'
						+'<td>'+value.bruto_acoplado+'</td>'
			            +'<td>'+value.tara_acoplado+'</td>'
						+'<td><b>'+value.neto_acoplado+'</b></td>'
						+'<td><b class="col-red">'+(parseFloat(value.neto_chasis) + parseFloat(value.neto_acoplado)).toFixed(2)+ ' Kgs' + '</b></td>'
			            +'<td>'
			               +'<button type="button" '+disabled+' onclick="quitarPesajeAsignado('+value.index+')" title="Quitar Pesaje" class="btn bg-red btn-circle waves-effect">'
			                    +'<i class="material-icons">delete</i>'
			                +'</button>'
			            +'</td>'
			        +'</tr>'
		);
		total_pesajes_neto = parseFloat(total_pesajes_neto) + (parseFloat(value.neto_chasis) + parseFloat(value.neto_acoplado));
	});

	total_pesajes_neto = (total_pesajes_neto > 0) ? total_pesajes_neto.toFixed(2) : 0;
	//Actualizamos total pesajes
	$("#form"+formNotaRecepcion+" [name=labelTotalPesajeNeto]").text(total_pesajes_neto);

}

//Quitar pesaje de la tabla de pesajes
function quitarPesajeAsignado(index) {
	var indiceElemento = listaPesajesAsignados.find(p => p.index == index);
	listaPesajesAsignados.splice(indiceElemento, 1);
	actualizarListaPesajesAsignados();
}

function limpiarFormularioPesajes () {
	$("#form"+formNotaRecepcion+" input[name=bruto_chasis]").val('');
	$("#form"+formNotaRecepcion+" input[name=neto_chasis]").val('');
	$("#form"+formNotaRecepcion+" input[name=tara_chasis]").val('');
	$("#form"+formNotaRecepcion+" input[name=bruto_acoplado]").val('');
	$("#form"+formNotaRecepcion+" input[name=neto_acoplado]").val('');
	$("#form"+formNotaRecepcion+" input[name=tara_acoplado]").val('');
}

function calcularNetoPesaje(tipoNeto) {
	
	if (tipoNeto == "chasis") {
		var bruto_chasis = parseFloat($("#form"+formNotaRecepcion+" input[name=bruto_chasis]").val());
		var tara_chasis = parseFloat($("#form"+formNotaRecepcion+" input[name=tara_chasis]").val());

		if (bruto_chasis != '' && tara_chasis != '') {
			if (bruto_chasis > tara_chasis) {
				$("#form"+formNotaRecepcion+" input[name=neto_chasis]").val(parseFloat(bruto_chasis) - parseFloat(tara_chasis));
			} else {
				showMessage('modal_messsage_pesajes_compras', 'bg-blue','<strong>Atención ! </strong>Recuede que el bruto chasis debe ser mayor que tara chasis', 'error');
				$("#form"+formNotaRecepcion+" input[name=tara_chasis]").val('');
				$("#form"+formNotaRecepcion+" input[name=neto_chasis]").val('');
			}
		} else {
			$("#form"+formNotaRecepcion+" input[name=neto_chasis]").val('');
		}
	} else {
		var bruto_acoplado = parseFloat($("#form"+formNotaRecepcion+" input[name=bruto_acoplado]").val());
		var tara_acoplado = parseFloat($("#form"+formNotaRecepcion+" input[name=tara_acoplado]").val());

		if (bruto_acoplado != '' && tara_acoplado != '') {
			if (bruto_acoplado > tara_acoplado) {
				$("#form"+formNotaRecepcion+" input[name=neto_acoplado]").val(parseFloat(bruto_acoplado) - parseFloat(tara_acoplado));
			} else {
				showMessage('modal_messsage_pesajes_compras', 'bg-blue','<strong>Atención ! </strong>Recuede que el bruto acoplado debe ser mayor que tara acoplado', 'error');
				$("#form"+formNotaRecepcion+" input[name=tara_acoplado]").val('');
				$("#form"+formNotaRecepcion+" input[name=neto_acoplado]").val('');
			}
		} else {
			$("#form"+formNotaRecepcion+" input[name=neto_acoplado]").val('');
		}
	}
}

//Asignar nuevo producto para compra
function asignarProductoLista() {
	var id_product = $("#form"+formNotaRecepcion+" select[name=id_product]").val();
	var nombre_producto_completo = $("#form"+formNotaRecepcion+" select[name=id_product] option:selected").text();
	var quantity = parseFloat($("#form"+formNotaRecepcion+" input[name=quantity]").val());
	var purchase_price = parseFloat($("#form"+formNotaRecepcion+" input[name=purchase_price]").val());

	var arr_nombre_producto = nombre_producto_completo.split('(');
	var nombre_producto = arr_nombre_producto[0];
	var min_purchase_price = $("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('min_purchase_price') != '' ? parseFloat($("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('min_purchase_price')) : 0;
	var max_purchase_price = $("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('max_purchase_price') != '' ? parseFloat($("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('max_purchase_price')) : 99999;
	var stock_disponible_producto = parseFloat($("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('stock'));
	var code_product = $("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('code_product');
	var type_metric_product = $("#form"+formNotaRecepcion+" select[name=id_product] option:selected").attr('type_metric_product');

	//Validcion general y cargamos el producto
	if (id_product != 0 && quantity != '' && purchase_price != ''){

		// Validacion minimo y maximo de precio habilitado para pagar
		// Si el rol es ROOT ignora la validacionde minimo y maximo
		if ((purchase_price >= min_purchase_price && purchase_price <= max_purchase_price) || data_user.type == 2) {
			var objProducto = { 
				index: listaProductosAsignados.length + 1,
				code_product: code_product,
				id_product: id_product,
				nombre_producto: nombre_producto,
				quantity: quantity,
				type_metric_product: type_metric_product,
				purchase_price: purchase_price,
				precio_total_producto: parseFloat(quantity) * parseFloat(purchase_price),
				stock_disponible_producto: stock_disponible_producto
			}

			listaProductosAsignados.push(objProducto);
			//Actualizamos la tabla
			actualizarListaProductosAsignados();
			limpiarFormularioProductos()

			showMessage('modal_messsage_productos_compras', 'bg-green','<strong> Bien ! </strong>Producto asignado a la compra con éxito', 'done');
		} else {
			showMessage('modal_messsage_productos_compras', 'bg-orange','<strong>Atención ! </strong>El precio ingresado no corresponde al rango mínimo('+min_purchase_price+') y máximo('+max_purchase_price+') de precio de compra habilitado para comprar', 'error');
		}
	} else {
		showMessage('modal_messsage_productos_compras', 'bg-orange','<strong>Atención ! </strong>Debe completar los campos Producto, Cantidad y Precio Compra para agregar un producto', 'error');
	}
}

//Actualizar la tabla de productos
function actualizarListaProductosAsignados(activarBotonesEliminar = true) {
	var tabla = $("#tbody_productos_asignados");
	tabla.html('');
	var precio_total_compra = 0;
	var total_cantidad_prod = 0;
	var disabled = (!activarBotonesEliminar) ? 'disabled="disabled"' : '';

	$(listaProductosAsignados).each(function(key,value){
			
		tabla.append('<tr>'
						+'<td>'+(value.code_product != null ? value.code_product : '')+'</td>'
			            +'<td>'+value.nombre_producto+'</td>'
						+'<td>'+value.quantity+'</td>'
						+'<td>'+value.type_metric_product+'</td>'
			            +'<td>$ '+value.purchase_price+'</td>'
			            +'<td><b class="col-red">$ '+value.precio_total_producto+'</b></td>'
			            +'<td>'
			               +'<button type="button" '+disabled+' onclick="quitarProductoAsignado('+value.index+')" title="Quitar Producto" class="btn bg-red btn-circle waves-effect">'
			                    +'<i class="material-icons">delete</i>'
			                +'</button>'
			            +'</td>'
			        +'</tr>'
		);
		precio_total_compra = parseFloat(precio_total_compra) + parseFloat(value.precio_total_producto);
		total_cantidad_prod = parseFloat(total_cantidad_prod) + parseFloat(value.quantity);
	});

	precio_total_compra = (precio_total_compra > 0) ? precio_total_compra.toFixed(2) : '';
	total_cantidad_prod = (total_cantidad_prod > 0) ? total_cantidad_prod.toFixed(2) : 0;

	//Actualizamos valor total de la compra
	$("#form"+formNotaRecepcion+" input[name=purchase_cost]").val(precio_total_compra);
	localStorage.setItem('total_purchase_cost', precio_total_compra);

	//Actualizamos cantidad total de producto
	$("#form"+formNotaRecepcion+" [name=labelTotalCantProd]").text(total_cantidad_prod);

}

//Quitar producto de la tabla de productos
function quitarProductoAsignado(index) {
	var indiceElemento = listaProductosAsignados.find(p => p.index == index);
	listaProductosAsignados.splice(indiceElemento, 1);
	actualizarListaProductosAsignados();
}

//Agregar Compra
function agregarNotaRecepcionCompra(validar){
	objetoNotaRecepcion = {};
	localStorage.setItem('total_purchase_cost', 0);
	localStorage.setItem('iva_compra', 0);
	var total_cantidad_producto = parseFloat($("#form"+formNotaRecepcion+" [name=labelTotalCantProd]").text());
	var total_pesaje_neto = parseFloat($("#form"+formNotaRecepcion+" [name=labelTotalPesajeNeto]").text());
	
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+formNotaRecepcion);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}

	// Validacion de totales de pesajes con totales de productos 
	// Para usuario tipo root ignora la validacion 
	data_user.type = 1;
	if (total_cantidad_producto != total_pesaje_neto && data_user.type != 2){
		showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>La cantidad de productos total debe ser igual a la cantidad total de los pesajes asignados</label> ', 'error');	
		return false;
	}

	var route = current_route+"";
	var datos = $("#form"+formNotaRecepcion+"").serialize();
	datos += '&productos=' +  JSON.stringify(listaProductosAsignados);
	datos += '&pesajes=' +  JSON.stringify(listaPesajesAsignados);
	datos += '&purchase_cost=' +  $("#form"+formNotaRecepcion+" input[name=purchase_cost]").val();
	datos += '&id_status_shipment=' +  1; // Siempre Se crea como activo, se agrega aca porq esta disabled

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:datos,
		beforeSend: function () {
			statusLoading('loading_modal', 1, 'Agregando '+modulo_msg_nota_recepcion+' ..');
		},
		success: function(result){
			showMessage('modal_mensaje', 'bg-green', '<strong>Buen Trabajo!</strong> Compra agregada con éxito. Y stock de productos incrementados con éxito ', 'done');
			actualizarListaProductosAsignados(false);
			$("#form"+formNotaRecepcion+" button[name=btnAsignarProducto"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');
			$("#form"+formNotaRecepcion+" button[name=btnguardar"+modals_btns_nota_recepcion+"]").attr('disabled', 'disabled');
			$("#form"+formNotaRecepcion+" button[name=btnImprimir"+modals_btns_nota_recepcion+"]").removeAttr('disabled');
			
			listar();
			statusLoading('loading_modal', 0);

			var data_compra =  result.data_compra[0];
			objetoNotaRecepcion = data_compra;
			
			objetoNotaRecepcion.person_desc = data_compra.person_lastname + ' ' + data_compra.person_name;
			objetoNotaRecepcion.person_cuit = (data_compra.person_cuit != null ? data_compra.person_cuit : '');
			objetoNotaRecepcion.person_domicilio = (data_compra.adress != null ? data_compra.adress : '');
			objetoNotaRecepcion.person_iva = (data_compra.condicion_iva_desc != null ? data_compra.condicion_iva_desc : '');
			objetoNotaRecepcion.productos = listaProductosAsignados;
			objetoNotaRecepcion.pesajes = listaPesajesAsignados;

			// Abrir modal de ordenes de pago
			var total_amount = parseFloat($("#form"+formNotaRecepcion+" input[name=purchase_cost]").val());
			var nombre_persona = $("#form"+formNotaRecepcion+" select[name=id_person] option:selected").text();

			$("#btnModalConfirmAddReciboAfterAlta").attr('onclick', 'abrirCargarRecibosYAfterAlta(' + total_amount + ', ' + total_amount + ', \'' + nombre_persona + ' \'' + ', ' + result.id_compra+')');
			$("#btnOpenModalConfirmAddReciboAfterAlta").click();
		},
		error: function(result){
			showMessage('modal_mensaje', 'bg-red', '<strong>Atención!</strong> No se pudo Agregar el ' + modulo_msg_nota_recepcion + '. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'warning');
			statusLoading('loading_modal', 0);
		}
	});
}

function obtenerNuevoIdCompra() {
	var route = current_route+"_getNewPurchaseId";

	$.get(route, function(result){
		$("#form"+formNotaRecepcion+" [id=label_purchase_id]").text(result.id_new_purchase);
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
	});
}

// function updateNotaRecepcionCompra(id, validar) {
// 	$("#modal_mensaje").addClass('hidden');

// 	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
// 	if (validar == true){
// 		var array_validate = checkFormShowingMsg("form"+formNotaRecepcion);
// 		var msg = '';
// 		if (array_validate['puedeGuardar'] == false){
// 			msg = array_validate['msg'];
// 			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
// 			return false;
// 		}
// 	}

// 	var route = current_route+"/" + id;
// 	var datos = $("#form"+formNotaRecepcion+"").serialize();
// 	datos += '&sell_cost=' +  $("#form"+formNotaRecepcion+" input[name=sell_cost]").val();

// 	$.ajax({
// 		url: route,
// 		headers: {  'X-CSRF-TOKEN': token},
// 		type: 'PUT',
// 		dataType: 'json',
// 		data:datos,
// 		beforeSend: function () {
// 			statusLoading('loading_modal', 1, 'Modificando '+modulo_msg_nota_recepcion+' ..');
// 		},
// 		success: function(result){
// 			showMessage('modal_mensaje', 'bg-green', '<strong>Buen Trabajo!</strong> ' + modulo_msg_nota_recepcion + ' Modificado con Éxito ', 'done');
// 			listar();
// 			statusLoading('loading_modal', 0);
// 		},
// 		error: function(result){
// 			showMessage('modal_mensaje', 'bg-red', '<strong>Atención!</strong> No se pudo Modificar el ' + modulo_msg_nota_recepcion + '. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'warning');
// 			statusLoading('loading_modal', 0);
// 		}
// 	});
// }

function showDataProduct(formId, nameSelect, valueSelected) {
	var stock_disponible_producto = '';
	var last_purchase_price = '';
	var type_metric_product = '';
	var min_purchase_price = '';
	var max_purchase_price = '';
	
	if (valueSelected != 0) {
		stock_disponible_producto = parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('stock'));
		last_purchase_price = $("#"+formId+" select[name="+nameSelect+"] option:selected").attr('last_purchase_price') != '' ? parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('last_purchase_price')) : '-';
		min_purchase_price = $("#"+formId+" select[name="+nameSelect+"] option:selected").attr('min_purchase_price') != '' ? parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('min_purchase_price')) : '-';
		max_purchase_price = $("#"+formId+" select[name="+nameSelect+"] option:selected").attr('max_purchase_price') != '' ? parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('max_purchase_price')) : '-';
		type_metric_product = $("#"+formId+" select[name="+nameSelect+"] option:selected").attr('type_metric_product');
		
		$("#"+formId+" label[name=labelStockForProduct]").html(stock_disponible_producto + ' ' + type_metric_product);
		$("#"+formId+" label[name=labelLastPricePurchaseForProduct]").html("$ " +last_purchase_price);
		$("#"+formId+" label[name=labelMinPricePurchaseForProduct]").html("$ " +min_purchase_price);
		$("#"+formId+" label[name=labelMaxPricePurchaseForProduct]").html("$ " +max_purchase_price);
		$("#"+formId+" label[name=labelTypeMetricForProduct]").html(type_metric_product);

		$("#"+formId+" h6[name=datosProductoSelected]").removeClass('hidden');
	} else {
		$("#"+formId+" h6[name=datosProductoSelected]").addClass('hidden');
	}
}

function limpiarFormularioProductos () {
	$("#form"+formNotaRecepcion+" select[name=id_type_product]").val(0).selectpicker('refresh');
	changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Producto', 'form'+formNotaRecepcion, 0);
	$("#form"+formNotaRecepcion+" input[name=quantity]").val('');
	$("#form"+formNotaRecepcion+" input[name=purchase_price]").val('');
	showDataProduct("form"+formNotaRecepcion, 'id_product', 0);
}

function calcularMontoTotalConIva(valueIva) {
	var total_amount = parseFloat(localStorage.getItem('total_purchase_cost'));

	var iva = (parseFloat(total_amount) * parseFloat(valueIva)) / 100;
	var new_total_amount = parseFloat(total_amount) + parseFloat(iva);
	
	if (!isNaN(new_total_amount)){
		$("#form"+formNotaRecepcion+" input[name=purchase_cost]").val(parseFloat(new_total_amount).toFixed(2));
	} else {
		$("#form"+formNotaRecepcion+" input[name=purchase_cost]").val(parseFloat(total_amount).toFixed(2));
	}
}

// ========================================
// ========================================
// ========================================
// ========================================
// ========================================
// ========================================
// ========================================

// RECIBOS
function abrirModalCargarRecibo(monto_compra, purchase_total_deuda, nombre_persona, id_purchase = null) {
	var route = root_project+"compras_getPurchasesData";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'GET',
		dataType: 'json',
        beforeSend: function () {
         	statusLoading('loading_modal_cargar_recibo', 1, 'Cargando Operación..');
        },
		success: function(result){
			$("#CargarRecibo_id_purchase").html('<option value="0">Seleccione una Compra</option>');
			$(result.compras).each(function(key, compra){
				$("#CargarRecibo_id_purchase").append('<option monto_compra="'+compra.purchase_cost+'" monto_compra_deuda="'+compra.purchase_total_deuda+'" value="'+compra.id+'">Compra N: '+compra.id+' - '+compra.person_lastname+' '+compra.person_name+' - Monto: $ '+compra.purchase_cost+' - '+compra.purchase_date+' '+(compra.cuit != null ? compra.cuit : '')+'</option>');
			});
			$("#CargarRecibo_id_purchase").selectpicker('refresh');

			clearForm("formCargarReciboYMovimiento");
			listaChequesReciboAsignados = [];
			actualizarListaChequesRecibosAsignados();

			$("#label_persona_cargar_recibo"+modals_btns+"").text('');
			$("#label_total_monto_compra"+modals_btns+"").text('');
			$("#label_monto_adeudado_ordenes"+modals_btns+"").text('');

			if (id_purchase == undefined) {
				$("#CargarRecibo_id_purchase").removeAttr('disabled');
				$("#CargarRecibo_id_purchase").val(0).selectpicker('refresh');
			} else {
				$("#CargarRecibo_id_purchase").val(id_purchase).selectpicker('refresh');
				$("#CargarRecibo_id_purchase").attr('disabled', true).selectpicker('refresh');

				// Seteo el formulario para insercion de recibo y movimiento
				$("#formCargarReciboYMovimiento input[name=id_purchase]").val(id_purchase);
				$("#formCargarReciboYMovimiento input[name=monto_compra]").val(monto_compra);

				// Cargamos data compra:
				$("#label_persona_cargar_recibo"+modals_btns+"").text(nombre_persona);
				$("#label_total_monto_compra"+modals_btns+"").text(monto_compra);
				$("#label_monto_adeudado_ordenes"+modals_btns+"").text(purchase_total_deuda);

				coloresMontoYMontoDeudaCargarRecibo(monto_compra, purchase_total_deuda);

			}
			statusLoading('loading_modal_cargar_recibo', 0);
		},
		error: function(){
			showMessageModal('Error al obtener información de compras', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_cargar_recibo', 0);
		}
	});
	
	
}

// INSERTAR RECIBO
function insertReciboByPurchase(validar) {
	var route = root_project+"recibos_compras";
	var datos = $("#formCargarReciboYMovimiento").serialize();
	datos += '&cheques=' +  JSON.stringify(listaChequesReciboAsignados);

	if (!verificarTotalDeReciboPurchaseConMontoTotal()) {
		return false;
	}

	if ($("#CargarRecibo_id_purchase").val() == 0){
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>Debe seleccionar una compra para cargar una orden de pago</label> ', 'error');	
		return false;
	}
	
	if (parseFloat($("#formCargarReciboYMovimiento input[name=total_amount]").val()) == 0.00) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>El total de la orden de pago debe ser mayor a 0</label> ', 'error');	
		return false;
	}
	
	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal_cargar_recibo', 1, 'Cargando Operación..');
        },
		success: function(result){
			showMessageModal('Orden de Pago de ' + modulo_msg+' cargado con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');

			// Actualizar input de balance si todo sale ok
			var total_amount = $("#formCargarReciboYMovimiento input[name=total_amount]").val();
			$("#formCargarReciboYMovimiento input[name=balance]").val(parseFloat($("#formCargarReciboYMovimiento input[name=balance]").val()) - parseFloat(total_amount));

			statusLoading('loading_modal_cargar_recibo', 0);
			clearForm("formCargarReciboYMovimiento");
			$('#modalCargarRecibo'+modals_btns+'').modal('hide');
			listar();
		},
		error: function(){
			showMessageModal('No se pudo Generar la operación <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_cargar_recibo', 0);
		}
	});
}

function verificarTotalDeReciboPurchaseConMontoTotal() {
	var monto_adeudado_compra = parseFloat($("#label_monto_adeudado_ordenes"+modals_btns+"").text());
	var total_amount = parseFloat($("#formCargarReciboYMovimiento input[name=total_amount]").val());

	if (total_amount > monto_adeudado_compra) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>El total de orden de pago ingresado supera el monto total de la Compra</label> ', 'error');	
		return false;
	}

	if (total_amount == 0 && monto_adeudado_compra == 0) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>La Compra esta pagada por completo</label> ', 'error');	
		return false;
	}

	return true;
}

// CHEQUES
//Asignar nuevo cheque para recibo
function asignarChequeRecibosLista() {
	var nro_cheque = $("#formCargarReciboYMovimiento input[name=nro_cheque]").val();
	var banco = $("#formCargarReciboYMovimiento input[name=banco]").val();
	var concepto = $("#formCargarReciboYMovimiento input[name=concepto]").val();
	var nombre = $("#formCargarReciboYMovimiento input[name=nombre]").val();
	var cuit = $("#formCargarReciboYMovimiento input[name=cuit]").val();
	var importe = parseFloat($("#formCargarReciboYMovimiento input[name=importe]").val());
	var fecha_emision = $("#formCargarReciboYMovimiento input[name=fecha_emision]").val();
	var fecha_pago = $("#formCargarReciboYMovimiento input[name=fecha_pago]").val();

	//Validcion general y cargamos el cheque
	if (nro_cheque != '' && banco != '' && nombre != '' && cuit != '' && importe != ''){
		var objCheque = {
			id_cheque: listaChequesReciboAsignados.length + 1,
			nro_cheque: nro_cheque,
			banco: banco,
			concepto: concepto,
			nombre: nombre,
			cuit: cuit,
			importe: parseFloat(importe).toFixed(2),
			fecha_emision: fecha_emision,
			fecha_pago: fecha_pago,
		}

		listaChequesReciboAsignados.push(objCheque);
		//Actualizamos la tabla
		actualizarListaChequesRecibosAsignados();
		limpiarFormularioChequesRecibo()

		showMessage('modal_messsage_cheques_recibo', 'bg-green','<strong> Bien ! </strong>Cheque asignado con éxito', 'done');
		
	} else {
		showMessage('modal_messsage_cheques_recibo', 'bg-orange','<strong>Atención ! </strong>Debe completar los campos Nro Cheque, Banco, Nombre, Cuit e Importe para agregar un cheque', 'error');
	}
}

//Actualizar la tabla de cheques
function actualizarListaChequesRecibosAsignados(activarBotonesEliminar = true) {
	var tabla = $("#tbody_chequesrecibo_asignados");
	tabla.html('');
	var importe_total_cheques = 0;
	var disabled = (!activarBotonesEliminar) ? 'disabled="disabled"' : '';
	
	$(listaChequesReciboAsignados).each(function(key,value){
		tabla.append('<tr>'
						+'<td>'+value.nro_cheque+'</td>'
						+'<td>'+value.banco+'</td>'
						+'<td><b class="col-red">$ '+value.importe+'</b></td>'
						+'<td>'+value.fecha_emision+'</td>'
						+'<td>'+value.concepto+'</td>'
						+'<td>'+value.nombre+'</td>'
						+'<td>'+value.cuit+'</td>'
						+'<td>'+value.fecha_pago+'</td>'
			            +'<td>'
			               +'<button type="button" '+disabled+' onclick="quitarChequeReciboAsignado('+value.id_cheque+')" title="Eliminar Cheque" class="btn bg-red btn-circle waves-effect">'
			                    +'<i class="material-icons">delete</i>'
			                +'</button>'
			            +'</td>'
			        +'</tr>'
		);
		importe_total_cheques = parseFloat(importe_total_cheques) + parseFloat(value.importe);
	});

	importe_total_cheques = (importe_total_cheques > 0) ? importe_total_cheques.toFixed(2) : 0;

	//Actualizamos valor total del los cheques
	localStorage.setItem('total_cheques_recibo', importe_total_cheques);
	calcularTotalReciboFinal();
}

//Quitar cheque de la tabla de cheques
function quitarChequeReciboAsignado(id_cheque) {
	var indiceElemento = listaChequesReciboAsignados.find(p => p.id_cheque == id_cheque);
	listaChequesReciboAsignados.splice(indiceElemento, 1);
	actualizarListaChequesRecibosAsignados();
}

function limpiarFormularioChequesRecibo() {
 	$("#formCargarReciboYMovimiento input[name=nro_cheque]").val('');
	$("#formCargarReciboYMovimiento input[name=banco]").val('');
	$("#formCargarReciboYMovimiento input[name=concepto]").val('');
	$("#formCargarReciboYMovimiento input[name=nombre]").val('');
	$("#formCargarReciboYMovimiento input[name=cuit]").val('');
	$("#formCargarReciboYMovimiento input[name=importe]").val('');
	$("#formCargarReciboYMovimiento input[name=fecha_emision]").val('');
	$("#formCargarReciboYMovimiento input[name=fecha_pago]").val('');
}

function calcularTotalReciboFinal() {
	var effective_amount = parseFloat($("#formCargarReciboYMovimiento input[name=effective_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=effective_amount]").val() : 0);
	var card_amount = parseFloat($("#formCargarReciboYMovimiento input[name=card_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=card_amount]").val() : 0);
	var retencion_amount = parseFloat($("#formCargarReciboYMovimiento input[name=retencion_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=retencion_amount]").val() : 0);
	var retencion2_amount = parseFloat($("#formCargarReciboYMovimiento input[name=retencion2_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=retencion2_amount]").val() : 0);
	
	var total_recibo_final = effective_amount + card_amount + parseFloat(localStorage.getItem('total_cheques_recibo')) - retencion_amount - retencion2_amount;

	if (!isNaN(total_recibo_final)){
		$("#formCargarReciboYMovimiento input[name=total_amount]").val(parseFloat(total_recibo_final).toFixed(2));
	}
}

// Modal Ver Recibos
function abrirModalVerRecibos(id_purchase, monto_compra, nombre_persona, monto_adeudado_compra) {
	actualizarTablaVerRecibos(id_purchase);

	// Cargamos data:
	$("#label_persona_ver_recibos"+modals_btns+"").text(nombre_persona);
	$("#label_compra_ver_recibos"+modals_btns+"").text(id_purchase);
	$("#label_total_monto_compra_ver_recibos"+modals_btns+"").text(monto_compra);
	$("#label_monto_adeudado_ver_recibos"+modals_btns+"").text(monto_adeudado_compra);

	// Color monto_compra
	if (parseFloat(monto_compra) <= 0){
		$("#label_total_monto_compra_ver_recibos"+modals_btns+"").removeClass('label-warning');
		$("#label_total_monto_compra_ver_recibos"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_total_monto_compra_ver_recibos"+modals_btns+"").removeClass('label-success');
		$("#label_total_monto_compra_ver_recibos"+modals_btns+"").addClass('label-warning');
	}

	// Color monto_adeudado
	if (parseFloat(monto_adeudado_compra) <= 0){
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").removeClass('label-danger');
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").removeClass('label-success');
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").addClass('label-danger');
	}
}

// Mostra recibos en talba
function actualizarTablaVerRecibos(id_purchase) {
	statusLoading('loading_modal_ver_recibo', 1, 'Actualizando Listado ..');
	
	var route = root_project+"recibos_compras/GetRecibosByPurchase/"+id_purchase;
	var tabla = $("#table_VerRecibos"+modals_btns+"");
	tabla.empty();

	var displayContentData = []; 
	var displayTitleData = [
		{ title: "N° Orden" },
		{ title: "Fecha/Hora Emisión" },
		{ title: "N° Factura" },
		{ title: "Monto Efectivo" },
		{ title: "Cheques" },
		{ title: "Retención" },
		{ title: "Retención 2" },
		{ title: "Total Orden de Pago" },
		{ title: "Ver Orden de Pago" },
	];
	
	$.get(route, function(result){
		$(result).each(function(key,value){
			//carga del array contenido desde el array de la respuesta, el orden va de la mano de el orden de los titulos
			displayContentData.push([ 
				value.id,
				(value.created_at != null) ? formatDateTime(value.created_at, 'd-m-Y h:i') : '',
				(value.nro_invoice != null) ? value.nro_invoice : '',
				(value.effective_amount != null) ? value.effective_amount : 0,
				(value.cant_cheques != null) ? value.cant_cheques : 0,
				(value.retencion_amount != null) ? value.retencion_amount : 0,
				(value.retencion2_amount != null) ? value.retencion2_amount : 0,
				(value.total_amount != null) ? '<b class="col-red">$ ' + value.total_amount + '</b>' : 0,
				'<div class="icon-button-demo">'
					+'<button type="button" href="#" data-toggle="modal" title="Ver Recibo" data-backdrop="static" data-keyboard="false" onclick="verReciboByIdRecibo('+value.id+');" data-target="#modalVerReciboById'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
						+'<i class="material-icons">search</i>'
					+'</button>'
				+'</div>'
			]);
		});		

		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_modal_ver_recibo', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_ver_recibo', 0);
	});
}

function verReciboByIdRecibo(id_receipt) {
	statusLoading('loading_modal_ver_recibo_imprimir', 1, 'Actualizando Datos ..');
	limpiarFormVerReciboImprimir();

	var route = root_project+"recibos_compras/GetReciboByIdRecibo/"+id_receipt;
	var tabla = $("#tbody_chequesrecibo_ver_recibo_imprimir");
	tabla.empty();

	var displayContentData = []; 
	var displayTitleData = [
		{ title: "Nro Cheque" },
		{ title: "Banco" },
		{ title: "Importe" },
		{ title: "F. Emisión" },
		{ title: "Concepto" },
		{ title: "Nombre" },
		{ title: "Cuit" },
		{ title: "F. Pago" },
	];

	$.get(route, function(result){
		var recibo = result.data_recibo[0];

		// Guardamos datos de orden de pago en objeto
		objetoReciboCompra = recibo;
		objetoReciboCompra.cheques = result.data_cheques;
		objetoReciboCompra.data_purchase = result.data_purchase_receipt[0];

		// Cargamos orden de pago
		$("#label_numero_recibo_ver_recibo_imprimir"+modals_btns+"").text('Nº ' + recibo.id);
		$("#LabelFormVerReciboImprimir [name=label_numero_recibo]").text(recibo.id);
		$("#LabelFormVerReciboImprimir [name=label_numero_compra]").text(recibo.id_purchase);
		$("#LabelFormVerReciboImprimir [name=label_effective_amount]").text(recibo.effective_amount);
		$("#LabelFormVerReciboImprimir [name=label_card_amount]").text(recibo.card_amount);
		$("#LabelFormVerReciboImprimir [name=label_retencion_amount]").text(recibo.retencion_amount);
		$("#LabelFormVerReciboImprimir [name=label_retencion2_amount]").text(recibo.retencion2_amount);
		$("#LabelFormVerReciboImprimir [name=label_fecha]").text(formatDateTime(recibo.created_at, 'd-m-Y h:i', true));
		$("#LabelFormVerReciboImprimir [name=label_nro_invoice]").text(recibo.nro_invoice != null ? recibo.nro_invoice : '');
		$("#LabelFormVerReciboImprimir [name=label_total_amount]").text('$ ' + recibo.total_amount);

		// Cargamos cheques
		$(result.data_cheques).each(function(key,value){
			displayContentData.push([ 
				value.nro_cheque,
				(value.banco != null) ? value.banco : '',
				(value.importe != null) ? '$ ' + value.importe : 0,
				(value.fecha_emision != null) ? formatDateTime(value.fecha_emision, 'd-m-Y', true) : '',
				(value.concepto != null) ? value.concepto : 0,
				(value.nombre != null) ? value.nombre : 0,
				(value.cuit != null) ? value.cuit : 0,
				(value.fecha_pago != null) ? formatDateTime(value.fecha_pago, 'd-m-Y', true) : '',
			]);
		});		


		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_modal_ver_recibo_imprimir', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_ver_recibo_imprimir', 0);
	});
}

function limpiarFormVerReciboImprimir() {
	$("#label_numero_recibo_ver_recibo_imprimir"+modals_btns+"").text('');
	$("#LabelFormVerReciboImprimir [name=label_numero_recibo]").text('');
	$("#LabelFormVerReciboImprimir [name=label_numero_compra]").text('');
	$("#LabelFormVerReciboImprimir [name=label_effective_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_card_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_retencion_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_retencion2_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_fecha]").text('');
	$("#LabelFormVerReciboImprimir [name=label_total_amount]").text('');
}

function changeSelectCompraIdPurchaseCofing(valuePurchaseSelected) {
	$("#formCargarReciboYMovimiento input[name=id_purchase]").val(valuePurchaseSelected);
	var monto_compra = $("#CargarRecibo_id_purchase option:selected").attr('monto_compra');
	var total_amount_deuda = $("#CargarRecibo_id_purchase option:selected").attr('monto_compra_deuda');
	$("#label_total_monto_compra"+modals_btns+"").text(parseFloat(monto_compra))
	$("#label_monto_adeudado_ordenes"+modals_btns+"").text(parseFloat(total_amount_deuda))

	$("#formCargarReciboYMovimiento input[name=monto_compra]").val(monto_compra);

	coloresMontoYMontoDeudaCargarRecibo(monto_compra, total_amount_deuda);
}

function coloresMontoYMontoDeudaCargarRecibo(monto_compra, monto_compra_deuda) {
	// Color monto_compra cuenta
	if (parseFloat(monto_compra) <= 0){
		$("#label_total_monto_compra"+modals_btns+"").removeClass('label-warning');
		$("#label_total_monto_compra"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_total_monto_compra"+modals_btns+"").removeClass('label-success');
		$("#label_total_monto_compra"+modals_btns+"").addClass('label-warning');
	}

	// Color monto_adeudado
	if (parseFloat(monto_compra_deuda) <= 0){
		$("#label_monto_adeudado_ordenes"+modals_btns+"").removeClass('label-danger');
		$("#label_monto_adeudado_ordenes"+modals_btns+"").addClass('label-success');
		$("#btnCargarReciboOrden"+modals_btns+"").attr('disabled', 'disabled');
	} else {
		$("#label_monto_adeudado_ordenes"+modals_btns+"").removeClass('label-success');
		$("#label_monto_adeudado_ordenes"+modals_btns+"").addClass('label-danger');
		$("#btnCargarReciboOrden"+modals_btns+"").removeAttr('disabled');
	}
}

function closeModalConfirmAddReciboAfterAlta() {
	$("#modalConfirmAddReciboAfterAlta").modal("hide");
}

function abrirCargarRecibosYAfterAlta(total_amount, purchase_total_deuda, nombre_persona, id_compra) {
	$('#modal' + modals_btns).modal('hide');
	$("#btnModalCargarOrdenDePago" + modals_btns).click();
	abrirModalCargarRecibo(total_amount, purchase_total_deuda, nombre_persona, id_compra);
	closeModalConfirmAddReciboAfterAlta();
}