// La variable objetoNotaRecepcion la es creada en el archivo compras.js
// La recibo porque importo los 2 archivos en un mismo html. compras_print.js debe ser importado luego de compras.js

function imprimirNotaRecepcion(){
    var doc = new jsPDF();
    var altura = 0;
    var altura_post_tabla = 0;
    
    // Si tiene menos de 4 productos cambiamos altura para que imprima el duplicado sino imprime lo mismo y la FIRMA al FINAL
    if (objetoNotaRecepcion.productos.length <= 4) {

        doc = dibujarNotaRecepcionPDF(doc, altura, altura_post_tabla, objetoNotaRecepcion)

        altura = 144;
        altura_post_tabla = altura;

        // Linea divisora
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(5, 147.5, '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _');
        doc = dibujarNotaRecepcionPDF(doc, altura, altura_post_tabla, objetoNotaRecepcion)
    } else {
        altura = 0;
        altura_post_tabla = 144;
        doc = dibujarNotaRecepcionPDF(doc, altura, altura_post_tabla, objetoNotaRecepcion)
    }

    doc.save('RemitoCompra_' + objetoNotaRecepcion.id + '_' + objetoNotaRecepcion.created_at + '.pdf');
    window.open(doc.output('bloburl'), '_blank');
}

function dibujarNotaRecepcionPDF(doc, altura, altura_post_tabla, objetoNotaRecepcion) {
    const header = function (data) {

        // PRIMER CUADRANTE

        // Rectangulo primero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 10 + altura, 181.6, 44);
    
        // Vertical linea central
        doc.setLineWidth(0.1);
        doc.line(107, 25 + altura, 107, 54 + altura); 

        doc.setFontSize(12);
        doc.setFontStyle('bold');
        doc.text(22, 23 + altura, (objetoNotaRecepcion.person_desc != null ? objetoNotaRecepcion.person_desc : ''));

        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(22, 35 + altura, (objetoNotaRecepcion.person_domicilio != null ? objetoNotaRecepcion.person_domicilio : ''));
        
        // Horizontal linea central
        doc.line(14.1, 49 + altura, 107, 49 + altura); 

        // Iva Vendedor
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(22, 53 + altura, (objetoNotaRecepcion.person_iva != null ? objetoNotaRecepcion.person_iva : ''));

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(124, 15 + altura, 'Documento No Válido Como Factura');

        // Rectangulo de fecha
        doc.setDrawColor(0,0,0);
        doc.rect(160, 27 + altura, 30, 10);
        // Linea fecha 1
        doc.setLineWidth(0.1);
        doc.line(170, 37 + altura, 170, 27 + altura);
        // Linea fecha 2
        doc.setLineWidth(0.1);
        doc.line(180, 37 + altura, 180, 27 + altura);
        // Linea hoizontal fecha 
        doc.line(160, 30 + altura, 190, 30 + altura); 
        // Texto Fecha
        doc.setFontSize(7.5);
        doc.setFontStyle('normal');
        doc.text(163, 29.5 + altura, 'Dia       Mes       Año');
        // Fecha ISSUE DATE
        doc.setFontSize(11);
        doc.setFontStyle('bold');
        var fecha_compra = formatDateTime(objetoNotaRecepcion.purchase_date, 'd-m-Y').split('/');
        doc.text(163, 34.9 + altura, (objetoNotaRecepcion.purchase_date != undefined ? fecha_compra[0] != 'undefined' ? fecha_compra[0] : fecha_compra[2] : ''));
        doc.text(172.5, 34.9 + altura, (objetoNotaRecepcion.purchase_date != undefined ? fecha_compra[0] != 'undefined' ? fecha_compra[1] : fecha_compra[3] : ''));
        doc.text(180.8, 34.9 + altura, (objetoNotaRecepcion.purchase_date != undefined ? fecha_compra[0] != 'undefined' ? fecha_compra[2] : fecha_compra[4] : ''));

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(122, 23 + altura, 'NOTA DE RECEPCIÓN');
        
        doc.setFontSize(16);
        doc.setFontStyle('normal');
        doc.text(122, 34 + altura, 'N°. ' + objetoNotaRecepcion.id);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 43 + altura, 'CUIT N°.: ' + (objetoNotaRecepcion.person_cuit != null ? objetoNotaRecepcion.person_cuit : ''));

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 48 + altura, 'ING. BRUTOS: ');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 52.8 + altura, 'INICIO DE ACTIVIDADES: ');

        // Rectangulo central R
        doc.setDrawColor(0,0,0);
        doc.rect(101.5, 10 + altura, 11, 11);

        doc.setFontSize(28);
        doc.setFontStyle('bold');
        doc.text(103.4, 19 + altura, 'R');

        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(102, 24 + altura, 'Cod. 91');

        // SEGUNDO CUADRANTE

        // Rectangulo segundo grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 55 + altura, 181.7, 14);

        // PERSONA CLIENTE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 61 + altura, 'RAZÓN SOCIAL: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoNotaRecepcion.person_desc != null ? 'bold' : 'normal');
        doc.text(45, 61 + altura, 'Cooperativa de Trabajo Barranca Yaco Lim        102287')

        // DOMICILIO
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 67.5 + altura, 'DOMICILIO: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoNotaRecepcion.person_domicilio != null ? 'bold' : 'normal');
        doc.text(37, 67.5 + altura, 'Mancha y Velasco 1574 - B°. Ayacucho');

        // TERCER CUADRANTE
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 70 + altura, 181.7, 16);

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 76 + altura, 'IVA: ');
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(26, 76 + altura, 'Responsable Inscripto');
        // Linea hoizontal IVA 
        doc.setLineWidth(0.4);
        doc.line(14.1, 78 + altura, 127, 78 + altura); 
        // Vertical linea IVA IZQUIERDA
        doc.setLineWidth(0.4);
        doc.line(127, 70 + altura, 127, 86 + altura); 
        // Vertical linea IVA DERECHA
        doc.setLineWidth(0.4);
        doc.line(25, 70 + altura, 25, 78 + altura); 

        // TRANSPORTISTA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 84 + altura, 'TRANSPORTISTA:  _______________________________________');
        
        // CUIT
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(129, 76 + altura, 'CUIT N°.:');
        doc.setFontStyle(objetoNotaRecepcion.person_cuit != null ? 'bold' : 'normal');
        doc.text(145, 76 + altura, '33-71161816-9');

        // ING BRUTOS
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(129, 84 + altura, 'ING. BRUTOS N°.: ');
        doc.setFontStyle('bold');
        doc.text(160, 84 + altura, '280.369.632');

        // CUADRADO FINAL DE FIRMA
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 122 + altura_post_tabla, 181.7, 20);

        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(16.5, 125.5 + altura_post_tabla, 'La mercadería viaja por cuenta y riesgo del comprador. Valor Declarado:');

        // RECIBIO LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(20, 137 + altura_post_tabla, '_________________________________');
        // RECIBIO TEXTO
        doc.text(40, 140.5 + altura_post_tabla, 'RECIBIÓ');

        // FIRMA LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(80, 137 + altura_post_tabla, '_________________________________');
        // FIRMA TEXTO
        doc.text(102, 140.5 + altura_post_tabla, 'FIRMA');

        // ACLARACION LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(139, 137 + altura_post_tabla, '_________________________________');
        // ACLARACION TEXTO
        doc.text(158, 140.5 + altura_post_tabla, 'ACLARACIÓN');
    };

    const table = document.getElementById('table_productos_asignados');
    var data = doc.autoTableHtmlToJson(table);
    
    const options = {
        didDrawPage: header,
        margin: {
            top: 87 + altura
        },
        startY: 87 + altura,
        theme: 'striped',
        headerStyles: {
            lineWidth: 0.1,
            lineColor: [0, 0, 0]
        },
        columnStyles: {
            0: {
                cellWidth: 10,
                halign: 'center',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            1: {
                cellWidth: 70,
                halign: 'left',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            2: {
                cellWidth: 15,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            3: {
                cellWidth: 5,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
        },
        styles: {
            overflow: 'linebreak',
            fontSize: 8,
        }
    };

    data.columns.splice(5, 1); // Quitamos columna delete
    data.columns.splice(4, 1); // Quitamos columna total
    data.columns.splice(3, 1); // Quitamos columna precio

    var columns = [
        {title: "Código Producto"},
        {title: "Nombre Producto"}, 
        {title: "Cantidad"}, 
        {title: "U. Medida"}, 
    ];
        
    doc.autoTable(columns, data.rows, options);

    return doc;
}

function imprimirOrdenPagoCompra(){
    var doc = new jsPDF();
    var altura = 0;
    var altura_post_tabla = 0;
    
    altura = 0;
    altura_post_tabla = 144;
    doc = dibujarOrdenPagoPDF(doc, altura, altura_post_tabla, objetoReciboCompra);

    doc.save('Recibocompra_' + objetoReciboCompra.data_purchase.id + '_' + objetoReciboCompra.id + '_' + objetoReciboCompra.created_at + '.pdf');
    window.open(doc.output('bloburl'), '_blank');
}

function dibujarOrdenPagoPDF(doc, altura, altura_post_tabla, objetoRecibo) {

    objetoRecibo.data_purchase.person_desc = objetoRecibo.data_purchase.person_lastname + ' ' + objetoRecibo.data_purchase.person_name;
    objetoRecibo.data_purchase.person_cuit = (objetoRecibo.data_purchase.person_cuit != null ? objetoRecibo.data_purchase.person_cuit : '');
    objetoRecibo.data_purchase.person_domicilio = (objetoRecibo.data_purchase.adress != null ? objetoRecibo.data_purchase.adress : '');
    objetoRecibo.data_purchase.person_iva = (objetoRecibo.data_purchase.condicion_iva_desc != null ? objetoRecibo.data_purchase.condicion_iva_desc : '');

    const header = function (data) {
        // PRIMER CUADRANTE

        // Rectangulo primero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 10 + altura, 181.55, 44);
    
        // Vertical linea central
        doc.setLineWidth(0.1);
        doc.line(107, 25 + altura, 107, 54 + altura); 

        doc.setFontSize(12);
        doc.setFontStyle('bold');
        doc.text(22, 23 + altura, (objetoRecibo.data_purchase.person_desc != null ? objetoRecibo.data_purchase.person_desc : ''));

        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(22, 35 + altura, (objetoRecibo.data_purchase.person_domicilio != null ? objetoRecibo.data_purchase.person_domicilio : ''));
        
        // Horizontal linea central
        doc.line(14.1, 49 + altura, 107, 49 + altura); 

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(22, 53 + altura, (objetoRecibo.data_purchase.person_iva != null ? objetoRecibo.data_purchase.person_iva : ''));

        // Rectangulo de fecha
        doc.setDrawColor(0,0,0);
        doc.rect(161, 27 + altura, 31, 10);
        // Linea fecha 1
        doc.setLineWidth(0.1);
        doc.line(171, 37 + altura, 171, 27 + altura);
        // Linea fecha 2
        doc.setLineWidth(0.1);
        doc.line(181, 37 + altura, 181, 27 + altura);
        // Linea hoizontal fecha 
        doc.line(161, 30 + altura, 192, 30 + altura); 
        // Texto Fecha
        doc.setFontSize(7.5);
        doc.setFontStyle('normal');
        doc.text(164, 29.5 + altura, 'Dia       Mes        Año');
        // Fecha ISSUE DATE
        doc.setFontSize(11);
        doc.setFontStyle('bold');
        var fecha_recibo = formatDateTime(objetoRecibo.created_at, 'd-m-Y').split('/');
        doc.text(164, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[0] : fecha_recibo[2] : ''));
        doc.text(173.5, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[1] : fecha_recibo[3] : ''));
        doc.text(181.8, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[2] : fecha_recibo[4] : ''));

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(124, 15 + altura, 'Documento No Válido Como Factura');

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(122, 23 + altura, 'RECIBO OFICIAL');
        
        doc.setFontSize(16);
        doc.setFontStyle('normal');
        doc.text(122, 34 + altura, 'N°. ' + objetoRecibo.id);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 43 + altura, 'CUIT N°.: ' + (objetoRecibo.data_purchase.person_cuit != null ? objetoRecibo.data_purchase.person_cuit : ''));

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 48 + altura, 'ING. BRUTOS: ');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 52.8 + altura, 'INICIO DE ACTIVIDADES: ');

        // Rectangulo central A
        doc.setDrawColor(0,0,0);
        doc.rect(101.5, 10 + altura, 11, 11);

        doc.setFontSize(28);
        doc.setFontStyle('bold');
        doc.text(103.4, 19 + altura, 'X');

        // SEGUNDO CUADRANTE

        // Rectangulo segundo grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 55 + altura, 181.55, 28);

        // PERSONA CLIENTE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 61 + altura, 'RAZÓN SOCIAL: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoRecibo.data_purchase.person_desc != null ? 'bold' : 'normal');
        doc.text(45, 61 + altura, 'Cooperativa de Trabajo Barranca Yaco Lim        102287')

        // DOMICILIO
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 67.5 + altura, 'DOMICILIO: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoRecibo.data_purchase.person_domicilio != null ? 'bold' : 'normal');
        doc.text(37, 67.5 + altura, 'Mancha y Velasco 1574 - B°. Ayacucho');

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 76 + altura, 'IVA: ');
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(26, 76 + altura, 'Responsable Inscripto');
    
        // CUIT
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(110, 74.5 + altura, 'CUIT N°.:');
        doc.setFontStyle('bold');
        doc.text(127, 74.5 + altura, '33-71161816-9');

        // ING BRUTOS
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(16.5, 81.5 + altura, 'ING. BRUTOS N°.: ');
        doc.setFontStyle('bold');
        doc.text(48, 81.5 + altura, '280.369.632');

        // NRO REMITO
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(110, 81.5 + altura, 'REMITO N°.:');
        doc.setFontStyle(objetoRecibo.data_purchase.id_sale != null ? 'bold' : 'normal');
        doc.text(132, 81.5 + altura, (objetoRecibo.data_purchase.id_sale != null ? objetoRecibo.data_purchase.id_sale + '' : ' _______________________________'));

        // TECER CUADRANTE

        // Rectangulo tercero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 84 + altura, 181.55, 42.5);

        // RECIBO TANTO
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(16.5, 90 + altura, 'Recibi(mos) la suma de pesos ');
        doc.setFontStyle(objetoRecibo.total_amount != null ? 'bold' : 'normal');
        var total_monto_en_letras = NumeroALetras(objetoRecibo.total_amount)
        doc.text(66, 90 + altura, (objetoRecibo.total_amount != null ? objetoRecibo.total_amount + '' : ' ________________________________________________________________'));
        doc.text(16.5, 97 + altura, (objetoRecibo.total_amount != null ? ' ( ' + total_monto_en_letras + ' )'  : '__________________________________________________________________________________________'));
        
        // EN CONCEPTO DE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 104 + altura, 'en concepto de');
        doc.text(42, 104 + altura, '_____________________________________________________________________________');
        doc.text(16.5, 111 + altura, '__________________________________________________________________________________________');
        doc.text(16.5, 118 + altura, '__________________________________________________________________________________________');
    
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 125 + altura, 'Efectivo: ');
        var efectivo_en_letras = NumeroALetras(objetoRecibo.effective_amount)
        doc.setFontStyle(objetoRecibo.effective_amount != null ? 'bold' : 'normal');
        doc.text(31, 125 + altura, (objetoRecibo.effective_amount != null ? efectivo_en_letras + '' : ' _______________________________________________________'));
        doc.text(143, 125 + altura, (objetoRecibo.effective_amount != null ? '$ ' + objetoRecibo.effective_amount : '$ ________________________'));

        // CUADRADO FINAL
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 112 + altura_post_tabla, 181.8, 29.8);

        objetoRecibo.total_amount = objetoRecibo.total_amount != null ? objetoRecibo.total_amount : 0;

        doc.setFontSize(11);
        doc.setFontStyle('bold');
        doc.text(158.5, 118 + altura_post_tabla, 'Importe Total: ' + '$ ', 'right');
        doc.text(193, 118 + altura_post_tabla, parseFloat(objetoRecibo.total_amount).toFixed(2).replace('.', ',') + '', 'right');

        // FIRMA LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(22, 137 + altura_post_tabla, '___________________________________________________');
        // FIRMA TEXTO
        doc.text(57, 140.5 + altura_post_tabla, 'FIRMA');

        // ACLARACION LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(110, 137 + altura_post_tabla, '___________________________________________________');
        // ACLARACION TEXTO
        doc.text(142, 140.5 + altura_post_tabla, 'ACLARACIÓN');

        // Vertical linea GRANDE LATERAL 1
        doc.setLineWidth(0.1);
        doc.line(14.1, 256, 14.1, 140.5 ); 

        // Vertical linea GRANDE LATERAL 2
        doc.setLineWidth(0.1);
        doc.line(195.9, 256, 195.9, 140.5 ); 
    };

    const table = document.getElementById('tbody_chequesrecibo_ver_recibo_imprimir');
    var data = doc.autoTableHtmlToJson(table);

    const options = {
        didDrawPage: header,
        margin: {
            top: 127.5 + altura
        },
        startY: 127.5 + altura,
        theme: 'striped',
        headerStyles: {
            lineWidth: 0.1,
            lineColor: [0, 0, 0]
        },
        columnStyles: {
            0: {
                cellWidth: 30,
                halign: 'left',
                fontStyle: 'normal',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            1: {
                cellWidth: 40,
                halign: 'left',
                fontStyle: 'normal',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            2: {
                cellWidth: 30,
                fontStyle: 'bold',
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
        },
        styles: {
            overflow: 'linebreak',
            fontSize: 8,
        }
    };

    var columns = [
        {title: "Nro Cheque"},
        {title: "Banco"}, 
        {title: "Importe"}, 
    ];
        
    doc.autoTable(columns, data.rows, options);

    return doc;
}
