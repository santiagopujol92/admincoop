$(document).ready(function(){
	activarMenu('cuentas_corrientes');
	listar();
})

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Cuenta Corriente';
var form = 'CuentaCorriente';
var module = 'cuentas_corrientes';
var modals_btns = 'CurrentsAccount';

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	$("#form"+form+" input[name=balance]").removeClass('col-red');
	$("#form"+form+" input[name=balance]").removeClass('col-green');
	$("#form"+form+" input[name=balance]").addClass('col-teal');
}

// VARIABLES GLOBALES
var displayContentData = []; 
var displayTitleData = [
	{ title: "Nro de Cuenta" },
	{ title: "Persona de Cuenta" },
	// { title: "Estado Cuenta" },
	{ title: "Tipo de Persona" },
	{ title: "Saldo" },
	{ title: "Último Movimiento" },
	// { title: "Descripción" },
	{ title: "Opciones" }
];

var totalSaldoGeneralListado = 0;

function listar(){
	displayContentData = [];
	totalSaldoGeneralListado = 0;
	
    statusLoading('loading_list', 1, 'Actualizando Listado ..');
	var route = current_route+"_listar";
	var tabla = $("#table_"+modals_btns+"");

	var filtro_id_type_people = $("select[name=filtro_id_type_people]").val();

	$.get(route, function(result){
		$(result.data).each(function(key,value){
			if (value.id_type_people == filtro_id_type_people || filtro_id_type_people == 0){
				applyDataInTable(key, value, result.data_user);
			}
		});

		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_list', 0);
		
		$("#totalSaldo" + modals_btns).text("$ " + totalSaldoGeneralListado);
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

 //APLICAR DATA EN TABLA, SE APLICA DENTRO DEL FOREACH DESPUES DE OBTENER LA DATA
function applyDataInTable(key,value, data_user){
	var boton_agregar_movimiento = '';
	// Si el rol es ROOT agrego el boton agregar
	if (data_user.type == 2) {
		boton_agregar_movimiento = '<button type="button" href="#" title="Cargar Operación" data-backdrop="static" data-keyboard="false" onclick="abrirModalCargarOperación('+value.id+', '+value.balance+');" data-toggle="modal" data-target="#modalCargarOperacion'+modals_btns+'" class="btn bg-green btn-circle waves-effect waves-circle waves-float">'
			+'<i class="material-icons">add</i>'
		+'</button>';
	}
	
	var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

	totalSaldoGeneralListado = totalSaldoGeneralListado + parseFloat(value.balance);

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentData.push([ 
		value.id,
		value.person_name + " " + value.person_lastname,
		value.desc_type_person,
		// (value.id_status_current_account == 2) ? '<b class="col-red">' + value.desc_status_current_account + '</b>' : '<b class="col-green">' + value.desc_status_current_account + '</b>',
		(parseFloat(value.balance) < 0 ) ? '<b class="col-red">$ '+ value.balance + '</b>' : '<b class="col-green">$ '+ value.balance + '</b>',
		(value.last_movement_account != null) ? formatDateTime(value.last_movement_account, 'd-m-Y h:i', true) : '',
		// value.description,
		'<div class="icon-button-demo">'
			 +'<button type="button" href="#" title="Ver Movimientos" data-backdrop="static" data-keyboard="false" onclick="abrirModalVerMovimientosByCuentaCorriente('+value.id+', \' '+value.person_name + " " + value.person_lastname+' \', '+value.balance+');" data-toggle="modal" data-target="#modalVerMovimientos'+modals_btns+'" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">search</i>'
			+'</button>'
			 +boton_agregar_movimiento
			// +'<button type="button" href="#" data-toggle="modal" title="Editar" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
			// 	+'<i class="material-icons">edit</i>'
			   // +'</button>'
			 // +'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
			// 	+'<i class="material-icons">delete</i>'
			// +'</button>'
		+'</div>'
	]);
}

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	//Limpiamos el color
	$("#form"+form+" input[name=balance]").removeClass('col-teal');
	$("#form"+form+" input[name=balance]").removeClass('col-red');
	$("#form"+form+" input[name=balance]").removeClass('col-green');

	$.get(route, function(result){
		var colorSaldo = (parseFloat(result.balance) < 0) ? 'col-red' : 'col-green';
		$("#form"+form+" input[name=balance]").addClass(colorSaldo)

		$("#form"+form+" select[name=id_person]").val(result.id_person).selectpicker('refresh');
		$("#form"+form+" select[name=id_status_current_account]").val(result.id_status_current_account).selectpicker('refresh');
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" input[name=balance]").val(result.balance);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');
		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}

/*ABRIR MODAL CARGAR OPERACION */
function abrirModalCargarOperación(id, balance){
	clearForm("formCargarOperacion"+form);
	changeTipoComprobante(1); //Inicia como Factura
	//Limpiamos el color
	$("#formCargarOperacion"+form+" input[name=balance]").removeClass('col-red');
	$("#formCargarOperacion"+form+" input[name=balance]").removeClass('col-green');
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	statusLoading('loading_modal_aumentar_stock', 1);
	var route = current_route+"/"+id+"/edit";
	
	$.get(route, function(result){
		var colorSaldo = (parseFloat(result.balance) < 0) ? 'col-red' : 'col-green';
		$("#formCargarOperacion"+form+" input[name=balance]").addClass(colorSaldo)

		$("#formCargarOperacion"+form+" input[name=id_current_account]").val(id);
		$("#formCargarOperacion"+form+" input[name=balance]").val(balance);
		$("#formCargarOperacion"+form+" select[name=id_type_movement_account]").val(0).selectpicker('refresh');
		$("#formCargarOperacion"+form+" input[name=total_amount]").val(0);
		$("#formCargarOperacion"+form+" button[name=btnCargarOperacion"+modals_btns+"]").attr('onclick', 'insertOperacionCuentaCorriente('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("formCargarOperacion"+form);
		statusLoading('loading_modal_aumentar_stock', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_aumentar_stock', 0);
	});
}

/*CARGAR OPEREARCION CUENTA CORRIENTE - POST AL CONTROLADOR DE MOVIMIENTOS */
function insertOperacionCuentaCorriente(id, validar){
	var route = root_project+"movimientos_cuentas";
	var datos = $("#formCargarOperacion"+form+"").serialize();
	var saldo_actual = $("#formCargarOperacion"+form+" input[name=balance]").val();
	datos = datos + '&balance='+saldo_actual+'';

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("formCargarOperacion"+form);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje_cargar_operacion', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}
	
	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal_cargar_operacion', 1, 'Cargando Operación..');
        },
		success: function(result){
			showMessageModal('Operación de ' + modulo_msg+' cargada  con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
			listar();
			$('#modalCargarOperacion'+modals_btns+'').modal('hide');
			statusLoading('loading_modal_cargar_operacion', 0);
			clearForm("formCargarOperacion"+form);
		},
		error: function(){
			showMessageModal('No se pudo Generar la operación <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_cargar_operacion', 0);
		}
	})
}

/*ABRIR MODAL CARGAR OPERACION - APUNTA A MOVIMIENTOS */
function abrirModalVerMovimientosByCuentaCorriente(id, nombre_persona, saldo){
	//Cargamos data cuenta:
	$("#label_persona_ver_movimientos"+modals_btns+"").text(nombre_persona);
	$("#label_saldo_ver_movimientos"+modals_btns+"").text('$ ' + saldo);

	//Color saldo cuenta
	if (parseFloat(saldo) <= 0){
		$("#label_saldo_ver_movimientos"+modals_btns+"").removeClass('label-success');
		$("#label_saldo_ver_movimientos"+modals_btns+"").addClass('label-danger');
	} else {
		$("#label_saldo_ver_movimientos"+modals_btns+"").removeClass('label-danger');
		$("#label_saldo_ver_movimientos"+modals_btns+"").addClass('label-success');
	}


	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	statusLoading('loading_modal_ver_movimientos', 1, 'Actualizando Listado ..');
	var route = current_route+"/GetMovimientosByCuentaCorriente/"+id;
	var tabla = $("#table_VerMovimientos"+modals_btns+"");
	
	var displayContentData = []; 
	var displayTitleData = [
		{ title: "Nro Movimiento" },
		{ title: "Fecha Emisión" },
		{ title: "Hora Emisión" },
		{ title: "Tipo Comprobante" },
		{ title: "Nro Comprobante" },
		{ title: "Importe" },
		{ title: "Tipo Operación" },
		{ title: "Saldo Cuenta" },
	];
	
	$.get(route, function(result){
		$(result).each(function(key,value){
			//carga del array contenido desde el array de la respuesta, el orden va de la mano de el orden de los titulos
			displayContentData.push([ 
				value.id,
				(value.date_movement != null) ? formatDateTime(value.date_movement, 'd-m-Y') : '',
				(value.date_movement != null) ? formatDateTime(value.date_movement, 'h:i', true) : '',
				value.desc_type_movement_account,
				(value.nro_receipt != null) ? value.nro_receipt : 'Sin Datos',
				(value.type_operation == 'DEBITO') ? '<b class="col-red">- $ ' + value.total_amount + '</b>' : '<b class="col-green">$ ' + value.total_amount + '</b>',
				(value.type_operation != null) ? value.type_operation : 'Sin Datos',
				(value.current_balance_account != null) ? '<b>' + value.current_balance_account + '</b>' : 'Sin Datos'
			]);
		});
		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_modal_ver_movimientos', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_ver_movimientos', 0);
	});
}

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL CARGAR OPERACION EJECUTAR ACTION DEL BOTON*/
$("#formCargarOperacion"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#formCargarOperacion"+form+" button[name=btnModalCargarOperacion"+modals_btns+"]").click();
    }
});

function changeTipoComprobante(value) {
	// Si elije factura
	if (value == 1) {
		$("#formCargarOperacion"+form+" [id=nro_receipt_ordenDePago]").attr('disabled', 'disabled')
		$("#formCargarOperacion"+form+" [id=divNroOrdenDePago]").addClass('hidden');
		$("#formCargarOperacion"+form+" [id=nro_receipt_factura]").removeAttr('disabled').selectpicker('refresh');
		$("#formCargarOperacion"+form+" [id=divNroFactura]").removeClass('hidden');
		$("#formCargarOperacion"+form+" [id=nro_receipt_ordenDePago]").val(null);

	// Si elije Orden de Pago
	} else if (value == 2) {
		$("#formCargarOperacion"+form+" [id=nro_receipt_factura]").attr('disabled', 'disabled').selectpicker('refresh');
		$("#formCargarOperacion"+form+" [id=divNroFactura]").addClass('hidden');
		$("#formCargarOperacion"+form+" [id=nro_receipt_ordenDePago]").removeAttr('disabled');
		$("#formCargarOperacion"+form+" [id=divNroOrdenDePago]").removeClass('hidden');
		$("#formCargarOperacion"+form+" [id=nro_receipt_factura]").val(0).selectpicker('refresh');
	}
}