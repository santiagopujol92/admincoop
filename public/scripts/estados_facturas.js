$(document).ready(function(){
	activarMenu('admin', 'estados_facturas');
	listar();
})

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Estado de Factura';
var form = 'EstadoFactura';
var module = 'estados_facturas';
var modals_btns = 'StatusInvoice';

function listar(){
    statusLoading('loading_list', 1, 'Actualizando Listado ..');
	var route = current_route+"_listar";
	var tabla = $("#table_"+modals_btns+"");

	var displayContentData = []; 
	var displayTitleData = [
        { title: "Descripción" },
        { title: "Creación" },
        { title: "Opciones" }
    ];

	$.get(route, function(result){
		$(result).each(function(key,value){

			var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";
			
			//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
			displayContentData.push([ 
				value.description,
				formatDateTime(value.created_at, 'd-m-Y h:i', true),
				'<div class="icon-button-demo">'
					+'<button type="button" href="#" data-toggle="modal" title="Editar" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
	                	+'<i class="material-icons">edit</i>'
	               	+'</button>'
	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
	    				+'<i class="material-icons">delete</i>'
	        		+'</button>'
				+'</div>'
			]);
		});
		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_list', 0);

	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

/*ABRIR MODAL EDIT CON DATOS*/
function mostrarEnModal(id){
	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+id+"/edit";
	$("#form"+form+"").attr('method', 'PUT');
	
	statusLoading('loading_modal', 1);

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+')');
		
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}