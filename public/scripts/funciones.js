
/* VARIALBES PARA LOS RUTEO, current_rout se usa para los abm_basic 
    root_project se usa para las llamadas especiales
*/
var url = window.location;
var current_route = url.origin+url.pathname;
var root_project = '';


//Si el origen es local se deja para local, sino se obtiene la ruta dinamica, quitando el index.php
if (url.origin == 'http://localhost:8000'){
    root_project = url.origin + "/";
}else{
    var cantidad_caracteres_hasta = url.href.indexOf("index.php/"); 
    root_project = url.href.substring(0, cantidad_caracteres_hasta);
}

function showMessage(inputId, color, msg, icon, closebutton, msgPosition){
    if (typeof(closebutton) == 'undefined'){
        closebutton = true;
    }
    if (typeof(msgPosition) == 'undefined'){
        msgPosition = 'center';
    }
    if (typeof(icon) == 'undefined'){
        icon = '';
    }

	$("#"+inputId+"").removeClass();
	$("#"+inputId+"").addClass("alert alert-dismissible " + color);
	$("#"+inputId+"").attr('align', msgPosition).attr('role', 'alert');

	if (closebutton == true){
		$("#"+inputId+"").html('<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
		                        	+'<span aria-hidden="true">&times;</span>'
		                      +'</button>'+msg+"");	
	}else{
		$("#"+inputId+"").html(''+msg+"<i class='material-icons'>"+icon+"</i>");	
	}

	setTimeout(function(){
        $("#"+inputId+"").empty().removeClass();
    }, 6000);
}

function clearForm(form){
	$("#"+form+"").trigger("reset");
    $("#"+form+" select").val(0);
    $("#"+form+" select").val(0).selectpicker('refresh');
    $("#"+form+" [type=checkbox]").prop('checked', false);
    $("#"+form+" .label-money").html('0.00');
}

function resetearPassword(formId){
	var email = $("#"+formId+" input[name=email]").val();
	showMessage('email_reset_mensaje', 'bg-green','Nueva contraseña enviada a <strong>'+email+'</strong>', '');	
}

function statusLoading(idDivLoading, status, text){    
    if (typeof(text) == 'undefined'){
        text = 'Cargando ..';
    }

    if (typeof(status) == 'undefined'){
        status = 1;
    }

	if (status == 1){
		$("#"+idDivLoading+"").html('<div style="margin-bottom:10px;"><p style="text-align:center;"><b>'
                                        +'<img width="30px" style="margin-bottom:5px;" src="../assets_md/images/Rolling.gif"/> ' +text+ '</b>'
                                    +'</p></div>'
									);
		$("#"+idDivLoading+"").removeClass("hidden");
	}else if (status == 0){
		$("#"+idDivLoading+"").addClass("hidden");
	}
}

function activeLoadingInButton(idButton, status, text, color){      
    if (typeof(text) == 'undefined'){
        text = 'Cargando ..';
    }

    if (typeof(status) == 'undefined'){
        status = 1;
    }

    if (typeof(color) == 'undefined'){
        color = '';
    }

    if (status == 1){
        $("#"+idButton+"").html('<div><p style="text-align:center;"><b class="'+color+'">'
                                        +'<img width="24px" src="../assets_md/images/Rolling.gif"/> ' +text+ '</b>'
                                    +'</p></div>'
                                    );
    }else if (status == 0){
        $("#"+idButton+"").html(text)
    }
}

//INICIALIZAR DATATABLES
function startDatatable(selectorTabla){
    $(selectorTabla).DataTable( {
        destroy: true,
        searching: true,
        paging: true,
        responsive: true,
    });
}

//INICIALIZAR DATATABLES EXPORTABLES
function startDatatableExportable(selectorTabla){
    $(selectorTabla).DataTable({
        dom: 'Bfrtip',
        "bDestroy": true,
        destroy: true,
        searching: true,
        paging: true,
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

//INICIALIZAR Y CARGAR DATA TABLE CON DATOS Y EXPORTACION
//EL ORDEN DE LOS TITULOS Y LA DATA CONTENIDO DEBE SER EN EL MISMO ORDEN
function fillDataTableExportable(selectorTabla, displayTitlesData, displayContentData){
    $(selectorTabla).DataTable( {
        data: displayContentData,
        columns: displayTitlesData,
        dom: 'Bfrtip',
        "bDestroy": true,
        destroy: true,
        searching: true,
        paging: true,
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "lengthMenu": [[5, 10, 15 , -1], [10, 25, 50, "All"]]
    });
}

function fillDataTable(selectorTabla, displayTitlesData, displayContentData){
    $(selectorTabla).DataTable( {
        data: displayContentData,
        columns: displayTitlesData,
        dom: 'Bfrtip',
        "bDestroy": true,
        destroy: true,
        searching: false,
        paging: false,
        responsive: true,
        buttons: [],
    });
}

//INICIALIZAR DATEPICKER POR SELECTOR
function startDatePicker(selectorPicker, minDate = new Date()){
    $(selectorPicker).bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1,
        minDate : minDate,
    });
}

//INICIALIZAR DATETIMEPICKER POR SELECTOR
function startDateTimePicker(selectorPicker, minDate = new Date()){
    $(selectorPicker).bootstrapMaterialDatePicker({ 
        format : 'DD/MM/YYYY HH:mm', 
        weekStart : 1,
        minDate : minDate,
    });
}

/* GENERAR SELECT DE CUALQUIER MODULO CON ID DESCRIPCION*/
function generarSelect(route, divIdSelect, nameSelect, formSelect, defaultOption, defaultValue){
    var route = root_project+route+"";
    var div = $("#"+divIdSelect+"");

    div.html('<select name="'+nameSelect+'" class="form-control show-tick" required data-live-search="true" dropdown>z');
    $("#"+formSelect+" select[name="+nameSelect+"]").append('<option value="'+defaultValue+'">Seleccione '+defaultOption+'</option')

    $.get(route, function(result){
        $(result).each(function(key,value){
            $("#"+formSelect+" select[name="+nameSelect+"]").append('<option value="'+value.id+'">'+value.description+'</option>'); 
        });
        
    }).fail(function(data) {
        if (data.error == "Unauthenticated") {
            showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
                                +'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                    +'CONECTARSE'
                                +'</a>', 'ATENCION', 'bg-red', 'error');
        }else {
            showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
        }
    });

}

//FUNCION CHECK FORM PARA VALIDAR NULOS "solo los que tienen el atr required"
function checkForm(idForm){
    var retorno = true;
    $('form#'+idForm+'').find('input').each(function(){
        if($(this).prop('required') && !$(this).hasClass('hidden')){
            if (this.value == ''){
                retorno = false;
                return false;
            }
        }
    });

    /*PARA SELECTS*/
    $('form#'+idForm+'').find('select').each(function(){
        if($(this).prop('required') && !$(this).hasClass('hidden')){
            if (this.value == 0 || this.value == ''){
                retorno = false;
                return false;
            }
        }
    });
    return retorno;
}

//VALIDAR EMAIL
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

//ACTIVAR MENU
// El menu es el boton principal que llama la seccion.
// Si el menu tiene desplegable se utiliza submenu para los submenues.
// EL nombre de submenu debe ser igual al href del boton que hace el submenu EN EL MAIN.BLADE
function activarMenu(menu, submenu){

    //BORRAMOS EL ACTIVO TODOS LOS LI PRINCIPALES DE MENU
    $('.menu .list').find('li').each(function(){
        $(this).removeClass('active');
    });

    //ASIGNAMOS EL ACTIVO A LA NUEVA SECCION
    $("#menu-"+menu+"").addClass('active');

    //ASIGNAMOS EL ACTIVO CON SU ICONO DE SUBMENU SI TIENE
    if (submenu != ''){
        $("#submenu-"+menu+"").show();
        $("#submenu-"+menu+"-"+submenu+"").prop('class', 'active2');
        $("#submenu-"+menu+"-"+submenu+"").addClass('active2');
        $("#submenu-"+menu+" [href='/"+submenu+"']").prepend('<i class="material-icons submenu-icon-custom">keyboard_arrow_right</i> ');
    }
}

//FUNCION PARA FORMATEAR CON DATETIME
//Puede recibir un formato:
//2017-08-06 22:47:01
//2017-08-06 22:47
//2017-08-06 22
//2017-08-06
//22:47
//Caso contrario modificar funcion
function formatDateTime(datetime, format, includePMAM = false){
    var retorno = '';

    if (datetime != null && datetime != ''){
        var dateArr;
        var fullDateTime
        var hourArr;
        var ampm;

        if (format == 'd-m-Y'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0];
            retorno = new_date;
        }

        if (format == 'd-m-Y h'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1];
            ampm = (includePMAM) ? ((parseInt(hourArr[0]) >= 0 && parseInt(hourArr[0]) < 12 ) ? 'am' : 'pm') : '';
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0] + ' ' + ampm;
            retorno = new_date;
        }

        if (format == 'd-m-Y h:i'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1].split(':');
            ampm = (includePMAM) ? ((parseInt(hourArr[0]) >= 0 && parseInt(hourArr[0]) < 12 ) ? 'am' : 'pm') : '';
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0] + ':' + hourArr[1] + ' ' + ampm;
            retorno = new_date;
        }

        if (format == 'd-m-Y h:i:s'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1].split(':');
            ampm = (includePMAM) ? ((parseInt(hourArr[0]) >= 0 && parseInt(hourArr[0]) < 12 ) ? 'am' : 'pm') : '';
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0] + ':' + hourArr[1] + ':' + hourArr[2] + ' ' + ampm;
            retorno = new_date;
        }

        if (format == 'h:i'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1].split(':');
            ampm = (includePMAM) ? ((parseInt(hourArr[0]) >= 0 && parseInt(hourArr[0]) < 12 ) ? 'am' : 'pm') : '';
            new_date = hourArr[0] + ':' + hourArr[1] + ' ' + ampm;
            retorno = new_date;
        }
    }
    return retorno;
}

/*
Funcion para generar un select desp de seleccionar valor en otro, los parametros se identifican,
la data que debe recibir es tipo arary y solo nombres id y description, si posee algun otro campo 
el result darle alias correspondiente
La ruta dirigida debe ser ejemplo: provincias_findByCountry/{id} y en el controlador correspondiente con su funcion
*/
function changeDataSelectTarget(modulo, func, targetSelectName, moduleMsg, formId, valueSelectedSource, optionSelectedTarget, defaultMsg){
    if (typeof(defaultMsg) == 'undefined'){
        defaultMsg = true;
    }

    var route = root_project+modulo+"/"+func+"/"+valueSelectedSource+"";
    var targetSelect = $("#"+formId+" select[name="+targetSelectName+"]")

    if (valueSelectedSource == 0){
        //Anular select target si se selecciona 0 en el source
        validateNoSelectionToSelect(targetSelectName, moduleMsg, formId, valueSelectedSource);
    }else{
        $.ajax({
            url: route,
            headers: {  'X-CSRF-TOKEN': token},
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                targetSelect.html('<option value="">Cargando '+moduleMsg+'s ..</option>');
            },
            success: function(result){
                targetSelect.attr('disabled', false);
                targetSelect.html('<option value="0">Seleccione '+moduleMsg+'</option>');
                
                // //Default Msg falso y se anula el option 0 por 1(Utilizado para facturas con el tema de personas y empresas)
                if (!defaultMsg){
                    targetSelect.html('<option value="1">'+moduleMsg+' No Asignada - </option>');
                }

                $(result).each(function(index, element) {
                    if (modulo == 'productos'){
                        var last_sale_price = element.last_sale_price != null ? element.last_sale_price : '';
                        var stock = element.stock != null ? element.stock : '';
                        var min_purchase_price = element.min_purchase_price != null ? element.min_purchase_price : '';
                        var max_purchase_price = element.max_purchase_price != null ? element.max_purchase_price : '';
                        var type_metric_product = element.type_metric_product != null ? element.type_metric_product : '';
                        var last_purchase_price = element.last_purchase_price != null ? element.last_purchase_price : '';
                        var code_product = element.code_product != null ? element.code_product : '';

                        targetSelect.append('<option code_product="'+code_product+'" last_purchase_price="'+last_purchase_price+'" type_metric_product="'+type_metric_product+'" min_purchase_price="'+min_purchase_price+'" max_purchase_price="'+max_purchase_price+'" last_sale_price="'+last_sale_price+'" stock="'+stock+'" value="'+element.id+'">'+element.description+'</b></option>');
                    } else {
                        targetSelect.append('<option value="'+element.id+'">'+element.description+'</option>');
                    }
                });

                /*VALOR SELECCIONADO EN EL TARGET SOLO SI VIENE LA VARIABLE Y ES MAYOR A 0*/
                if (optionSelectedTarget && optionSelectedTarget > 0){
                    targetSelect.val(optionSelectedTarget);
                }

                targetSelect.selectpicker('refresh');
            },
            error: function(){
                console.log("No se pudo obtener los datos");
            }
        })    
    }
}

/**
* Funcion para anular un select luego de un onchange to 0 de otro select
* Es utilizada cuando se selecciona 0 en la funcion anterior en el source select, y se afecta el target select
*/
function validateNoSelectionToSelect(nameSelect, textDefaultDisplay, formId, valueSelected){
    var select = $("#"+formId+" select[name="+nameSelect+"]")
    if (valueSelected == 0){
        select.attr('disabled', true);
        select.html('<option value="0">Seleccione '+textDefaultDisplay+'</option>');
        select.val(0).selectpicker('refresh');
    }
}

/*
Funcion para generar select con data de la basedesde js
*/
function actualizarSelectByDB(modulo, func, targetSelectName, moduleMsg, formId, optionSelectedTarget){
    if (typeof(defaultMsg) == 'undefined'){
        defaultMsg = true;
    }

    var route = root_project+modulo+"/"+func;
    var targetSelect = $("#"+formId+" select[name="+targetSelectName+"]")

    $.ajax({
        url: route,
        headers: {  'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        beforeSend: function () {
            targetSelect.html('<option value="">Cargando '+moduleMsg+'s ..</option>');
        },
        success: function(result){
            targetSelect.attr('disabled', false);
            targetSelect.html('<option value="0">Seleccione '+moduleMsg+'</option>');
            
            targetSelect.html('<option value="1">Seleccione un/a '+moduleMsg+'</option>');

            $(result).each(function(index, element) {
                //Para ventas
                if (modulo == 'ventas'){
                    targetSelect.append('<option value="'+element.id+'">Nro: '+element.id+' - '+formatDateTime(element.sell_date, 'd-m-Y h:i')+' hs - '+element.employee_last_name+' '+element.employee_name+' ($ '+element.sell_price+')</option>');
                }else{
                    if (element.description2 && element.description3){
                        targetSelect.append('<option value="'+element.id+'">'+element.description+' '+element.description2+' - '+element.description3+'</option>');
                    }else if(element.description2){
                        targetSelect.append('<option value="'+element.id+'">'+element.description+' '+element.description2+'</option>');
                    }else{
                        targetSelect.append('<option value="'+element.id+'">'+element.description+'</option>');
                    }
                }
            });

            /*VALOR SELECCIONADO EN EL TARGET SOLO SI VIENE LA VARIABLE Y ES MAYOR A 0*/
            if (optionSelectedTarget && optionSelectedTarget > 0 && optionSelectedTarget != 'max_id'){
                targetSelect.val(optionSelectedTarget);
            }

            targetSelect.selectpicker('refresh');

        },
        error: function(){
            console.log("No se pudo obtener los datos");
        }
    })    
}

/**
* Mostrar mensaje generico en modal parametrizado, se debe incluir un archivo modal_message_global_blade.php
* Se ejecuta un boton que abre el modal para que funcione como modal y luego se configura el contenido por parametro
*/
/**
* Se modifico mensaje con otro tipo de modal y los iconos cambiaron y se excluyo background.
*/
function showMessageModal(msg, title = 'ATENCIÓN', background = 'bg-yellow', icon = 'warning'){
    $("#executeMessageModal").click();
    // $("#modalMessageHeader").removeClass();
    // $("#modalMessageHeader").addClass('modal-header '+background);

    if (icon == 'done'){
        icon = '<div class="sa-icon sa-success animate" style="display: block;">'
                    +'<span class="sa-line sa-tip animateSuccessTip"></span>'
                    +'<span class="sa-line sa-long animateSuccessLong"></span>'
                    +'<div class="sa-placeholder"></div>'
                    +'<div class="sa-fix"></div>'
               +'</div>';
    }else if (icon == 'error'){
        icon = '<div class="sa-icon sa-error animateErrorIcon" style="display: block;">'
                    +'<span class="sa-x-mark animateXMark">'
                        +'<span class="sa-line sa-left"></span>'
                        +'<span class="sa-line sa-right"></span>'
                  +'</span>'
               +'</div>';
    }else if (icon == 'warning'){
        icon = '<div class="sa-icon sa-warning pulseWarning" style="display: block;">'
                   +'<span class="sa-body pulseWarningIns"></span>'
                   +'<span class="sa-dot pulseWarningIns"></span>'
                +'</div>';
    }

    $("#modalMessageIcon").html(icon);
    $("#modalMessageBody").html(msg);
    $("#modalMessageTitle").html(title);

}









//------------------OTRAS NO UTILIZADOS POR EL MOMENTO -------------------------

function cargarLoading(divId, msg, width){
    if (typeof(msg) == 'undefined'){
        msg = '';
    }
    if (typeof(width) == 'undefined'){
        width = '';
    }

    $("#"+divId+"").html("<div algin='center' style='text-align:center'>"+msg+"<img width='"+width+"' src='../images/ring-alt.gif'></img></div>");
}

function cargarProgressBar(input, porcentaje, texto){
    if (typeof(texto) == 'undefined'){
        texto = '';
    }

	$("."+input+"").css('width', porcentaje);
	$("."+input+"").attr('aria-valuenow', porcentaje);
	$("."+input+"").html(porcentaje + ' ' + texto);
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : ((event != undefined) ? event.keyCode : 0);
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isIntegerNumberKey(evt){
    var charCode = (evt.which) ? evt.which : ((event != undefined) ? event.keyCode : 0);
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

//VALIDAR CUIT
function esCUITValida(inputValor) {
    inputString = inputValor.toString()
    if (inputString.length == 11) {
        var Caracters_1_2 = inputString.charAt(0) + inputString.charAt(1)
        if (Caracters_1_2 == "20" || Caracters_1_2 == "23" || Caracters_1_2 == "24" || Caracters_1_2 == "27" || Caracters_1_2 == "30" || Caracters_1_2 == "33" || Caracters_1_2 == "34") {
            var Count = inputString.charAt(0) * 5 + inputString.charAt(1) * 4 + inputString.charAt(2) * 3 + inputString.charAt(3) * 2 + inputString.charAt(4) * 7 + inputString.charAt(5) * 6 + inputString.charAt(6) * 5 + inputString.charAt(7) * 4 + inputString.charAt(8) * 3 + inputString.charAt(9) * 2 + inputString.charAt(10) * 1
            Division = Count / 11;
            if (Division == Math.floor(Division)) {
                return true
            }
        }
    }
    return false;
}



/*
* Funcion checkFormShowingMsg para validar nulos y campos select en 0 "solo los que tienen el atr required" en formularios
* Devuelve un array con retorno true o false que se denomina "puedeGuardar" y msg con los campos que no estan correctos
* LOS INPUTS, SELECT o TEXTAREA DEBEN TENER una propiedad required y UN title CON EL NOMBRE DEL MSG QUE CORRESPONDA ESE CAMPO.
*/
function checkFormShowingMsg(idForm){
    var retorno = true;
    var msg = '';

    /*PARA INPUT TEXT*/
    $('form#'+idForm+'').find('input').each(function(){
        if ($(this).prop('required') && !$(this).prop('disabled')){
            setBorderInput(this, "#999");
            setErrorElement(this, 'success')
            if (this.value == ''){
                msg = msg + '- ' + getMsgForcheckForm(this) + '<br>';
                setBorderInput(this, "#F00");
                setErrorElement(this, 'error')
                retorno = false;
            //SI POSEE CLASE noNegativeValues y es menor a 0 devuelve msj a ese input
            }else if ($(this).hasClass('noNegativeValues') && this.value < 0){
                msg = msg + '- ' + getMsgForcheckForm(this) + '<br>';
                setBorderInput(this, "#F00");
                setErrorElement(this, 'error')
                retorno = false;
            }
        }
    });

    /*PARA SELECTS*/
    $('form#'+idForm+'').find('select').each(function(){
        if ($(this).prop('required') && !$(this).prop('disabled')){
            setBorderInput(this, "#999");
            setErrorElement(this, 'success')
            //SI NO TIENE LA PROPIEDAD MULTIPLE SELECT
            if (!$(this).prop('multiple')){
                if (this.value == 0 || this.value == ''){
                    msg = msg + '- ' + getMsgForcheckForm(this) + '<br>';
                    setBorderInput(this, "#F00");
                    setErrorElement(this, 'error')
                    retorno = false;
                }
            //SI TIENE LA PROPIEDAD ES MULTIPLE SELECT
            }else {
                if (this.length == 0){
                    msg = msg + '- ' + getMsgForcheckForm(this) + '<br>';
                    setBorderInput(this, "#F00");
                    setErrorElement(this, 'error')
                    retorno = false;
                }
            }
        }
    });

    /*PARA TEXTAREA*/
    $('form#'+idForm+'').find('textarea').each(function(){
        if ($(this).prop('required') && !$(this).prop('disabled')){
            setBorderInput(this, "#999");
            setErrorElement(this, 'success')
            if (this.value == ''){
                msg = msg + '- ' + getMsgForcheckForm(this) + '<br>';
                setBorderInput(this, "#F00");
                setErrorElement(this, 'error')
                retorno = false;
            }        
        }
    });

    /* SI POSEE UN DIV CON INPUTS TIPO CHECKS Y POSEE MAS DE 0 INPUTS
    PARA BUSCAR EN UN DIV CON validateCheck QUE ESTE AL MENOS UN CHECK SELECCIONADO. 
    EL QUE PINTA DE ROJO ES EL MISMO DIV(POR SI ESTUVIERA SOLO EN UN DIV) Y EL ABUELO(EN EL CASO QUE SE USA ACA SE NECESITA PINTAR ESE DIV)*/
    if ($('form#'+idForm+' .validateCheck').find('input[type=checkbox]').length > 0){
        var checkeados = false;
        $('form#'+idForm+' .validateCheck').find('input').each(function(){
            var divAbuelo = $('form#'+idForm+' .validateCheck').parents(1);
            setBorderInput($('form#'+idForm+' .validateCheck'), "#999");
            setErrorElement($('form#'+idForm+' .validateCheck'), 'success')
            setBorderInput(divAbuelo, "#999");
            setErrorElement(divAbuelo, 'success')

            if (checkeados == false){
                if ($(this).attr('checked') != 'checked'){
                    setBorderInput($('form#'+idForm+' .validateCheck'), "#F00");
                    setErrorElement($('form#'+idForm+' .validateCheck'), 'error')
                    setBorderInput(divAbuelo, "#F00");
                    setErrorElement(divAbuelo, 'error')
                }else{
                    checkeados = true;
                }
            }

        });
        /*SI NUNCA FUE CHECKEADO SE ASIGNA EL MENSAJE CORRESPONDIENTE DE SU ATRIBUTO msg o title EN LA VARIABLE msg*/
        if (checkeados == false){
            msg = msg + '- ' + getMsgForcheckForm($('form#'+idForm+' .validateCheck')) + '<br>';
            retorno = false;
        }
    }

    var array_retorno = new Array();
    array_retorno['puedeGuardar'] = retorno;
    array_retorno['msg'] = msg = '<u>Debe completar los siguientes campos:</u><br>' + msg.substring(0, msg.length - 2);
              
    return array_retorno;
}

/*OBTENER MENSAJES DE INPUT. UTILIZADO PARA LA FUNCION DE VALIDACION checkFormShowingMsg*/
function getMsgForcheckForm(input){
    var msg = '';
    if ($(input).prop('title') == ''){
        msg = $(input).attr('msg');
    }else{
        msg = $(input).prop('title');
    } 
    return msg;
}

function setBorderInput(input, color){
    $(input).css("border-color", color);
}

//PARA SETEAR ESTADO DE INPUT CON COLORES
function setErrorElement(input, status){
    if (status == 'error'){
        $(input).parent('div').removeClass('success');
        $(input).parent('div').parent('div').removeClass('success');
        $(input).parent('div').addClass('error focused');
        $(input).parent('div').parent('div').addClass('error focused');
    }else if (status == 'success'){
        $(input).parent('div').removeClass('error');
        $(input).parent('div').parent('div').removeClass('error');
        $(input).parent('div').addClass('success focused');
        $(input).parent('div').parent('div').addClass('success focused');
    }
}

//Obtener fecha de hoy formateada de un date
function getTodayDateFormated(dateTime) {
    return ('0' + dateTime.getDate()).slice(-2) + '/' 
        + ('0' + (dateTime.getMonth()+1)).slice(-2) + '/' 
        + dateTime.getFullYear() + ' ' 
        + dateTime.getHours()  + ':' 
        + dateTime.getMinutes();
}