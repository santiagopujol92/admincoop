$(document).ready(function(){
  //Por defecto 20 notificaciones
	actualizarNotificaciones('registros_logs', 'getDataAuditToNotificaction', 20);
});

/* ACTUALIZAR NOTIFICAICONES DE LOS DATOS DE AUDITORIA Y MOSTRARLA EN EL DIV DE AUDITORIA*/
function actualizarNotificaciones(modulo, func, limit){
    //Declaramos varialbes de uso
    var route = root_project+modulo+"/"+func+'/'+limit;
    var accion = '';
    var seccion = '';
    var tiempo = 'Hoy';
    var class_color_seccion = '';
    var icon_seccion = '';
    var articulo = 'un/a';

    //Buscamos la data
    $.get(route, function(result){
        //Contador de notificaciones
        $("#span_contador_notififaciones").html(result.length);
        $(result).each(function(key,value){

        	//Formateo de dato para el evento
        	if (value.event == 'updated'){
        		accion = 'ha modificado';
        	}else if (value.event == 'deleted'){
        		accion = 'ha eliminado';
        	}else if (value.event == 'created'){
        		accion = 'ha creado';
        	}

        	//Formateo de dato para los dias
        	if (value.days > 0 && value.days < 7){
        		var palabra_dia = ' día';

        		if (value.days > 1 ){
        			palabra_dia = ' días';
        		}

    			tiempo = value.days + ' ' + palabra_dia;
        	}else if (value.days >= 7 && value.days < 14){
        		tiempo = 'Más de 1 semana ';
        	}else if (value.days >= 14 && value.days < 21){
        		tiempo = 'Más de 2 semanas ';
			}else if (value.days >= 21 && value.days < 28){
        		tiempo = 'Más de 3 semanas ';
			}else if (value.days >= 28 && value.days < 32){
        		tiempo = 'Aprox. 1 mes ';
			}else if (value.days >= 32 && value.days < 60){
        		tiempo = 'Más de 1 mes ';
			}else if (value.days >= 60 && value.days < 63 ){
        		tiempo = 'Aprox. 2 meses ';
			}else if (value.days >= 63 && value.days < 90 ){
        		tiempo = 'Más de 2 meses ';
			}else if (value.days >= 90 && value.days < 94 ){
        		tiempo = 'Aprox. 3 meses ';
			}else if (value.days >= 94 && value.days < 182 ){
        		tiempo = 'Más de 3 meses ';
			}else if (value.days >= 180 && value.days < 186 ){
        		tiempo = 'Aprox. 6 meses ';
			}else if (value.days >= 186 && value.days < 360 ){
        		tiempo = 'Más de 6 meses ';
			}else if (value.days >= 360 && value.days < 365 ){
        		tiempo = 'Aprox. 1 año ';
			}else if (value.days >= 365){
        		tiempo = 'Más de 1 año ';
    	   }

        	//Formateo para la seccion afectada
        	//Al aplicar estas modificaciones se excluye los logs de login y logout
    			var extract_section = value.url.split(root_project).pop(); //Quitando la url raiz que traemos ya de funciones el valor
        	var char_index_separator = extract_section.indexOf('/'); //Buscamos el index de la barra del parametro
        	var str_section = extract_section.substring(0, char_index_separator); //Seleccioonamos la string hasta la barra del parametro
        	var format_simbols_section = str_section.replace("_", " "); //Reemplzamos los que tienen _ por espacio
        	var capitalizing_section = format_simbols_section.replace(/\b\w/g, l => l.toUpperCase()); //Ponemos las primeras en mayus
        	seccion = capitalizing_section.substring(0, capitalizing_section.length - 1); //Le quitamos la ultima 's' ya que todos son plural


    		//Class de color e icono de acuerdo a la seccion
			switch(seccion) {
			    case 'Persona':
                    class_color_seccion = 'bg-cyan';
                    icon_seccion = 'people';
                    articulo = 'una';
			        break;
			    case 'Producto':
                    class_color_seccion = 'bg-light-green';
                    icon_seccion = 'shopping_basket';
                    articulo = 'un';
			        break;
			    case 'Comida':
                    class_color_seccion = 'bg-deep-purple';
                    icon_seccion = 'local_pizza';
                    articulo = 'una';
			        break;
			    case 'Stock':
                    class_color_seccion = 'bg-orange';
                    icon_seccion = 'store';
                    articulo = 'un';
			        break;
			    case 'Venta':
                    class_color_seccion = 'bg-blue';
                    icon_seccion = 'attach_money';
                    articulo = 'una';
			        break;
                //Para los sub-secciones de seccion administrar
			    default:
                    class_color_seccion = 'bg-grey';
                    icon_seccion = 'https';
                    articulo = 'un/a';
			}
        	
        	//Mostramos notificacion solo si la seccion es distinta de vacia por el formateo usado, 
        	//ya que las demas siempre tienen valor.
    		if (seccion != ''){
	        	$("#ul-notificaciones").append('<li>'
		                                            +'<a href="javascript:void(0);">'
		                                                +'<div class="icon-circle '+class_color_seccion+'">'
		                                                    +'<i class="material-icons">'+icon_seccion+'</i>'
		                                                +'</div>'
		                                                +'<div class="menu-info">'
		                                                    +'<h6 class="col-black">Se ' + accion + ' ' + articulo + ' ' + seccion + '</h6>'
		                                                    +'<p>'
		                                                        +'<i class="material-icons">access_time</i> '+ tiempo
		                                                    +'</p>'
		                                                +'</div>'
		                                            +'</a>'
		                                        +'</li>'
		                                    );
    		}

        });
        
    }).fail(function(data) {
        if (data.error == "Unauthenticated") {
            showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
                                +'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                    +'CONECTARSE'
                                +'</a>', 'ATENCION', 'bg-red', 'error');
        }else {
            showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
        }
    });
}