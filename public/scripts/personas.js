$(document).ready(function(){
	activarMenu('personas');
	listar(); //Recibe id persona para el filtro
});

var token = $('meta[name="csrf-token"]').attr('content');

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	$("#form"+form+" select[name=id_province]").attr('disabled', true).selectpicker('refresh');
	$("#form"+form+" select[name=id_city]").attr('disabled', true).selectpicker('refresh');
}

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Persona';
var form = 'Persona';
var module = 'personas';
var modals_btns = 'People';

/* VARIABLES GLOBALES */ 
var tabla = $("#table_"+modals_btns+"");
var displayContentData = []; 
var displayTitleData = [
    { title: "Nombre Completo" },
    { title: "Cuit" },
    { title: "Tipo" },
    { title: "Opciones" }
];

//Filtrar por tipo de persona Solamente para filtros importantes, ya otro tipo de filtro busca por el busccar
function listar(){
    displayContentData = [];
	var route = current_route+"_listar";
	statusLoading('loading_list', 1, 'Actualizando Listado ..');
	
	var filtro_id_type_people = $("select[name=filtro_id_type_people]").val();
    
	$.get(route, function(result){
		$(result).each(function(key,value){
			if (value.id_type_people == filtro_id_type_people || filtro_id_type_people == 0){
				applyDataInTable(key, value);
			}
		});
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_list', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

//APLICAR DATA EN TABLA, SE APLICA DENTRO DEL FOREACH DESPUES DE OBTENER LA DATA
function applyDataInTable(key,value){
	var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentData.push([ 
		value.lastname+' '+value.name,
		(value.cuit != null) ? value.cuit : '', 
		value.type_people,
		'<div class="icon-button-demo">'
			+'<button type="button" href="#" data-toggle="modal" title="Editar" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
            	+'<i class="material-icons">edit</i>'
           	+'</button>'
         	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">delete</i>'
    		+'</button>'
		+'</div>'
	]);
}


/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	$.get(route, function(result){
		$("#form"+form+" input[name=name]").val(result[0]['name']);
		$("#form"+form+" input[name=lastname]").val(result[0]['lastname']);
		$("#form"+form+" input[name=email]").val(result[0]['email']);
		$("#form"+form+" input[name=cuit]").val(result[0]['cuit']);
		$("#form"+form+" input[name=adress]").val(result[0]['adress']);
		$("#form"+form+" input[name=floor]").val(result[0]['floor']);
		$("#form"+form+" input[name=department]").val(result[0]['department']);
		$("#form"+form+" input[name=phone_1]").val(result[0]['phone_1']);
		$("#form"+form+" input[name=phone_2]").val(result[0]['phone_2']);
		$("#form"+form+" select[name=personality]").val(result[0]['personality']).selectpicker('refresh');
		$("#form"+form+" select[name=id_condicion_iva]").val(result[0]['id_condicion_iva']).selectpicker('refresh');
		$("#form"+form+" select[name=id_type_people]").val(result[0]['id_type_people']).selectpicker('refresh');
		$("#form"+form+" select[name=id_country]").val(result[0]['id_country']).selectpicker('refresh');

		/*GENERAR SELECT DE PROVINCIAS DE ACUERDO A LA SELECCION EN PAIS Y ASIGNARLE EL VALOR SELECCIONADO*/
		changeDataSelectTarget('provincias', 'findByCountryId', 'id_province', 'Provincia', 'form'+form+'', result[0]['id_country'], result[0]['id_province']);
		/**/
		/*GENERAR SELECT DE CIUDADES DE ACUERDO A LA SELECCION EN PROVINCIAS Y ASIGNARLE EL VALOR SELECCIONADO*/
		changeDataSelectTarget('ciudades', 'findByProvinceId', 'id_city', 'Ciudad', 'form'+form+'', result[0]['id_province'], result[0]['id_city']);
		/**/

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}