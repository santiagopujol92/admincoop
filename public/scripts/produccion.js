$(document).ready(function(){
	activarMenu('produccion');
	listar(); //Recibe id persona para el filtro
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Producción';
var form = 'Produccion';
var module = 'produccion';
var modals_btns = 'Production';

/* VARIABLES GLOBALES */ 
var tabla = $("#table_"+modals_btns+"");
var displayContentData = []; 
var displayTitleData = [
    { title: "Empleado" },
    { title: "Lote" },
	{ title: "Turno" },
	{ title: "Material" },
	{ title: "Cantidad Prod." },
	{ title: "Máquina" },
	{ title: "Fecha Creación" },
    { title: "Opciones" }
];

var totalCantidadProdGeneralListado = 0;

//Filtrar por tipo de persona Solamente para filtros importantes, ya otro tipo de filtro busca por el busccar
function listar(){
	displayContentData = [];
	totalCantidadProdGeneralListado = 0;
	var route = current_route+"_listar";
	statusLoading('loading_list', 1, 'Actualizando Listado ..');
	
	var filtro_id_type_people = $("select[name=filtro_id_type_people]").val();
    
	$.get(route, function(result){
		$(result).each(function(key,value){
			if (value.id_type_people == filtro_id_type_people || filtro_id_type_people == 0){
				applyDataInTable(key, value);
			}
		});
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_list', 0);
		
		$("#totalCantidadProducto" + modals_btns).text(totalCantidadProdGeneralListado);

	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

//APLICAR DATA EN TABLA, SE APLICA DENTRO DEL FOREACH DESPUES DE OBTENER LA DATA
function applyDataInTable(key,value){
	var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

	totalCantidadProdGeneralListado = totalCantidadProdGeneralListado + parseFloat(value.quantity_product != null ? value.quantity_product : 0);

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentData.push([ 
		value.person_lastname+' '+value.person_name,
		(value.lote != null) ? value.lote : '', 
		(value.turno != null) ? value.turno : '', 
		(value.product_desc != null) ? value.product_desc : '',
		(value.quantity_product != null) ? value.quantity_product : '', 
		(value.machine_desc != null) ? value.machine_desc : '', 
		(value.created_at != null) ? formatDateTime(value.created_at, 'd-m-Y h:i', true) : '', 
		'<div class="icon-button-demo">'
			+'<button type="button" href="#" data-toggle="modal" title="Editar" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
            	+'<i class="material-icons">edit</i>'
           	+'</button>'
         	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">delete</i>'
    		+'</button>'
		+'</div>'
	]);
}


/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){
	$.get(route, function(result){
		$("#form"+form+" select[name=id_person]").val(result[0]['id_person']).selectpicker('refresh');
		$("#form"+form+" input[name=lote]").val(result[0]['lote']);
		$("#form"+form+" select[name=id_machine]").val(result[0]['id_machine']).selectpicker('refresh');
		$("#form"+form+" select[name=turno]").val(result[0]['turno']).selectpicker('refresh');
		$("#form"+form+" select[name=id_type_product]").val(result[0]['id_type_product']).selectpicker('refresh');
		changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Producto', 'form'+form, result[0]['id_type_product'], result[0]['id_product']);
		$("#form"+form+" input[name=quantity_product]").val(result[0]['quantity_product']);

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}