$(document).ready(function(){
	activarMenu('productos');
	listar();		    
});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Material';
var form = 'Producto';
var module = 'productos';
var modals_btns = 'Product';

/* VARIABLES GLOBALES */ 
var tabla = $("#table_"+modals_btns+"");
var displayContentData = []; 
var displayTitleData = [
	{ title: "Nombre" },
	{ title: "Código" },
	{ title: "Tipo Material" },
	{ title: "Precio Mínimo Compra" },
	{ title: "Precio Máximo Compra" },
	{ title: "Último Precio Compra" },
	{ title: "Último Precio Venta" },
	{ title: "Stock Disponible" },
	{ title: "Valor Stock" },
	{ title: "Opciones" },
];

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	changeLabelUnidad(null);
}

function listar(){
	displayContentData = []; 
	var route = current_route+"_listar";
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	var filtro_ult_minimo_precio_venta = $("input[name=filtro_ult_precio_minimo_venta]").val();
	var filtro_ult_maximo_precio_venta = $("input[name=filtro_ult_precio_maximo_venta]").val();
	var filtro_stock_minimo = $("input[name=filtro_stock_minimo]").val();
	var filtro_stock_maximo = $("input[name=filtro_stock_maximo]").val();
	var filtro_tipo_material = $("select[name=filtro_tipo_material]").val();

	$.get(route, function(result){

		$(result).each(function(key,value){
			// Agregar filtros, si cumple el filtro entonces se agrega al array y se muestra
			if ((filtro_ult_minimo_precio_venta == '' || filtro_ult_minimo_precio_venta <= value.last_sale_price)
				&& (filtro_ult_maximo_precio_venta == '' || filtro_ult_maximo_precio_venta >= value.last_sale_price)
				&& (filtro_stock_minimo == '' || filtro_stock_minimo <= value.stock)
				&& (filtro_stock_maximo == '' || filtro_stock_maximo >= value.stock)
				&& (filtro_tipo_material == null || jQuery.inArray(value.id_type_product.toString(), filtro_tipo_material) > -1)
				) {
				applyDataInTable(key, value);
			}
		});
				
		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_list', 0);

	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

//APLICAR DATA EN TABLA, SE APLICA DENTRO DEL FOREACH DESPUES DE OBTENER LA DATA
function applyDataInTable(key, value){
	var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentData.push([ 
		value.description,
		value.code_product = (value.code_product != null) ? value.code_product : '-',
		value.type_description, 
		min_purchase_price = (value.min_purchase_price != null) ? '$ ' + value.min_purchase_price : '-',
		max_purchase_price = (value.max_purchase_price != null) ? '$ ' + value.max_purchase_price : '-',
		last_purchase_price = (value.last_purchase_price != null) ? '$ ' + value.last_purchase_price : '-',
		last_sale_price = (value.last_sale_price != null) ? '$ ' + value.last_sale_price : '-',
		stock = (value.stock != null) ? '<b class="col-red">' + value.stock + '</b> ' + value.type_metric_product : '-',
		(value.stock != null) ? '<b class="col-green"> ' + (!isNaN(parseFloat(value.last_purchase_price)) ? '$ ' + (parseFloat(value.stock) * parseFloat(value.last_purchase_price)).toFixed(2) : '-') + '</b> ' : 0,
		'<div class="icon-button-demo">'
			+'<button type="button" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" title="Editar" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">edit</i>'
			+'</button>'
			+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">delete</i>'
			+'</button>'
			+'<button type="button" href="#" title="Aumentar Stock" data-backdrop="static" data-keyboard="false" onclick="abrirModalAumentarStock('+value.id+');" data-toggle="modal" data-target="#modalAumentarStock'+modals_btns+'" class="btn bg-green btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">add</i>'
			+'</button>'
		+'</div>'
	]);
}

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result.description);
		$("#form"+form+" input[name=code_product]").val(result.code_product);
		$("#form"+form+" select[name=id_type_product]").val(result.id_type_product).selectpicker('refresh');
		$("#form"+form+" select[name=type_metric_product]").val(result.type_metric_product).selectpicker('refresh');
		$("#form"+form+" input[name=min_purchase_price]").val(result.min_purchase_price);
		$("#form"+form+" input[name=max_purchase_price]").val(result.max_purchase_price);
		$("#form"+form+" input[name=last_purchase_price]").val(result.last_purchase_price);
		$("#form"+form+" input[name=last_sale_price]").val(result.last_sale_price);
		$("#form"+form+" input[name=max_quantity_stock]").val(result.max_quantity_stock);
		$("#form"+form+" input[name=min_quantity_stock]").val(result.min_quantity_stock);
		$("#form"+form+" input[name=stock]").val(result.stock);
		
		changeLabelUnidad(result.type_metric_product);

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+', true)');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+form);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}

/*ABRIR MODAL AUMENTAR STOCK*/
function abrirModalAumentarStock(id){
	//Cerramos modal de opciones
	$("#modal_opciones").modal('hide');
	statusLoading('loading_modal_aumentar_stock', 1);
	var route = current_route+"/"+id+"/edit";

	$.get(route, function(result){

		$("#formAumentarStock"+form+" input[name=increment_stock]").val(0);
		$("#formAumentarStock"+form+" input[name=stock]").val(result.stock);
		$("#formAumentarStock"+form+" button[name=btnguardarAumentarStock"+modals_btns+"]").attr('onclick', 'updateStock('+id+', true)');
		
		//Label
		$("#formAumentarStock"+form+" .label_unidad_product").text(' (por ' + result.type_metric_product + ')');

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("formAumentarStock"+form);
		statusLoading('loading_modal_aumentar_stock', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_aumentar_stock', 0);
	});
}

/*GUARDAR STOCK*/
function updateStock(id, validar){
	var route = current_route+"/"+id+"";
	var datos = $("#formAumentarStock"+form+"").serialize();

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("formAumentarStock"+form);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje_aumentar_stock', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}
	
	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal_aumentar_stock', 1, 'Guardando ..');
        },
		success: function(result){
			showMessageModal('Stock de ' + modulo_msg+' cargado  con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
			listar();
			$('#modalAumentarStock'+modals_btns+'').modal('hide');
			statusLoading('loading_modal_aumentar_stock', 0);
		},
		error: function(){
			showMessageModal('No se pudo Modificar el/la '+modulo_msg+'. <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_aumentar_stock', 0);
		}
	})	
}

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL AUMENTAR STOCK EJECUTAR ACTION DEL BOTON*/
$("#modalAumentarStock"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#formAumentarStock"+form+" button[name=btnguardarAumentarStock"+modals_btns+"]").click();
    }
});

// Evento al cambio de unidad cambio labels
function changeLabelUnidad(value) {
	var text = '';
	if (value != 0 && value != null){
		text = ' (por ' + value + ')';
	}
	// Labels
	$("#form"+form+" .label_unidad_product").text(text);
}
