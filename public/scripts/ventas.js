$(document).ready(function(){
	activarMenu('ventas');
	startDatePicker('.datetimepicker');
	startDatePicker('.datetimepickerFilter', null);

	listar();

});

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPELTAR PARA CADA ABM */
var modulo_msg = 'Venta';
var modals_btns = 'Sale';
var form = 'Venta';
var module = 'ventas';

var modulo_msg_remito = 'Remito Venta';
var formRemito = 'RemitoVenta';
var modals_btns_remito = 'RemitoVenta';

var modulo_msg_factura = 'Factura Venta';
var formFactura = 'FacturaVenta';
var modals_btns_factura = 'FacturaVenta';

var listaProductosAsignados = [];
var objetoFactura = {};

var objetoReciboVenta = {};
var listaChequesReciboAsignados = [];

// Para factura
localStorage.setItem('iva', 0);
localStorage.setItem('descuento', 0);
localStorage.setItem('retencion', 0);

// Para recibos
localStorage.setItem('total_cheques_recibo', 0);

//Setear boton para agregar
function configSaveButtonAddRemitoFactura(){
	$("#form"+formRemito+" button[name=btnguardar"+modals_btns_remito+"]").attr('onclick', 'agregarRemitoVenta(true)');
	$("#form"+formRemito+" button[name=btnguardar"+modals_btns_remito+"]").removeAttr('disabled');
	$("#form"+formRemito+" button[name=btnImprimir"+modals_btns_remito+"]").attr('disabled', 'disabled');
	$("#form"+formFactura+" button[name=btnguardar"+modals_btns_factura+"]").attr('onclick', 'agregarFacturaVenta(true)');

	var titulo = 'Agregar '+modulo_msg;
	$(".titulo-modal-abm").html(titulo.toUpperCase());
}

//Inicializar formulario como esta por default donde corresponda
function restartConfigFormRemitoFactura(){

	// Clear remito 
	$("#btnTab"+modals_btns_remito+"").click();
	clearForm('form'+formRemito);
	activeTabFactura(false);
	limpiarFormularioProductos();
	$("#form"+formRemito+" button[name=btnAsignarProducto"+modals_btns_remito+"]").removeAttr('disabled');
	
	//INICIANDO FORMULARIO REMITO VENTA

	//Obteniendo id para nueva venta
	obtenerNuevoIdVenta();

	// Por defecto el remito va en estado 4
	$("#form"+formRemito+" select[name=id_status_shipment]").val(1).selectpicker('refresh');
	
	// inicia en fecha de hoy
	$("#form"+formRemito+" input[name=sell_date]").val(getTodayDateFormated(new Date()));
	

	// Actualizo la lista de productos
	listaProductosAsignados = [];
	actualizarListaProductosAsignados();
	
	// -------------------------------

	// Clear de facutar y reiniciado
	clearForm('form'+formFactura);
	inicializarObjectoFactura();
	limpiarDatosFactura();

	//INICIANDO FORMULARIO FACTURA VENTA
	changeTipoCalculablesFactura('PORC', 'divSelectAlicuotaIvaPorc', 'divSelectAlicuotaIvaUnit', 'alicuota_iva_porc', 'alicuota_iva')
	changeTipoCalculablesFactura('PORC', 'divSelectDescuentoPorc', 'divSelectDescuentoUnit', 'descuento_porc', 'descuento')
	changeTipoCalculablesFactura('PORC', 'divSelectRetencionPorc', 'divSelectRetencionUnit', 'retencion_porc', 'retencion')
}

var displayContentDataRemito = []; 
var displayTitleDataRemito = [
	{ title: "Nro. Remito" },
	{ title: "Cliente" },
	{ title: "Estado Remito" },
	{ title: "Fecha Venta" },
	{ title: "Nro Factura" },
	{ title: "Estado Factura" },
	{ title: "Total Factura" },
	{ title: "Total Adeudado Factura" },
	{ title: "Cant. Recibos" },
	{ title: "Opciones" },
];

var totalFacturadoGeneralListado = 0;
var totalCantidadProductoGeneralListado = 0;

function listar(){
	displayContentDataRemito = []; 
	totalFacturadoGeneralListado = 0;
	totalCantidadProductoGeneralListado = 0;

    statusLoading('loading_list', 1, 'Actualizando Listado ..');
	var route = current_route+"_listar";
	var tabla = $("#table_"+modals_btns+"");

	var filtro_sell_date_desde = $("input[name=filtro_sell_date_desde]").val();
	var filtro_sell_date_hasta = $("input[name=filtro_sell_date_hasta]").val();
	var filtro_id_type_product = $("select[name=filtro_id_type_product]").val();
	var filtro_id_product = $("select[name=filtro_id_product]").val();

	$.get(route, function(result){
		$(result.data).each(function(key,value){
			var lst_ids_products = [];
			var lst_ids_type_products = [];
			var cant_producto_by_sale = 0;

			//Agregar los id de productos y tipos de producto que tiene cada venta en una propiedad del objeto desde el otro array
			// Recorro la lista productos por venta
			$(result.data_productos_by_invoice).each(function(index, producto){

				//Si coinciden los id venta voy acumulando los ids productos y type productos
				if (producto.id_sale == value.id) {
					lst_ids_products.push(producto.id_product);
					lst_ids_type_products.push(producto.id_type_product);
					cant_producto_by_sale = cant_producto_by_sale + producto.cantidad_producto;
				}
			});

			// Agrego los ids de typo products y id_products en el objeto
			value.ids_products = lst_ids_products;
			value.ids_type_products = lst_ids_type_products;
			value.cantidad_producto = cant_producto_by_sale;
			
			// Agregar filtros, si cumple el filtro entonces se agrega al array y se muestra
			if ((filtro_sell_date_desde == '' || filtro_sell_date_desde <= formatDateTime(value.sell_date, 'd-m-Y'))
				&& (filtro_sell_date_hasta == '' || filtro_sell_date_hasta >= formatDateTime(value.sell_date, 'd-m-Y'))
				&& (filtro_id_type_product == '' ||  value.ids_type_products.find(p => filtro_id_type_product) == filtro_id_type_product)
				&& (filtro_id_product == '' || filtro_id_product == 0 || value.ids_products.find(p => filtro_id_product) == filtro_id_product))
				{
				applyDataInTable(key, value, result.data_user);
			}

		});
		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleDataRemito, displayContentDataRemito);
		statusLoading('loading_list', 0);

		$("#totalFacturado" + modals_btns).text("$ " + totalFacturadoGeneralListado);
		$("#totalCantidadProducto" + modals_btns).text(totalCantidadProductoGeneralListado);

	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_list', 0);
	});
 }

 function applyDataInTable(key, value, data_user){
	// Si el rol es ROOT agrego el boton eliminar
	var boton_eliminar_venta = '';
	if (data_user.type == 2) {
		boton_eliminar_venta = '<button type="button" href="#" title="Eliminar" '+facturaPagada+' onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
			+'<i class="material-icons">delete</i>'
		+'</button>';
	}

	var funcion_opciones = "abrirModalOpciones('"+value.id+"', '"+modals_btns+"');";

	totalFacturadoGeneralListado = totalFacturadoGeneralListado + parseFloat(value.monto_factura != null ? value.monto_factura: 0);
	totalCantidadProductoGeneralListado = totalCantidadProductoGeneralListado + parseFloat(value.cantidad_producto != null ? value.cantidad_producto : 0);

	var tieneFactura = value.invoice_id > null ? '' : 'disabled="true"' ;
	var facturaPagada = value.monto_factura_deuda == 0 ? 'disabled="true"' : '';
	var colorEstadoFactura = value.desc_status_invoice == 'Pagada' ? 'col-green' : 'col-red'

	//carga del array contenido desde el array de la repsuesta, el orden va de la mano de el orden de los titulos
	displayContentDataRemito.push([ 
		value.id,
		value.person_lastname+' '+value.person_name,
		value.desc_status_shipment, 
		(value.sell_date != null) ? formatDateTime(value.sell_date, 'd-m-Y h:i', true) : '',
		value.invoice_id != null ? value.invoice_id: '-',
		value.desc_status_invoice != null ? '<b class="'+colorEstadoFactura+'">'+ value.desc_status_invoice +'</b>': '-', 
		(value.monto_factura != null) ? '$ ' + value.monto_factura : '-',
		(value.monto_factura_deuda != null) ? '$ ' + value.monto_factura_deuda : '-',
		(value.cant_recibos !== null) ? value.cant_recibos : 0,
		'<div class="icon-button-demo" style="width:200px">'
			+'<button type="button" href="#" title="Cargar Recibo" id="btnListadoCargarRecibo_'+value.invoice_id+'" '+facturaPagada+' '+tieneFactura+' data-backdrop="static" data-keyboard="false" onclick="abrirModalCargarRecibo('+value.monto_factura+', \''+value.person_name + " " + value.person_lastname+' \', '+value.balance_account+', '+value.id_current_account+', '+value.monto_factura_deuda+', '+value.invoice_id+');" data-toggle="modal" data-target="#modalCargarRecibo'+modals_btns+'" class="btn bg-green btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">add</i>'
			+'</button>'
			+'<button type="button" href="#" title="Ver Recibos" '+tieneFactura+' data-backdrop="static" data-keyboard="false" onclick="abrirModalVerRecibos('+value.invoice_id+', '+value.monto_factura+', \''+value.person_name + " " + value.person_lastname+' \', '+value.balance_account+', '+value.id_current_account+', '+value.monto_factura_deuda+');" data-toggle="modal" data-target="#modalVerRecibos'+modals_btns+'" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">receipt</i>'
			+'</button>'
			+'<button type="button" href="#" data-toggle="modal" title="Ver Venta" data-backdrop="static" data-keyboard="false" onclick="prepareModalDataEdit('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
				+'<i class="material-icons">search</i>'
			+'</button>'
			+boton_eliminar_venta
		+'</div>'
	]);
}

/* METODOS DE REMITO */

/*OBTENER Y MOSTRAR DATA PARA EDIT EN FORM*/
function getAndShowDataEdit(id, route){

	$("#btnTab"+modals_btns_remito+"").click();
	$("#form"+formRemito+" button[name=btnguardar"+modals_btns_remito+"]").attr('disabled', 'disabled');
	$("#form"+formRemito+" button[name=btnAsignarProducto"+modals_btns_remito+"]").attr('disabled', 'disabled');
	$("#form"+formRemito+" button[name=btnImprimir"+modals_btns_remito+"]").removeAttr('disabled');

	limpiarFormularioProductos();
	
	$.get(route, function(result){
		var data_venta =  result.data_venta[0];
		var data_invoice =  result.data_invoice[0];

		$("#form"+formRemito+" [id=label_sale_id]").text(data_venta.id);
		$("#form"+formRemito+" select[name=id_person]").val(data_venta.id_person).selectpicker('refresh');
		$("#form"+formRemito+" select[name=id_status_shipment]").val(data_venta.id_status_shipment).selectpicker('refresh');
		$("#form"+formRemito+" input[name=sell_cost]").val(data_venta.sell_cost);
		$("#form"+formRemito+" input[name=sell_date]").val(formatDateTime(data_venta.sell_date, 'd-m-Y h:i'));
		
		//Cargar productos a la lista para la tabla
		listaProductosAsignados = result.data_products_by_venta;
		actualizarListaProductosAsignados(false);

		//Checkeo el form para que pinte los inputs con sus colores correspondientes.
		checkFormShowingMsg("form"+formRemito);

		inicializarObjectoFactura();
		if (data_invoice) {
			// Si tiene factura 
			$("#form"+formFactura+" button[name=btnguardar"+modals_btns_factura+"]").attr('disabled', 'disabled');
			$("#form"+formFactura+" button[name=btnImprimir"+modals_btns_factura+"]").removeAttr('disabled');

			//Llenar objeto factura con la data de la base
			var person_desc = data_invoice.person_lastname + ' ' + data_invoice.person_name;
			var person_cuit = (data_invoice.person_cuit != null ? data_invoice.person_cuit : '');
			var person_domicilio = (data_invoice.adress != null ? data_invoice.adress : '');
			var person_iva = (data_invoice.condicion_iva_desc != null ? data_invoice.condicion_iva_desc : '');

			objetoFactura = {
				id_invoice: data_invoice.id,
				id_sale: data_invoice.id_sale, 
				
				// Data person
				id_person: data_invoice.id_person,
				person_desc: person_desc,
				person_cuit: person_cuit,
				person_domicilio: person_domicilio,
				person_iva: person_iva,

				issue_date: data_invoice.issue_date,
				id_status_invoice: data_invoice.id_status_invoice, 
				status_invoice_desc: data_invoice.status_invoice_desc,
				productos: listaProductosAsignados, // le pongo los mismos que tiene remito
				sell_cost: data_venta.sell_cost,
				total_amount: data_invoice.total_amount,
				retencion: data_invoice.retencion,
				alicuota_iva: data_invoice.alicuota_iva,
				descuento: data_invoice.descuento,
				retencion_porc: data_invoice.retencion_porc,
				alicuota_iva_porc: data_invoice.alicuota_iva_porc,
				descuento_porc: data_invoice.descuento_porc,
				expiration_date: data_invoice.expiration_date,
			};

			showDataCalculablesFactura(objetoFactura);
			bloquearCamposFacturaEdicion();
		} else {
			// Si no tiene factura
			$("#form"+formFactura+" button[name=btnguardar"+modals_btns_factura+"]").removeAttr('disabled');
			$("#form"+formFactura+" button[name=btnImprimir"+modals_btns_factura+"]").attr('disabled', 'disabled');
			
			//Llenar objeto factura con la data  de remito para crear nuevo
			objetoFactura = {
				id_invoice: $("#form"+formFactura+" [id=label_invoice_id]").text(),
				id_sale: $("#form"+formRemito+" [id=label_sale_id]").text(), 
				issue_date: $("#form"+formRemito+" input[name=sell_date]").val(),
				
				// Data Person
				id_person: $("#form"+formRemito+" select[name=id_person]").val(),
				person_desc: $("#form"+formRemito+" select[name=id_person] option:selected").text(),
				person_cuit: $("#form"+formRemito+" select[name=id_person] option:selected").attr('cuit'),
				person_domicilio: $("#form"+formRemito+" select[name=id_person] option:selected").attr('domicilio'),
				person_iva: $("#form"+formRemito+" select[name=id_person] option:selected").attr('condicion_iva'),

				id_status_invoice: 1, 
				status_invoice_desc: 'Emitida', 
				productos: listaProductosAsignados,
				sell_cost: $("#form"+formRemito+" input[name=sell_cost]").val(),
				total_amount: $("#form"+formRemito+" input[name=sell_cost]").val(),
				retencion: null,
				alicuota_iva: null,
				descuento: null,
				retencion_porc: null,
				alicuota_iva_porc: null,
				descuento_porc: null,
				expiration_date: null,
			};

			changeTipoCalculablesFactura('PORC', 'divSelectAlicuotaIvaPorc', 'divSelectAlicuotaIvaUnit', 'alicuota_iva_porc', 'alicuota_iva');
			changeTipoCalculablesFactura('PORC', 'divSelectDescuentoPorc', 'divSelectDescuentoUnit', 'descuento_porc', 'descuento');
			changeTipoCalculablesFactura('PORC', 'divSelectRetencionPorc', 'divSelectRetencionUnit', 'retencion_porc', 'retencion');
			desbloquearCamposFacturaEdicion();
		}

		cargarDatosAFactura();
		activeTabFactura(true);
		statusLoading('loading_modal', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal', 0);
	});
}

//Asignar nuevo producto para remito
function asignarProductoLista() {
	var id_product = $("#form"+formRemito+" select[name=id_product]").val();
	var nombre_producto_completo = $("#form"+formRemito+" select[name=id_product] option:selected").text();
	var quantity = parseFloat($("#form"+formRemito+" input[name=quantity]").val());
	var purchase_price = $("#form"+formRemito+" input[name=purchase_price]").val();

	var arr_nombre_producto = nombre_producto_completo.split('(');
	var nombre_producto = arr_nombre_producto[0];
	var stock_disponible_producto = parseFloat($("#form"+formRemito+" select[name=id_product] option:selected").attr('stock'));
	var code_product = $("#form"+formRemito+" select[name=id_product] option:selected").attr('code_product');
	var type_metric_product = $("#form"+formRemito+" select[name=id_product] option:selected").attr('type_metric_product');

	//Buscar en el array que el producto ingresado no exista ya en la tabla
	// if (listaProductosAsignados.length > 0 ) {
	// 	var existe_producto = listaProductosAsignados.find(p => p.id_product == id_product);
	// 	if (existe_producto){
	// 		showMessage('modal_messsage_productos_remito_venta', 'bg-orange','<strong>Atención ! </strong>El producto que quiere asignar ya está asignado en la lista', 'error');
	// 		return false;
	// 	}
	// }

	//Validcion general y cargamos el producto
	if (id_product != 0 && quantity != '' && purchase_price != ''){
		// Validacion de cantidad de stock
		if (quantity <= stock_disponible_producto){
			var objProducto = { 
				index: listaProductosAsignados.length + 1,
				code_product: code_product,
				id_product: id_product,
				nombre_producto: nombre_producto,
				quantity: quantity,
				purchase_price: purchase_price,
				type_metric_product: type_metric_product,
				precio_total_producto: parseFloat(quantity) * parseFloat(purchase_price),
				stock_disponible_producto: stock_disponible_producto
			}

			listaProductosAsignados.push(objProducto);
			//Actualizamos la tabla
			actualizarListaProductosAsignados();
			limpiarFormularioProductos()

			showMessage('modal_messsage_productos_remito_venta', 'bg-green','<strong> Bien ! </strong>Producto asignado con éxito', 'done');
		} else {
			showMessage('modal_messsage_productos_remito_venta', 'bg-orange','<strong>Atención ! </strong>La cantidad de producto ingresada esta superando el stock disponible del mismo', 'error');
		}
	} else {
		showMessage('modal_messsage_productos_remito_venta', 'bg-orange','<strong>Atención ! </strong>Debe completar los campos Producto, Cantidad y Precio Venta para agregar un producto', 'error');
	}
}

//Actualizar la tabla de productos
function actualizarListaProductosAsignados(activarBotonesEliminar = true) {
	var tabla = $("#tbody_productos_asignados");
	tabla.html('');
	var precio_total_remito = 0;
	var total_cantidad_prod = 0;
	var disabled = (!activarBotonesEliminar) ? 'disabled="disabled"' : '';

	$(listaProductosAsignados).each(function(key,value){
		tabla.append('<tr>'
						+'<td>'+(value.code_product != null ? value.code_product : '')+'</td>'
			            +'<td>'+value.nombre_producto+'</td>'
									+'<td>'+value.quantity+'</td>'
									+'<td>'+value.type_metric_product+'</td>'
			            +'<td>$ '+value.purchase_price+'</td>'
			            +'<td><b class="col-red">$ '+value.precio_total_producto+'</b></td>'
			            +'<td>'
			               +'<button type="button" '+disabled+' onclick="quitarProductoAsignado('+value.index+')" title="Quitar Producto" class="btn bg-red btn-circle waves-effect">'
			                    +'<i class="material-icons">delete</i>'
			                +'</button>'
			            +'</td>'
			        +'</tr>'
		);
		precio_total_remito = parseFloat(precio_total_remito) + parseFloat(value.precio_total_producto);
		total_cantidad_prod = parseFloat(total_cantidad_prod) + parseFloat(value.quantity);
	});

	precio_total_remito = (precio_total_remito > 0) ? precio_total_remito.toFixed(2) : '';
	total_cantidad_prod = (total_cantidad_prod > 0) ? total_cantidad_prod.toFixed(2) : 0;

	//Actualizamos valor total del remito
	$("#form"+formRemito+" input[name=sell_cost]").val(precio_total_remito);

	//Actualizamos cantidad total de producto
	$("#form"+formRemito+" [name=labelTotalCantProd]").text(total_cantidad_prod);
	$("#form"+formFactura+" [name=labelTotalCantProd]").text(total_cantidad_prod);
}

//Quitar producto de la tabla de productos
function quitarProductoAsignado(index) {
	var indiceElemento = listaProductosAsignados.find(p => p.index == index);
	listaProductosAsignados.splice(indiceElemento, 1);
	actualizarListaProductosAsignados();
}

//Agregar Remito Venta
function agregarRemitoVenta(validar){
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+formRemito);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}

	var route = current_route+"";
	var datos = $("#form"+formRemito+"").serialize();
	datos += '&productos=' +  JSON.stringify(listaProductosAsignados);
	datos += '&sell_cost=' +  $("#form"+formRemito+" input[name=sell_cost]").val();
	datos += '&id_status_shipment=' +  1; // Siempre Se crea como activo, se agrega aca porq esta disabled

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data:datos,
		beforeSend: function () {
			statusLoading('loading_modal', 1, 'Agregando '+modulo_msg_remito+' ..');
		},
		success: function(result){
			showMessage('modal_mensaje', 'bg-green', '<strong>Buen Trabajo!</strong> Remito de venta agregado con éxito. Y stock de productos asignados descontados con éxito ', 'done');
			actualizarListaProductosAsignados(false);
			$("#form"+formRemito+" button[name=btnAsignarProducto"+modals_btns_remito+"]").attr('disabled', 'disabled');
			$("#form"+formRemito+" button[name=btnguardar"+modals_btns_remito+"]").attr('disabled', 'disabled');
			$("#form"+formRemito+" button[name=btnImprimir"+modals_btns_remito+"]").removeAttr('disabled');

			listar();
			statusLoading('loading_modal', 0);

			// Preparacion para dirigirse a factura
			$("#form"+formFactura+" button[name=btnguardar"+modals_btns_factura+"]").removeAttr('disabled');
			$("#form"+formFactura+" button[name=btnImprimir"+modals_btns_factura+"]").attr('disabled', 'disabled');

			//Llenar objeto factura con la nueva data
			// cargarNuevoIdFactura();
			objetoFactura = {
				id_invoice: result.id_new_invoice,
				id_sale: $("#form"+formRemito+" [id=label_sale_id]").text(), 
				id_person: $("#form"+formRemito+" select[name=id_person]").val(),
				person_desc: $("#form"+formRemito+" select[name=id_person] option:selected").text(),
				issue_date: $("#form"+formRemito+" input[name=sell_date]").val(),
				id_status_invoice: 1, 
				status_invoice_desc: 'Emitida', 
				productos: listaProductosAsignados,
				total_amount: $("#form"+formRemito+" input[name=sell_cost]").val(),
				retencion: null,
				alicuota_iva: null,
				descuento: null,
				retencion_porc: null,
				alicuota_iva_porc: null,
				descuento_porc: null,
				expiration_date: null,
			};
			
			activeTabFactura(true);
			desbloquearCamposFacturaEdicion();
			cargarDatosAFactura();
			$("#btnTab"+modals_btns_factura+"").click();
		},
		error: function(result){
			showMessage('modal_mensaje', 'bg-red', '<strong>Atención!</strong> No se pudo Agregar el ' + modulo_msg_remito + '. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'warning');
			statusLoading('loading_modal', 0);
		}
	});
}

function obtenerNuevoIdVenta() {
	var route = current_route+"_getNewSaleId";

	$.get(route, function(result){
		$("#form"+formRemito+" [id=label_sale_id]").text(result.id_new_sale);
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
	});
}

function updateRemitoVenta(id, validar) {
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+formRemito);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}

	var route = current_route+"/" + id;
	var datos = $("#form"+formRemito+"").serialize();
	datos += '&sell_cost=' +  $("#form"+formRemito+" input[name=sell_cost]").val();

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data:datos,
		beforeSend: function () {
			statusLoading('loading_modal', 1, 'Modificando '+modulo_msg_remito+' ..');
		},
		success: function(result){
			showMessage('modal_mensaje', 'bg-green', '<strong>Buen Trabajo!</strong> ' + modulo_msg_remito + ' Modificado con Éxito ', 'done');
			listar();
			statusLoading('loading_modal', 0);
		},
		error: function(result){
			showMessage('modal_mensaje', 'bg-red', '<strong>Atención!</strong> No se pudo Modificar el ' + modulo_msg_remito + '. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'warning');
			statusLoading('loading_modal', 0);
		}
	});
}

function showDataProduct(formId, nameSelect, valueSelected) {
	var stock_disponible_producto = '';
	var last_sale_price = ''
	var type_metric_product = ''

	if (valueSelected != 0) {
		stock_disponible_producto = parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('stock'));
		last_sale_price = parseFloat($("#"+formId+" select[name="+nameSelect+"] option:selected").attr('last_sale_price'));
		type_metric_product = $("#"+formId+" select[name="+nameSelect+"] option:selected").attr('type_metric_product');
		
		$("#"+formId+" label[name=labelStockForProduct]").html(stock_disponible_producto + ' ' + type_metric_product);
		$("#"+formId+" label[name=labelLastPriceForProduct]").html("$ " +last_sale_price);
		$("#"+formId+" label[name=labelTypeMetricForProduct]").html(type_metric_product);
		
		$("#"+formId+" h6[name=datosProductoSelected]").removeClass('hidden');
	} else {
		$("#"+formId+" h6[name=datosProductoSelected]").addClass('hidden');
	}
}

function limpiarFormularioProductos () {
	$("#form"+formRemito+" select[name=id_type_product]").val(0).selectpicker('refresh');
	changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Producto', 'form'+formRemito, 0);
	$("#form"+formRemito+" input[name=quantity]").val('');
	$("#form"+formRemito+" input[name=purchase_price]").val('');
	showDataProduct("form"+formRemito, 'id_product', 0);
}

function activeTabFactura(activar) {
	if (activar) {
		$("#liTab"+modals_btns_factura+"").removeClass('disabled-li');
	} else {
		$("#liTab"+modals_btns_factura+"").addClass('disabled-li');
	}
}

/* METODOS DE FACTURA */

function limpiarDatosFactura() {
	objetoFactura = {};
	// Limpiar INPUTS DE FACTURA
}

// Get new id invoice
function cargarNuevoIdFactura() {
	var route = current_route+"_getNewInvoiceId";

	$.get(route, function(result){
		$("#form"+formFactura+" [id=label_invoice_id]").text(result.id_new_invoice);
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
	});	
}

function cargarDatosAFactura() {
	$("#form"+formFactura+" [id=label_invoice_id]").text(objetoFactura.id_invoice);
	$("#form"+formFactura+" [name=id_sale]").text(objetoFactura.id_sale);
	$("#form"+formFactura+" [name=issue_date]").text(objetoFactura.issue_date);
	$("#form"+formFactura+" [name=person_desc]").text(objetoFactura.person_desc);
	$("#form"+formFactura+" [name=status_invoice_desc]").text(objetoFactura.status_invoice_desc);
	$("#form"+formFactura+" input[name=expiration_date]").val(formatDateTime(objetoFactura.expiration_date, 'd-m-Y'));
	$("#form"+formFactura+" input[name=descuento]").val(objetoFactura.descuento);
	$("#form"+formFactura+" input[name=alicuota_iva]").val(objetoFactura.alicuota_iva);
	$("#form"+formFactura+" input[name=retencion]").val(objetoFactura.retencion);
	$("#form"+formFactura+" input[name=descuento_porc]").val(objetoFactura.descuento_porc);
	$("#form"+formFactura+" input[name=alicuota_iva_porc]").val(objetoFactura.alicuota_iva_porc);
	$("#form"+formFactura+" input[name=retencion_porc]").val(objetoFactura.retencion_porc);
	mostrarProductosFacturados();
	$("#form"+formFactura+" input[name=total_amount]").val(objetoFactura.total_amount);
}

// Mostrar productos que se cargaron
function mostrarProductosFacturados() {
	var tabla = $("#tbody_productos_facturados");
	tabla.html('');

	$(objetoFactura.productos).each(function(key,value){
		tabla.append('<tr>'
						+'<td>'+(value.code_product != null ? value.code_product :  '')+'</td>'
						+'<td>'+value.nombre_producto+'</td>'
						+'<td>'+value.quantity+'</td>'
						+'<td>'+value.type_metric_product+'</td>'
						+'<td>$ '+value.purchase_price+'</td>'
						+'<td><b class="col-red">$ '+value.precio_total_producto+'</b></td>'
					+'</tr>'
		);
	});
}

//Agregar Factura Venta
function agregarFacturaVenta(validar){
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		var array_validate = checkFormShowingMsg("form"+formFactura);
		var msg = '';
		if (array_validate['puedeGuardar'] == false){
			msg = array_validate['msg'];
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong>'+msg+'</label> ', 'error');	
			return false;
		}
	}

	var facturaInsert = {
		id_sale: objetoFactura.id_sale, 
		id_person: objetoFactura.id_person,
		issue_date: objetoFactura.issue_date != '' ? objetoFactura.issue_date.replace('/', '-').replace('/', '-') : null,
		id_status_invoice: objetoFactura.id_status_invoice, 
		total_amount: $("#form"+formFactura+" input[name=total_amount]").val(),
		retencion: $("#form"+formFactura+" input[name=retencion]").val() !== '' ? $("#form"+formFactura+" input[name=retencion]").val() : null,
		alicuota_iva: $("#form"+formFactura+" input[name=alicuota_iva]").val() !== '' ? $("#form"+formFactura+" input[name=alicuota_iva]").val() : null,
		descuento: $("#form"+formFactura+" input[name=descuento]").val() !== '' ? $("#form"+formFactura+" input[name=descuento]").val() : null,
		retencion_porc: $("#form"+formFactura+" input[name=retencion_porc]").val() !== '' ? $("#form"+formFactura+" input[name=retencion_porc]").val() : null,
		alicuota_iva_porc: $("#form"+formFactura+" input[name=alicuota_iva_porc]").val() !== '' ? $("#form"+formFactura+" input[name=alicuota_iva_porc]").val() : null,
		descuento_porc: $("#form"+formFactura+" input[name=descuento_porc]").val() !== '' ? $("#form"+formFactura+" input[name=descuento_porc]").val() : null,
		expiration_date: $("#form"+formFactura+" input[name=expiration_date]").val() !== '' ? $("#form"+formFactura+" input[name=expiration_date]").val().replace('/', '-').replace('/', '-') : null,
	}

	var route = current_route+'/InsertInvoiceSale/' + JSON.stringify(facturaInsert);

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			statusLoading('loading_modal', 1, 'Agregando '+modulo_msg_factura+' ..');
		},
		success: function(result){
			showMessage('modal_mensaje', 'bg-green', '<strong>Buen Trabajo!</strong> Factura agregada con éxito. Y generación de credito en cuenta corriente de cliente ', 'done');
			actualizarListaProductosAsignados(false);
			$("#form"+formFactura+" button[name=btnguardar"+modals_btns_factura+"]").attr('disabled', 'disabled');
			$("#form"+formFactura+" button[name=btnImprimir"+modals_btns_factura+"]").removeAttr('disabled');

			// Guardar datos factura en objeto factura
			var data_invoice = result.data_invoice[0];
			objetoFactura.id_invoice = data_invoice.id;
			objetoFactura.person_desc = data_invoice.person_lastname + ' ' + data_invoice.person_name;
			objetoFactura.person_cuit = (data_invoice.person_cuit != null ? data_invoice.person_cuit : '');
			objetoFactura.person_domicilio = (data_invoice.adress != null ? data_invoice.adress : '');
			objetoFactura.person_iva = (data_invoice.condicion_iva_desc != null ? data_invoice.condicion_iva_desc : '');
			objetoFactura.sell_cost = data_invoice.sell_cost;
			objetoFactura.total_amount = data_invoice.total_amount;
			objetoFactura.expiration_date = data_invoice.expiration_date;
			objetoFactura.retencion = data_invoice.retencion;
			objetoFactura.alicuota_iva = data_invoice.alicuota_iva;
			objetoFactura.descuento = data_invoice.descuento;
			objetoFactura.retencion_porc = data_invoice.retencion_porc;
			objetoFactura.alicuota_iva_porc = data_invoice.alicuota_iva_porc;
			objetoFactura.descuento_porc = data_invoice.descuento_porc;

			listar();
			statusLoading('loading_modal', 0);
			
			// Abrir modal recibos
			$("#btnModalConfirmAddReciboAfterAlta").attr('onclick', 'abrirCargarRecibosYAfterAlta('+objetoFactura.total_amount + ', \'' + objetoFactura.person_desc +' \'' + ', \'\'' +', \'\'' + ', ' + objetoFactura.total_amount + ', ' + result.id_factura+')');
			$("#btnOpenModalConfirmAddReciboAfterAlta").click();
		},
		error: function(result){
			showMessage('modal_mensaje', 'bg-red', '<strong>Atención!</strong> No se pudo Agregar la ' + modulo_msg_factura + '. <br>Complete los campos obligatorios(*) y correctamente<br> Puede que lo que este guardando ya exista y/o algun campo ya este en uso.', 'warning');
			statusLoading('loading_modal', 0);
		}
	});
}

function inicializarObjectoFactura() {
	objetoFactura = {
		id_invoice: '',
		id_sale: '', 
		person_desc: '',
		issue_date: '',
		status_invoice_desc: '', 
	};
}

function bloquearCamposFacturaEdicion() {
	$("#form"+formFactura+" input[name=expiration_date]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=descuento]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=alicuota_iva]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=retencion]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=descuento_porc]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=alicuota_iva_porc]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=retencion_porc]").attr('disabled', 'disabled');
	$("#form"+formFactura+" input[name=total_amount]").attr('disabled', 'disabled');
	$("#form"+formFactura+" select[name=selectAlicuotaIva]").attr('disabled', 'disabled').selectpicker('refresh');
	$("#form"+formFactura+" select[name=selectDescuento]").attr('disabled', 'disabled').selectpicker('refresh');
	$("#form"+formFactura+" select[name=selectRetencion]").attr('disabled', 'disabled').selectpicker('refresh');
}

function desbloquearCamposFacturaEdicion() {
	$("#form"+formFactura+" input[name=expiration_date]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=descuento]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=alicuota_iva]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=retencion]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=descuento_porc]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=alicuota_iva_porc]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=retencion_porc]").removeAttr('disabled');
	$("#form"+formFactura+" input[name=total_amount]").removeAttr('disabled');
	$("#form"+formFactura+" select[name=selectAlicuotaIva]").removeAttr('disabled').selectpicker('refresh');
	$("#form"+formFactura+" select[name=selectDescuento]").removeAttr('disabled').selectpicker('refresh');
	$("#form"+formFactura+" select[name=selectRetencion]").removeAttr('disabled').selectpicker('refresh');
}

function calcularMontoConVariables(calculo, porcentual, nombre_variable) {
	var total_amount = objetoFactura.total_amount;
	var variable = 0;
	if (calculo !== ''){
		// Si es porcentual
		if (porcentual) {
			variable = (parseFloat(total_amount) * parseFloat(calculo)) / 100;
			// Si no
		} else {
			variable = calculo;
		}
	}

	if (nombre_variable == 'iva'){
		localStorage.setItem('iva', parseFloat(variable).toFixed(2))
	} else if (nombre_variable == 'descuento') {
		localStorage.setItem('descuento', parseFloat(variable).toFixed(2))
	} else if (nombre_variable == 'retencion') {
		localStorage.setItem('retencion', parseFloat(variable).toFixed(2))
	}
	sumarCalculosVariables();
}

function sumarCalculosVariables() {
	var new_total_amount = parseFloat(objetoFactura.total_amount) + parseFloat(localStorage.getItem('iva')) - parseFloat(localStorage.getItem('descuento')) - parseFloat(localStorage.getItem('retencion'));
	
	if (!isNaN(new_total_amount)){
		$("#form"+formFactura+" input[name=total_amount]").val(parseFloat(new_total_amount).toFixed(2));
	} else {
		$("#form"+formFactura+" input[name=total_amount]").val(parseFloat(objetoFactura.total_amount).toFixed(2));
	}
}

function changeTipoCalculablesFactura(value, divPorc, divUnit, inputPorc, inputUnit) {
	if (value == 'PORC') {
		$("#form"+formFactura+" div[name="+divUnit+"]").hide();
		$("#form"+formFactura+" input[name="+inputUnit+"]").val(null);
		$("#form"+formFactura+" div[name="+divPorc+"]").show();
	} else {
		$("#form"+formFactura+" div[name="+divPorc+"]").hide();
		$("#form"+formFactura+" input[name="+inputPorc+"]").val(null);
		$("#form"+formFactura+" div[name="+divUnit+"]").show();
	}
}

function showDataCalculablesFactura(element) {
	// Alicuota Iva
	if (element.alicuota_iva !== null) {
		changeTipoCalculablesFactura('UNIT', 'divSelectAlicuotaIvaPorc', 'divSelectAlicuotaIvaUnit', 'alicuota_iva_porc', 'alicuota_iva')
		$("#form"+formFactura+" select[name=selectAlicuotaIva]").val('UNIT').selectpicker('refresh');
	// Alicuota Iva %
	} else {
		changeTipoCalculablesFactura('PORC', 'divSelectAlicuotaIvaPorc', 'divSelectAlicuotaIvaUnit', 'alicuota_iva_porc', 'alicuota_iva')
		$("#form"+formFactura+" select[name=selectAlicuotaIva]").val('PORC').selectpicker('refresh');
	}

	// Descuento
	if (element.descuento !== null) {
		changeTipoCalculablesFactura('UNIT', 'divSelectDescuentoPorc', 'divSelectDescuentoUnit', 'descuento_porc', 'descuento')
		$("#form"+formFactura+" select[name=selectDescuento]").val('UNIT').selectpicker('refresh');
	// Descuento %
	} else {
		changeTipoCalculablesFactura('PORC', 'divSelectDescuentoPorc', 'divSelectDescuentoUnit', 'descuento_porc', 'descuento')
		$("#form"+formFactura+" select[name=selectDescuento]").val('PORC').selectpicker('refresh');
	}

	// Retencion
	if (element.retencion !== null) {
		changeTipoCalculablesFactura('UNIT', 'divSelectRetencionPorc', 'divSelectRetencionUnit', 'retencion_porc', 'retencion')
		$("#form"+formFactura+" select[name=selectRetencion]").val('UNIT').selectpicker('refresh');
	// Retencion %
	} else {
		changeTipoCalculablesFactura('PORC', 'divSelectRetencionPorc', 'divSelectRetencionUnit', 'retencion_porc', 'retencion')
		$("#form"+formFactura+" select[name=selectRetencion]").val('PORC').selectpicker('refresh');
	}

}

// RECIBOS
function abrirModalCargarRecibo(monto_venta, nombre_persona, balance_account, id_current_account, monto_venta_deuda, id_invoice = null) {
	var route = root_project+"ventas_getInvoicesData";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'GET',
		dataType: 'json',
        beforeSend: function () {
         	statusLoading('loading_modal_cargar_recibo', 1, 'Cargando Operación..');
        },
		success: function(result){
			$("#CargarRecibo_id_invoice").html('<option value="0">Seleccione una Factura</option>');
			$(result.factura).each(function(key, factura){
				$("#CargarRecibo_id_invoice").append('<option total_amount_deuda="'+factura.total_amount_deuda+'" id_current_account="'+factura.id_current_account+'" balance_account="'+factura.balance_account+'" monto_venta="'+factura.total_amount+'" value="'+factura.id+'">Factura N: '+factura.id+' - '+factura.person_lastname+' '+factura.person_name+' - Monto: $ '+factura.total_amount+' - Monto Deuda: $ '+factura.total_amount_deuda+' - '+factura.issue_date+' '+(factura.cuit != null ? factura.cuit : '')+'</option>');
				id_current_account = factura.id_current_account;
				balance_account = factura.total_amount;
			});
			$("#CargarRecibo_id_invoice").selectpicker('refresh');

			clearForm("formCargarReciboYMovimiento");
			listaChequesReciboAsignados = [];
			actualizarListaChequesRecibosAsignados();

			$("#label_persona_cargar_recibo"+modals_btns+"").text('');
			$("#label_factura_cargar_recibo"+modals_btns+"").text('');
			$("#label_total_monto_factura"+modals_btns+"").text('');
			$("#label_monto_adeudado_recibos"+modals_btns+"").text('');

			if (id_invoice == undefined) {
				$("#CargarRecibo_id_invoice").removeAttr('disabled');
				$("#CargarRecibo_id_invoice").val(0).selectpicker('refresh');
			} else {
				$("#CargarRecibo_id_invoice").val(id_invoice).selectpicker('refresh');
				$("#CargarRecibo_id_invoice").attr('disabled', true).selectpicker('refresh');

				// Seteo el formulario para insercion de recibo y movimiento
				$("#formCargarReciboYMovimiento input[name=id_invoice]").val(id_invoice);
				$("#formCargarReciboYMovimiento input[name=monto_venta]").val(monto_venta);
				$("#formCargarReciboYMovimiento input[name=id_current_account]").val(id_current_account);
				$("#formCargarReciboYMovimiento input[name=balance]").val(balance_account);

				// Cargamos data cuenta:
				$("#label_persona_cargar_recibo"+modals_btns+"").text(nombre_persona);
				$("#label_factura_cargar_recibo"+modals_btns+"").text(id_invoice);
				$("#label_total_monto_factura"+modals_btns+"").text(monto_venta);
				$("#label_monto_adeudado_recibos"+modals_btns+"").text(monto_venta_deuda);

				coloresMontoYMontoDeudaCargarRecibo(monto_venta, monto_venta_deuda);
			}
			statusLoading('loading_modal_cargar_recibo', 0);
		},
		error: function(){
			showMessageModal('Error al obtener información de facturas', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_cargar_recibo', 0);
		}
	});
	
	
}

// INSERTAR RECIBO
function insertReciboByInvoice(validar) {
	var route = root_project+"recibos";
	var datos = $("#formCargarReciboYMovimiento").serialize();
	datos += '&cheques=' +  JSON.stringify(listaChequesReciboAsignados);

	if (!verificarTotalDeReciboConMontoAdeudado()) {
		return false;
	}

	if ($("#CargarRecibo_id_invoice").val() == 0){
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>Debe seleccionar una factura para cargar un recibo</label> ', 'error');	
		return false;
	}

	if (parseFloat($("#formCargarReciboYMovimiento input[name=total_amount]").val()) == 0.00) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>El total del recibo debe ser mayor a 0</label> ', 'error');	
		return false;
	}

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal_cargar_recibo', 1, 'Cargando Operación..');
        },
		success: function(result){
			showMessageModal('Recibo de Factura de ' + modulo_msg+' cargado con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');

			// Actualizar input de balance si todo sale ok
			var total_amount = $("#formCargarReciboYMovimiento input[name=total_amount]").val();
			$("#formCargarReciboYMovimiento input[name=balance]").val(parseFloat($("#formCargarReciboYMovimiento input[name=balance]").val()) - parseFloat(total_amount));

			statusLoading('loading_modal_cargar_recibo', 0);
			clearForm("formCargarReciboYMovimiento");
			$('#modalCargarRecibo'+modals_btns+'').modal('hide');
			listar();
		},
		error: function(){
			showMessageModal('No se pudo Generar la operación <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
			statusLoading('loading_modal_cargar_recibo', 0);
		}
	});
}

// CHEQUES
//Asignar nuevo cheque para recibo
function asignarChequeRecibosLista() {
	var nro_cheque = $("#formCargarReciboYMovimiento input[name=nro_cheque]").val();
	var banco = $("#formCargarReciboYMovimiento input[name=banco]").val();
	var concepto = $("#formCargarReciboYMovimiento input[name=concepto]").val();
	var nombre = $("#formCargarReciboYMovimiento input[name=nombre]").val();
	var cuit = $("#formCargarReciboYMovimiento input[name=cuit]").val();
	var importe = parseFloat($("#formCargarReciboYMovimiento input[name=importe]").val());
	var fecha_emision = $("#formCargarReciboYMovimiento input[name=fecha_emision]").val();
	var fecha_pago = $("#formCargarReciboYMovimiento input[name=fecha_pago]").val();

	//Validcion general y cargamos el cheque
	if (nro_cheque != '' && banco != '' && nombre != '' && cuit != '' && importe != ''){
		var objCheque = {
			id_cheque: listaChequesReciboAsignados.length + 1,
			nro_cheque: nro_cheque,
			banco: banco,
			concepto: concepto,
			nombre: nombre,
			cuit: cuit,
			importe: parseFloat(importe).toFixed(2),
			fecha_emision: fecha_emision,
			fecha_pago: fecha_pago,
		}

		listaChequesReciboAsignados.push(objCheque);
		//Actualizamos la tabla
		actualizarListaChequesRecibosAsignados();
		limpiarFormularioChequesRecibo()

		showMessage('modal_messsage_cheques_recibo', 'bg-green','<strong> Bien ! </strong>Cheque asignado con éxito', 'done');
		
	} else {
		showMessage('modal_messsage_cheques_recibo', 'bg-orange','<strong>Atención ! </strong>Debe completar los campos Nro Cheque, Banco, Nombre, Cuit e Importe para agregar un cheque', 'error');
	}
}

//Actualizar la tabla de cheques
function actualizarListaChequesRecibosAsignados(activarBotonesEliminar = true) {
	var tabla = $("#tbody_chequesrecibo_asignados");
	tabla.html('');
	var importe_total_cheques = 0;
	var disabled = (!activarBotonesEliminar) ? 'disabled="disabled"' : '';

	$(listaChequesReciboAsignados).each(function(key,value){
		tabla.append('<tr>'
						+'<td>'+value.nro_cheque+'</td>'
						+'<td>'+value.banco+'</td>'
						+'<td><b class="col-red">$ '+value.importe+'</b></td>'
						+'<td>'+value.fecha_emision+'</td>'
						+'<td>'+value.concepto+'</td>'
						+'<td>'+value.nombre+'</td>'
						+'<td>'+value.cuit+'</td>'
						+'<td>'+value.fecha_pago+'</td>'
			            +'<td>'
			               +'<button type="button" '+disabled+' onclick="quitarChequeReciboAsignado('+value.id_cheque+')" title="Eliminar Cheque" class="btn bg-red btn-circle waves-effect">'
			                    +'<i class="material-icons">delete</i>'
			                +'</button>'
			            +'</td>'
			        +'</tr>'
		);
		importe_total_cheques = parseFloat(importe_total_cheques) + parseFloat(value.importe);
	});

	importe_total_cheques = (importe_total_cheques > 0) ? importe_total_cheques.toFixed(2) : 0;

	//Actualizamos valor total del los cheques
	localStorage.setItem('total_cheques_recibo', importe_total_cheques);
	calcularTotalReciboFinal();
}

//Quitar producto de la tabla de productos
function quitarChequeReciboAsignado(id_cheque) {
	var indiceElemento = listaChequesReciboAsignados.find(p => p.id_cheque == id_cheque);
	listaChequesReciboAsignados.splice(indiceElemento, 1);
	actualizarListaChequesRecibosAsignados();
}

function limpiarFormularioChequesRecibo() {
 	$("#formCargarReciboYMovimiento input[name=nro_cheque]").val('');
	$("#formCargarReciboYMovimiento input[name=banco]").val('');
	$("#formCargarReciboYMovimiento input[name=concepto]").val('');
	$("#formCargarReciboYMovimiento input[name=nombre]").val('');
	$("#formCargarReciboYMovimiento input[name=cuit]").val('');
	$("#formCargarReciboYMovimiento input[name=importe]").val('');
	$("#formCargarReciboYMovimiento input[name=fecha_emision]").val('');
	$("#formCargarReciboYMovimiento input[name=fecha_pago]").val('');
}

function calcularTotalReciboFinal() {
	var effective_amount = parseFloat($("#formCargarReciboYMovimiento input[name=effective_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=effective_amount]").val() : 0);
	var card_amount = parseFloat($("#formCargarReciboYMovimiento input[name=card_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=card_amount]").val() : 0);
	var retencion_amount = parseFloat($("#formCargarReciboYMovimiento input[name=retencion_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=retencion_amount]").val() : 0);
	var retencion2_amount = parseFloat($("#formCargarReciboYMovimiento input[name=retencion2_amount]").val() != '' ? $("#formCargarReciboYMovimiento input[name=retencion2_amount]").val() : 0);
	
	var total_recibo_final = effective_amount + card_amount + parseFloat(localStorage.getItem('total_cheques_recibo')) - retencion_amount - retencion2_amount;

	if (!isNaN(total_recibo_final)){
		$("#formCargarReciboYMovimiento input[name=total_amount]").val(parseFloat(total_recibo_final).toFixed(2));
	}
}

function verificarTotalDeReciboConMontoAdeudado() {
	var monto_adeudado = parseFloat($("#label_monto_adeudado_recibos"+modals_btns+"").text());
	var total_amount = parseFloat($("#formCargarReciboYMovimiento input[name=total_amount]").val());

	if (total_amount > monto_adeudado) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>El total de recibo ingresado supera el monto total de la factura</label> ', 'error');	
		return false;
	}

	if (total_amount == 0 && monto_adeudado == 0) {
		showMessage('modal_mensaje_cargar_recibo', 'bg-orange','<strong>Atención ! </strong>La factura esta pagada por completo</label> ', 'error');	
		return false;
	}

	return true;
}

// Modal Ver Recibos
function abrirModalVerRecibos(id_invoice, monto_venta, nombre_persona, balance_account, id_current_account, monto_venta_deuda) {
	actualizarTablaVerRecibos(id_invoice);

	// Cargamos data cuenta:
	$("#label_persona_ver_recibos"+modals_btns+"").text(nombre_persona);
	$("#label_factura_ver_recibos"+modals_btns+"").text(id_invoice);
	$("#label_total_monto_factura_ver_recibos"+modals_btns+"").text(monto_venta);
	$("#label_monto_adeudado_ver_recibos"+modals_btns+"").text(monto_venta_deuda);

	// Color monto_venta cuenta
	if (parseFloat(monto_venta) <= 0){
		$("#label_total_monto_factura_ver_recibos"+modals_btns+"").removeClass('label-warning');
		$("#label_total_monto_factura_ver_recibos"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_total_monto_factura_ver_recibos"+modals_btns+"").removeClass('label-success');
		$("#label_total_monto_factura_ver_recibos"+modals_btns+"").addClass('label-warning');
	}

	// Color monto_adeudado
	if (parseFloat(monto_venta_deuda) <= 0){
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").removeClass('label-danger');
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").removeClass('label-success');
		$("#label_monto_adeudado_ver_recibos"+modals_btns+"").addClass('label-danger');
	}
}

// Mostra recibos en talba
function actualizarTablaVerRecibos(id_invoice) {
	statusLoading('loading_modal_ver_recibo', 1, 'Actualizando Listado ..');
	
	var route = root_project+"recibos/GetRecibosByInvoice/"+id_invoice;
	var tabla = $("#table_VerRecibos"+modals_btns+"");
	tabla.empty();

	var displayContentData = []; 
	var displayTitleData = [
		{ title: "Nro Recibo" },
		{ title: "Fecha/Hora Emisión" },
		{ title: "N° Orden de Pago" },
		{ title: "Monto Efectivo" },
		{ title: "Cheques" },
		{ title: "Retención" },
		{ title: "Retención 2" },
		{ title: "Total Recibo" },
		{ title: "Ver Recibo" },
	];
	
	$.get(route, function(result){
		$(result).each(function(key,value){
			//carga del array contenido desde el array de la respuesta, el orden va de la mano de el orden de los titulos
			displayContentData.push([ 
				value.id,
				(value.created_at != null) ? formatDateTime(value.created_at, 'd-m-Y h:i') : '',
				(value.nro_orden_pago != null) ? value.nro_orden_pago : '',
				(value.effective_amount != null) ? value.effective_amount : 0,
				(value.cant_cheques != null) ? value.cant_cheques : 0,
				(value.retencion_amount != null) ? value.retencion_amount : 0,
				(value.retencion2_amount != null) ? value.retencion2_amount : 0,
				(value.total_amount != null) ? '<b class="col-red">$ ' + value.total_amount + '</b>' : 0,
				'<div class="icon-button-demo">'
					+'<button type="button" href="#" data-toggle="modal" title="Ver Recibo" data-backdrop="static" data-keyboard="false" onclick="verReciboByIdRecibo('+value.id+');" data-target="#modalVerReciboById'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
						+'<i class="material-icons">search</i>'
					+'</button>'
				+'</div>'
			]);
		});		

		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTableExportable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_modal_ver_recibo', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_ver_recibo', 0);
	});
}

function verReciboByIdRecibo(id_receipt) {
	objetoReciboVenta = {};
	statusLoading('loading_modal_ver_recibo_imprimir', 1, 'Actualizando Datos ..');
	limpiarFormVerReciboImprimir();

	var route = root_project+"recibos/GetReciboByIdRecibo/"+id_receipt;
	var tabla = $("#tbody_chequesrecibo_ver_recibo_imprimir");
	tabla.empty();

	var displayContentData = []; 
	var displayTitleData = [
		{ title: "Nro Cheque" },
		{ title: "Banco" },
		{ title: "Importe" },
		{ title: "F. Emisión" },
		{ title: "Concepto" },
		{ title: "Nombre" },
		{ title: "Cuit" },
		{ title: "F. Pago" },
	];

	$.get(route, function(result){
		var recibo = result.data_recibo[0];
		
		// Guardamos datos de recibo en objeto
		objetoReciboVenta = recibo;
		objetoReciboVenta.cheques = result.data_cheques;
		objetoReciboVenta.data_invoice = result.data_invoice_receipt[0];

		// Cargamos recibo
		$("#label_numero_recibo_ver_recibo_imprimir"+modals_btns+"").text('Nº ' + recibo.id);
		$("#LabelFormVerReciboImprimir [name=label_numero_recibo]").text(recibo.id);
		$("#LabelFormVerReciboImprimir [name=label_numero_factura]").text(recibo.id_invoice);
		$("#LabelFormVerReciboImprimir [name=label_effective_amount]").text(recibo.effective_amount);
		$("#LabelFormVerReciboImprimir [name=label_card_amount]").text(recibo.card_amount);
		$("#LabelFormVerReciboImprimir [name=label_retencion_amount]").text(recibo.retencion_amount != null ? recibo.retencion_amount : 0);
		$("#LabelFormVerReciboImprimir [name=label_retencion2_amount]").text(recibo.retencion2_amount != null ? recibo.retencion2_amount : 0);
		$("#LabelFormVerReciboImprimir [name=label_fecha]").text(formatDateTime(recibo.created_at, 'd-m-Y h:i', true));
		$("#LabelFormVerReciboImprimir [name=label_nro_orden_pago]").text(recibo.nro_orden_pago != null ? recibo.nro_orden_pago : '');
		$("#LabelFormVerReciboImprimir [name=label_total_amount]").text('$ ' + recibo.total_amount);

		// Cargamos cheques
		$(result.data_cheques).each(function(key,value){
			displayContentData.push([ 
				value.nro_cheque,
				(value.banco != null) ? value.banco : '',
				(value.importe != null) ? '$ ' + value.importe : 0,
				(value.fecha_emision != null) ? formatDateTime(value.fecha_emision, 'd-m-Y', true) : '',
				(value.concepto != null) ? value.concepto : 0,
				(value.nombre != null) ? value.nombre : 0,
				(value.cuit != null) ? value.cuit : 0,
				(value.fecha_pago != null) ? formatDateTime(value.fecha_pago, 'd-m-Y', true) : ''
			]);
		});		


		//Llamo funcion generica que carga la tabla con titulos y contenido
		fillDataTable(tabla, displayTitleData, displayContentData);
		statusLoading('loading_modal_ver_recibo_imprimir', 0);
		
	}).fail(function(data) {
    	if (data.error == "Unauthenticated") {
			showMessageModal('La sesión ha expirado, por favor debe iniciar nuevamente a la sesión. <br>'
								+'<br><a href="logout" class="btn btn-link bg-yellow waves-effect">'
                                	+'CONECTARSE'
                            	+'</a>', 'ATENCION', 'bg-red', 'error');
    	}else {
			showMessageModal('Ocurrio un error al conectarse con el servidor, por favor vuelva a intentarlo o contacte con un Administrador', 'ATENCION', 'bg-red', 'error');
    	}
    	statusLoading('loading_modal_ver_recibo_imprimir', 0);
	});
}

function limpiarFormVerReciboImprimir() {
	$("#label_numero_recibo_ver_recibo_imprimir"+modals_btns+"").text('');
	$("#LabelFormVerReciboImprimir [name=label_numero_recibo]").text('');
	$("#LabelFormVerReciboImprimir [name=label_numero_factura]").text('');
	$("#LabelFormVerReciboImprimir [name=label_effective_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_card_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_retencion_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_retencion2_amount]").text('');
	$("#LabelFormVerReciboImprimir [name=label_fecha]").text('');
	$("#LabelFormVerReciboImprimir [name=label_total_amount]").text('');
}

function changeSelectFacturaIdInvoiceCofing(valueInvoiceSelected) {
	$("#formCargarReciboYMovimiento input[name=id_invoice]").val(valueInvoiceSelected);
	var monto_venta = $("#CargarRecibo_id_invoice option:selected").attr('monto_venta');
	var id_current_account = $("#CargarRecibo_id_invoice option:selected").attr('id_current_account');
	var balance_account = $("#CargarRecibo_id_invoice option:selected").attr('balance_account');
	var total_amount_deuda = $("#CargarRecibo_id_invoice option:selected").attr('total_amount_deuda');
	$("#label_monto_adeudado_recibos"+modals_btns+"").text(parseFloat(total_amount_deuda))
	$("#label_total_monto_factura"+modals_btns+"").text(parseFloat(monto_venta))

	$("#formCargarReciboYMovimiento input[name=monto_venta]").val(monto_venta);
	$("#formCargarReciboYMovimiento input[name=id_current_account]").val(id_current_account != undefined ? id_current_account : 0);
	$("#formCargarReciboYMovimiento input[name=balance]").val(balance_account != undefined ? balance_account : 0);

	coloresMontoYMontoDeudaCargarRecibo(monto_venta, total_amount_deuda);
}

function coloresMontoYMontoDeudaCargarRecibo(monto_venta, monto_venta_deuda) {
	// Color monto_venta cuenta
	if (parseFloat(monto_venta) <= 0){
		$("#label_total_monto_factura"+modals_btns+"").removeClass('label-warning');
		$("#label_total_monto_factura"+modals_btns+"").addClass('label-success');
	} else {
		$("#label_total_monto_factura"+modals_btns+"").removeClass('label-success');
		$("#label_total_monto_factura"+modals_btns+"").addClass('label-warning');
	}

	// Color monto_adeudado
	if (parseFloat(monto_venta_deuda) <= 0){
		$("#label_monto_adeudado_recibos"+modals_btns+"").removeClass('label-danger');
		$("#label_monto_adeudado_recibos"+modals_btns+"").addClass('label-success');
		$("#formCargarReciboYMovimiento button[name=btnCargarRecibo"+modals_btns+"]").attr('disabled', 'disabled');
	} else {
		$("#label_monto_adeudado_recibos"+modals_btns+"").removeClass('label-success');
		$("#label_monto_adeudado_recibos"+modals_btns+"").addClass('label-danger');
		$("#formCargarReciboYMovimiento button[name=btnCargarRecibo"+modals_btns+"]").removeAttr('disabled');
	}
}

function closeModalConfirmAddReciboAfterAlta() {
	$("#modalConfirmAddReciboAfterAlta").modal("hide");
}

function abrirCargarRecibosYAfterAlta(total_amount, person_desc, balance_account, id_current_account, total_amount, id_factura) {
	$('#modal'+ modals_btns).modal('hide');
	$("#btnModalCargarRecibo" + modals_btns).click();
	abrirModalCargarRecibo(total_amount, person_desc, balance_account, id_current_account, objetoFactura.total_amount, id_factura);
	closeModalConfirmAddReciboAfterAlta();
}
