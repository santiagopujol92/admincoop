// La variable objetoFactura la es creada en el archivo ventas.js
// La recibo porque importo los 2 archivos en un mismo html. ventas_print.js debe ser importado luego deventas.js

function imprimirRemitoVenta(){
    var doc = new jsPDF();
    var altura = 0;
    var altura_post_tabla = 0;
    
    // Si tiene menos de 4 productos cambiamos altura para que imprima el duplicado sino imprime lo mismo y la FIRMA al FINAL
    if (objetoFactura.productos.length <= 4) {

        doc = dibujarRemitoPDF(doc, altura, altura_post_tabla, objetoFactura)

        altura = 144;
        altura_post_tabla = altura;

        // Linea divisora
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(5, 147.5, '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _');
        doc = dibujarRemitoPDF(doc, altura, altura_post_tabla, objetoFactura)
    } else {
        altura = 0;
        altura_post_tabla = 144;
        doc = dibujarRemitoPDF(doc, altura, altura_post_tabla, objetoFactura)
    }

    doc.save('RemitoVenta_' + objetoFactura.id_sale + '_' + objetoFactura.issue_date + '.pdf');
    window.open(doc.output('bloburl'), '_blank');
}

function dibujarRemitoPDF(doc, altura, altura_post_tabla, objetoFac) {
    const header = function (data) {

        // PRIMER CUADRANTE

        // Rectangulo primero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 10 + altura, 181.6, 44);
    
        // Vertical linea central
        doc.setLineWidth(0.1);
        doc.line(107, 25 + altura, 107, 54 + altura); 

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(26, 23 + altura, 'Cooperativa de Trabajo');
    
        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(25, 30 + altura, 'Barranca Yaco Limitada');

        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(25, 42 + altura, 'Mancha y Velasco 1574 - B°. Ayacucho');
        
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(41, 46 + altura, 'C.P. 5000 Córdoba');
        
        // Horizontal linea central
        doc.line(14.1, 49 + altura, 107, 49 + altura); 

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(31, 53 + altura, 'IVA RESPONSABLE INSRIPTO');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(124, 15 + altura, 'Documento No Válido Como Factura');

        // Rectangulo de fecha
        doc.setDrawColor(0,0,0);
        doc.rect(160, 17 + altura, 30, 10);
        // Linea fecha 1
        doc.setLineWidth(0.1);
        doc.line(170, 27 + altura, 170, 17 + altura);
        // Linea fecha 2
        doc.setLineWidth(0.1);
        doc.line(180, 27 + altura, 180, 17 + altura);
        // Linea hoizontal fecha 
        doc.line(160, 20 + altura, 190, 20 + altura); 
        // Texto Fecha
        doc.setFontSize(7.5);
        doc.setFontStyle('normal');
        doc.text(163, 19.5 + altura, 'Dia       Mes       Año');
        // Fecha ISSUE DATE
        doc.setFontSize(11);
        doc.setFontStyle('bold');
        var fecha_venta = formatDateTime(objetoFac.issue_date, 'd-m-Y').split('/');
        doc.text(163, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[0] : fecha_venta[2] : ''));
        doc.text(172.5, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[1] : fecha_venta[3] : ''));
        doc.text(180.8, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[2] : fecha_venta[4] : ''));

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(122, 23 + altura, 'REMITO');
        
        doc.setFontSize(16);
        doc.setFontStyle('normal');
        doc.text(122, 34 + altura, 'N°. ' + objetoFac.id_sale);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 43 + altura, 'CUIT N°.: 33-71161816-9');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 48 + altura, 'ING. BRUTOS: 280.369.632');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 52.8 + altura, 'INICIO DE ACTIVIDADES: 01/2016');

        // Rectangulo central R
        doc.setDrawColor(0,0,0);
        doc.rect(101.5, 10 + altura, 11, 11);

        doc.setFontSize(28);
        doc.setFontStyle('bold');
        doc.text(103.4, 19 + altura, 'R');

        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(102, 24 + altura, 'Cod. 91');

        // SEGUNDO CUADRANTE

        // Rectangulo segundo grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 55 + altura, 181.7, 14);

        // PERSONA CLIENTE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 61 + altura, 'SEÑOR/ES: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_desc != null ? 'bold' : 'normal');
        doc.text(37, 61 + altura, (objetoFac.person_desc != null ? objetoFac.person_desc : '________________________________________________________________________________'))

        // DOMICILIO
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 67.5 + altura, 'DOMICILIO: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_domicilio != null ? 'bold' : 'normal');
        doc.text(37, 67.5 + altura, (objetoFac.person_domicilio != null ? objetoFac.person_domicilio : '________________________________________________________________________________'))

        // TERCER CUADRANTE
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 70 + altura, 181.7, 16);

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 76 + altura, 'IVA: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_iva != null ? 'bold' : 'normal');
        doc.text(26, 76 + altura, (objetoFac.person_iva != null ? objetoFac.person_iva : '___________________________________________________'))
        // Linea hoizontal IVA 
        doc.setLineWidth(0.4);
        doc.line(14.1, 78 + altura, 127, 78 + altura); 
        // Vertical linea IVA IZQUIERDA
        doc.setLineWidth(0.4);
        doc.line(127, 70 + altura, 127, 86 + altura); 
        // Vertical linea IVA DERECHA
        doc.setLineWidth(0.4);
        doc.line(25, 70 + altura, 25, 78 + altura); 

        // TRANSPORTISTA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 84 + altura, 'TRANSPORTISTA:  _______________________________________');
        
        // CUIT
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(129, 76 + altura, 'CUIT N°.:');
        doc.setFontStyle(objetoFac.person_cuit != null ? 'bold' : 'normal');
        doc.text(145, 76 + altura, (objetoFac.person_cuit != null ? objetoFac.person_cuit : '_________________________'))

        // ING BRUTOS
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(129, 84 + altura, 'ING. BRUTOS N°.:  _________________');

        // CUADRADO FINAL DE FIRMA
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 122 + altura_post_tabla, 181.7, 20);

        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(16.5, 125.5 + altura_post_tabla, 'La mercadería viaja por cuenta y riesgo del comprador. Valor Declarado:');

        // RECIBIO LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(20, 137 + altura_post_tabla, '_________________________________');
        // RECIBIO TEXTO
        doc.text(40, 140.5 + altura_post_tabla, 'RECIBIÓ');

        // FIRMA LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(80, 137 + altura_post_tabla, '_________________________________');
        // FIRMA TEXTO
        doc.text(102, 140.5 + altura_post_tabla, 'FIRMA');

        // ACLARACION LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(139, 137 + altura_post_tabla, '_________________________________');
        // ACLARACION TEXTO
        doc.text(158, 140.5 + altura_post_tabla, 'ACLARACIÓN');
    };

    const table = document.getElementById('table_productos_asignados');
    var data = doc.autoTableHtmlToJson(table);
    
    const options = {
        didDrawPage: header,
        margin: {
            top: 87 + altura
        },
        startY: 87 + altura,
        theme: 'striped',
        headerStyles: {
            lineWidth: 0.1,
            lineColor: [0, 0, 0]
        },
        columnStyles: {
            0: {
                cellWidth: 10,
                halign: 'center',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            1: {
                cellWidth: 70,
                halign: 'left',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            2: {
                cellWidth: 15,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            3: {
                cellWidth: 5,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
        },
        styles: {
            overflow: 'linebreak',
            fontSize: 8,
        }
    };

    data.columns.splice(5, 1); // Quitamos columna delete
    data.columns.splice(4, 1); // Quitamos columna total
    data.columns.splice(3, 1); // Quitamos columna precio

    var columns = [
        {title: "Código Producto"},
        {title: "Nombre Producto"}, 
        {title: "Cantidad"}, 
        {title: "U. Medida"}, 
    ];
        
    doc.autoTable(columns, data.rows, options);

    return doc;
}

function imprimirFacturaVenta(){
    var doc = new jsPDF();
    var altura = 0;
    var altura_post_tabla = 0;
    
    // Si tiene menos de 4 productos cambiamos altura para que imprima el duplicado sino imprime lo mismo
    if (objetoFactura.productos.length <= 4) {

        doc = dibujarFacturaPDF(doc, altura, altura_post_tabla, objetoFactura)

        altura = 144;
        altura_post_tabla = altura;

        // Linea divisora
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(5, 147.5, '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _');
        doc = dibujarFacturaPDF(doc, altura, altura_post_tabla, objetoFactura)
    } else {
        altura = 0;
        altura_post_tabla = 144;
        doc = dibujarFacturaPDF(doc, altura, altura_post_tabla, objetoFactura)
    }

    doc.save('FacturaVenta_' + objetoFactura.id_invoice + '_' + objetoFactura.id_sale + '_' + objetoFactura.issue_date + '.pdf');
    window.open(doc.output('bloburl'), '_blank');
}

function dibujarFacturaPDF(doc, altura, altura_post_tabla, objetoFac) {
    const header = function (data) {
        // PRIMER CUADRANTE

        // Rectangulo primero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 10 + altura, 181.6, 44);
    
        // Vertical linea central
        doc.setLineWidth(0.1);
        doc.line(107, 25 + altura, 107, 54 + altura); 

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(26, 23 + altura, 'Cooperativa de Trabajo');
    
        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(25, 30 + altura, 'Barranca Yaco Limitada');

        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(25, 42 + altura, 'Mancha y Velasco 1574 - B°. Ayacucho');
        
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(41, 46 + altura, 'C.P. 5000 Córdoba');
        
        // Horizontal linea central
        doc.line(14.1, 49 + altura, 107, 49 + altura); 

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(31, 53 + altura, 'IVA RESPONSABLE INSRIPTO');

        // Rectangulo de fecha
        doc.setDrawColor(0,0,0);
        doc.rect(160, 17 + altura, 30, 10);
        // Linea fecha 1
        doc.setLineWidth(0.1);
        doc.line(170, 27 + altura, 170, 17 + altura);
        // Linea fecha 2
        doc.setLineWidth(0.1);
        doc.line(180, 27 + altura, 180, 17 + altura);
        // Linea hoizontal fecha 
        doc.line(160, 20 + altura, 190, 20 + altura); 
        // Texto Fecha
        doc.setFontSize(7.5);
        doc.setFontStyle('normal');
        doc.text(163, 19.5 + altura, 'Dia       Mes       Año');
        // Fecha ISSUE DATE
        doc.setFontSize(11);
        doc.setFontStyle('bold');
        var fecha_venta = formatDateTime(objetoFac.issue_date, 'd-m-Y').split('/');
        doc.text(163, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[0] : fecha_venta[2] : ''));
        doc.text(172.5, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[1] : fecha_venta[3] : ''));
        doc.text(180.8, 24.9 + altura, (objetoFac.issue_date != undefined ? fecha_venta[0] != 'undefined' ? fecha_venta[2] : fecha_venta[4] : ''));

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(122, 23 + altura, 'FACTURA');
        
        doc.setFontSize(16);
        doc.setFontStyle('normal');
        doc.text(122, 34 + altura, 'N°. ' + objetoFac.id_invoice);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 43 + altura, 'CUIT N°.: 33-71161816-9');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 48 + altura, 'ING. BRUTOS: 280.369.632');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 52.8 + altura, 'INICIO DE ACTIVIDADES: 01/2016');

        // Rectangulo central A
        doc.setDrawColor(0,0,0);
        doc.rect(101.5, 10 + altura, 11, 11);

        doc.setFontSize(28);
        doc.setFontStyle('bold');
        doc.text(103.4, 19 + altura, 'A');

        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(102, 24 + altura, 'Cod. 01');

        // SEGUNDO CUADRANTE

        // Rectangulo segundo grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 55 + altura, 181.6, 21);

        // PERSONA CLIENTE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 61 + altura, 'SEÑOR/ES: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_desc != null ? 'bold' : 'normal');
        doc.text(37, 61 + altura, (objetoFac.person_desc != null ? objetoFac.person_desc : '________________________________________________________________________________'))

        // DOMICILIO
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 67.5 + altura, 'DOMICILIO: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_domicilio != null ? 'bold' : 'normal');
        doc.text(37, 67.5 + altura, (objetoFac.person_domicilio != null ? objetoFac.person_domicilio : '________________________________________________________________________________'))

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 74.5 + altura, 'IVA: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoFac.person_iva != null ? 'bold' : 'normal');
        doc.text(24.5, 74.5 + altura, (objetoFac.person_iva != null ? objetoFac.person_iva : '_________________________'))
        
        // CUIT
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(75, 74.5 + altura, 'CUIT N°.:');
        doc.setFontStyle(objetoFac.person_cuit != null ? 'bold' : 'normal');
        doc.text(92, 74.5 + altura, (objetoFac.person_cuit != null ? objetoFac.person_cuit : '__________________'))

        // ING BRUTOS
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(129, 74.5 + altura, 'ING. BRUTOS N°.:  _________________');

        // CUADRADO FINAL
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 112 + altura_post_tabla, 181.6, 29.8);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(17, 117 + altura_post_tabla, 'Fecha Vencimiento: ' + formatDateTime(objetoFac.expiration_date, 'd-m-Y'));

        var iva_neto = objetoFac.alicuota_iva != null ? objetoFac.alicuota_iva : objetoFac.alicuota_iva_porc != null ? ((parseFloat(objetoFac.total_amount) * parseFloat(objetoFac.alicuota_iva_porc)) / 100).toFixed(2).replace('.', ',')  : '0,00';
        var descuento_neto = objetoFac.descuento != null ? objetoFac.descuento : objetoFac.descuento_porc != null ? ((parseFloat(objetoFac.total_amount) * parseFloat(objetoFac.descuento_porc)) / 100).toFixed(2).replace('.', ',') : '0,00';
        var retencion_neto = objetoFac.retencion != null ? objetoFac.retencion : objetoFac.retencion_porc != null ? ((parseFloat(objetoFac.total_amount) * parseFloat(objetoFac.retencion_porc)) / 100).toFixed(2).replace('.', ',') :'0,00';
        objetoFac.total_amount = objetoFac.total_amount != null ? objetoFac.total_amount : 0;

        var iva_nombre_valor = objetoFac.alicuota_iva_porc != null ? '+ (' + objetoFac.alicuota_iva_porc + ' %)' : '';
        var descuento_nombre_valor = objetoFac.descuento_porc != null ? '- (' + objetoFac.descuento_porc + ' %)' : '';
        var retencion_nombre_valor = objetoFac.retencion_porc != null ? '- (' + objetoFac.retencion_porc + ' %)' : '';

        // IMPORTE NETO
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(158, 117 + altura_post_tabla, 'Importe Neto: $', 'right');
        doc.text(193, 117 + altura_post_tabla, parseFloat(objetoFac.sell_cost).toFixed(2).replace('.', ',') + '', 'right');

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(150, 122 + altura_post_tabla, iva_nombre_valor, 'right');
        doc.setFontStyle('normal');
        doc.text(158, 122 + altura_post_tabla, ' Iva: $', 'right');
        doc.text(193, 122 + altura_post_tabla, iva_neto + '', 'right');

        // DESCUENTO
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(138, 127 + altura_post_tabla, descuento_nombre_valor, 'right');
        doc.setFontStyle('normal');
        doc.text(158, 127 + altura_post_tabla, ' Descuento: $', 'right');
        doc.text(193, 127 + altura_post_tabla, descuento_neto + '', 'right');

        // RETENCION
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(139, 132 + altura_post_tabla, retencion_nombre_valor, 'right');
        doc.setFontStyle('normal');
        doc.text(158, 132 + altura_post_tabla, ' Retención: $', 'right');
        doc.text(193, 132 + altura_post_tabla, retencion_neto + '', 'right');

        doc.setFontSize(11);
        doc.setFontStyle('bold');
        doc.text(158.5, 139 + altura_post_tabla, 'Importe Total: ' + '$ ', 'right');
        doc.text(193, 139 + altura_post_tabla, parseFloat(objetoFac.total_amount).toFixed(2).replace('.', ',') + '', 'right');
    };

    const table = document.getElementById('table_productos_asignados');
    var data = doc.autoTableHtmlToJson(table);
    
    const options = {
        didDrawPage: header,
        margin: {
            top: 77 + altura
        },
        startY: 77 + altura,
        theme: 'striped',
        headerStyles: {
            lineWidth: 0.1,
            lineColor: [0, 0, 0]
        },
        columnStyles: {
            0: {
                cellWidth: 10,
                halign: 'center',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            1: {
                cellWidth: 40,
                halign: 'left',
                fontStyle: 'bold',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            2: {
                cellWidth: 15,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            3: {
                cellWidth: 5,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            4: {
                cellWidth: 15,
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            5: {
                cellWidth: 15,
                halign: 'left',
                lineWidth: 0.1,
                fontStyle: 'bold',
                lineColor: [0, 0, 0]
            },
        },
        styles: {
            overflow: 'linebreak',
            fontSize: 8,
        }
    };

    data.columns.splice(5, 1); // Quitamos columna delete

    var columns = [
        {title: "Código Producto"},
        {title: "Nombre Producto"}, 
        {title: "Cantidad"}, 
        {title: "U. Medida"}, 
        {title: "Precio Unit."}, 
        {title: "Precio Subtotal"}, 
    ];
        
    doc.autoTable(columns, data.rows, options);

    return doc;
}

function imprimirReciboVenta(){
    var doc = new jsPDF();
    var altura = 0;
    var altura_post_tabla = 0;
    
    altura = 0;
    altura_post_tabla = 144;
    doc = dibujarReciboPDF(doc, altura, altura_post_tabla, objetoReciboVenta);

    doc.save('ReciboVenta_' + objetoReciboVenta.id_invoice + '_' + objetoReciboVenta.id + '_' + objetoReciboVenta.created_at + '.pdf');
    window.open(doc.output('bloburl'), '_blank');
}

function dibujarReciboPDF(doc, altura, altura_post_tabla, objetoRecibo) {

    objetoRecibo.data_invoice.person_desc = objetoRecibo.data_invoice.person_lastname + ' ' + objetoRecibo.data_invoice.person_name;
    objetoRecibo.data_invoice.person_cuit = (objetoRecibo.data_invoice.person_cuit != null ? objetoRecibo.data_invoice.person_cuit : '');
    objetoRecibo.data_invoice.person_domicilio = (objetoRecibo.data_invoice.adress != null ? objetoRecibo.data_invoice.adress : '');
    objetoRecibo.data_invoice.person_iva = (objetoRecibo.data_invoice.condicion_iva_desc != null ? objetoRecibo.data_invoice.condicion_iva_desc : '');

    const header = function (data) {
        // PRIMER CUADRANTE

        // Rectangulo primero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 10 + altura, 181.55, 44);
    
        // Vertical linea central
        doc.setLineWidth(0.1);
        doc.line(107, 25 + altura, 107, 54 + altura); 

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(26, 23 + altura, 'Cooperativa de Trabajo');
    
        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(25, 30 + altura, 'Barranca Yaco Limitada');

        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(25, 42 + altura, 'Mancha y Velasco 1574 - B°. Ayacucho');
        
        doc.setFontSize(10);
        doc.setFontStyle('bold');
        doc.text(41, 46 + altura, 'C.P. 5000 Córdoba');
        
        // Horizontal linea central
        doc.line(14.1, 49 + altura, 107, 49 + altura); 

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(31, 53 + altura, 'IVA RESPONSABLE INSRIPTO');

        // Rectangulo de fecha
        doc.setDrawColor(0,0,0);
        doc.rect(161, 27 + altura, 31, 10);
        // Linea fecha 1
        doc.setLineWidth(0.1);
        doc.line(171, 37 + altura, 171, 27 + altura);
        // Linea fecha 2
        doc.setLineWidth(0.1);
        doc.line(181, 37 + altura, 181, 27 + altura);
        // Linea hoizontal fecha 
        doc.line(161, 30 + altura, 192, 30 + altura); 
        // Texto Fecha
        doc.setFontSize(7.5);
        doc.setFontStyle('normal');
        doc.text(164, 29.5 + altura, 'Dia       Mes        Año');
        // Fecha ISSUE DATE
        doc.setFontSize(11);
        doc.setFontStyle('bold');
        var fecha_recibo = formatDateTime(objetoRecibo.created_at, 'd-m-Y').split('/');
        doc.text(164, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[0] : fecha_recibo[2] : ''));
        doc.text(173.5, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[1] : fecha_recibo[3] : ''));
        doc.text(181.8, 34.9 + altura, (objetoRecibo.created_at != undefined ? fecha_recibo[0] != 'undefined' ? fecha_recibo[2] : fecha_recibo[4] : ''));

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(124, 15 + altura, 'Documento No Válido Como Factura');

        doc.setFontSize(16);
        doc.setFontStyle('bold');
        doc.text(122, 23 + altura, 'RECIBO OFICIAL');
        
        doc.setFontSize(16);
        doc.setFontStyle('normal');
        doc.text(122, 34 + altura, 'N°. ' + objetoRecibo.id);

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 43 + altura, 'CUIT N°.: 33-71161816-9');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 48 + altura, 'ING. BRUTOS: 280.369.632');

        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(122, 52.8 + altura, 'INICIO DE ACTIVIDADES: 01/2016');

        // Rectangulo central A
        doc.setDrawColor(0,0,0);
        doc.rect(101.5, 10 + altura, 11, 11);

        doc.setFontSize(28);
        doc.setFontStyle('bold');
        doc.text(103.4, 19 + altura, 'X');

        // SEGUNDO CUADRANTE

        // Rectangulo segundo grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 55 + altura, 181.55, 28);

        // PERSONA CLIENTE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 61 + altura, 'SEÑOR/ES: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoRecibo.data_invoice.person_desc != null ? 'bold' : 'normal');
        doc.text(37, 61 + altura, (objetoRecibo.data_invoice.person_desc != null ? objetoRecibo.data_invoice.person_desc : '________________________________________________________________________________'));

        // DOMICILIO
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 67.5 + altura, 'DOMICILIO: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoRecibo.data_invoice.person_domicilio != null ? 'bold' : 'normal');
        doc.text(37, 67.5 + altura, (objetoRecibo.data_invoice.person_domicilio != null ? objetoRecibo.data_invoice.person_domicilio : '________________________________________________________________________________'));

        // IVA
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 74.5 + altura, 'IVA: ');
        doc.setFontSize(10);
        doc.setFontStyle(objetoRecibo.data_invoice.person_iva != null ? 'bold' : 'normal');
        doc.text(24.5, 74.5 + altura, (objetoRecibo.data_invoice.person_iva != null ? objetoRecibo.data_invoice.person_iva : ' __________________________________________'));
        
        // CUIT
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(110, 74.5 + altura, 'CUIT N°.:');
        doc.setFontStyle(objetoRecibo.data_invoice.person_cuit != null ? 'bold' : 'normal');
        doc.text(127, 74.5 + altura, (objetoRecibo.data_invoice.person_cuit != null ? objetoRecibo.data_invoice.person_cuit : '__________________________________'));

        // ING BRUTOS
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(16.5, 81.5 + altura, 'ING. BRUTOS N°.: _______________________________');

        // NRO REMITO
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(110, 81.5 + altura, 'REMITO N°.:');
        doc.setFontStyle(objetoRecibo.data_invoice.id_sale != null ? 'bold' : 'normal');
        doc.text(132, 81.5 + altura, (objetoRecibo.data_invoice.id_sale != null ? objetoRecibo.data_invoice.id_sale + '' : ' _______________________________'));

        // TECER CUADRANTE

        // Rectangulo tercero grande
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 84 + altura, 181.55, 42.5);

        // RECIBO TANTO
        doc.setFontSize(10);
        doc.setLineWidth(0.1);
        doc.setFontStyle('normal');
        doc.text(16.5, 90 + altura, 'Recibi(mos) la suma de pesos ');
        doc.setFontStyle(objetoRecibo.total_amount != null ? 'bold' : 'normal');
        var total_monto_en_letras = NumeroALetras(objetoRecibo.total_amount)
        doc.text(66, 90 + altura, (objetoRecibo.total_amount != null ? objetoRecibo.total_amount + '' : ' ________________________________________________________________'));
        doc.text(16.5, 97 + altura, (objetoRecibo.total_amount != null ? ' ( ' + total_monto_en_letras + ' )'  : '__________________________________________________________________________________________'));
        
        // EN CONCEPTO DE
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 104 + altura, 'en concepto de');
        doc.text(42, 104 + altura, '_____________________________________________________________________________');
        doc.text(16.5, 111 + altura, '__________________________________________________________________________________________');
        doc.text(16.5, 118 + altura, '__________________________________________________________________________________________');
    
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(16.5, 125 + altura, 'Efectivo: ');
        var efectivo_en_letras = NumeroALetras(objetoRecibo.effective_amount)
        doc.setFontStyle(objetoRecibo.effective_amount != null ? 'bold' : 'normal');
        doc.text(31, 125 + altura, (objetoRecibo.effective_amount != null ? efectivo_en_letras + '' : ' _______________________________________________________'));
        doc.text(143, 125 + altura, (objetoRecibo.effective_amount != null ? '$ ' + objetoRecibo.effective_amount : '$ ________________________'));

        // CUADRADO FINAL
        doc.setDrawColor(0,0,0);
        doc.rect(14.1, 112 + altura_post_tabla, 181.8, 29.8);

        objetoRecibo.total_amount = objetoRecibo.total_amount != null ? objetoRecibo.total_amount : 0;

        doc.setFontSize(11);
        doc.setFontStyle('bold');
        doc.text(158.5, 118 + altura_post_tabla, 'Importe Total: ' + '$ ', 'right');
        doc.text(193, 118 + altura_post_tabla, parseFloat(objetoRecibo.total_amount).toFixed(2).replace('.', ',') + '', 'right');

        // FIRMA LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(22, 137 + altura_post_tabla, '___________________________________________________');
        // FIRMA TEXTO
        doc.text(57, 140.5 + altura_post_tabla, 'FIRMA');

        // ACLARACION LINEA
        doc.setFontSize(8);
        doc.setFontStyle('normal');
        doc.text(110, 137 + altura_post_tabla, '___________________________________________________');
        // ACLARACION TEXTO
        doc.text(142, 140.5 + altura_post_tabla, 'ACLARACIÓN');

        // Vertical linea GRANDE LATERAL 1
        doc.setLineWidth(0.1);
        doc.line(14.1, 256, 14.1, 140.5 ); 

        // Vertical linea GRANDE LATERAL 2
        doc.setLineWidth(0.1);
        doc.line(195.9, 256, 195.9, 140.5 ); 
    };

    const table = document.getElementById('tbody_chequesrecibo_ver_recibo_imprimir');
    var data = doc.autoTableHtmlToJson(table);

    const options = {
        didDrawPage: header,
        margin: {
            top: 127.5 + altura
        },
        startY: 127.5 + altura,
        theme: 'striped',
        headerStyles: {
            lineWidth: 0.1,
            lineColor: [0, 0, 0]
        },
        columnStyles: {
            0: {
                cellWidth: 30,
                halign: 'left',
                fontStyle: 'normal',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            1: {
                cellWidth: 40,
                halign: 'left',
                fontStyle: 'normal',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
            2: {
                cellWidth: 30,
                fontStyle: 'bold',
                halign: 'left',
                lineWidth: 0.1,
                lineColor: [0, 0, 0]
            },
        },
        styles: {
            overflow: 'linebreak',
            fontSize: 8,
        }
    };

    var columns = [
        {title: "Nro Cheque"},
        {title: "Banco"}, 
        {title: "Importe"}, 
    ];
        
    doc.autoTable(columns, data.rows, options);

    return doc;
}
