{{-- FORMULARIO NOTA RECEPCION --}}
{!! Form::open(['id' => "formNotaRecepcion", 'method' => 'POST']) !!}
    <h4 align="left">Nro Nota Recepción: 
        <span class="label label-primary" id="label_purchase_id">{{$id_new_purchase}}</span>
    </h4>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label for="id_person">Vendedor:</label>
            <div class="input-group input-above4">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <select name="id_person" required class="form-control show-tick" msg="Proveedor" data-live-search="true" >
                        <option value="0">Seleccione un Proveedor</option>
                        @foreach ($data_persons as $reg) 
                            <option value="{{$reg->id}}">{{$reg->lastname}} {{$reg->name}} @if (!empty($reg->cuit)) - {{$reg->cuit}} @endif</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="id_status_shipment">Estado Nota Recepción:</label>
            <div class="input-group input-above3">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">arrow_forward_ios</i>
                </span>
                <div class="form-line">
                    <select name="id_status_shipment" disabled required class="form-control show-tick" msg="Estado Nota Recepción" data-live-search="true" >
                        <option value="0">Seleccione un Estado Nota Recepción</option>
                        @foreach ($data_status_shipments as $reg) 
                            <option value="{{$reg->id}}">{{$reg->description}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <b class="col-red">Pesajes Asignados (en Kgs) </b>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <br>
                <div class="col-md-2">
                    <label for="description">Bruto Chasis:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('bruto_chasis', null, ['class' => 'form-control', 'onkeyup' => 'calcularNetoPesaje("chasis")', 'msg' => 'Bruto Chasis', 'placeholder' => "Peso Bruto C", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Tara Chasis:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('tara_chasis', null, ['class' => 'form-control', 'msg' => 'Tara Chasis', 'onkeyup' => 'calcularNetoPesaje("chasis")', 'placeholder' => "Peso Tara C", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Neto Chasis:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('neto_chasis', null, ['class' => 'form-control font-bold', 'readonly' => 'true', 'msg' => 'Neto Chasis',  'placeholder' => "Peso Neto C", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Bruto Acoplado:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('bruto_acoplado', null, ['class' => 'form-control', 'msg' => 'Bruto Acoplado', 'onkeyup' => 'calcularNetoPesaje("acoplado")', 'placeholder' => "Peso Bruto A", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Tara Acoplado:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('tara_acoplado', null, ['class' => 'form-control', 'msg' => 'Tara Acoplado', 'onkeyup' => 'calcularNetoPesaje("acoplado")', 'placeholder' => "Peso Tara A", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Neto Acoplado:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('neto_acoplado', null, ['class' => 'form-control font-bold', 'readonly' => 'true', 'msg' => 'Neto Acoplado', 'placeholder' => "Peso Neto A", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12" align="right">
                    <button type="button" href="#" title="Asignar Pesaje" name="btnasignarPesajeNotaRecepcion" onclick="asignarPesajeLista();" class="btn bg-green btn-link waves-effect">
                        ASIGNAR PESAJE
                    </button>
                </div>
            </div>
        </div>
        <br>
        <div id="modal_messsage_pesajes_compras"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Peso Bruto Chasis</th>
                                <th>Peso Tara Chasis</th>
                                <th>Peso Neto Chasis</th>
                                <th>Peso Bruto Acoplado</th>
                                <th>Peso Tara Acoplado</th>
                                <th>Peso Neto Acoplado</th>
                                <th>Total Peso Neto</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_pesajes_asignados"></tbody>
                    </table>
                    <label class="pull-right">Total Pesaje Neto: <b class="col-red" name="labelTotalPesajeNeto"></b><b class="col-red"> Kgs</b></label>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <b class="col-red">Productos Asignados</b>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <br>
                <div class="col-md-4">
                    <label for="id_type_product">Tipo de Material:</label>
                    <div class="input-group input-above2">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            <select name="id_type_product" class="form-control show-tick" msg="Tipo de Material" data-live-search="true" onchange="changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Material', this.form.id, this.value);showDataProduct(this.form.id, 'id_product', 0);">
                                <option value="0">Seleccione un Tipo de Material</option>
                                @foreach ($data_type_products as $reg) 
                                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="id_product">Material:</label>
                    <div class="input-group input-above2">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            <select name="id_product" class="form-control show-tick" onchange="showDataProduct(this.form.id, this.name, this.value)" msg="Material" data-live-search="true" >
                                <option value="0">Seleccione un Material</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Cantidad:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('quantity', null, ['class' => 'form-control', 'msg' => 'Cantidad', 'placeholder' => "Cantidad", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Precio Compra:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('purchase_price', null, ['class' => 'form-control', 'msg' => 'Precio Compra', 'placeholder' => "Precio", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6" align="left">
                    <h6 name="datosProductoSelected" class="hidden">
                        <label class="col-blue"><u>Información de Producto:</u> </label><br>
                        Stock Disponible: <label name="labelStockForProduct" class="col-red"></label><br>
                        Ult. Precio Compra: <label name="labelLastPricePurchaseForProduct" class="col-red"></label><br>
                        Min. Precio Compra: <label name="labelMinPricePurchaseForProduct" class="col-red"></label>
                        - Max. Precio Compra: <label name="labelMaxPricePurchaseForProduct" class="col-red"></label><br>
                        Unidad de Medida: <label name="labelTypeMetricForProduct" class="col-red"></label>
                    </h6>
                </div>
                <div class="col-md-6" align="right">
                    <button type="button" href="#" title="Asignar Producto" name="btnAsignarProductoNotaRecepcion" onclick="asignarProductoLista();" class="btn bg-green btn-link waves-effect">
                        ASIGNAR PRODUCTO
                    </button>
                </div>
            </div>
        </div>
        <br>
        <div id="modal_messsage_productos_compras"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped table-hover" id="table_productos_asignados">
                        <thead>
                            <tr>
                                <th>Código Producto</th>
                                <th>Nombre Producto</th>
                                <th>Cantidad</th>
                                <th>Unidad de Medida</th>
                                <th>Precio Unit.</th>
                                <th>Precio Subtotal</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_productos_asignados"></tbody>
                    </table>
                    <label class="pull-right">Total Cantidad Producto: <b class="col-red" name="labelTotalCantProd"></b></label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <label for="purchase_date">Fecha Compra:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">date_range</i>
                </span>
                <div class="form-line">
                    {!! Form::text('purchase_date', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Compra', 'placeholder' => "Seleccione Fecha de Compra", 'required' => true ]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="alicuota_iva">Alicuota Iva:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">%</i>
                </span>
                <div class="form-line">
                    {!! Form::text('alicuota_iva', null, ['class' => 'form-control', 'onkeyup' => 'calcularMontoTotalConIva(this.value)', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Alicuota Iva', 'placeholder' => "Ingrese Iva" ]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <label for="purchase_cost">Costo Total:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('purchase_cost', null, ['class' => 'form-control font-bold col-green', 'onkeypress' => 'return isNumberKey(event)', 'style' => 'text-align: center; font-size:20px', 'msg' => 'Costo Total', 'placeholder' => "Costo Total Compra", 'required' => true, 'readonly' => 'readonly', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn bg-grey btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
    {!! Form::button('GENERAR NOTA RECEPCIÓN', ['name' => "btnguardarNotaRecepcion", 'onclick' => 'agregarNotaRecepcion(true)', 'class' => 'btn btn-link bg-green waves-effect pull-right']) !!}
    <button type="button" href="#" style="margin-right: 5px" name="btnImprimirNotaRecepcion" title="Imprimir Nota Recepcion" onclick="imprimirNotaRecepcion();" class="pull-right btn bg-blue btn-link waves-effect">
        IMPRIMIR NOTA RECEPCIÓN
    </button>
{!! Form::close()!!}
{{-- FIN FORMULARIO NOTA RECEPCION --}}