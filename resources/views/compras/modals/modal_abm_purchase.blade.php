{{-- MODAL ABM COMPRA --}}
<div class="modal fade scrollable" id="modal{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title titulo-modal-abm">{{$titulo}}</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-col-pink" style="width:100%" role="tablist">
                        <li role="presentation" class="active" style="width:100%" >
                            <a data-toggle="tab" align="center" aria-expanded="false">
                                <i class="material-icons">description</i> NOTA DE RECEPCIÓN
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active">
                            @include($module . '.forms.form_nota_recepcion_purchase')
                        </div>
                    </div>
                </div>
            <div class="modal-footer" >
            </div>
            <!-- LOADING -->
            <div id="loading_modal"></div>
            <!-- FIN LOADING -->
            <div id="modal_mensaje"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL ABM COMPRA --}}