{{-- MODAL VER RECIBOS COMPRA --}}
<div class="modal fade scrollable" id="modalVerRecibos{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    VER ORDEN DE PAGOS DE {{$titulo}}S: 
                    <b id="label_persona_ver_recibos{{$modals_btns}}" class="col-blue"></b>
                </h4> 
                <h4>
                    Nº Compra:
                    <span id="label_compra_ver_recibos{{$modals_btns}}" class="label label-primary"></span>
                </h4>
                <h4>
                    Monto Total Compra: $
                    <span id="label_total_monto_compra_ver_recibos{{$modals_btns}}" class="label"></span>
                </h4>
                <h4>
                    Monto Adeudado Compra: $
                    <span id="label_monto_adeudado_ver_recibos{{$modals_btns}}" class="label"></span>
                </h4>
            </div>
            {{--  BODY  --}}
            <div class="modal-body">
                <div class="body">
                    <div class="{{$modals_btns}}s">
                        <table width="100%" id="table_VerRecibos{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
                        </table>
                    </div>
                </div>
            </div>
            <!-- LOADING -->
            <div id="loading_modal_ver_recibo"></div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>      
            </div>
            <!-- MENSAJE -->
            <div id="modal_mensaje_ver_recibo"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL VER RECIBOS COMPRA --}}
