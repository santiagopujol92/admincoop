@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING2 -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING2 -->
	                     	<div class="icon-button-demo">
				                {{-- <button class="btn btn-link bg-pink waves-effect pull-right" data-backdrop="static" type="button" onclick="clearForm('form{{$form}}');configSaveButtonAdd();restartConfigForm();" data-backdrop="static" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	<span class="icon-name">NUEVA {{$titulo}}</span> 
	                               	</div>
				        		</button> --}}
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}</h4>
						</div>
	                    <div class="body table-responsive">
							<label><u>FILTROS:</u></label>
							<div class="row">
								<div class="col-lg-12">
									<label for="name">Filtrar por Tipo Persona:</label>
									<div class="input-group input-above">
										<span class="input-group-addon">
											<i class="material-icons">person</i>
										</span>
										<div class="form-line">
											<select name="filtro_id_type_people" onchange="listar()" class="form-control show-tick" msg="Tipo" required data-live-search="true" >
												<option value="0">Todos</option>
												@foreach ($data_type_person as $reg) 
													<option value="{{$reg->id}}">{{$reg->description}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
	                        <div class="{{$modals_btns}}s">
		                        <table id="table_{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
		                        </table>
                        	</div>
							<label align="center">Total Saldo: <b class="col-green" id="totalSaldo{{$modals_btns}}"></b></label>
						</div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>

   	 @include('global_modals.modal_opciones_abm')
  	 @include('global_modals.modal_edit_add_global')
	 @include('global_modals.modal_delete_global')
	 @include('cuentas_corrientes.modals.modal_cargar_operacion')
	 @include('cuentas_corrientes.modals.modal_ver_movimientos')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/{{$module}}.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection