<label for="id_person">Responsable:</label>
<div class="input-group input-above4">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">person</i>
    </span>
    <div class="form-line">
        <select name="id_person" required class="form-control show-tick" disabled msg="Responsable" data-live-search="true" >
            <option value="0">Seleccione un Responsable</option>
            @foreach ($data_persons as $reg) 
                <option value="{{$reg->id}}">{{$reg->lastname}} {{$reg->name}} @if (!empty($reg->cuit)) - {{$reg->cuit}} @endif</option>
            @endforeach
        </select>
    </div>
</div>

<label for="id_status_current_account">Estado Cuenta Corriente:</label>
<div class="input-group input-above3">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">arrow_forward_ios</i>
    </span>
    <div class="form-line">
        <select name="id_status_current_account" required class="form-control show-tick" msg="Estado" data-live-search="true" >
            <option value="0">Seleccione un Estado</option>
            @foreach ($data_status_current_account as $reg) 
                <option value="{{$reg->id}}">{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>

<label for="balance">Saldo Actual:</label>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">attach_money</i>
    </span>
    <div class="form-line">
        {!! Form::text('balance', 0, ['class' => 'form-control font-bold', 'disabled' => 'true', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Saldo', 'placeholder' => "Ingrese Saldo Inicial", 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>

<label for="description">Descripción:</label>
<div class="input-group">
    <span class="input-group-addon">
        <i class="material-icons">arrow_forward_ios</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Descripción', 'placeholder' => "Ingrese Descripción", 'autofocus' => true]) !!}
    </div>
</div>