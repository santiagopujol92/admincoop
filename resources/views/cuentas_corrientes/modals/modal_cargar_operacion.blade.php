{{-- MODAL CARGAR OPERACION / MOVIMIENTO --}}
<div class="modal fade scrollable" id="modalCargarOperacion{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">CARGA DE OPERACIÓN {{$titulo}}</h4>
            </div>
            {!! Form::open(['id' => "formCargarOperacion" . $form, 'method' => 'POST']) !!}
                {!! Form::text('id_current_account', 0, ['class' => 'form-control font-bold hidden']) !!}
                <div class="modal-body">
                    <label for="balance" class="font-bold">Saldo Disponible: </label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">attach_money</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text('balance', 0, ['class' => 'form-control font-bold', 'disabled' => true, 'placeholder' => "Saldo Disponible " . $modulo_msg, 'autofocus' => true]) !!}
                        </div>
                    </div>
                    <label for="id_type_movement_account">Tipo de Comprobante:</label>
                    <div class="input-group input-above3">
                        <span class="input-group-addon">
                            <label class="col-red">*</label> 
                            <i class="material-icons">arrow_forward_ios</i>
                        </span>
                        <div class="form-line">
                            <select name="id_type_movement_account" value="1" onchange="changeTipoComprobante(this.value)" required class="form-control show-tick" msg="Tipo de Comprobante" data-live-search="true" >
                                @foreach ($data_types_movement_account as $reg) 
                                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="divNroFactura" class="hidden">
                        <label for="nro_receipt">Nro. Factura:</label>
                        <div class="input-group input-above2">
                            <span class="input-group-addon">
                                <label class="col-red">*</label> 
                                <i class="material-icons">arrow_forward_ios</i>
                            </span>
                            <div class="form-line">
                                <select name="nro_receipt" id="nro_receipt_factura" disabled required class="form-control show-tick" msg="Nro de Factura" data-live-search="true" >
                                    <option value="0">Seleccione un Nro Factura</option>
                                    @foreach ($data_invoices as $reg) 
                                        <option value="{{$reg->id}}">Factura: {{$reg->id}} - Remito: {{$reg->id_sale}} - {{$reg->person_name}} {{$reg->person_lastname}} - ${{$reg->total_amount}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="divNroOrdenDePago" class="hidden">
                        <label for="nro_receipt">Nro. Orden de Pago: </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <label class="col-red">*</label> 
                                <i class="material-icons">arrow_forward_ios</i>
                            </span>
                            <div class="form-line">
                                {!! Form::text('nro_receipt', null, ['class' => 'form-control', 'disabled' => 'true', 'id' => 'nro_receipt_ordenDePago', 'onkeypress' => 'return isNumberKey(event)', 'required' => 'true', 'msg' => 'Nro Orden de Pago', 'placeholder' => "Ingrese Nro Orden de Pago", 'autofocus' => true]) !!}
                            </div>
                        </div>
                    </div>
                    <label for="type_operation">Tipo de Operación:</label>
                    <div class="input-group input-above">
                        <span class="input-group-addon">
                            <label class="col-red">*</label> 
                            <i class="material-icons">arrow_forward_ios</i>
                        </span>
                        <div class="form-line">
                            <select name="type_operation" required class="form-control show-tick" msg="Tipo de Operación" data-live-search="true" >
                                <option value="0">Seleccione un Tipo de Operación</option>
                                <option value="CREDITO">CRÉDITO</option>
                                <option value="DEBITO">DÉBITO</option>
                            </select>
                        </div>
                    </div>
                    <label for="total_amount" class="col-green">Importe:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <label class="col-red">*</label> 
                            <i class="material-icons">attach_money</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text('total_amount', 0, ['required' => 'true', 'msg' => 'Importe', 'class' => 'form-control col-teal font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Importe " . $modulo_msg, 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    {!! Form::button('CARGAR', ['name' => "btnCargarOperacion" . $modals_btns, 'onclick' => 'insertOperacionCuentaCorriente(true)', 'class' => 'btn btn-link bg-green waves-effect']) !!}
                </div>
            {!! Form::close()!!}
            <!-- LOADING -->
            <div id="loading_modal_cargar_operacion"></div>
            <!-- FIN LOADING -->
            <div id="modal_mensaje_cargar_operacion"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL CARGAR OPERACION / MOVIMIENTO --}}
