{{-- MODAL VER MOVIMIENTOS CUENTA CORRIENTE --}}
<div class="modal fade scrollable" id="modalVerMovimientos{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">VER MOVIMIENTOS {{$titulo}}: <b id="label_persona_ver_movimientos{{$modals_btns}}" class="col-blue"></b></h4> 
                <h3>
                    Saldo Disponible:
                    <span id="label_saldo_ver_movimientos{{$modals_btns}}" class="label"></span>
                </h3>
            </div>
            {{--  BODY  --}}
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="body table-responsive">
                            <div class="{{$modals_btns}}s">
                                <br>
                                <table width="100%" id="table_VerMovimientos{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- LOADING -->
            <div id="loading_modal_ver_movimientos"></div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
            </div>
            <!-- MENSAJE -->
            <div id="modal_mensaje_ver_movimientos"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL VER MOVIMIENTOS CUENTA CORRIENTE --}}
