<div class="input-group">
    <span class="input-group-addon">
    	<label class="col-red">*</label> 
        <i class="material-icons">arrow_forward_ios</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>