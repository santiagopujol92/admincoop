{{-- NEW MODAL CONFIRM ADD RECIBO --}}
<button class="hidden" id="btnOpenModalConfirmAddReciboAfterAlta" data-backdrop="static" type="button" 
data-backdrop="static" data-toggle="modal" data-target="#modalConfirmAddReciboAfterAlta">
</button>
<div class="modal fade" id="modalConfirmAddReciboAfterAlta" tabindex="-1" role="dialog">
        <div class="sweet-overlay" tabindex="-1" style="opacity: 1.09; display: block;"></div>
        <div role="document">
            <div class="modal-content">
                <div class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="true" data-animation="pop" data-timer="null" style="display: block; margin-top: -148px;"><div class="sa-icon sa-error" style="display: none;">
                    <span class="sa-x-mark">
                        <span class="sa-line sa-left"></span>
                        <span class="sa-line sa-right"></span>
                    </span>
                    </div>
                    <h2>¿Desea agregar un recibo u orden de pago según corresponda?</h2>
                    <fieldset>
                        <input type="text" tabindex="3" placeholder="">
                        <div class="sa-input-error"></div>
                    </fieldset>
                    <div class="sa-error-container">
                        <div class="icon">!</div>
                        <p>Not valid!</p>
                    </div>
                    <div class="sa-button-container">
                        <button class="cancel" tabindex="2" type="button" onclick="closeModalConfirmAddReciboAfterAlta()" style="display: inline-block; box-shadow: none;">NO</button>
                        <div class="sa-confirm-button-container">
                            <button class="confirm btn-success" id="btnModalConfirmAddReciboAfterAlta" tabindex="1" onclick="closeModalConfirmAddReciboAfterAlta();" style="display: inline-block;">SI
                            </button>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    {{-- FIN MODAL CONFIRM ADD RECIBO --}}