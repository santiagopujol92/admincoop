{{-- NEW MODAL DELETE --}}
<div class="modal fade" id="modalDelete{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="sweet-overlay" tabindex="-1" style="opacity: 1.09; display: block;"></div>
    <div role="document">
        <div class="modal-content">
            <div class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="true" data-animation="pop" data-timer="null" style="display: block; margin-top: -148px;"><div class="sa-icon sa-error" style="display: none;">
                <span class="sa-x-mark">
                    <span class="sa-line sa-left"></span>
                    <span class="sa-line sa-right"></span>
                </span>
                </div>
                <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                    <span class="sa-body pulseWarningIns"></span>
                    <span class="sa-dot pulseWarningIns"></span>
                </div>
                <div class="sa-icon sa-info" style="display: none;"></div>
                <div class="sa-icon sa-success" style="display: none;">
                    <span class="sa-line sa-tip"></span>
                    <span class="sa-line sa-long"></span>
                    <div class="sa-placeholder"></div>
                    <div class="sa-fix"></div>
                </div>
                <div class="sa-icon sa-custom" style="display: none; background-image: url(&quot;../../images/thumbs-up.png&quot;); width: 80px; height: 80px;">
                </div>
                <h2>¿Está seguro que desea eliminar?</h2>
                <p style="display: block;">Confirmar eliminar esta/e {{$modulo_msg}}</p>
                <fieldset>
                    <input type="text" tabindex="3" placeholder="">
                    <div class="sa-input-error"></div>
                </fieldset>
                <div class="sa-error-container">
                    <div class="icon">!</div>
                    <p>Not valid!</p>
                </div>
                <div class="sa-button-container">
                    <button class="cancel" tabindex="2" data-dismiss="modal" style="display: inline-block; box-shadow: none;">CANCELAR</button>
                    <div class="sa-confirm-button-container">
                        <button class="confirm" id="btnModalConfirmDelete{{$modals_btns}}" tabindex="1" onclick="eliminar(0);" style="display: inline-block; background-color: rgb(221, 107, 85); box-shadow: rgba(221, 107, 85, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.0470588) 0px 0px 0px 1px inset;">ELIMINAR
                        </button>
                        <div class="la-ball-fall">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="loading_modal_delete"></div>
            </div>
        </div>
    </div>
</div>
{{-- FIN NEW MODAL DELETE --}}