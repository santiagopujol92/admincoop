{{-- NEW MODAL --}}
<button type="button" id="executeMessageModal" data-toggle="modal" data-target="#smallModalMessage"></button>
<div class="modal fade" id="smallModalMessage" tabindex="-1" role="dialog">
    <div class="sweet-overlay" tabindex="-1" style="opacity: 1.09; display: block;"></div>
    <div role="document">
        <div class="modal-content">
            <div class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block; margin-top: -148px;">
                <div id="modalMessageIcon"></div>
                <h2 id="modalMessageTitle"></h2>
                <p style="display: block;" id="modalMessageBody"></p>
                <div class="sa-button-container">
                    <button class="confirm" tabindex="1" class="btn btn-link waves-effect" data-dismiss="modal">ACEPTAR</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- FIN NEW MODAL --}}