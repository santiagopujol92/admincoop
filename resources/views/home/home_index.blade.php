@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection     

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}</h3>
            </div>
            <div class="row clearfix">
                {{-- PERSONAS --}}
                <a href="personas" role="button" >
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-cyan hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">people</i>
                            </div>
                            <div class="content">
                                <div class="text">PERSONAS</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_people}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END PERSONAS --}}
                {{-- PRODUCTOS --}}
                <a href="productos" role="button" >
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-brown hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">kitchen</i>
                            </div>
                            <div class="content">
                                <div class="text">MATERIALES</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_products}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END PRODUCTOS --}}
                {{-- COMPRAS --}}
                <a href="compras" role="button">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-orange hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">store</i>
                            </div>
                            <div class="content">
                                <div class="text">COMPRAS</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_purchases}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END COMPRAS --}}
                {{-- PRODUCCION --}}
                <a href="produccion" role="button">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-light-green hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">shopping_basket</i>
                            </div>
                            <div class="content">
                                <div class="text">PRODUCCIÓN</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_productions}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END PRODUCCION --}}
                {{-- VENTAS --}}
                <a href="ventas" role="button">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-blue-grey hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">VENTAS</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_sales}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END VENTAS --}}
                {{-- GASTOS --}}
                <a href="gastos" role="button">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-deep-purple hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">GASTOS</div>
                                <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END GASTOS --}}
                {{-- CUENTAS CORRIENTES --}}
                <a href="cuentas_corrientes" role="button">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-pink hover-expand-effect" style="cursor:pointer !important;">
                            <div class="icon">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">CUENTAS CORRIENTES</div>
                                <div class="number count-to" data-from="0" data-to="{{$count_current_accounts}}" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </a>
                {{-- END CUENTAS CORRIENTES --}}
            </div> 
            <!-- STOCKS BAJOS -->
{{--             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-pink">
                        <div class="font-bold m-b--35">PRODUCTOS CON BAJO STOCK </div>
                        <ul class="dashboard-stat-list">
                             @foreach ($data_products_lowstock as $reg) 
                                <li>
                                    {{$reg->description}}
                                    <span class="pull-right"><b>{{$reg->stock}}</b> <small>{{$reg->type_description}}</small></span>
                                </li>
                            @endforeach 
                        </ul>
                    </div>
                </div>
            </div> --}}
            <!-- #END# STOCKS BAJOS -->
            <!-- CANT VENTAS REALIZADAS -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-teal">
                        <div class="font-bold m-b--35">VENTAS REALIZADAS</div>
                        <ul class="dashboard-stat-list">
                            <li>
                                HOY
                                <span class="pull-right"><b>{{$count_sales_today}}</b> <small>VENTAS</small></span>
                            </li>
                            <li>
                                AYER
                                <span class="pull-right"><b>{{$count_sales_yesterday}}</b> <small>VENTAS</small></span>
                            </li>
                            <li>
                                ÚLTIMA SEMANA
                                <span class="pull-right"><b>{{$count_sales_last_week}}</b> <small>VENTAS</small></span>
                            </li>
                            <li>
                                ÚLTIMO MES
                                <span class="pull-right"><b>{{$count_sales_last_month}}</b> <small>VENTAS</small></span>
                            </li>
                            <li>
                                ÚLTIMO AÑO
                                <span class="pull-right"><b>{{$count_sales_last_year}}</b> <small>VENTAS</small></span>
                            </li>
                            <li>
                                TODAS
                                <span class="pull-right"><b>{{$count_sales}}</b> <small>VENTAS</small></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #END# CANT VENTAS REALIZADAS -->
            <!-- PRODUCCION VENTAS REALIZADAS -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                    <div class="card bg-white">
                        <div class="header">
                            <h2>TOTAL PRODUCCIÓN DE VENTAS</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>Período</th>
                                            <th>Costo</th>
                                            <th>Ganancia</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>HOY</td>
                                            {{-- <td class="col-red">$ {{$sum_cost_price_sales_today}}</td> --}}
                                            <td class="col-green">$ {{$sum_sell_price_sales_today}}</td>
                                        </tr>
                                        <tr>
                                            <td>AYER</td>
                                            <td class="col-red">$ {{$sum_cost_price_sales_yesterday}}</td>
                                            {{-- <td class="col-green">$ {{$sum_sell_price_sales_yesterday}}</td> --}}
                                        </tr>
                                        <tr>
                                            <td>ÚLTIMA SEMANA</td>
                                            <td class="col-red">$ {{$sum_cost_price_sales_last_week}}</td>
                                            {{-- <td class="col-green">$ {{$sum_sell_price_sales_last_week}}</td> --}}
                                        </tr>
                                        <tr>
                                            <td>ÚLTIMO MES</td>
                                            <td class="col-red">$ {{$sum_cost_price_sales_last_month}}</td>
                                            {{-- <td class="col-green">$ {{$sum_sell_price_sales_last_month}}</td> --}}
                                        </tr>
                                        <tr>
                                            <td>ÚLTIMO AÑO</td>
                                            <td class="col-red">$ {{$sum_cost_price_sales_last_year}}</td>
                                            {{-- <td class="col-green">$ {{$sum_sell_price_sales_last_year}}</td> --}}
                                        </tr>
                                        <tr>
                                            <td>DEL PRINCIPIO</td>
                                            {{-- <td class="col-red">$ {{$cost_price_sales}}</td> --}}
                                            <td class="col-green">$ {{$sell_price_sales}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- #END# PRODUCCION VENTAS REALIZADAS -->
            <!-- MEJORES CLIENTES -->
            @if (count($data_best_clients) > 0)
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                        <div class="card">
                            <div class="header">
                                <h2>TOP 5 MEJORES CLIENTES</h2>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover dashboard-task-infos">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Compras</th>
                                                <th>Monto Total Comprado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data_best_clients as $reg) 
                                                <tr>
                                                    <td>{{$reg->lastname}} {{$reg->name}}</td>
                                                    <td>{{$reg->cant_ventas}} </td>
                                                    <td>$ {{$reg->monto_ventas}} </td>
                                                </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            @endif
            <!-- #END# MEJORES CLIENTES-->

            <!-- VENTAS POR EMPLEADO POR PERIODO-->
        {{--     <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
                    <div class="card">
                        <div class="header">
                            <h2>VENTAS POR EMPLEADO POR PERÍODO</h2>
                        </div>
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active" style="margin-left:2px;">
                                <a href="#hoy" data-toggle="tab">HOY</a>
                            </li>
                            <li role="presentation">
                                <a href="#semana" data-toggle="tab">ÚLTIMA SEMANA</a>
                            </li>
                            <li role="presentation">
                                <a href="#mes" data-toggle="tab">ÚLTIMO MES</a>
                            </li>
                            <li role="presentation">
                                <a href="#principio" data-toggle="tab">DESDE EL PRINCIPIO</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in animated flipInX" id="hoy">
                                <div class="header">
                                    <h2>HOY</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cantidad Ventas</th>
                                                    <th>Monto Total Vendido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_sales_by_employee_today as $reg) 
                                                    <tr>
                                                        <td>{{$reg->lastname}} {{$reg->name}}</td>
                                                        <td>{{$reg->cant_ventas}}</td>
                                                        <td>$ {{$reg->monto_ventas}}</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated fadeInRight" id="semana">
                                <div class="header">
                                    <h2>ÚLTIMA SEMANA</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cantidad Ventas</th>
                                                    <th>Monto Total Vendido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_sales_by_employee_last_week as $reg) 
                                                    <tr>
                                                        <td>{{$reg->lastname}} {{$reg->name}}</td>
                                                        <td>{{$reg->cant_ventas}}</td>
                                                        <td>$ {{$reg->monto_ventas}}</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated fadeInLeft" id="mes">
                                <div class="header">
                                    <h2>ÚLTIMO MES</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cantidad Ventas</th>
                                                    <th>Monto Total Vendido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_sales_by_employee_last_month as $reg) 
                                                    <tr>
                                                        <td>{{$reg->lastname}} {{$reg->name}}</td>
                                                        <td>{{$reg->cant_ventas}}</td>
                                                        <td>$ {{$reg->monto_ventas}}</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade animated flipInX" id="principio">
                                <div class="header">
                                    <h2>DESDE EL PRINCIPIO</h2>
                                </div>
                                <div class="body">
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Cantidad Ventas</th>
                                                    <th>Monto Total Vendido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data_sales_by_employee_all as $reg) 
                                                    <tr>
                                                        <td>{{$reg->lastname}} {{$reg->name}}</td>
                                                        <td>{{$reg->cant_ventas}}</td>
                                                        <td>$ {{$reg->monto_ventas}}</td>
                                                    </tr>
                                                @endforeach 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> --}}
        </div> 
    </section>
@endsection   

<!-- Scripts Only For This Page -->
@section('scripts')
    <!-- Custom Js -->
    <script src="../scripts/home.js"></script>
    <!-- Jquery CountTo Plugin Js -->
    <script src="../assets_md/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="../assets_md/js/pages/index.js"></script>
@endsection     