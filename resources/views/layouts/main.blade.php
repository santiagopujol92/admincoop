﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }} - {{ config('app.client') }}</title>
        <!-- Favicon-->
        <link rel="icon" href="../assets_md/favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="../assets_md/css/fonts.googleapis_css_family.css" rel="stylesheet" type="text/css">
        <link href="../assets_md/css/fonts.googleapis_material_icons.css" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="../assets_md/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="../assets_md/plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="../assets_md/plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Sweetalert Css -->
        <link href="../assets_md/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

        <!-- JQuery DataTable Css -->
        <link href="../assets_md/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="../assets_md/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

        <!-- Wait Me Css -->
        <link href="../assets_md/plugins/waitme/waitMe.css" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="../assets_md/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

        <!-- Multi Select Css -->
        <link href="../assets_md/plugins/multi-select/css/multi-select.css" rel="stylesheet">

        <!-- Custom Css -->
        <link href="../assets_md/css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="../assets_md/css/themes/all-themes.css" rel="stylesheet" />

        <link href="../mycss/mycss.css" rel="stylesheet">
        
        <!-- CSS For Each Index Page -->
        @section('css')
        @show

    </head>

    <body class="theme-red">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Cargando...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Search Bar -->
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="Buscar Sección...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>
        <!-- #END# Search Bar -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid bg-indigo">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="home">{{ config('app.name') }} - {{ config('app.client') }}</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse" class="success">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Call Search -->
                        <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                        <!-- #END# Call Search -->
                        <!-- Notifications -->
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">notifications</i>
                                <span class="label-count" id="span_contador_notififaciones"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">NOTIFICACIONES</li>
                                <li class="body">
                                    {{-- NOTIFICACIONES ASIGNADAS DESDE notificaciones.js --}}
                                    <ul class="menu" id="ul-notificaciones">
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a onclick="actualizarNotificaciones('registros_logs', 'getDataAuditToNotificaction', 0);">Ver Todas las Notificaciones</a>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Notifications -->
                        <!-- Tasks -->
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">flag</i>
                                <span class="label-count">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">TAREAS</li>
                                <li class="body">
                                    <ul class="menu tasks">
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Notificación de Tarea Proxima a realizar
                                                    <small>Estado de tarea Ej: 32%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                       {{--  <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Make new buttons
                                                    <small>45%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Create new dashboard
                                                    <small>54%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Solve transition issue
                                                    <small>65%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Answer GitHub questions
                                                    <small>92%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="javascript:void(0);">Ver Todas las Tareas</a>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Tasks -->
                        <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- #Top Bar -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info bg-indigo">
                    <div class="image">
                        <img src="../assets_md/images/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{!!Auth::user()->name!!} {!!Auth::user()->lastname!!}</div>
                        <div class="email">{!!Auth::user()->email!!}</div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="logout">
                                        <i class="material-icons">input</i>
                                        Cerrar Sesión
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">MENU PRINCIPAL</li>
                        <li id="menu-home" class="active">
                            <a href="home">
                                <i class="material-icons">dashboard</i>
                                <span>Inicio</span>
                            </a>
                        </li>
                        @if (Auth::user()->type == 1 || Auth::user()->type == 2)
                            <li id="menu-personas">
                                <a href="personas">
                                    <i class="material-icons">people</i>
                                    <span>Personas</span>
                                </a>
                            </li>
                            <li id="menu-productos">
                                <a href="productos">
                                    <i class="material-icons">kitchen</i>
                                    <span>Materiales</span>
                                </a>
                            </li>
                            <li id="menu-produccion">
                                <a href="produccion">
                                    <i class="material-icons">assignment_turned_in</i>
                                    <span>Producción</span>
                                </a>
                            </li>
                            <li id="menu-compras">
                                <a href="compras">
                                    <i class="material-icons">add_shopping_cart</i>
                                    <span>Compras</span>
                                </a>
                            </li>
                            <li id="menu-ventas">
                                <a href="ventas">
                                    <i class="material-icons">attach_money</i>
                                    <span>Ventas</span>
                                </a>
                            </li>
                            <li id="menu-gastos">
                                <a href="#" class="menu-blocked">
                                    <i class="material-icons">money_off</i>
                                    <span>Gastos</span>
                                </a>
                            </li>
                            <li id="menu-cuentas-corrientes">
                                <a href="cuentas_corrientes">
                                    <i class="material-icons">local_atm</i>
                                    <span>Cuentas Corrientes</span>
                                </a>
                            </li>
                            @if (Auth::user()->type == 2)
                                <li id="menu-admin">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="material-icons">https</i>
                                        <span>Administrar Más</span>
                                    </a>
                                    <ul class="ml-menu" id="submenu-admin" >
                                        <li id="submenu-admin-tipo_productos">
                                            <a href="tipo_productos">Tipo de Materiales</a>
                                        </li>
                                        {{--  <li id="submenu-admin-tipo_personas">
                                            <a href="tipo_personas">Tipo de Personas</a>
                                        </li>  --}}
                                        <li id="submenu-admin-condiciones_ivas">
                                            <a href="condiciones_ivas">Condiciones de Iva</a>
                                        </li>
                                        {{--  <li id="submenu-admin-formas_de_pago">
                                            <a href="formas_de_pago">Formas de Pagos</a>
                                        </li>  --}}
                                        {{--  <li id="submenu-admin-estados_remitos">
                                            <a href="estados_remitos">Estados de Remitos</a>
                                        </li>
                                        <li id="submenu-admin-estados_facturas">
                                            <a href="estados_facturas">Estados de Facturas</a>
                                        </li>  --}}
                                        {{--  <li id="submenu-admin-tipo_movimientos_cuentas">
                                            <a href="tipo_movimientos_cuentas">Tipo de Comprobante de Cuenta C.</a>
                                        </li>  --}}
                                        {{--  <li id="submenu-admin-estados_cuentas_corrientes">
                                            <a href="estados_cuentas_corrientes">Estados de Cuenta C.</a>
                                        </li>  --}}
                                        <li id="submenu-admin-paises">
                                            <a href="paises">Países</a>
                                        </li>
                                        <li id="submenu-admin-provincias">
                                            <a href="provincias">Provincias</a>
                                        </li>
                                        <li id="submenu-admin-ciudades">
                                            <a href="ciudades">Ciudades</a>
                                        </li>
                                        <li id="submenu-admin-maquinas">
                                            <a href="maquinas">Máquinas</a>
                                        </li>
                                        <li id="submenu-admin-usuarios">
                                            <a href="usuarios">Usuarios</a>
                                        </li>
                                        {{-- <li id="submenu-admin-tipo_usuarios">
                                                <a href="tipo_usuarios">Tipos de Usuarios</a>
                                        </li> --}}
                                        <li id="submenu-admin-registros_logs">
                                                <a href="registros_logs"> Registros de Actividades</a>
                                        </li>
                                   </ul>
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        &copy; {{ config('app.year') }} <a href="javascript:void(0);">{{ config('app.name') }} - {{ config('app.creator') }}</a>.
                    </div>
                    <div class="version">
                        <b>Version: </b> {{ config('app.version') }}
                    </div>
                </div>
                <!-- #Footer -->
            </aside>
            <!-- #END# Left Sidebar -->
            <!-- Right Sidebar -->
            <aside id="rightsidebar" class="right-sidebar">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#skins" data-toggle="tab">COLOR</a>
                    </li>
 {{--                    <li role="presentation">
                        <a href="#settings" data-toggle="tab">AJUSTES</a>
                    </li> --}}
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                        <ul class="demo-choose-skin">
                            <li data-theme="red">
                                <div class="red"></div>
                                <span>Rojo</span>
                            </li>
                            <li data-theme="pink">
                                <div class="pink"></div>
                                <span>Rosa</span>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                                <span>Púrpura</span>
                            </li>
                            <li data-theme="deep-purple">
                                <div class="deep-purple"></div>
                                <span>Púrpura Profundo</span>
                            </li>
                            <li data-theme="indigo" class="active">
                                <div class="indigo"></div>
                                <span>Indigo</span>
                            </li>
                            <li data-theme="blue">
                                <div class="blue"></div>
                                <span>Azul</span>
                            </li>
                            <li data-theme="light-blue">
                                <div class="light-blue"></div>
                                <span>Celeste</span>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>
                                <span>Cian</span>
                            </li>
                            <li data-theme="teal">
                                <div class="teal"></div>
                                <span>Verde Azulado</span>
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                                <span>Verde</span>
                            </li>
                            <li data-theme="light-green">
                                <div class="light-green"></div>
                                <span>Verde Claro</span>
                            </li>
                            <li data-theme="lime">
                                <div class="lime"></div>
                                <span>Lima</span>
                            </li>
                            <li data-theme="yellow">
                                <div class="yellow"></div>
                                <span>Amarillo</span>
                            </li>
                            <li data-theme="amber">
                                <div class="amber"></div>
                                <span>Amber</span>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                                <span>Naranja</span>
                            </li>
                            <li data-theme="deep-orange">
                                <div class="deep-orange"></div>
                                <span>Naranja Profundo</span>
                            </li>
                            <li data-theme="brown">
                                <div class="brown"></div>
                                <span>Marron</span>
                            </li>
                            <li data-theme="grey">
                                <div class="grey"></div>
                                <span>Gris</span>
                            </li>
                            <li data-theme="blue-grey">
                                <div class="blue-grey"></div>
                                <span>Gris Azulado</span>
                            </li>
                            <li data-theme="black">
                                <div class="black"></div>
                                <span>Negro</span>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="settings">
                        <div class="demo-settings">
                            <p>AJUSTES GENERALES</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Uso de Panel de Informes</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Redirección de Email</span>
                                    <div class="switch">
                                        <label><input type="checkbox"><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>AJUSTES DE SISTEMA</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Notificaciones</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Actualizaciones Automáticas</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>AJUSTES DE CUENTA</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Offline</span>
                                    <div class="switch">
                                        <label><input type="checkbox"><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Location Permission</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- #END# Right Sidebar -->
        </section>

    @yield('content')
     
        <!-- Jquery Core Js -->
        <script src="../assets_md/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="../assets_md/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../assets_md/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../assets_md/plugins/node-waves/waves.js"></script>

        <!-- Custom Js -->
        <script src="../assets_md/js/admin.js"></script>

        <script src="../assets_md/js/pages/ui/dialogs.js"></script>

        <!-- Demo Js -->
        <script src="../assets_md/js/demo.js"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="../assets_md/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

        <!-- Moment Plugin Js -->
        <script src="../assets_md/plugins/momentjs/moment.js"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Bootstrap Notify Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-notify/bootstrap-notify.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../assets_md/plugins/node-waves/waves.js"></script>

        <!-- SweetAlert Plugin Js -->
        <script src="../assets_md/plugins/sweetalert/sweetalert.min.js"></script>

        <!-- Scripts Personalized Functions -->
        <script src="../scripts/funciones.js"></script>
        {{-- <script src="../scripts/notificaciones.js"></script> --}}

        <!-- Scripts For Each Index Page -->
        @section('scripts')
        @show

        {{-- Modal generico de Mensaje --}}
        @include('global_modals.modal_message_global')

    </body>
</html>