<div class="row">
    <div class="col-sm-6">
        <label for="id_person">Empleado:</label>
        <div class="input-group input-above4">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                <select name="id_person" required class="form-control show-tick" msg="Empleado" data-live-search="true" >
                    <option value="0">Seleccione un Empleado</option>
                    @foreach ($data_persons as $reg) 
                        <option domicilio="{{$reg->adress}}" condicion_iva="{{$reg->condicion_iva_desc}}" cuit="{{$reg->cuit}}" value="{{$reg->id}}">{{$reg->lastname}} {{$reg->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <label for="lote">Lote:</label>
        <div class="input-group">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                {!! Form::text('lote', null, ['class' => 'form-control', 'placeholder' => "Lote", 'msg' => 'Lote', 'required' => true, 'autofocus' => true]) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <label for="id_machine">Máquina:</label>
        <div class="input-group input-above3">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                <select name="id_machine" required class="form-control show-tick" msg="Máquina" data-live-search="true" >
                    <option value="0">Seleccione una Máquina</option>
                    @foreach ($data_machines as $reg) 
                        <option value="{{$reg->id}}">{{$reg->description}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <label for="turno">Turno:</label>
        <div class="input-group input-above3">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                <select name="turno" required class="form-control show-tick" msg="Turno" data-live-search="true" >
                    <option value="0">Seleccione un Turno</option>
                    <option value="M">MAÑANA</option>
                    <option value="D">TARDE</option>
                    <option value="N">NOCHE</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <label for="id_type_product">Tipo de Material:</label>
        <div class="input-group input-above2">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                <select name="id_type_product" class="form-control show-tick" msg="Tipo de Material" required data-live-search="true" onchange="changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Material', this.form.id, this.value);">
                    <option value="0">Seleccione un Tipo de Material</option>
                    @foreach ($data_type_products as $reg) 
                        <option value="{{$reg->id}}">{{$reg->description}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="id_product">Material:</label>
        <div class="input-group input-above2">
            <span class="input-group-addon">
                <label class="col-red">*</label> 
            </span>
            <div class="form-line">
                <select name="id_product" required class="form-control show-tick" msg="Material" data-live-search="true" >
                    <option value="0">Seleccione un Material</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="quantity_product">Cantidad Prod.:</label>
        <div class="input-group">
            <div class="form-line">
                {!! Form::text('quantity_product', null, ['required' => true, 'class' => 'form-control', 'msg' => 'Cantidad Producto', 'placeholder' => "Cantidad Producto", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
            </div>
        </div>
    </div>
</div>