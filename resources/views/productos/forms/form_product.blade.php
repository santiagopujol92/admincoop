<div class="col-sm-6">
    <label for="description">Nombre:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">description</i>
        </span>
        <div class="form-line">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'msg' => 'Nombre', 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="description">Código {{$modulo_msg}}:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">#</i>
        </span>
        <div class="form-line">
            {!! Form::text('code_product', null, ['class' => 'form-control', 'placeholder' => "Ingrese Código " . $modulo_msg, 'msg' => 'Código ' . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="id_type_product">Tipo de {{$modulo_msg}}:</label>
    <div class="input-group input-above2">
        <span class="input-group-addon">
            <label class="col-red">*</label>
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            <select name="id_type_product" class="form-control show-tick" msg="Tipo de Materia Prima" required data-live-search="true" >
                <option value="0">Seleccione Tipo de {{$modulo_msg}}</option>
                @foreach ($data_type_products as $reg)
                    <option value="{{$reg->id}}" >{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="type_metric_product">Unidad de Medida {{$modulo_msg}}:</label>
    <div class="input-group input-above">
        <span class="input-group-addon">
            <label class="col-red">*</label>
            <i class="material-icons">arrow_forward_ios</i>
        </span>
        <div class="form-line">
            <select name="type_metric_product" onchange="changeLabelUnidad(this.value)" class="form-control show-tick" msg="Tipo de Materia Prima" required data-live-search="true" >
                <option value="0">Seleccione Tipo de Medición de {{$modulo_msg}}</option>
                <option value="KG">KG</option>
                <option value="UNIDAD/ES">UNIDAD/ES</option>
            </select>
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="min_purchase_price">Precio Mínimo Compra<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            {!! Form::text('min_purchase_price', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Precio Mínimo Compra " . $modulo_msg, 'msg' => 'Precio Total Compra', 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="max_purchase_price">Precio Máximo Compra<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            {!! Form::text('max_purchase_price', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Precio Máximo Compra " . $modulo_msg,  'msg' => 'Precio Total Compra', 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="last_purchase_price">Último Precio Compra<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            {!! Form::text('last_purchase_price', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Último Precio Compra " . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="last_sale_price">Último Precio Venta<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">attach_money</i>
        </span>
        <div class="form-line">
            {!! Form::text('last_sale_price', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Último Precio Venta " . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="min_quantity_stock">Mínimo Nivel de Stock<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            {!! Form::text('min_quantity_stock', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Mínimo Nivel de Stock " . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <label for="max_quantity_stock">Máximo Nivel de Stock<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            {!! Form::text('max_quantity_stock', null, ['class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Máximo Nivel de Stock " . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>

<div class="col-sm-12">
    <label for="stock" class="col-red">Stock Disponible<b class="label_unidad_product"></b>:</label>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">shopping_basket</i>
        </span>
        <div class="form-line">
            {!! Form::text('stock', 0, ['class' => 'form-control col-teal font-bold', 'disabled' => 'true', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Stock Disponible " . $modulo_msg, 'autofocus' => true]) !!}
        </div>
    </div>
</div>