{{-- MODAL AUMENTAR STOCK --}}
<div class="modal fade scrollable" id="modalAumentarStock{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog" id="modal_size_class" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">CARGA DE STOCK {{$titulo}}</h4>
            </div>
            {!! Form::open(['id' => "formAumentarStock" . $form, 'method' => 'POST']) !!}
                <div class="modal-body">
                    <label for="stock" class="col-red">Stock Disponible<b class="label_unidad_product"></b>:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">arrow_forward_ios</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text('stock', 0, ['class' => 'form-control col-r  ed font-bold', 'disabled' => true, 'placeholder' => "Stock Disponible " . $modulo_msg, 'autofocus' => true]) !!}
                        </div>
                    </div>
                    <label for="increment_stock" class="col-green">Incremento Stock<b class="label_unidad_product"></b>:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">arrow_forward_ios</i>
                        </span>
                        <div class="form-line">
                            {!! Form::text('increment_stock', 0, ['class' => 'form-control col-green font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Incremento de Stock" . $modulo_msg, 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    {!! Form::button('CARGAR', ['name' => "btnguardarAumentarStock" . $modals_btns, 'onclick' => 'updateStock(true)', 'class' => 'btn btn-link bg-green waves-effect']) !!}
                </div>
            {!! Form::close()!!}
            <!-- LOADING -->
            <div id="loading_modal_aumentar_stock"></div>
            <!-- FIN LOADING -->
            <div id="modal_mensaje_aumentar_stock"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL AUMENTAR STOCK --}}
