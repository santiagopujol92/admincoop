@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" data-backdrop="static" type="button" onclick="clearForm('form{{$form}}');configSaveButtonAdd();restartConfigForm();" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVO {{$titulo}}</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}ES</h4>
	                    </div>
	                    <div class="body table-responsive">
							<label><u>FILTROS:</u></label>
							<div class="row">
								<div class="col-lg-2">
									<label for="filtro_ult_precio_minimo_venta">Últ. Precio Mín Venta:</label>
									<div class="input-group">
										<div class="form-line">
											{!! Form::text('filtro_ult_precio_minimo_venta', null, ['class' => 'form-control', 'onchange' => 'listar()', 'onkeypress' => 'return isNumberKey(event);']) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-2">
									<label for="filtro_ult_precio_maximo_venta">Últ. Precio Máx Venta:</label>
									<div class="input-group">
										<div class="form-line">
											{!! Form::text('filtro_ult_precio_maximo_venta', null, ['class' => 'form-control', 'onchange' => 'listar()', 'onkeypress' => 'return isNumberKey(event);']) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-2">
									<label for="filtro_stock_minimo">Stock Mínimo:</label>
									<div class="input-group">
										<div class="form-line">
											{!! Form::text('filtro_stock_minimo', null, ['class' => 'form-control', 'onchange' => 'listar()', 'onkeypress' => 'return isNumberKey(event);']) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-2">
									<label for="filtro_stock_maximo">Stock Máximo:</label>
									<div class="input-group">
										<div class="form-line">
											{!! Form::text('filtro_stock_maximo', null, ['class' => 'form-control', 'onchange' => 'listar()', 'onkeypress' => 'return isNumberKey(event);']) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<label for="name">Tipo de Material:</label>
									<div class="input-group input-above">
										<div class="form-line">
											<select name="filtro_tipo_material" multiple onchange="listar()" class="form-control show-tick" msg="Tipo" required data-live-search="true" >
												@foreach ($data_type_products as $reg)
													<option value="{{$reg->id}}" >{{$reg->description}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
	                        <div class="{{$modals_btns}}s">
		                        <table id="table_{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>

	 @include('global_modals.modal_opciones_abm')
 	 @include('global_modals.modal_edit_add_global')
	 @include('global_modals.modal_delete_global')
	 @include('productos.modals.modal_aumentar_stock')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/{{$module}}.js"></script>
    <script src="../scripts/abm_basic.js"></script>
@endsection