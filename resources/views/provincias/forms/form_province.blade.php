<div class="input-group input-above">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">location_city</i>
    </span>
    <div class="form-line">
        <select name="id_country" autofocus="true" msg="País" class="form-control show-tick" required data-live-search="true" >
            <option value="0">Seleccione un País</option>
            @foreach ($data_countries as $reg)
                <option value="{{$reg->id}}" >{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">location_city</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'msg' => 'Nombre', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true ]) !!}
    </div>
</div>