@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection    

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>{{$modulo_msg}}s</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">
	    		            <!-- LOADING2 -->
			                <div id="loading_list"></div>
			                <!-- FIN LOADING2 -->
	                     	<div class="icon-button-demo">
				                <button class="btn btn-link bg-pink waves-effect pull-right" data-backdrop="static" type="button" onclick="clearForm('form{{$form}}');restartConfigForm();" data-toggle="modal" data-target="#modal{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	{{-- <i class="material-icons">add_circle</i>  --}}
	                                	<span class="icon-name">NUEVO {{$titulo}}</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE {{$titulo}}S</h4>
	                    </div>
	                    <div class="body table-responsive">
	                         <div class="{{$modals_btns}}s">
		                        <table id="table_Usuarios" class="table table-bordered table-striped table-hover dataTable">
		                        </table>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>
    
   	 @include('global_modals.modal_opciones_abm')
  	 @include('global_modals.modal_edit_add_global')
	 @include('global_modals.modal_delete_global')

@endsection

<!-- Scripts Only For This Page -->
@section('scripts')
	<!-- Custom Js -->
	<script src="../scripts/usuarios.js"></script>
@endsection    