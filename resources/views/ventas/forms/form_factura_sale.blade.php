{{-- FORMULARIO FACTURA VENTA --}}
{!! Form::open(['id' => "formFacturaVenta", 'method' => 'POST']) !!}
    <h4 align="right">Nro Factura: 
        <span class="label label-primary" id="label_invoice_id">{{$id_new_invoice}}</span>
    </h4>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label><u>Nro de Remito:</u> <b name="id_sale"></b></label>
        </div>
        <div class="col-md-6">
            <label><u>Fecha Emisión:</u> <b name="issue_date"></b></label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label><u>Comprador:</u> <b name="person_desc"></b></label>
        </div>
        <div class="col-md-6">
            <label><u>Estado Factura:</u> <b name="status_invoice_desc"></b></label>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="header">
            <b class="col-red">Productos Facturados</b>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Código Producto</th>
                                <th>Nombre Producto</th>
                                <th>Cantidad</th>
                                <th>Unidad de Medida</th>
                                <th>Precio Unit.</th>
                                <th>Precio Subtotal</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_productos_facturados"></tbody>
                    </table>
                    <label class="pull-right">Total Cantidad Producto: <b class="col-red" name="labelTotalCantProd"></b></label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <label for="selectAlicuotaIva">Tipo Alicuota Iva:</label>
            <div class="input-group input-above4">
                <div class="form-line">
                    <select name="selectAlicuotaIva" value="PORC" onchange="changeTipoCalculablesFactura(this.value, 'divSelectAlicuotaIvaPorc', 'divSelectAlicuotaIvaUnit', 'alicuota_iva_porc', 'alicuota_iva')" class="form-control show-tick" data-live-search="true" >
                        <option value="PORC">Iva %</option>
                        <option value="UNIT">Iva Unit.</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectAlicuotaIvaUnit">
            <label for="alicuota_iva">Alicuota Iva:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('alicuota_iva', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, false, "iva")', 'onkeypress' => 'return isNumberKey(event);', 'msg' => 'Alicuota Iva', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectAlicuotaIvaPorc">
            <label for="alicuota_iva_porc">Alicuota Iva %:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">%</i>
                </span>
                <div class="form-line">
                    {!! Form::text('alicuota_iva_porc', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, true, "iva")', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Alicuota Iva', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label for="selectDescuento">Tipo Descuento:</label>
            <div class="input-group input-above3">
                <div class="form-line">
                    <select name="selectDescuento" value="PORC" onchange="changeTipoCalculablesFactura(this.value, 'divSelectDescuentoPorc', 'divSelectDescuentoUnit', 'descuento_porc', 'descuento')" class="form-control show-tick" data-live-search="true" >
                        <option value="PORC">Descuento %</option>
                        <option value="UNIT">Descuento Unit.</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectDescuentoUnit">
            <label for="descuento">Descuento:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('descuento', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, false, "descuento")', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Descuento', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectDescuentoPorc">
            <label for="descuento_porc">Descuento %:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">%</i>
                </span>
                <div class="form-line">
                    {!! Form::text('descuento_porc', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, true, "descuento")', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Descuento', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <label for="selectRetencion">Tipo Retención:</label>
            <div class="input-group input-above2">
                <div class="form-line">
                    <select name="selectRetencion" value="PORC" onchange="changeTipoCalculablesFactura(this.value, 'divSelectRetencionPorc', 'divSelectRetencionUnit', 'retencion_porc', 'retencion')" class="form-control show-tick" data-live-search="true" >
                        <option value="PORC">Retención %</option>
                        <option value="UNIT">Retención Unit.</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectRetencionUnit">
            <label for="retencion">Retención:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('retencion', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, false, "retencion")', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Retención', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-2" name="divSelectRetencionPorc">
            <label for="retencion_porc">Retención %:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">%</i>
                </span>
                <div class="form-line">
                    {!! Form::text('retencion_porc', 0, ['class' => 'form-control font-bold', 'onkeyup' => 'calcularMontoConVariables(this.value, true, "retencion")', 'onkeypress' => 'return isNumberKey(event)', 'msg' => 'Retención', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label for="expiration_date">Fecha Vencimiento:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">date_range</i>
                </span>
                <div class="form-line">
                    {!! Form::text('expiration_date', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Vencimiento', 'placeholder' => "Seleccione Fecha Vencimiento" ]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="total_amount">Importe Total:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('total_amount', null, ['class' => 'form-control font-bold col-green', 'onkeypress' => 'return isNumberKey(event)', 'style' => 'text-align: center; font-size:20px', 'msg' => 'Costo Total', 'placeholder' => "Importe Total Factura", 'required' => true, 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
    </div>
	<button type="button" class="btn bg-grey btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
    {!! Form::button('GENERAR FACTURA', ['name' => "btnguardarFacturaVenta", 'onclick' => 'agregarFacturaVenta(true)', 'class' => 'btn btn-link bg-green waves-effect pull-right']) !!}
    <button type="button" href="#" disabled="disabled" name="btnImprimirFacturaVenta" style="margin-right: 5px" title="Imprimir Factura" onclick="imprimirFacturaVenta();" class="pull-right btn bg-blue btn-link waves-effect">
        IMPRIMIR FACTURA
    </button>
{!! Form::close()!!}

{{-- FIN FORMULARIO FACTURA VENTA --}}