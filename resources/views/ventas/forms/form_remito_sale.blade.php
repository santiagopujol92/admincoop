{{-- FORMULARIO REMITO VENTA --}}
{!! Form::open(['id' => "formRemitoVenta", 'method' => 'POST']) !!}
    <h4 align="left">Nro Remito: 
        <span class="label label-primary" id="label_sale_id">{{$id_new_sale}}</span>
    </h4>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label for="id_person">Comprador:</label>
            <div class="input-group input-above4">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <select name="id_person" required class="form-control show-tick" msg="Cliente" data-live-search="true" >
                        <option value="0">Seleccione un Cliente</option>
                        @foreach ($data_persons as $reg) 
                            <option domicilio="{{$reg->adress}}" condicion_iva="{{$reg->condicion_iva_desc}}" cuit="{{$reg->cuit}}" value="{{$reg->id}}">{{$reg->lastname}} {{$reg->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="id_status_shipment">Estado Remito:</label>
            <div class="input-group input-above3">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">arrow_forward_ios</i>
                </span>
                <div class="form-line">
                    <select name="id_status_shipment" disabled required class="form-control show-tick" msg="Estado Remito" data-live-search="true" >
                        <option value="0">Seleccione un Estado Remito</option>
                        @foreach ($data_status_shipments as $reg) 
                            <option value="{{$reg->id}}">{{$reg->description}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <b class="col-red">Productos Asignados</b>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <br>
                <div class="col-md-4">
                    <label for="id_type_product">Tipo de Material:</label>
                    <div class="input-group input-above2">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            <select name="id_type_product" class="form-control show-tick" msg="Tipo de Material" data-live-search="true" onchange="changeDataSelectTarget('productos', 'findByTypeProductId', 'id_product', 'Material', this.form.id, this.value);showDataProduct(this.form.id, 'id_product', 0);">
                                <option value="0">Seleccione un Tipo de Material</option>
                                @foreach ($data_type_products as $reg) 
                                    <option value="{{$reg->id}}">{{$reg->description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="id_product">Material:</label>
                    <div class="input-group input-above2">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            <select name="id_product" class="form-control show-tick" onchange="showDataProduct(this.form.id, this.name, this.value)" msg="Material" data-live-search="true" >
                                <option value="0">Seleccione un Material</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Cantidad:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('quantity', null, ['class' => 'form-control', 'msg' => 'Nro Remito', 'placeholder' => "Cantidad", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="description">Precio Venta:</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line">
                            {!! Form::text('purchase_price', null, ['class' => 'form-control', 'msg' => 'Nro Remito', 'placeholder' => "Precio", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6" align="left">
                    <h6 name="datosProductoSelected" class="hidden">
                        <label class="col-blue"><u>Información de Producto:</u> </label><br>
                        Stock Disponible: <label name="labelStockForProduct" class="col-red"></label><br>
                        Ult. Precio Venta: <label name="labelLastPriceForProduct" class="col-red"></label><br>
                        Unidad de Medida: <label name="labelTypeMetricForProduct" class="col-red"></label>
                    </h6>
                </div>
                <div class="col-md-6" align="right">
                    <button type="button" href="#" title="Asignar Producto" name="btnAsignarProductoRemitoVenta" onclick="asignarProductoLista();" class="btn bg-green btn-link waves-effect">
                        ASIGNAR PRODUCTO
                    </button>
                </div>
            </div>
        </div>
        <br>
        <div id="modal_messsage_productos_remito_venta"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <table id="table_productos_asignados" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Código Producto</th>
                                <th>Nombre Producto</th>
                                <th>Cantidad</th>
                                <th>Unidad de Medida</th>
                                <th>Precio Unit.</th>
                                <th>Precio Subtotal</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_productos_asignados"></tbody>
                    </table>
                    <label class="pull-right">Total Cantidad Producto: <b class="col-red" name="labelTotalCantProd"></b></label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <label for="sell_date">Fecha Venta:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">date_range</i>
                </span>
                <div class="form-line">
                    {!! Form::text('sell_date', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Venta', 'placeholder' => "Seleccione Fecha de Venta", 'required' => true ]) !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <label for="sell_cost">Costo Total:</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <label class="col-red">*</label> 
                    <i class="material-icons">attach_money</i>
                </span>
                <div class="form-line">
                    {!! Form::text('sell_cost', null, ['class' => 'form-control font-bold col-green', 'onkeypress' => 'return isNumberKey(event)', 'style' => 'text-align: center; font-size:20px', 'msg' => 'Costo Total', 'placeholder' => "Costo Total de Venta", 'required' => true, 'readonly' => 'readonly', 'autofocus' => true]) !!}
                </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn bg-grey btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
    {!! Form::button('GENERAR REMITO', ['name' => "btnguardarRemitoVenta", 'onclick' => 'agregarRemitoVenta(true)', 'class' => 'btn btn-link bg-green waves-effect pull-right']) !!}
    <button type="button" href="#" style="margin-right: 5px" name="btnImprimirRemitoVenta" title="Imprimir Remito" onclick="imprimirRemitoVenta();" class="pull-right btn bg-blue btn-link waves-effect">
        IMPRIMIR REMITO
    </button>
{!! Form::close()!!}
{{-- FIN FORMULARIO REMITO VENTA --}}