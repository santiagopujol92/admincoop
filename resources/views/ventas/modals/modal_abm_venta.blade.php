{{-- MODAL ABM VENTA --}}
<div class="modal fade scrollable" id="modal{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title titulo-modal-abm">{{$titulo}}</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tab-col-pink" style="width:100%" role="tablist">
                        <li role="presentation" class="active" style="width:50%" >
                            <a href="#tabRemitoVenta" id="btnTabRemitoVenta" data-toggle="tab" align="center" aria-expanded="false">
                                <i class="material-icons">description</i> REMITO
                            </a>
                        </li>
                        <li role="presentation" style="width:50%" id="liTabFacturaVenta" >
                            <a href="#tabFacturaVenta" id="btnTabFacturaVenta" data-toggle="tab" align="center" aria-expanded="false">
                                <i class="material-icons">payment</i> FACTURA
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tabRemitoVenta">
                            @include($module . '.forms.form_remito_sale')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabFacturaVenta">
                            @include($module . '.forms.form_factura_sale')
                        </div>
                    </div>
                </div>
            <div class="modal-footer" >
            </div>
            <!-- LOADING -->
            <div id="loading_modal"></div>
            <!-- FIN LOADING -->
            <div id="modal_mensaje"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL ABM VENTA --}}