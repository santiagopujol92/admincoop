{{-- MODAL RECIBOS FACTURA --}}
<div class="modal fade scrollable" id="modalCargarRecibo{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    CARGAR RECIBO FACTURA DE {{$titulo}}: 
                    <b id="label_persona_cargar_recibo{{$modals_btns}}" class="col-blue"></b>
                </h4> 
                <br>
                <div class="row">
                    <div class="input-group input-above4" >
                        <span class="input-group-addon">
                        </span>
                        <div class="form-line col-md-12" >
                            <select id="CargarRecibo_id_invoice" class="form-control show-tick" onchange="changeSelectFacturaIdInvoiceCofing(this.value)" msg="Factura" data-live-search="true" >
                                <option value="0">Seleccione una Factura</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            {{--  BODY  --}}
            <div class="modal-body" style="margin-top:-18px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! Form::open(['id' => "formCargarReciboYMovimiento" , 'method' => 'POST']) !!}
                        {!! Form::text('id_current_account', 0, ['class' => 'form-control font-bold hidden']) !!}
                        {!! Form::hidden('id_invoice', null) !!}
                        {!! Form::hidden('type_operation', 'DEBITO') !!}
                        {!! Form::hidden('id_type_movement_account', 1) !!}
                        {!! Form::hidden('id_current_account', null) !!}
                        {!! Form::hidden('monto_venta', null) !!}
                        {!! Form::hidden('balance', 0) !!}
                        <div class="row"> 
                            <div class="col-md-6">
                                <h4>
                                    Monto Total Factura: $
                                    <span id="label_total_monto_factura{{$modals_btns}}" class="label"></span>
                                </h4>
                                <h4>
                                    Monto Adeudado Factura: $
                                    <span id="label_monto_adeudado_recibos{{$modals_btns}}" class="label"></span>
                                </h4>
                            </div>
                            <div class="col-md-6">
                                <label for="nro_orden_pago">Nro. Orden de Pago:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">#</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('nro_orden_pago', null, ['class' => 'form-control', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Nro Orden de Pago", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3">
                                <label for="effective_amount">Importe Efectivo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('effective_amount', 0, ['onkeyup' => 'calcularTotalReciboFinal()', 'class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Importe en Efectivo", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="card_amount">Importe Tarjeta:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('card_amount', 0, ['onkeyup' => 'calcularTotalReciboFinal()', 'class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Importe en Tarjeta", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="retencion_amount">Retención:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('retencion_amount', 0, ['onkeyup' => 'calcularTotalReciboFinal()', 'class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Retención", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="retencion2_amount">Retención 2:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('retencion2_amount', 0, ['onkeyup' => 'calcularTotalReciboFinal()', 'class' => 'form-control font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Ingrese Retención 2", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- CHEQUES --}}
                        <div class="card">
                            <div class="header">
                                <b class="col-red">Cheques Asignados</b>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div class="col-md-3">
                                        <label for="nro_cheque">Nro Cheque:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('nro_cheque', null, ['class' => 'form-control', 'placeholder' => "Cheque", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="banco">Banco:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('banco', null, ['class' => 'form-control', 'placeholder' => "Banco", 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="concepto">Concepto:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('concepto', null, ['class' => 'form-control', 'placeholder' => "Concepto", 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="nombre">Nombre:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => "Nombre", 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label for="cuit">Cuit:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('cuit', null, ['class' => 'form-control', 'placeholder' => "Cuit", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="importe">Importe:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('importe', null, ['class' => 'form-control', 'placeholder' => "Importe", 'onkeypress' => 'return isNumberKey(event)', 'autofocus' => true]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="fecha_emision">Fecha Emisión:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('fecha_emision', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Emisión', 'placeholder' => "Selec F. Emisión" ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="fecha_pago">Fecha Pago:</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                {!! Form::text('fecha_pago', null, ['class' => 'datetimepicker form-control', 'msg' => 'Fecha Pago', 'placeholder' => "Selec F. Pago" ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" align="right"  style="padding-top:26px;">
                                        <button type="button" href="#" title="Asignar Cheque" onclick="asignarChequeRecibosLista();" class="btn bg-green btn-link waves-effect">
                                            ASIGNAR CHEQUE
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="modal_messsage_cheques_recibo"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Nro Cheque</th>
                                                    <th>F. Emisión</th>
                                                    <th>Banco</th>
                                                    <th>Concepto</th>
                                                    <th>Nombre</th>
                                                    <th>Cuit</th>
                                                    <th>Importe</th>
                                                    <th>F. Pago</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_chequesrecibo_asignados"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="total_amount" class="col-green">Total Importe Recibo:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <label class="col-red">*</label> 
                                        <i class="material-icons">attach_money</i>
                                    </span>
                                    <div class="form-line">
                                        {!! Form::text('total_amount', null, ['readonly' => true, 'required' => 'true', 'msg' => 'Importe', 'class' => 'form-control col-teal font-bold', 'onkeypress' => 'return isNumberKey(event)', 'placeholder' => "Total Importe Recibo", 'autofocus' => true]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {!! Form::close()!!}
                </div>
            </div>
            <!-- LOADING -->
            <div id="loading_modal_cargar_recibo"></div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
                {!! Form::button('CARGAR RECIBO', ['name' => "btnCargarRecibo" . $modals_btns, 'onclick' => 'insertReciboByInvoice(true)', 'class' => 'btn btn-link bg-green waves-effect pull-right']) !!}
            </div>
            <!-- MENSAJE -->
            <div id="modal_mensaje_cargar_recibo"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL RECIBOS FACTURA --}}