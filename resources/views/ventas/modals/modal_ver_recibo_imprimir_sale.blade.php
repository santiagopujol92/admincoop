{{-- MODAL RECIBO DE FACTURA IMPRIMIR --}}
<div class="modal fade scrollable" id="modalVerReciboById{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">VER RECIBO FACTURA DE {{$titulo}}: <b id="label_numero_recibo_ver_recibo_imprimir{{$modals_btns}}" class="col-blue"></b></h4> 
            </div>
            {{--  BODY  --}}
            <div class="modal-body">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <hr>
                    {!! Form::open(['id' => "LabelFormVerReciboImprimir"]) !!}
                        <div class="row">
                            <div class="col-md-4">
                                <label><u>Número Recibo:</u> <b name="label_numero_recibo"></b></label>
                            </div>
                            <div class="col-md-4">
                                <label><u>Número Factura:</u> <b name="label_numero_factura"></b></label>
                            </div>
                            <div class="col-md-4">
                                <label><u>Importe Efectivo:</u> <b name="label_effective_amount"></b></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label><u>Importe Tarjeta:</u> <b name="label_card_amount"></b></label>
                            </div>
                            <div class="col-md-4">
                                <label><u>Retención:</u> <b name="label_retencion_amount"></b></label>
                            </div>
                            <div class="col-md-4">
                                <label><u>Retención 2:</u> <b name="label_retencion2_amount"></b></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label><u>Fecha Creación:</u> <b name="label_fecha"></b></label>
                            </div>
                            <div class="col-md-4">
                                <label><u>Nro. Orden de Pago:</u> <b name="label_nro_orden_pago"></b></label>
                            </div>
                        </div>
                        
                        <br>
                        <div class="header">
                            <b class="col-red">Cheques Asignados</b>
                        </div>
                        <table width="100%" id="tbody_chequesrecibo_ver_recibo_imprimir" class="table table-bordered table-striped table-hover dataTable">
                        </table>
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <label style="font-size:18px"><u>Total Monto:</u> <b class="col-red" name="label_total_amount"></b></label>
                            </div>
                        </div>
                    {!! Form::close()!!}
                    <br>
                </div>
            </div>
            <!-- LOADING -->
            <div id="loading_modal_ver_recibo_imprimir"></div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>
                <button type="button" href="#" style="margin-right: 5px" name="btnImprimirReciboVenta" title="Imprimir Recibo" onclick="imprimirReciboVenta();" class="pull-right btn bg-blue btn-link waves-effect">
                    IMPRIMIR RECIBO
                </button>                
            </div>
            <!-- MENSAJE -->
            <div id="modal_mensaje_ver_recibo_imprimir"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL RECIBO DE FACTURA IMPRIMIR--}}
    