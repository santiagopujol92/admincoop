{{-- MODAL VER RECIBOS FACTURA --}}
<div class="modal fade scrollable" id="modalVerRecibos{{$modals_btns}}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    VER RECIBOS DE FACTURA DE {{$titulo}}: 
                    <b id="label_persona_ver_recibos{{$modals_btns}}" class="col-blue"></b>
                </h4> 
                <h4>
                    Nº Factura:
                    <span id="label_factura_ver_recibos{{$modals_btns}}" class="label label-primary"></span>
                </h4>
                <h4>
                    Monto Total Factura: $
                    <span id="label_total_monto_factura_ver_recibos{{$modals_btns}}" class="label"></span>
                </h4>
                <h4>
                    Monto Adeudado Factura: $
                    <span id="label_monto_adeudado_ver_recibos{{$modals_btns}}" class="label"></span>
                </h4>
            </div>
            {{--  BODY  --}}
            <div class="modal-body">
                <div class="body">
                    <div class="{{$modals_btns}}s">
                        <table width="100%" id="table_VerRecibos{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
                        </table>
                    </div>
                </div>
            </div>
            <!-- LOADING -->
            <div id="loading_modal_ver_recibo"></div>
            <div class="modal-footer" >
                <button type="button" class="btn btn-link waves-effect pull-left" data-dismiss="modal">CERRAR</button>      
            </div>
            <!-- MENSAJE -->
            <div id="modal_mensaje_ver_recibo"></div>
        </div>
    </div>
</div>
{{-- FIN MODAL VER RECIBOS FACTURA --}}
