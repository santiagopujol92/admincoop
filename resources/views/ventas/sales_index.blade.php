@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection   
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>Ventas</h3>
            </div>
			<!-- LISTADO -->
	        <div class="card">
	        	<!-- MENSAGE --> 
                <div id="mensaje_principal"></div>
	            <!-- FIN MENSAJE -->
	            <div class="row clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <div class="header">

							<!-- FIN LOADING2 -->
							<div class="icon-button-demo">
							   <button class="btn btn-link bg-pink waves-effect pull-right" data-backdrop="static" type="button" onclick="configSaveButtonAddRemitoFactura();restartConfigFormRemitoFactura();" data-backdrop="static" data-toggle="modal" data-target="#modal{{$modals_btns}}">
								   <div class="demo-google-material-icon"> 
									   {{-- <i class="material-icons">add_circle</i>  --}}
									   <span class="icon-name">NUEVA {{$titulo}}</span> 
									  </div>
							   </button>
						   </div>
							<div class="icon-button-demo">
								<button class="btn btn-link bg-green waves-effect pull-right" id="btnModalCargarRecibo{{$modals_btns}}" data-backdrop="static" type="button" onclick="abrirModalCargarRecibo()" 
								data-backdrop="static" data-toggle="modal" data-target="#modalCargarRecibo{{$modals_btns}}">
	                                <div class="demo-google-material-icon"> 
	                                	<span class="icon-name">CARGAR RECIBO</span> 
	                               	</div>
				        		</button>
				        	</div>
			        		<h4>LISTADO DE VENTAS</h4>
	                    </div>
	                    <div class="body table-responsive">
							<label><u>FILTROS:</u></label>
							<div class="row" id="formFiltrosVenta">
								<div class="col-lg-3">
									<label for="filtro_sell_date_desde">Fecha Venta Desde:</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-line">
											{!! Form::text('filtro_sell_date_desde', null, ['class' => 'datetimepickerFilter form-control', 'onchange' => 'listar()', 'placeholder' => "Seleccione Fecha y Hora de Venta Desde"]) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label for="filtro_sell_date_hasta">Fecha Venta Hasta:</label>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-line">
											{!! Form::text('filtro_sell_date_hasta', null, ['class' => 'datetimepickerFilter form-control', 'onchange' => 'listar()', 'placeholder' => "Seleccione Fecha y Hora de Venta Hasta"]) !!}
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label for="filtro_id_type_product">Tipo de Material:</label>
									<div class="input-group input-above2">
										<div class="form-line">
											<select name="filtro_id_type_product" onchange="changeDataSelectTarget('productos', 'findByTypeProductId', 'filtro_id_product', 'Material', 'formFiltrosVenta', this.value);showDataProduct('formFiltrosVenta', 'filtro_id_product', 0);listar()" class="form-control show-tick" data-live-search="true" >
												<option value="">Seleccione Tipo de Material</option>
												@foreach ($data_type_products as $reg)
													<option value="{{$reg->id}}" >{{$reg->description}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<label for="filtro_id_product">Material:</label>
									<div class="input-group input-above">
										<span class="input-group-addon">
										</span>
										<div class="form-line">
											<select name="filtro_id_product" class="form-control show-tick" onchange="listar()" data-live-search="true" >
												<option value="">Seleccione Material</option>
											</select>
										</div>
									</div>
								</div>
							</div>
	                        <div class="{{$modals_btns}}s">
		                        <table id="table_{{$modals_btns}}" class="table table-bordered table-striped table-hover dataTable">
		                        </table>
							</div>
							<label align="center">Total Facturado: <b class="col-green" id="totalFacturado{{$modals_btns}}"></b></label>
							<br>
							<label align="center">Total Cantidad Producto: <b class="col-blue" id="totalCantidadProducto{{$modals_btns}}"></b></label>
                        </div>
    		            <!-- LOADING2 -->
		                <div id="loading_list"></div>
                    </div>
                </div>
            </div>
            <!--# LISTADO -->
        </div>
    </div>
    </section>

	@include('ventas.modals.modal_ver_recibos_sale')
	@include('ventas.modals.modal_cargar_recibos_sale')
	@include('ventas.modals.modal_ver_recibo_imprimir_sale')
	@include('global_modals.modal_opciones_abm')
	@include('global_modals.modal_delete_global')
	@include('global_modals.modal_message_global')
	@include('ventas.modals.modal_abm_venta')
	@include('global_modals.modal_confirm_add_recibo_orden_de_pago')

@endsection

@section('scripts')
    <!-- Custom Js -->
	<script src="../scripts/{{$module}}.js"></script>
	<script src="../scripts/abm_basic.js"></script>
	<script src="../assets_md/js/jspdf/jspdf.min.js"></script>
	<script src="../assets_md/js/jspdf/jspdf-autotable.js"></script>
	<script src="../scripts/{{$module}}_print.js"></script>
	<script src="../scripts/numeros_a_letras.js"></script>

@endsection
