<?php

/*
/* RUTEO FRENTE */
Route::get('home', 'HomeController@index');

/* RUTEO LOG */
Route::resource('log', 'LogController');
Route::resource('/', 'LogController');
Route::get('logout', 'LogController@logout');

/* RUTEOS MODULOS ABM */
Route::resource('usuarios', 'UserController');
Route::resource('tipo_usuarios', 'TypeUserController');
Route::resource('tipo_productos', 'TypeProductController');
Route::resource('productos', 'ProductoController');
Route::resource('condiciones_ivas', 'CondicionIvaController');
Route::resource('tipo_personas', 'TypePersonController');
Route::resource('formas_de_pago', 'PaymentMethodController');
Route::resource('estados_remitos', 'StatusShipmentController');
Route::resource('estados_facturas', 'StatusInvoiceController');
Route::resource('personas', 'PersonController');
Route::resource('registros_logs', 'AuditController');
Route::resource('paises', 'CountryController');
Route::resource('provincias', 'ProvinceController');
Route::resource('ciudades', 'CityController');
Route::resource('ventas', 'SaleController');
Route::resource('tipo_movimientos_cuentas', 'TypeMovementsAccountController');
Route::resource('estados_cuentas_corrientes', 'StatusCurrentAccountController');
Route::resource('cuentas_corrientes', 'CurrentAccountController');
Route::resource('movimientos_cuentas', 'MovementsAccountController');
Route::resource('recibos', 'ReceiptController');
Route::resource('compras', 'PurchaseController');
Route::resource('recibos_compras', 'ReceiptPurchaseController');
Route::resource('maquinas', 'MachineController');
Route::resource('produccion', 'ProductionController');

/* RUTEOS DE PETICIONES DE DATOS PARA LLAMADAS AJAX, EN EL CONTROLADOR TRAER DATOS Y LLAMAR DESDE JS RUTA */
Route::get('usuarios_listar', 'UserController@listing');
Route::get('tipo_usuarios_listar', 'TypeUserController@listing');
Route::get('tipo_productos_listar', 'TypeProductController@listing');
Route::get('productos_listar', 'ProductoController@listing');
Route::get('condiciones_ivas_listar', 'CondicionIvaController@listing');
Route::get('tipo_personas_listar', 'TypePersonController@listing');
Route::get('formas_de_pago_listar', 'PaymentMethodController@listing');
Route::get('estados_facturas_listar', 'StatusInvoiceController@listing');
Route::get('estados_remitos_listar', 'StatusShipmentController@listing');
Route::get('personas_listar', 'PersonController@listing');
Route::get('paises_listar', 'CountryController@listing');
Route::get('provincias_listar', 'ProvinceController@listing');
Route::get('ciudades_listar', 'CityController@listing');
Route::get('ventas_listar', 'SaleController@listing');
Route::get('tipo_movimientos_cuentas_listar', 'TypeMovementsAccountController@listing');
Route::get('estados_cuentas_corrientes_listar', 'StatusCurrentAccountController@listing');
Route::get('cuentas_corrientes_listar', 'CurrentAccountController@listing');
Route::get('compras_listar', 'PurchaseController@listing');
Route::get('maquinas_listar', 'MachineController@listing');
Route::get('produccion_listar', 'ProductionController@listing');

/* RUTEOS PARA DEVOLVER DATA DE SELECTS A PARTIR DE UN VALOR DE OTRO SELECT. Funciones findBy..*/
Route::get('provincias/findByCountryId/{id}', 'ProvinceController@findByCountryId');
Route::get('ciudades/findByProvinceId/{id}', 'CityController@findByProvinceId');
Route::get('personas/findByTypePersonId/{id}', 'PersonController@findByTypePersonId');
Route::get('productos/findByTypeProductId/{id}', 'ProductoController@findByTypeProductId');

/* RUTEOS PARA OTRA DATA DE FUNCIONES ESPECIALES*/
Route::get('registros_logs/getDataAuditToNotificaction/{limit}', 'AuditController@getDataAuditToNotificaction');
Route::get('cuentas_corrientes/GetMovimientosByCuentaCorriente/{id_cuenta_corriente}', 'CurrentAccountController@GetMovimientosByCuentaCorriente');
Route::get('ventas_getNewSaleId', 'SaleController@getNewSaleId');
Route::get('ventas_getNewInvoiceId', 'SaleController@getNewInvoiceId');
Route::post('ventas/InsertInvoiceSale/{data_factura}', 'SaleController@InsertInvoiceSale');
Route::get('recibos/GetRecibosByInvoice/{id_invoice}', 'ReceiptController@GetRecibosByInvoice');
Route::get('recibos/GetReciboByIdRecibo/{id_receipt}', 'ReceiptController@GetReciboByIdRecibo');
Route::get('ventas_getInvoicesData', 'SaleController@getInvoicesData');
Route::get('compras_getNewPurchaseId', 'PurchaseController@getNewPurchaseId');
Route::get('compras_getPurchasesData', 'PurchaseController@getPurchasesData');
Route::get('recibos_compras/GetRecibosByPurchase/{id_purchase}', 'ReceiptPurchaseController@GetRecibosByPurchase');
Route::get('recibos_compras/GetReciboByIdRecibo/{id_receipt}', 'ReceiptPurchaseController@GetReciboByIdRecibo');

Auth::routes();
